<!DOCTYPE html>
<html lang= "{{ app()->getLocale() }}">
	<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
	@php
	$navegation = 1;
	@endphp
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
		<meta name="description" content="">
		<meta name="author" content="">

		<title>.:Licensing Assurance:.</title>
		<link rel="shortcut icon" href="{{asset('img/Logo.ico')}}">
		<!-- Bootstrap Core CSS -->
		<link href="{{asset('css/bootstrap.min.css')}}"  rel="stylesheet">

		<!-- include font awesome -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
		
		<!--inicio fuentes para eventos-->
		<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Signika" rel="stylesheet">
		<!--fin fuentes para eventos-->
		<link href="{{asset('css/alert.min.css')}}"  rel="stylesheet" />
		<link href="{{asset('css/theme.min.css')}}"  rel="stylesheet" />
		 <link href="{{asset('css/mynew.css')}}"  rel="stylesheet" />
		 <link href="{{asset('css/mynew2.css')}}"  rel="stylesheet" />

		 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css"/>

		<style>   
			.fuenteEventos{
				font-family: 'Anton', sans-serif;
			}
			
			.fuenteEventos1{
				font-family: 'Signika', sans-serif;
			}
			
			body{
				font-family: 'Raleway', sans-serif;
				background-color: #FAFAFA;
			}
			
			input[type="text"]{
				-webkit-border-radius: 0px;
				-moz-border-radius: 0px;
				border-radius: 0px;
			}
			
			.fuenteOpenSan{
				font-family: 'Open Sans', sans-serif;
			}
			
			.textarea{
				-webkit-border-radius: 0px;
				-moz-border-radius: 0px;
				border-radius: 0px;
			}
			
			.tituloHeader{
				font-weight:bold; 
				font-style: italic; 
				font-size:2.5vw; 
				text-align: center; 
				padding-top:530px; 
				color:#ffffff;
			}
			
			.form-control::-webkit-input-placeholder { color: #ffffff; }
			.form-control:-moz-placeholder { /* Firefox 18- */ color: #ffffff; } 
			.form-control::-moz-placeholder { /* Firefox 19+ */ color: #ffffff; } 
			.form-control:-ms-input-placeholder { color: #ffffff; }
			
			input.form-control, textarea.form-control{
				color: #ffffff;
			}
			
			.btn{
				/*-webkit-border-radius: 0px;
				-moz-border-radius: 0px;
				border-radius: 0px;*/
				-webkit-box-shadow: -4px 4px 20px 1px rgba(0,0,0,0.75);
				-moz-box-shadow: -4px 4px 20px 1px rgba(0,0,0,0.75);
				box-shadow: -4px 4px 20px 1px rgba(0,0,0,0.75);
			}
			
			.btn:hover{
				opacity: 0.75;
				color: #ffffff;
			}
			
			.backcolorAzul{
				background-color: #075ba6; /*#0C3C60;*/
			}
			
			.backcolorGrisClaro{
				background-color: #FAFAFA;
			}
			
			.backcolorAzulClaro{
				background-color: #79AACD;
			}
			
			.backcolorAzulGrisaseo{
				background-color: #D2E0EB;
				padding-top: 40px;
				padding-bottom: 40px;
			}
			
			.backcolorWhite{
				background-color: #ffffff;
			}
			
			.azulOscuro{
				color: #0C3C60;
			}
					   
			/*inicio menu*/
			.navbar-white {
				background-color: #ffffff;
				background-repeat: repeat-x;
				box-shadow: 0px 1.5px 0px #999999;
				height:80px;
				font-family: 'Open Sans', sans-serif;
			}
			
			.navbar .nav > li.active > a{
				color: #1FABE2;
				background-color: transparent;
			}

			.navbar-white .brand,
			.navbar-white .nav > li > a {
				color: #075ba6; /*#0C3C60;*/
				/*line-height: 50px;
				height: 50px;*/
				font-weight: bold;
			}

			.navbar-white .brand:hover,
			.navbar-white .nav > li > a:hover,
			.navbar-white .nav > li.active > a:hover,
			.navbar-white .nav > li > a:focus,
			.navbar-white .nav > li.active > a:focus{
				color: 	#04a1de;
				background-color: transparent;
			}
			
			.navbar-white .nav > li > ul.dropdown-menu{
				/*background-color: #0C3C60;*/
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#96dffb+0,8fd2e3+100 */
				/*background: #96dffb; /* Old browsers */
				/*background: -moz-linear-gradient(-45deg, #96dffb 0%, #8fd2e3 100%); /* FF3.6-15 */
				/*background: -webkit-linear-gradient(-45deg, #96dffb 0%,#8fd2e3 100%); /* Chrome10-25,Safari5.1-6 */
				/*background: linear-gradient(135deg, #96dffb 0%,#8fd2e3 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				/*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#96dffb', endColorstr='#8fd2e3',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
			   background-color: #fff;

border: solid 1px #88d2f2;
margin-right: -29px;
			}
			
			.navbar-white .nav > li > ul.dropdown-menu > li > a{
				color: #075ba6; /*#0C3C60;*/
				font-weight: bold;
				padding: 6px 20px;
				border-bottom: solid 1px #5cc4f3;
			}
			
			.navbar-white .nav > li > ul.dropdown-menu > li > a:hover{
				background-color: transparent;
				color: #31B5E7;
			}
			/*fin menu*/
			
			.crop{
				margin:.5em 10px .5em 0;
				overflow:hidden; /* IMPORTANTE */
				width: 100%;
				height:430px;
				position: relative;
			}
			
			.crop1{
				margin:.5em 10px .5em 0;
				overflow:hidden; /* IMPORTANTE */
				width: 100%;
				height:180px;
				position: relative;
			}

			 /* Indicamos los márgenes que dejamos para simular el crop. */
			.crop img,
			.crop1 img{
				width:100%;
			}
			
			.tituloLicensing{
				/*font-size:50px;*/
				font-size: 3vw;
				font-weight: bold;
				letter-spacing: 10px;
				z-index:1024;
				position: absolute;
				color: #ffffff;
				text-align:center;
				margin-top: -30px;
			}
			
			.tituloLicensing1{
				/*font-size:20px;*/
				font-size: 3vw;
				font-weight: bold;
				letter-spacing: 10px;
				z-index:1024;
				color: #ffffff;
				text-align:center;
				margin-top: -15px;
			}
			
			.subtituloLicensing{
				font-family: Verdana, Geneva, sans-serif;
				/*font-size:20px;*/
				font-size: 1.5vw;
				z-index:1024;
				position: absolute;
				color: #ffffff;
				text-align:center;
				margin-top: 50px;
			}
			
			.subtituloLicensing1{
				font-family: Verdana, Geneva, sans-serif;
				/*font-size:12px;*/
				font-size: 1vw;
				z-index:1024;
				position: absolute;
				color: #ffffff;
				text-align:center;
				margin-top: 15px;
			}
			
			.centrado{
				position: absolute; 
				left: 50%; 
				top: 50%; 
				transform: translate(-50%, -50%); 
				-webkit-transform: translate(-50%, -50%);
			}
			
			.tituloLicensing2{
				font-size:55px;
			 /*   font-size: 4vw;*/
				color: #0C3C60;
				text-align: center;
			}
			
			.subtituloLicensing2{
				font-size:22px;
				/*font-size: 1.6vw;*/
				color: #0C3C60;
			}
			
			.subtituloLicensing3{
				font-size:22px;
			  /*  font-size: 2.5vw;*/
				color: #0C3C60;
			}
			
			.tituloLicensing4{
				font-size: 38px;
				color: #0C3C60;
			}
			
			.subtituloLicensing4{
				font-size: 16px;
				color: #0C3C60;
			}
			
			.subtituloLicensing5{
				font-size: 1.35vw;
				color: #0C3C60;
			}
			
			.subtituloLicensing6{
				/*font-family: Verdana, Geneva, sans-serif;*/
				/*font-size:12px;*/
				font-size: 1.1vw;
			}
			
			.subtituloLicensing7{
				/*font-size:22px;*/
				font-size: 1.5vw;
				color: #0C3C60;
			}
			
			.subtituloLicensing8{
				/*font-size:22px;*/
				font-size: 0.8vw;
				color: #0C3C60;
			}
			
			.subtituloLicensing9{
				/*font-family: Verdana, Geneva, sans-serif;*/
				/*font-size:12px;*/
				font-size: 0.97vw;
			}
						
			.center-block{
				float: none;
			}
			
			.btn-licensing{
				cursor: pointer;
				background-color: #269CF3;
				color: #ffffff;
				font-size: 20px;
				box-shadow: 0px 0px 0px 5px #269cf3;
				border: solid 2px #fff;
				text-transform: uppercase;
				font-weight: 600;
				transition: 0.5s;
			}
			
			.btn-licensing:hover{
				opacity: 1;
				border-radius: 25px;  
			}
			
			.contactStyle{
				background-color: #075ba6; /*#0C3C60;*/
				color: #ffffff;
				padding-top: 40px;
				padding-bottom: 40px;
			}
			
			.colorWhite{
				color: #ffffff;
			}
			
			.fondo{
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#5ebaed+0,5acef9+100 */
				background: #5ebaed; /* Old browsers */
				background: -moz-linear-gradient(-45deg, #5ebaed 0%, #5acef9 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(-45deg, #5ebaed 0%,#5acef9 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(135deg, #5ebaed 0%,#5acef9 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5ebaed', endColorstr='#5acef9',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
			}
			
			.fondo1{
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#96dffb+0,8fd2e3+100 */
				/*background: #96dffb; /* Old browsers */
				/*background: -moz-linear-gradient(-45deg, #96dffb 0%, #8fd2e3 100%); /* FF3.6-15 */
				/*background: -webkit-linear-gradient(-45deg, #96dffb 0%,#8fd2e3 100%); /* Chrome10-25,Safari5.1-6 */
				/*background: linear-gradient(135deg, #96dffb 0%,#8fd2e3 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				/*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#96dffb', endColorstr='#8fd2e3',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
				background-color: #88D2F2;
			}
			
			.fondo2{
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#0b5da6+0,0758ad+100 */
				/*background: #0b5da6; /* Old browsers */
				/*background: -moz-linear-gradient(-45deg, #0b5da6 0%, #0758ad 100%); /* FF3.6-15 */
				/*background: -webkit-linear-gradient(-45deg, #0b5da6 0%,#0758ad 100%); /* Chrome10-25,Safari5.1-6 */
				/*background: linear-gradient(135deg, #0b5da6 0%,#0758ad 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				/*filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0b5da6', endColorstr='#0758ad',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
				background-color: #075ba6;
			}
			
			.banderaInglesa{
				width:45px;
			}
			
			.banderaSpain{
				width:45px;
				margin-left: -30px;
			}
			
			
			.divTooltip{
				display: none;
				position:absolute;
				border: none;
				width: 210px;
				padding:20px;
				height: 135px;
				opacity: 0.8;
			}
			
			.color-chart {
				width:500px;
				height:500px;
				border: 0px solid #89D8FD; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				top:50%;
				left:50%;
				margin-top:-275px;
				margin-left:-255px;
				position:absolute;
			}   
					  
			.color-chart1 {
				width:400px;
				height:400px;
				border: 0px solid #89D8FD; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				top:50%;
				left:50%;
				margin-top:-200px;
				margin-left:-200px;
				position:absolute;
			}   
			
			.color-chart2 {
				width:300px;
				height:300px;
				border: 0px solid #89D8FD; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				background: #ffffff;
				top:50%;
				left:50%;
				margin-top:-150px;
				margin-left:-150px;
				position:absolute;
				padding: 30px;
			}  
			
			.color-chart3 {
				width:200px;
				height:200px; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				background: #ffffff;
			}   
			
			.img-licensing{
				width:240px; 
				height:auto;
				top:50%;
				left:50%;
				margin-top:-100px;
				margin-left:-120px;
				position:absolute;
			}
			
			.img-licensing-xs{
				width:100px; 
				height:auto;
				top:50%;
				left:50%;
				margin-top:-40px;
				margin-left:-50px;
				position:absolute;
			}
			
			.color-chart-xs {
				width:200px;
				height:200px;
				border: 6px solid #555; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				top:50%;
				left:50%;
				margin-top:-100px;
				margin-left:-100px;
				position:absolute;
			}   
			
			.color-chart1-xs {
				width:250px;
				height:250px;
				border: 6px solid #555; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				top:50%;
				left:50%;
				margin-top:-125px;
				margin-left:-125px;
				position:absolute;
			}   
			
			.color-chart2-xs {
				width:150px;
				height:150px;
				border: 6px solid #555; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				background: #ffffff;
				top:50%;
				left:50%;
				margin-top:-75px;
				margin-left:-75px;
				position:absolute;
				padding: 30px;
			}   
			
			.color-chart3-xs {
				width:70px;
				height:70px; 
				-moz-border-radius: 100%;
				-webkit-border-radius: 100%;
				border-radius: 100%;
				background: #ffffff;
			}   
			
			.color-carousel{
				color: #99C0D0;
			}
			
			.img-foot-licensing{
				width:150px; 
				height:auto;
				top:50%;
				left:50%;
				margin-top:-60px;
				margin-left:-75px;
				position:absolute;
			}
			
			.img-foot-licensing-xs{
				width:55px; 
				height:auto;
				top:50%;
				left:50%;
				margin-top:-25px;
				margin-left:-27.5px;
				position:absolute;
			}
			
			.iframeVideo{
				width:100%;
				height:450px;
			}
			
			.iframeVideo-xs{
				width:100%;
				height:150px;
			}
			
			.iframeVideoDiagnostic{
				width:100%;
				height:200px;
			}
			
			.iframeVideoDiagnostic-xs{
				width:100%;
				height:75px;
			}
			
			.carousel-indicators{
				bottom:-20px;
			}

			.carousel-indicators li {
				display: inline-block;
				width: 10px;
				height: 10px;
				margin: 5px;
				text-indent: 0;
				cursor: pointer;
				border: none;
				/*border-radius: 50%;*/
				-moz-border-radius: 50%;
				-webkit-border-radius: 50%;
				border-radius: 50%;
				background-color: #075ba6; /*#0C3C60;*/
				box-shadow: inset 1px 1px 1px 1px rgba(0,0,0,0.5);    
			}
			
			.carousel-indicators .active {
				width: 10px;
				height: 10px;
				margin: 5px;
				background-color: #075ba6; /*#0C3C60;*/
				opacity: .75;  
			}
			
			footer{
				min-height:400px;
				color: #ffffff;
			}
			
			.letraFoot, .letraFoot1, .letraFoot-xs, .letraFoot1-xs{
				font-size: 20px;
			}
		   
			a.letraFoot:link,
			a.letraFoot:visited,
			a.letraFoot:active,
			a.letraFoot:hover{
				color: #ffffff;
				text-decoration: none;
			}
			
			a.article:link,
			a.article:visited,
			a.article:active,
			a.article:hover{
				color: #0C3C60;
				text-decoration: none;
			}
			
			a.article1:link,
			a.article1:visited,
			a.article1:active,
			a.article1:hover{
				color: #ffffff;
				text-decoration: none;
			}
			
			p.letraFoot1{
				line-height: 20px;
			}
			
			p.letraFoot1-xs{
				line-height: 10px;
			}
			
			/*inicio desvanecer*/
			.div-imagen {
				display:inline-block;
				position:relative;
				z-index: 10;
				overflow: hidden;
			}

			.div-imagen > div {
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				-webkit-transform: translate(-50%, -50%);
				opacity: 1;
				z-index: -1;
			}
			
			.desvanecer:hover {
				opacity: 0;
				-webkit-transition: opacity 500ms;
				-moz-transition: opacity 500ms;
				-o-transition: opacity 500ms;
				-ms-transition: opacity 500ms;
				transition: opacity 500ms;
			}
			/*fin desvanecer*/
			
			/*inicio cuadrados*/
			.divCuadrado{
				height:240px;
				-moz-border-radius: 10%;
				-webkit-border-radius: 10%;
				border-radius: 10%;
			}
			/*fin cuadrados*/
			
			.centradoPorcentual{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				-webkit-transform: translate(-50%, -50%);
			}
			
			.pointer{
				cursor: pointer;
			}
			
			#tooltip {
				position: absolute;
				z-index: 3000;
				border: 1px solid #111;
				background-color: #88d2f2;
				padding: 5px;
				opacity: 0.85;
			}
		   
			#tooltip h3, #tooltip div { 
				font-family: 'Open Sans', sans-serif;
				font-size:12px;
				color: #075ba6;
				font-weight: bold;
				margin: 0; 
			}
			
			#tooltip.pretty {
				border: none;
				width: 270px;
				/*padding:20px;*/
				height: auto;
				/*opacity: 0.8;*/
				/*background: url('shadow.png');*/
			}
			
			input.formPersonal::-webkit-input-placeholder {
				color:    #00008B;
				font-weight: bold;
			}

			input.form-control{
				border-radius: 0;
				color: #000000;
			}
			
			.fondoOscuro{
				position:fixed;
				top:0;
				right:0;
				bottom:0;
				left:0;
				z-index:1000;
				background-color:#000;
				filter:alpha(opacity=0.70);
				opacity:0.70;
			}
			
			.form-bottom form .input-error {
				border-color: #d05a4e;
			}
			
			.modal_video{
				width:100%;
				height: 15vw;
			}
			
			@media screen and (min-width: 414px){
				.modal_video{
					height: 200px;
				}
			}
			
			@media screen and (min-width: 768px){
				.modal_video{
					height: 200px;
				}
			}
			
			
		</style>
		
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<!--<link rel="stylesheet" href="/resources/demos/style.css">-->
		
		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="{{asset('js/bootstrap.js')}}"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
		<script src="{{asset('rwdImageMaps/jquery.rwdImageMaps.min.js')}}"></script>
		<script src="{{asset('js/jquery.smoove.js')}}"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="{{asset('js/jquery.tooltip.js')}}"></script>
		<script src="{{asset('js/alert.js')}}"></script>




		<script src="{{asset('js/util.js')}}"></script>
		<script src="{{asset('js/pipeline.js')}}"></script>


	</head>
	
	<body>
		<!--<div style="position:fixed; margin-top: 14px; margin-left:30px; z-index: 600">
			<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
			<script type="IN/FollowCompany" data-id="9211105" datak-counter="top"></script>
		</div>-->
		
		<div class="fondoOscuro" id="fondo" style="display:none;">
			<img src="{{asset('img/loading.gif')}}" style="width:50px; height:50px; top:50%; margin-top:-25px; left:50%; margin-left:-25px; position:absolute;">
		</div>
		
		<div class="container-fluid" style='margin-top:80px;'>
			<div class="row">

				<nav class="navbar navbar-white navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header" style='margin-left:5%;'>
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar backcolorAzul"></span>
			<span class="icon-bar backcolorAzul"></span>
			<span class="icon-bar backcolorAzul"></span>
		</button>
		<a class="navbar-brand hidden-sm" href="#" style='margin-left:-20px;'><img src="{{asset('img/logo LA.png')}}" class="img img-responsive" style="height: 80px; width:auto; margin-top:-15px;"></a>
		<a class="navbar-brand hidden-sm hidden-md" href="#" style='margin-left:-20px;'><img src="{{asset('img/IAITAM.png')}}" class="img img-responsive" style="height: 20px; width:auto; margin-top:40px;"></a>
		<a class="navbar-brand hidden-sm hidden-md" href="#" style='margin-left:-20px;'><img src="{{asset('img/normas-iso.png')}}" class="img img-responsive" style="height: 20px; width:auto; margin-top:40px;"></a>
		<a class="navbar-brand hidden-sm hidden-md" href="#" style='margin-left:-20px;'><img src="{{asset('img/itil-logo.png')}}" class="img img-responsive" style="height: 20px; width:auto; margin-top:40px;"></a>
	</div>
<?php //echo "<pre>";print_r(trans('index/language.home'));//die;?>
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-right resmenusty9563"  id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav mar-topmy">
			<li><a class="menu_home" href="<?= $lib->nv0001 ?>">{{ trans('index/language.home') }}</a></li>

			<li><a class="menu_about_us" href="<?= $lib->nv0002 ?>">{{ trans('index/language.about_us') }}</a></li>
			<li class="dropdown <?php if($navegation == 3) echo 'active'; ?>">
				<a href="#" class="dropdown-toggle menu_services" data-toggle="dropdown" onclick="location.href='<?= $lib->nv0003 ?>'">{{ trans('index/language.services') }}<b></b></a>
				<ul class="dropdown-menu">
					<li><a href="<?= $lib->nv0004 ?>" class="menu_sub_sam_as_services">{{ trans('index/language.sam_as_a_service') }}</a></li>
					<li><a href="<?= $lib->nv0005 ?>" class="menu_sub_audit_defense">{{ trans('index/language.audit_defense') }}</a></li>
					<!--<li><a href="optimization.php">Optimization</a></li>-->
					<!-- <li><a href="<?= $lib->nv0006 ?>" class="menu_sub_spla">{{ trans('index/language.sam_as_a_spla') }}</a></li> -->
					<li><a href="<?= $lib->nvs0015 ?>" class="menu_sub_spla datacenter-active62">{{ trans('index/language.datacenter_solutions') }}</a></li>
					<li><a href="<?= $lib->nv0007 ?>" class="menu_sub_deployment">{{ trans('index/language.deployment_diagnostic') }}</a></li>
				</ul>
			</li>
			<!--<li class="<?php //if($navegation == 3) echo 'active'; ?>"><a href="solutions.php">Solutions</a></li>-->
			<li><a href="<?= $lib->nv0008 ?>" class="menu_overview">{{ trans('index/language.overview') }}</a></li>
		   <!--  <li class="<?php if($navegation == 5) echo 'active'; ?>"><a href="/articles">{{ trans('index/language.articles') }}</a></li>
 -->           
 <!--  <li class="dropdown <?php if($navegation == 7) echo 'active'; ?>">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ trans('index/language.events') }}<b></b></a>
				<ul class="dropdown-menu">
				  <li><a href="<?= $lib->nv0010 ?>"><?= $lib->lg0012 ?></a></li>
				  <li><a href="<?= $lib->nv0011 ?>"><?= $lib->lg0013 ?></a></li>
					<li><a href="<?= $lib->nv0013 ?>">{{ trans('index/language.webinar1') }}</a></li>
					<li><a href="<?= $lib->nv0014 ?>">{{ trans('index/language.webinar2') }}</a></li>
				</ul>
			</li> -->

			 <li class="dropdown">
				<a href="#" class="dropdown-toggle menu_resourses" data-toggle="dropdown">{{ trans('index/language.resources') }}<b></b></a>
				<ul class="dropdown-menu">
				
					<li><a href="<?= $lib->nvs0016 ?>" class="menu_sub_blog">{{ trans('index/language.blog') }}</a></li>
					<li><a href="<?= $lib->nvs0017 ?>" class="menu_sub_webinar">{{ trans('index/language.webinar') }}</a></li>
					<li><a href="<?= $lib->nvs0019 ?>" class="menu_sub_tools">{{ trans('index/language.tools') }}</a></li>
					<li><a href="<?= $lib->nvs0020 ?>" class="menu_sub_news">{{ trans('index/language.news') }}</a></li>
				</ul>
			</li>


			<li><a href="<?= $lib->nv0012 ?>" class="menu_contact">{{ trans('index/language.contact_us') }}</a></li>
		</ul>
	   <!--  <ul class="nav navbar-nav navbar-right">
			<li><a href="https://www.licensingassurance.com/webtool" target="_blank">Login</a></li>
			<li class="bandera"><a class="navbar-brand" href="{{ url('language/en') }}"><img src="{{asset('img/usaroundflag.png')}}" class="banderaInglesa" title="English"></a></li>
			<li class="bandera"><a class="navbar-brand" href="{{ url('language/es') }}"><img src="{{asset('img/spainroundflag.png')}}" class="banderaSpain" title="Español"></a></li>
		</ul> -->
		<!-- ======================================= -->
		 <div class="dropdown navbar-right main-buttonlanchange">
	<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-globe" aria-hidden="true"></i>

	<span class="caret arrow-spansty"></span></button>
	<ul class="dropdown-menu lang-changesty851">
	   <li class="bandera"><a class="navbar-brand" href="{{ url('language/en') }}"><img src="{{asset('img/usaroundflag.png')}}" class="banderaInglesa" title="English"> English</a></li>
	<li class="bandera"><a class="navbar-brand" href="{{ url('language/es') }}"><img src="{{asset('img/spainroundflag.png')}}" class="banderaSpain" title="Spanish"> Spanish</a></li>
	</ul>
  </div>
		<!-- ======================================== -->
	</div><!-- /.navbar-collapse -->
</nav>


<!--href="/"-->
<!--href="/es"-->
<div class="fondo hidden-xs content--canvas" style="width:100%; min-height:600px; position:relative;background: #213567">
	<div class="color-chart fadeInSmoove">
		<div class="color-chart1 fadeInSmoove">
			<div class="color-chart2 fadeInSmoove">
				<img src="{{asset('img/nuevaWeb/LOGO LA-02.png')}}" class="img img-licensing">
			</div>
		</div>
	</div>

	<p class="tituloHeader fadeInSmoove">{{ trans('index/language.software_made_easy') }}</p>   
</div>



			<!-- <div class="content--canvas"></div> -->


















































@yield('content')

<div class="col-xs-12" style="width:100%; height:2px; background-color: #ffffff;"></div>
				<footer class="col-xs-12 footer fondo2">  
<!-- ================================================================================ -->
<div class="container fadeInSmoove">
	<div class="row">
		<div class="col-md-3 col-sm-12 col-xs-12">
			<span class="footer-logomainbox">
				<img src="{{asset('img/nuevaWeb/LOGO LA-02.png')}}">
			</span>
			<ul class="menu-footersty856">
				<li>
					<a href="<?= $lib->nv0002 ?>">{{ trans('index/language.about_us') }}</a>
				</li>

				<li>
					<a href="<?= $lib->nv0008 ?>">{{ trans('index/language.services_overview') }}</a>
				</li>

				 <li>
					<a href="<?= $lib->nv0012 ?>">{{ trans('index/language.contact_us') }}</a>
				</li>

				 <li>
					<a href="<?= $lib->nvs0021 ?>">{{ trans('index/language.site_map') }}</a>
				</li>

			</ul>
		</div>
		<div class="col-md-2 col-sm-6 col-xs-12">
			<p class="footerhad-sty9631">{{ trans('index/language.our_solutions') }}</p>
			<ul class="menu-footersty856">
				<li>
					 <a href="<?= $lib->nv0004 ?>">{{ trans('index/language.sam_as_a_service') }}</a>
				</li>
				<li>
					<a href="<?= $lib->nv0005 ?>">{{ trans('index/language.audit_defense') }}</a>
				</li>

			   <!--  <li>    
					<a href="<?= $lib->nv0006 ?>">{{ trans('index/language.sam_as_a_spla') }}</a>
				 </li> -->

				 <li>   
					<a href="<?= $lib->nv0007 ?>">{{ trans('index/language.deployment_diagnostic') }}</a>
				 </li>  
<!-- 
				 <li> 
					<a href="<?= $lib->nv0008 ?>">{{ trans('index/language.overview') }}</a>
				</li> -->
				 <li>    
					<a href="<?= $lib->nvs0015 ?>">{{ trans('index/language.datacenter_solutions') }}</a>
				 </li>
			</ul>

			 <p class="footerhad-sty9631">{{ trans('index/language.resourses') }}</p>
			<ul class="menu-footersty856">
				<li>
					 <a href="<?= $lib->nvs0016 ?>">{{ trans('index/language.blog') }}</a>
				</li>
				<li>
					 <a href="<?= $lib->nvs0017 ?>">{{ trans('index/language.webinars') }}</a>
				</li>
				<li>
					 <a href="<?= $lib->nvs0020 ?>">{{ trans('index/language.news') }}</a>
				</li>
			   </ul> 
		</div>
		<div class="col-md-2 col-sm-6 col-xs-12">
			<p class="footerhad-sty9631">{{ trans('index/language.our_tools') }}</p>
			<ul class="menu-footersty856">
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.la_tool') }}</a>
				 </li>    
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.non_active') }}</a>
				 </li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.smart_sam') }} </a>
				 </li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.cloud_infraestructure') }} </a>
				</li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.la_vtool') }}</a>
				</li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.smart_control') }}</a>
				</li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.hyperv') }}</a>
				</li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.smart_control') }}</a>
				</li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.la_usage') }}</a>
				</li>
					 
				<li>
					 <a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.diagnostic') }}</a>
				</li>
					 
			   </ul> 
		</div>
		<div class="col-md-5 col-sm-12 col-xs-12">
			<p class="footer-weprovidesty">{{ trans('index/language.we_provide') }}  </p>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
						<a style="text-decoration: none !important; color: #fff !important;" href="<?= $lib->nv0012 ?>" class="footerhad-sty9631">{{ trans('index/language.contact_us2') }}</a>
								<ul class="menu-footeraddress8956">
									<li>{{ trans('index/language.phone_footer') }}</li>
									<li>{{ trans('index/language.email_footer') }}</li>
									<!-- <li>{{ trans('index/language.address_footer') }}</li> -->
									<br>
									<li>{{ trans('index/language.address_footer1') }}</li>
									<li>{{ trans('index/language.address_footer2') }}</li>
									<li>{{ trans('index/language.address_footer3') }}</li>
								</ul>
								
							   <!--  ​<p class="letraFoot1">{{ trans('index/language.we_are_available') }}</p>
								<p class="letraFoot1">{{ trans('index/language.monday_friday') }}</p>
								<p class="letraFoot1">{{ trans('index/language.am_pm') }}</p>
								<p class="letraFoot1">{{ trans('index/language.east_time') }}</p>  -->

				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<p class="letraFoot1">{{ trans('index/language.follow_us_and_share') }}</p>
								<div style="display:inline-block;"><img src="{{asset('img/LinkedIN.png')}}" class="img img-responsive footer-socialiconsty" onclick="window.open('https://www.linkedin.com/company/9211105/');"></div>

								<div style="display:inline-block;"><img src="{{asset('img/twitter.png')}}" class="img img-responsive footer-socialiconsty" onclick="window.open('https://twitter.com/LicAssurance');"></div>

								<div style="display:inline-block;"><img src="{{asset('img/facebook.png')}}" class="img img-responsive footer-socialiconsty" onclick="window.open('https://www.facebook.com/licensingassurance/');"></div>

								<div style="display:inline-block;"><img src="{{asset('img/youtube.png')}}" class="img img-responsive footer-socialiconsty" onclick="window.open('https://www.youtube.com/channel/UCZMnfzMegbU4vIrNbLKnzew');"></div>
				</div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12 footercopyright-data" style="text-align: center;">
			<p>{{ trans('index/language.copyright_la') }}</p>
		</div>
	</div>
</div>
<!-- ================================================================================= -->

				</footer>
				<script>
			$(document).ready(function(){
				$('ul.nav li.dropdown').hover(function() {
					$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
				}, function() {
					$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
				});
				
				$(".fadeInSmoove").smoove({offset:'40%'});
			});
		</script>

	 

<div class="modal fade" id="myModal001" role="dialog" style="display: none;">
	<div class="modal-dialog modal-md modalmidsty-862 mdl_0539130519">
	  <div class="modal-content">


		<div class="modal-body">
		  <form>
			 <button type="button" class="close" data-dismiss="modal">&times;</button>
			<h3>{{ trans('index/language.blog0017') }}</h3>
			<input type="email" placeholder="{{ trans('index/language.blog0018') }}">
			<p>{{ trans('index/language.blog0019') }}</p>
			<button>{{ trans('index/language.blog0020') }} <i class="fa fa-caret-down" aria-hidden="true"></i></button>
		  </form> 
		</div>


	  </div>
	</div>
  </div>


