@extends('layouts.header')

@section('content')

<style type="text/css">
    .menu_about_us{
        color: #1FABE2 !important;
      background-color: transparent;
    }
</style>
<p class="subtituloLicensing2 text-center fadeInSmoove about-haddingpdata">{{ trans('index/language.lg0034') }}</p>
   <div class="container">


<p class="tituloLicensing2 fadeInSmoove"> {{ trans('index/language.lg0035') }}</p>
<br>
<div class="col-xs-12 res-padzero">
    <p class="tituloLicensing4 text-center fadeInSmoove" title="ahora sale el titulo">
        {{ trans('index/language.lg0036') }}
    </p>
    <br class="hidden-xs">
    
    <div class="col-xs-12  col-sm-12 fadeInSmoove" style="margin-bottom: 20px;">
        <p class="subtituloLicensing2 text-justify">{{ trans('index/language.lg0037') }}</p>
    </div>
    
 
    <div class="col-sm-12 fadeInSmoove">
    
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <!-- ============================================ -->
                <div class="main-aboutbox856">
                    <div class="main-aboutsecbox">
                         <img src="{{asset('/img/nuevaWeb/target.png')}}"> 
                         <p>{{ trans('index/language.lg0038') }}</p>
                    </div>
                    <div class="main-aboutthirdbox">
                        <p>{{ trans('index/language.lg0039') }}</p>
                    </div>
                </div>

                <div class="main-aboutbox856">
                    <div class="main-aboutsecbox">
                         <img src="{{asset('/img/nuevaWeb/vision.png')}}"> 
                         <p>{{ trans('index/language.lg0040') }}</p>
                    </div>
                    <div class="main-aboutthirdbox">
                        <p>{{ trans('index/language.lg0041') }}</p>
                    </div>
                </div>

                <div class="main-aboutbox856">
                    <div class="main-aboutsecbox">
                         <img src="{{asset('/img/nuevaWeb/value.png')}}"> 
                         <p>{{ trans('index/language.lg0042') }}</p>
                    </div>
                    <div class="main-aboutthirdbox">
                         <ul>
                            <li>{{ trans('index/language.lg0043') }}</li>
                            <li>{{ trans('index/language.lg0044') }}</li>
                            <li>{{ trans('index/language.lg0045') }}</li>
                        </ul>
                    </div>
                </div>


                <div class="main-aboutbox856">
                    <div class="main-aboutsecbox">
                         <img src="{{asset('/img/nuevaWeb/purpose.png')}}"> 
                         <p>{{ trans('index/language.lg0046') }}</p>
                    </div>
                    <div class="main-aboutthirdbox">
                        <p>{{ trans('index/language.lg0047') }}</p>
                    </div>
                </div>
                <!-- ============================================ -->

                
                
               
              
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <img src="{{asset('/img/nuevaWeb/mapa ad2.png')}}" class="img img-responsive" usemap="#map">
                
                <map name="map">
                    <!-- #$-:Image map file created by GIMP Image Map plug-in -->
                    <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
                    <!-- #$-:Please do not edit lines starting with "#$" -->
                    <!-- #$VERSION:2.3 -->
                    <!-- #$AUTHOR:Dx -->
                    <area shape="rect" coords="383,73,412,110" title="1 (786) 246 2472 / (305) 851 3545 aaron@licensingassurance.com"/>
                    <area shape="rect" coords="171,233,199,270" title="52 (155) 543 70148 david.gonzalez@licensingassurance.com"/>
                    <area shape="rect" coords="274,299,302,323" title="502 503 69 119 luisjurado@licensingassurance.com"/>
                    <area shape="rect" coords="279,322,302,360" title="502 503 69 119 luisjurado@licensingassurance.com"/>
                    <!--<area shape="rect" coords="304,326,325,360"  nohref="nohref" />
                    <area shape="rect" coords="327,332,346,368"  nohref="nohref" />-->
                    <area shape="rect" coords="341,369,367,403" title="506 887 01840 giovannam@licensingassurance.com"/>
                    <!--<area shape="rect" coords="428,273,454,310" nohref="nohref" />-->
                    <area shape="rect" coords="375,378,402,415"  title="507 66 75 1020 / 507 6236 4994 ianshaw@licensingassurance.com"/>
                    <area shape="rect" coords="413,419,442,455"  title="57 315 305 2663 jaimevasquez@licensingassurance.com" />
                    <area shape="rect" coords="369,485,398,520"  title="5593 99 5043 7520 guillermoalmeida@licensingassurance.com" />
                    <area shape="rect" coords="560,532,588,568"  title="55 11 968986241 info@licensingassurance.com" />
                    <!--<area shape="rect" coords="399,570,431,606"  nohref="nohref" />-->
                    <area shape="rect" coords="477,623,507,660"  title="(591) 78181355 paola@licensingassurance.com" />
                    <area shape="rect" coords="442,690,473,724"  title="569 764 83992 ernestozapata@licensingassurance.com" />
                    <!--<area shape="rect" coords="482,793,510,830"  nohref="nohref" />
                    <area shape="rect" coords="548,787,577,823"  nohref="nohref" />-->
                </map>

            <div class="hidden-xs col-sm-6 customer-throwout">
                <div class="row">
                    <div class="col-sm-12 fadeInSmoove" data-rotate="360deg">
                        <p class="subtituloLicensing4 throw-outfont851"><strong><i>{{ trans('index/language.lg0048') }}</i></strong></p>
                        <p class="subtituloLicensing4 throw-outfont851"><strong><i>{{ trans('index/language.lg0049') }}</i></strong></p>
                    </div>
                    
                    <div class="col-sm-12">
                        <img src="{{asset('/img/nuevaWeb/sello3.png')}}" class="stanp-rotatesty" data-rotate="360deg">
                    </div>
                </div>
            </div>
            </div>
            
            <div class="visible-xs col-xs-12" style="bottom:100px; margin-bottom:-100px;">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-6 fadeInSmoove" data-rotate="360deg">
                        <p class="subtituloLicensing4 text-center"><strong><i>{{ trans('index/language.lg0048') }}</i></strong></p>
                        <p class="subtituloLicensing4 text-center"><strong><i>{{ trans('index/language.lg0049') }}</i></strong></p>
                    </div>
                    
                    <div class="col-sm-3 col-sm-offset-8">
                        <img src="{{asset('/img/nuevaWeb/sello3.png')}}" class="img img-responsive fadeInSmoove max-wdth965set" data-rotate="360deg">

                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
</div>
<!-- ============================================= -->
<div class="why-usmainpart">
    <div class="container">
        <div class="row">
            <h5 class="main-whyushad fadeInSmoove">{{ trans('index/language.wh0001') }}</h5>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="why-usmaindatacont fadeInSmoove">
                <div class="why-usfirst852">
                    <p>{{ trans('index/language.wh0002') }}</p>
                    <img src="{{asset('/img/nuevaWeb/reliability.png')}}" class="stanp-rotatesty"> 
                </div>
                <div class="why-ussec951">
                    <p>{{ trans('index/language.wh0003') }}</p>
                </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="why-usmaindatacont fadeInSmoove">
                <div class="why-usfirst852">
                    <p>{{ trans('index/language.wh0004') }}</p>
                    <img src="{{asset('/img/nuevaWeb/attention.png')}}" class="stanp-rotatesty"> 
                </div>
                <div class="why-ussec951">
                    <p>{{ trans('index/language.wh0005') }}</p>
                </div>
                </div>

            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 fadeInSmoove">
                <div class="why-usmaindatacont">
                <div class="why-usfirst852">
                    <p>{{ trans('index/language.wh0006') }}</p>
                    <img src="{{asset('/img/nuevaWeb/documents-security.png')}}" class="stanp-rotatesty"> 
                </div>
                <div class="why-ussec951">
                    <p>{{ trans('index/language.wh0007') }}</p>
                </div>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="we-combinesty">{{ trans('index/language.wh0008') }}</p>
            </div>
        </div>
    </div>
</div> 
<!-- ================================================ -->

<!-- ================================================== -->
<div class="what-ourcus">
    <div class="container">
        <div class="row">
              <h5 class="main-whyushad fadeInSmoove">{{ trans('index/language.oc0001') }} </h5>
              <div class="main-cont951datasto fadeInSmoove">
              <div class="customers-satsdata852">
                <p>{{ trans('index/language.oc0002') }}</p>
             </div>
                <span>
                     <img src="{{asset('/img/nuevaWeb/bancoG&T.jpg')}}">
                </span>
            </div>

            <div class="main-cont951datasto1 fadeInSmoove">
                <span>
                <img src="{{asset('/img/nuevaWeb/banistmo.jpg')}}">
                </span>
              <div class="customers-satsdata853">
                
                <p>{{ trans('index/language.oc0003') }} </p>
                 
              </div>
          </div>

            <div class="main-cont951datasto2 fadeInSmoove">
              <div class="customers-satsdata854">
                <p>{{ trans('index/language.oc0004') }}</p>
                
              </div>
              <span>
                 <img src="{{asset('/img/nuevaWeb/correo.jpg')}}">
                </span>
          </div>

        </div>
    </div>
</div>
<!-- =================================================== -->

<!-- ================ Subscribe start ================ -->

<div class="subscribe-maincont newsletter-boxsty951">
    <div class="newsletter-overlayerdata">
    <div class="container">
        <div class="row">
            <div class="subscribedata-newsty951 subs-firststep fadeInSmoove">
                <h6> {{ trans('index/language.subs0001') }} </h6>
                <h6> {{ trans('index/language.subs0002') }}</h6>

                <p> {{ trans('index/language.subs0003') }}</p>
                 <div class="col-md-4 col-sm-4 col-xs-12">
                 <input type="text" placeholder="{{ trans('index/language.abtform001') }}">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                 <input type="email" placeholder="{{ trans('index/language.abtform002') }}">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                 <input type="text" placeholder="{{ trans('index/language.abtform003') }}">
                </div>
                <button class="news-buttonsub613">{{ trans('index/language.subs0004') }} </button>
            </div>
        
        </div>
    </div>
</div>
</div>
<!-- ================ Subscribe Close ================ -->
<!--  <script>

    $('.subs-firstshow').click(function () {
        $('.subs-secstep').css('display','block');
        $('.subs-firststep').css('display','none');
        
    });

</script> -->
@endsection