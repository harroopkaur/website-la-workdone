@extends('layouts.header')

@section('content')
<style type="text/css">
  .menu_services,
  .datacenter-active62{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>
<div style="background-color: #fafafa; padding-bottom: 30px;">
<p class="subtituloLicensing2 text-center fadeInSmoove about-haddingpdata">
    {{ trans('index/language.auditd0001') }}</p>
   <div class="container">
<div class="row">

<p class="tituloLicensing2 fadeInSmoove">{{ trans('index/language.auditd0002') }}</p>

<div class="we-knowdatalist">
   <p> {{ trans('index/language.auditd0003') }}</p>
<ul>

<li>{{ trans('index/language.auditd0004') }}</li>
<li>{{ trans('index/language.auditd0005') }} </li>
<li>{{ trans('index/language.auditd0006') }}</li>
<li>{{ trans('index/language.auditd0007') }}</li>

</ul>
</div>  

<div class="rist-imgdatasty">
    <ul>
        <li>
             <img src="{{asset('/img/nuevaWeb/Loss-income.jpg')}}">
            <p>{{ trans('index/language.auditd0008') }} </p>
        </li>
        <li>
             <img src="{{asset('/img/nuevaWeb/Deployment-without.jpg')}}">
            <p>{{ trans('index/language.auditd0009') }} </p>
        </li>
        <li>
             <img src="{{asset('/img/nuevaWeb/Missing-report.jpg')}}">
            <p>{{ trans('index/language.auditd00010') }} </p>
        </li>
        <li>
             <img src="{{asset('/img/nuevaWeb/limited-access.jpg')}}">
            <p>{{ trans('index/language.auditd00011') }} </p>
        </li>
        <li>
             <img src="{{asset('/img/nuevaWeb/Incorrect-reports.jpg')}}">
            <p> {{ trans('index/language.auditd00012') }}  </p>
        </li>
        <li>
             <img src="{{asset('/img/nuevaWeb/Audit-risk.jpg')}}">
            <p>{{ trans('index/language.auditd00013') }} </p>
        </li>

    </ul>
</div> 


<h6 class="help-orgsty854">{{ trans('index/language.auditd00014') }} </h6>

<div class="row row-cussppl">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="row-cusspplimg">
          <img src="{{asset('/img/nuevaWeb/splaimg.jpg')}}">
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="row-cusspplpdata">
     <p>{{ trans('index/language.auditd00015') }} </p>
 </div>
    </div>

 </div>  

 <div class="col-md-12">
                <h3 class="hd3_0632100519">{{ trans('index/language.auditd00016') }}</h3>
            </div> 

            <div class="col-md-6">
                <ul class="lst2_0632100519">
                    <li>{{ trans('index/language.auditd00017') }}</li>
                    <li>{{ trans('index/language.auditd00018') }} </li>
                    <li>{{ trans('index/language.auditd00019') }} </li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="lst2_0632100519">
                    <li>{{ trans('index/language.auditd00020') }} </li>
                    <li>{{ trans('index/language.auditd00021') }} </li>
                </ul>
            </div>

</div>
</div>
</div>
<!-- ============================================= -->
<div class="why-usmainpart">
    <div class="container">
        <div class="row">
            <h6 class="main-sersolsty851">{{ trans('index/language.auditd00022') }}</h6>
            <div class="datacenter-concenter">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="why-usmaindatacont fadeInSmoove hover-shoalldata851">
                <div class="samas-sermain" style="background-color: #01b0f1;">
                    <img src="{{asset('/img/nuevaWeb/analysissam.png')}}" class="stanp-rotatesty">
                    <div class="data-showhoversam">
                        <p>{{ trans('index/language.auditd00023') }}</p>
                        <a href="#">{{ trans('index/language.sernew00012') }}</a>
                    </div> 
                </div>
                <div class="sams-footertxt">
                    <p>{{ trans('index/language.auditd00024') }}</p>
                </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="why-usmaindatacont fadeInSmoove hover-shoalldata851">
                <div class="samas-sermain" style="background-color: #01b0f1;">
                    <img src="{{asset('/img/nuevaWeb/analyticssam.png')}}" class="stanp-rotatesty"> 
                     <div class="data-showhoversam">
                        <p>{{ trans('index/language.auditd00025') }}</p>
                        <a href="#">{{ trans('index/language.sernew00012') }}</a>
                    </div> 
                </div>
                <div class="sams-footertxt">
                    <p>{{ trans('index/language.auditd00026') }}</p>
                </div>
                </div>

            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 fadeInSmoove">
                <div class="why-usmaindatacont hover-shoalldata851">
                <div class="samas-sermain" style="background-color: #01b0f1;">
                    <img src="{{asset('/img/nuevaWeb/monitorsam.png')}}" class="stanp-rotatesty"> 
                     <div class="data-showhoversam">
                        <p>{{ trans('index/language.auditd00027') }}</p>
                        <a href="#">{{ trans('index/language.sernew00012') }}</a>
                    </div> 
                </div>
                <div class="sams-footertxt">
                    <p>{{ trans('index/language.auditd00028') }}</p>
                </div>
                </div>
            </div>

        </div>

        </div>
    </div>
</div> 
<!-- ================================================ -->

<div class="serv-downloadmain">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                 <img src="{{asset('/img/nuevaWeb/datacenter-download.jpg')}}" class="ser-downloadimgsty"> 
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="download-portfoliosty">
                    <h4><p>{{ trans('index/language.auditd00029') }}</p></h4>
                    <a download href="http://45.248.162.197/website-la/public/img/nuevaWeb/download-serport.jpg"><p>{{ trans('index/language.sernew00021') }}</p></a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection