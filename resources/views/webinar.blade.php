@extends('layouts.header')

@section('content')
<style type="text/css">
  .menu_resourses,
  .menu_sub_webinar{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>
<div class="container" style="padding-bottom:30px;">
  <div class="row">

	<p class="subtituloLicensing2 text-center">{{ trans('index/language.webinar01') }} </p>
	<p class="hd1_0500100519">{{ trans('index/language.webinar02') }}</p>



	<!-- <div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/ZcbGO0gtJhY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<div class="our-software95423">
			<span class="color-darkblue5"></span>
			<div class="data-leftsoftware951">
				<h6>{{ trans('index/language.webinar03') }}</h6>
			</div> 
		</div>
	</div> -->
	<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/ZcbGO0gtJhY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<div class="our-software95423">
			<span class="color-darkblue5"></span>
			<div class="data-leftsoftware951">
				<h6>{{ trans('index/language.webinar04') }}</h6>
			</div> 
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/182ZcYgkcaQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<div class="our-software95423">
			<span class="color-darkblue5"></span>
			<div class="data-leftsoftware951">
				<h6>{{ trans('index/language.webinar05') }}</h6>
			</div> 
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/XKH4WzVPngI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<div class="our-software95423">
			<span class="color-darkblue5"></span>
			<div class="data-leftsoftware951">
				<h6>{{ trans('index/language.webinar06') }}</h6>
			</div> 
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		<div class="our-software95423">
			<span class="color-darkblue5"></span>
			<div class="data-leftsoftware951">
				<h6>{{ trans('index/language.webinar07') }}</h6>
			</div> 
		</div>
	</div>


  </div>
</div>














<div class="fondo1">
	<div class="container dv2_0156110519">
	  <div class="row">


			<p class="hd1_0500100519">{{ trans('index/language.webinar08') }}</p>


			<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="our-software95423">
					<span class="color-green3"></span>
					<div class="data-leftsoftware951">
						<h6>{{ trans('index/language.webinar09') }}</h6>
					</div> 
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="our-software95423">
					<span class="color-green3"></span>
					<div class="data-leftsoftware951">
						<h6>{{ trans('index/language.webinar010') }}</h6>
					</div> 
				</div>
			</div>



	  </div>
	</div>
</div>





	<div class="container dv3_0156110519">
	  <div class="row">

			<div class="col-md-12">
				<img src="{{asset('/img/nuevaWeb/webinar_bannr.jpg')}}" class="">
			</div>

		</div>
	</div>






@endsection