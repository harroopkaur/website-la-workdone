@extends('layouts.header')
@section('content')
<style type="text/css">
  .menu_overview{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>
<!-- <div class="col-xs-12 col-sm-12" style="padding-bottom:30px;">
  <div class="row">        
  <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
  <div class="row">
  <img src="{{asset('/img/nuevaWeb/'.$lib->lg0123)}}" usemap="#map" class="img img-responsive">
  </div>
  </div>
  </div>
  </div>
  -->
<div class="container">
  <div class="row">
    <div class="col-md-12 overview_table_wrrpr" style="overflow:auto; ">
      <table class="overview_table_dv" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="bottom"></td>
          <td valign="bottom">
            <div class="ovtabl_head1">
              <h3 class="color1_bg_thme1"></h3>
              <p>{{ trans('index/language.over001') }} <br> {{ trans('index/language.over002') }}</p>
              <img src="{{asset('/img/nuevaWeb/icon_deployment_diagnostic.png')}}">
              <a href="<?= $lib->nv0012 ?>" class="color1_bg_thme1">{{ trans('index/language.over0010') }}</a>
            </div>
          </td>
          <td valign="bottom">
            <div class="ovtabl_head1">
              <h3 class="color1_bg_thme2"></h3>
              <p>{{ trans('index/language.over003') }} <br> {{ trans('index/language.over004') }}</p>
              <img src="{{asset('/img/nuevaWeb/icon_cloud_assestment.png')}}">
              <a href="<?= $lib->nv0012 ?>" class="color1_bg_thme2">{{ trans('index/language.over0010') }}</a>
            </div>
          </td>
          <td valign="bottom">
            <div class="ovtabl_head1">
              <h3 class="color1_bg_thme3"></h3>
              <p>{{ trans('index/language.over005') }} <br> {{ trans('index/language.over006') }}</p>
              <img src="{{asset('/img/nuevaWeb/icon_sam_diagnosis.png')}}">
              <a href="<?= $lib->nv0012 ?>" class="color1_bg_thme3">{{ trans('index/language.over0010') }}</a>
            </div>
          </td>
          <td valign="bottom">
            <div class="ovtabl_head1">
              <h3 class="color1_bg_thme4">{{ trans('index/language.over007') }}</h3>
              <p>{{ trans('index/language.over008') }} <br> {{ trans('index/language.over009') }}</p>
              <img src="{{asset('/img/nuevaWeb/icon_sam_administration.png')}}">
              <a href="<?= $lib->nv0012 ?>" class="color1_bg_thme4">{{ trans('index/language.over0010') }}</a>
            </div>
          </td>
        </tr>
        <tr>
          <td>
            <div class="ovtabl_head_lft">
              <p>{{ trans('index/language.over0011') }}</p>
              <p>{{ trans('index/language.over0012') }}</p>
              <p>{{ trans('index/language.over0013') }}</p>
            </div>
          </td>
          <td>
            <div class="ovtabl_head2">
              <p class="color2_bg_thme1">{{ trans('index/language.over0014') }}</p>
              <p class="color2_bg_thme1">{{ trans('index/language.over0015') }} </p>
              <p class="color2_bg_thme1">{{ trans('index/language.over0016') }}</p>
            </div>
          </td>
          <td>
            <div class="ovtabl_head2">
              <p class="color2_bg_thme2">{{ trans('index/language.over0017') }}</p>
              <p class="color2_bg_thme2">{{ trans('index/language.over0018') }} </p>
              <p class="color2_bg_thme2">{{ trans('index/language.over0019') }} </p>
            </div>
          </td>
          <td>
            <div class="ovtabl_head2">
              <p class="color2_bg_thme3">{{ trans('index/language.over0020') }}</p>
              <p class="color2_bg_thme3">{{ trans('index/language.over0021') }}</p>
              <p class="color2_bg_thme3">{{ trans('index/language.over0022') }}</p>
            </div>
          </td>
          <td>
            <div class="ovtabl_head2">
              <p class="color2_bg_thme4">{{ trans('index/language.over0023') }}</p>
              <p class="color2_bg_thme4">{{ trans('index/language.over0024') }}</p>
              <p class="color2_bg_thme4">{{ trans('index/language.over0025') }}</p>
            </div>
          </td>
        </tr>
        <tr>
          <td class="ovtabl_head3"> {{ trans('index/language.over0026') }}</td>
          <td colspan="4" class="ovtabl_head3 ovtabl_head3_hd"> {{ trans('index/language.over0027') }} </td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0028') }} <span>{{ trans('index/language.over0029') }}</span>
          </td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0030') }} <span>{{ trans('index/language.over0031') }}</span>
          </td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0032') }} <span>{{ trans('index/language.over0033') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_head3"> {{ trans('index/language.over0034') }} </td>
          <td colspan="4" class="ovtabl_head3"></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0035') }} <span>{{ trans('index/language.over0036') }}</span>
          </td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
           {{ trans('index/language.over0037') }} <span>{{ trans('index/language.over0038') }}</span>
          </td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
           {{ trans('index/language.over0039') }} <span>{{ trans('index/language.over0040') }}</span>
          </td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_head3"> {{ trans('index/language.over0041') }} </td>
          <td colspan="4" class="ovtabl_head3"></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0042') }} <span>{{ trans('index/language.over0043') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
           {{ trans('index/language.over0044') }} <span>{{ trans('index/language.over0045') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
           {{ trans('index/language.over0046') }} <span>{{ trans('index/language.over0047') }}</span>
          </td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_head3"> {{ trans('index/language.over0048') }} </td>
          <td colspan="4" class="ovtabl_head3"></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0049') }} <span>{{ trans('index/language.over0050') }}</span>
          </td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0051') }} <span>{{ trans('index/language.over0052') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0053') }}
          </td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0054') }} <span>{{ trans('index/language.over0055') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_head3"> {{ trans('index/language.over0056') }} </td>
          <td colspan="4" class="ovtabl_head3"></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0057') }} <span>{{ trans('index/language.over0058') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0059') }} <span>{{ trans('index/language.over0060') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0061') }} <span>{{ trans('index/language.over0062') }}</span>
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_head3"> {{ trans('index/language.over0063') }} </td>
          <td colspan="4" class="ovtabl_head3"></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0064') }}
          </td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0065') }}
          </td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
          <td></td>
        </tr>
        <tr>
          <td class="ovtabl_body">
            {{ trans('index/language.over0066') }}
          </td>
          <td></td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
        <tr>
          <td class="ovtabl_head3"> {{ trans('index/language.over0067') }} </td>
          <td colspan="4" class="ovtabl_head3"></td>
        </tr>
        <tr>
          <td class="ovtabl_body"> {{ trans('index/language.over0068') }} </td>
          <td></td>
          <td></td>
          <td><i class="fa fa-check"></i></td>
          <td><i class="fa fa-check"></i></td>
        </tr>
      </table>
    </div>
  </div>
</div>
<?= $lib->lg0153 ?>
<script>
  $(document).ready(function(){
  $('img[usemap]').rwdImageMaps(); 
  });
</script>
@endsection