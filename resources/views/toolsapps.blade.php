@extends('layouts.header')

@section('content')
<style type="text/css">
  .menu_resourses,
  .menu_sub_tools{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>

<div class="container">
    <div class="row news-main rsp_0142140519">
       <p class="newsmain-hadsty">{{ trans('index/language.toolsapp01') }}</p>
       <p class="newsmain-hadsty2">{{ trans('index/language.toolsapp02') }}</p>

       <div class="col-md-4 col-sm-6 col-xs-12">
        <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool1.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp03') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp04') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ===================================== -->
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool2.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp05') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp06') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- =============================================================== -->
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool3.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp07') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp08') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ==================================================================== -->
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool4.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp09') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp010') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ================================================================== -->
         <div class="col-md-4 col-sm-6 col-xs-12">
            <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool5.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp011') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp012') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ======================================================================== -->
       <div class="col-md-4 col-sm-6 col-xs-12">
          <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool6.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp013') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp014') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ======================================================================== -->
       <div class="col-md-4 col-sm-6 col-xs-12">
          <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool7.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp015') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp016') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ======================================================================== -->
       <div class="col-md-4 col-sm-6 col-xs-12">
          <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool8.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp017') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp018') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ======================================================================== -->
       <div class="col-md-4 col-sm-6 col-xs-12">
          <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool9.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp019') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp020') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ======================================================================== -->
       <div class="col-md-4 col-sm-6 col-xs-12">
          <a class="own-toolslinkhref" href="#">
        <div class="own-toolmainlist">
        <div class="own-toolmainimg">
          <img src="{{asset('/img/nuevaWeb/la-tool10.jpg')}}"> 
          <p>{{ trans('index/language.toolsapp021') }}</p>
         </div> 
         <div class="own-toolmainfooter">
          <p>{{ trans('index/language.toolsapp022') }}</p>
         </div>
       </div>
     </a>
       </div>
       <!-- ======================================================================== -->

    </div>
</div>

 <!-- ============================ slider code start ========================= -->
 <div class="col-xs-12" style="padding-top:30px; padding-bottom:30px; background-color: #f6f6f6;">
    <div class="container">
    <div class="row">
        
        <div id="myCarousel1" class="carousel slide abc-sliderdata951" data-ride="carousel">
            <!-- Indicators -->
           <!--  <ol class="carousel-indicators">
              <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel1" data-slide-to="1"></li>
              <li data-target="#myCarousel1" data-slide-to="2"></li>
            </ol> -->

            <!-- Wrapper for slides -->
            <div class="carousel-inner tool-sliderappsty">
                <div class="item active">
                    <img src="{{asset('/img/nuevaWeb/toolsapps1.jpg')}}">
                </div>

                <div class="item">
                   <img src="{{asset('/img/nuevaWeb/toolsapps2.jpg')}}">  
                </div>

                <div class="item">
                  <img src="{{asset('/img/nuevaWeb/toolsapps3.jpg')}}"> 
                </div>
               

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control arrow-leftbgsty bg-transp956" href="#myCarousel1" data-slide="prev">
                <span class="fa fa-angle-left  arrow-slidersty" aria-hidden="true"></span>
                <span class="sr-only">{{ trans('index/language.toolsapp023') }}</span>
            </a>
            <a class="right carousel-control arrow-leftbgsty bg-transp956" href="#myCarousel1" data-slide="next">
                <span class="fa fa-angle-right arrow-slidersty" aria-hidden="true"></span>
                <span class="sr-only">{{ trans('index/language.toolsapp024') }}</span>
            </a>
        </div>
    </div>
</div>
</div>

 <!-- ==========================  Slider code close  ======================== -->

<div class="reasons-whymain954  col-md-12 col-sm-12 col-xs-12">
<div class="container">
  <div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12 six-reasonsimgsty">
      <img  src="{{asset('/img/nuevaWeb/six-reason.png')}}"> 
      
    </div>
  <div class="col-md-4 col-sm-6 col-xs-12 ">
      <div class="list-sixreasondata651">
         <h5> {{ trans('index/language.toolsapp025') }} </h5>
         <p>
           {{ trans('index/language.toolsapp026') }}</p>

<p>{{ trans('index/language.toolsapp027') }}</p>

      </div>
  </div>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <form class="dv6_0424110519">
      <h3>{{ trans('index/language.toolsapp028') }}</h3>
      <input type="text" class="form-control" placeholder="{{ trans('index/language.toolsapp032') }}*">

      <input type="email" class="form-control" placeholder="{{ trans('index/language.toolsapp033') }}*">

      <input type="number" class="form-control" placeholder="{{ trans('index/language.toolsapp034') }}*">

      <input type="text" class="form-control" placeholder="{{ trans('index/language.toolsapp035') }}*">

      <input type="text" class="form-control" placeholder="{{ trans('index/language.toolsapp036') }}">
      <p>{{ trans('index/language.toolsapp029') }} <a href="https://www.licensingassurance.com/declaracionPrivacidad.pdf">{{ trans('index/language.toolsapp030') }}</a></p>
    </form>
    <div class="ebook-downloadlink956">
      <a download="" href="https://www.licensingassurance.com/declaracionPrivacidad.pdf">{{ trans('index/language.toolsapp031') }} <i class="fa fa-caret-down" aria-hidden="true"></i>
</a>
    </div>
  </div>


  </div>
</div>
</div>
<!-- ===================================================== -->

@endsection