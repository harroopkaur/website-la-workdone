@extends('layouts.header')

<style type="text/css">
  .menu_resourses,
  .menu_sub_blog{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>

<style>
.search-form .form-group input.form-control::-webkit-input-placeholder{
  display:none;
}
.search-form .form-group input.form-control::-moz-placeholder{
  display:none;
}

.navbar-right form{
  width:100%;
}
.form-group{
  position:relative;
width:0%;
  min-width:60px;
height:60px;
overflow:hidden;
transition: width 1s;
backface-visibility:hidden;
}
.form-group input.form-control{
  position:absolute;
  top:0;
  right:0;
  outline:none;
  width:100%;
  height:60px;
  margin:0;
  z-index:10;
}
input[type="text"].form-control{
  -webkit-appearence:none;
  -webkit-border-radius:0;
}
.form-control-submit,
.search-label{
  width:60px;
  height:60px;
  position:absolute;
  right:0;
  top:0;
  padding:0;
  margin:0;
  text-align:center;
  cursor:pointer;
  line-height:60px;
  background:#fafafa;
}
.form-control-submit{
  background:#fff; /*stupid IE*/
    opacity: 0;
  color:transparent;
  border:none;
  outline:none;
  z-index:-1;
}
.search-label{
  z-index:90;
  border: solid 1px #ccc;
padding-top: 11px;
}
.search-label i{color: #00b0f0; font-size: 32px;}
.form-group.sb-search-open,
.no-js .sb-search-open{
  width:100%;
}
.form-group.sb-search-open .search-label,
.no-js .sb-search .search-label {
background: #fafafa; color: #fff; z-index: 11;
}
.form-group.sb-search-open .form-control-submit,
.no-js .form-control .form-control-submit {
  z-index: 90;
}
</style>
@section('content')

<div class="container">
    <div class="row news-main">
       <p class="newsmain-hadsty">{{ trans('index/language.blgdet001') }}</p>
       <div class="col-md-3 col-sm-12 col-xs-12 subscibe-blogsty954">
         <a href="#" data-toggle="modal" data-target="#myModal001">{{ trans('index/language.blgdet002') }} <i class="fa fa-caret-down" aria-hidden="true"></i></a>




       </div>
       <div class="col-md-5 col-sm-12 col-xs-12">
       <p class="newsmain-hadsty2">{{ trans('index/language.blgdet003') }}</p>
     </div>
     <div class="col-md-4 col-sm-12 col-xs-12">
       <form class="search-form" role="search">
        <div class="form-group pull-right" id="search">
          <input type="text" class="form-control" placeholder="{{ trans('index/language.blgdet004') }}">
          <button type="submit" class="form-control form-control-submit">{{ trans('index/language.blgdet005') }}</button>
          <span class="search-label"><i class="fa fa-search"></i></span>
        </div>
      </form>
     </div>

     
       <div class="col-md-12 col-sm-12 col-xs-12 we-createspacesty">
        <p>{{ trans('index/language.blgdet006') }} </p>
       </div>
       

    </div>
</div>








<!-- ============================ Blog data start==================== -->
<div class="blog-datalst6542">
  <div class="container">
    <div class="row">
      <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
        <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blgdet007') }}</p>
        </div>        
        <div class="col-md-2 col-sm-2 col-xs-12 mrg_b_20 txt-right965">
          <!-- ======================================== -->
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blgdet008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blgdet009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blgdet0010') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blgdet0011') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blgdet0012') }}</a>
              </li>

            </ul>

  
</div>
        </div>
</div>

        <span class="post-timesty">{{ trans('index/language.blgdet0013') }}</span>
        <div class="blog-datalist6315 blog2-datalist6315">
         <img src="{{asset('/img/nuevaWeb/blog_demo_image.jpg')}}"> 
          <p class="wdt_adjst">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum <b> dolore eu fugiat nulla pariatur.</b></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>





          <ul>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
            <li>Lorem ipsum dolor sit amet, consectetur <b>adipisicing elit, sed do eiusmod</b></li>
          </ul>



          <p><b>Lorem ipsum dolor sit amet,?</b></p>

          
          <p>Lorem ipsum dolor sit amet, <b>consectetur adipisicing elit,</b> <a href="#">sed do eiusmod tempor</a> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>



          
          <p>Lorem ipsum dolor sit amet, <b>consectetur adipisicing elit,</b> <a href="#">sed do eiusmod tempor</a> incididunt ut labore et dolore magna aliqua.</p>





         </div>




        
        <div class="col-md-12 col-sm-12 col-xs-12 mrg_b_20">
          <!-- ======================================== -->
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blgdet008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blgdet009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blgdet0010') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blgdet0011') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blgdet0012') }}</a>
              </li>

            </ul>

  
</div>
        </div>



         <div class="dv1_0358130519">
           <ul>
             <li>Licenses</li>
             <li>Licensing</li>
             <li>Licensing Assurance</li>
             <li>Microsoft</li>
             <li>SaaS</li>
             <li>SAM</li>
             <li>SAMasaService</li>
             <li>SAMforYOU</li>
             <li>Software</li>
             <li>Software Optimization</li>
             <li>Technology</li>
           </ul>
          <div class="textarea">
            <textarea>{{ trans('index/language.blgdet0014') }}</textarea>
            <img src="{{asset('/img/nuevaWeb/chat-iconsty.png')}}">
          </div>
         </div>





      </div>
      <!-- ==================================== -->

















      <div class="sof-manufblog951 sof2-manufblog951">

         <div class="col-md-12 col-sm-12 col-xs-12">  
          <p class="hd1_0457130519">{{ trans('index/language.blgdet0015') }}</p>
        </div>

        <div>
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blgdet0016') }}</p>
        </div>

        </div>
        <span class="post-timesty">{{ trans('index/language.blgdet0017') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blgdet0018') }}</p>
         </div>
         <div class="col-md-12 col-sm-12 col-xs-12 read-more964data">
           <a href="#">{{ trans('index/language.blgdet0019') }}</a>
         </div> 
      </div>


      <div class="sof-manufblog951 sof2-manufblog951">
        <div>
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blgdet0016') }}</p>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blgdet0017') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blgdet0018') }}</p>
         </div>
         <div class="col-md-12 col-sm-12 col-xs-12 read-more964data">
           <a href="#">{{ trans('index/language.blgdet0019') }}</a>
         </div> 
      </div>




















    </div>
  </div>
</div> 












<!-- ==========================Blog data close =========================== -->
<script>
  $(document).ready(function(){
  $('#search').on("click",(function(e){
  $(".form-group").addClass("sb-search-open");
    e.stopPropagation()
  }));
   $(document).on("click", function(e) {
    if ($(e.target).is("#search") === false && $(".form-control").val().length == 0) {
      $(".form-group").removeClass("sb-search-open");
    }
  });
    $(".form-control-submit").click(function(e){
      $(".form-control").each(function(){
        if($(".form-control").val().length == 0){
          e.preventDefault();
          $(this).css('border', '0px solid red');
        }
    })
  })
})
</script>
<script>
 $(document).ready(function(){
 
   $(".linkbutton").hide(),
   $(".facebookbutton").hide(),
   $(".twitterbutton").hide();
   $('.mainbutton').on('click', function(event) {
     $(this).siblings().find(".main-lisscoal965").slideToggle(),
     $(this).siblings().find('.linkbutton').delay(75).toggle('show'),
     $(this).siblings().find('.facebookbutton').delay(50).toggle('show'),
     $(this).siblings().find('.twitterbutton').delay(25).toggle('show');
   });
 });
 </script>
@endsection