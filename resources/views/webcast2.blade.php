@extends('layouts.header')

@section('content')

<div class="container" style="padding-bottom:30px;">
  <div class="row">

	<p class="subtituloLicensing2 text-center">{{ trans('index/language.webcast2001') }}</p>
	<p class="hd1_0500100519 mrg_b_0">{{ trans('index/language.webcast2002') }}</p>





	<div class="col-md-12 dv7_0458110519">
		<h3>{{ trans('index/language.webcast2003') }}</h3>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>





  </div>
</div>














<div class="fondo1">
	<div class="container dv2_0156110519">
	  <div class="row">


			<p class="hd1_0500100519">{{ trans('index/language.webcast2004') }}</p>


			<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="our-software95423">
					<span class="color-green3"></span>
					<div class="data-leftsoftware951">
						<h6>{{ trans('index/language.webcast2005') }}</h6>
					</div> 
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="our-software95423">
					<span class="color-green3"></span>
					<div class="data-leftsoftware951">
						<h6>{{ trans('index/language.webcast2006') }}</h6>
					</div> 
				</div>
			</div>



	  </div>
	</div>
</div>





	<div class="container dv3_0156110519">
	  <div class="row">

			<div class="col-md-12">
				<img src="{{asset('/img/nuevaWeb/webinar_bannr.jpg')}}" class="">
			</div>

		</div>
	</div>






@endsection