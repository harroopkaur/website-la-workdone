@extends('layouts.header')

@section('content')

<div class="container">
    <div class="row news-main">
       <p class="newsmain-hadsty">FIND OUT WHAT'S NEW</p>
       <p class="newsmain-hadsty2">NEWS</p>

       <div class="col-md-2 col-sm-2 col-xs-12">
           <img class="click-menunewsimg" src="{{asset('/img/nuevaWeb/menu-togglesty.png')}}"> 
           <div class="menusub-datanews">
            <ul>
                <li>Licensing Assurance firma alianza comercial con Mazars</li>
                <li>Licensing Assurance participa como sponsor en Common Perú 2018</li>
               <li>Empresarios de Colombia participan en Coffee Talk 2018</li>
                <li>Licensing Assurance organiza desayuno de Negocios en Mexico</li>

             </ul>   
           </div>
       </div>

       <div class="col-md-10 col-sm-10 col-xs-12">

           <div class="news-dataallmeg">
                <div class="row" style="margin-left: 0px; margin-right: 0px; margin-bottom: 35px;">
                    <div class="col-md-6 col-sm-6 col-xs-6 pdf-imgleft">
                        <img src="{{asset('/img/nuevaWeb/pfticon1.png')}}"> 
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 pdf-imgright">
                        <img src="{{asset('/img/nuevaWeb/pfticon2.png')}}"> 
                    </div>
                 </div>   
            <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>

            <p><span>Lorem ipsum dolor</span> sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation <span>ullamco laboris</span> nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
           </div>
       </div>

    </div>
</div> 

<script>

$(".click-menunewsimg").click(function(){
  $(".menusub-datanews").toggle();
}); 
</script>

@endsection