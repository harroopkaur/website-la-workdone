@extends('layouts.header')

<style type="text/css">
  .menu_resourses,
  .menu_sub_blog{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>
<style>
.search-form .form-group input.form-control::-webkit-input-placeholder{
  display:none;
}
.search-form .form-group input.form-control::-moz-placeholder{
  display:none;
}

.navbar-right form{
  width:100%;
}
.form-group{
  position:relative;
width:0%;
  min-width:60px;
height:60px;
overflow:hidden;
transition: width 1s;
backface-visibility:hidden;
}
.form-group input.form-control{
  position:absolute;
  top:0;
  right:0;
  outline:none;
  width:100%;
  height:60px;
  margin:0;
  z-index:10;
}
input[type="text"].form-control{
  -webkit-appearence:none;
  -webkit-border-radius:0;
}
.form-control-submit,
.search-label{
  width:60px;
  height:60px;
  position:absolute;
  right:0;
  top:0;
  padding:0;
  margin:0;
  text-align:center;
  cursor:pointer;
  line-height:60px;
  background:#fafafa;
}
.form-control-submit{
  background:#fff; /*stupid IE*/
    opacity: 0;
  color:transparent;
  border:none;
  outline:none;
  z-index:-1;
}
.search-label{
  z-index:90;
  border: solid 1px #ccc;
padding-top: 11px;
}
.search-label i{color: #00b0f0; font-size: 32px;}
.form-group.sb-search-open,
.no-js .sb-search-open{
  width:100%;
}
.form-group.sb-search-open .search-label,
.no-js .sb-search .search-label {
background: #fafafa; color: #fff; z-index: 11;
}
.form-group.sb-search-open .form-control-submit,
.no-js .form-control .form-control-submit {
  z-index: 90;
}
</style>
@section('content')

<div class="container">
    <div class="row news-main">
       <p class="newsmain-hadsty">{{ trans('index/language.blog001') }}</p>
       <div class="col-md-3 col-sm-12 col-xs-12 subscibe-blogsty954">
         <a href="#" data-toggle="modal" data-target="#myModal001">{{ trans('index/language.blog002') }} <i class="fa fa-caret-down" aria-hidden="true"></i></a>
       </div>
       <div class="col-md-5 col-sm-12 col-xs-12">
       <p class="newsmain-hadsty2">{{ trans('index/language.blog003') }}</p>
     </div>
     <div class="col-md-4 col-sm-12 col-xs-12">
       <form class="search-form" role="search">
        <div class="form-group pull-right" id="search">
          <input type="text" class="form-control" placeholder="{{ trans('index/language.blog0021') }}">
          <button type="submit" class="form-control form-control-submit">{{ trans('index/language.blog004') }}</button>
          <span class="search-label"><i class="fa fa-search"></i></span>
        </div>
      </form>
     </div>

     
       <div class="col-md-12 col-sm-12 col-xs-12 we-createspacesty">
        <p>{{ trans('index/language.blog005') }}</p>
       </div>
       

    </div>
</div>

<!-- ============================ Blog data start==================== -->
<div class="blog-datalst6542">
  <div class="container">
    <div class="row">
      <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== -->

          
     <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== -->

      <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== -->

      <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== -->

      <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== --><!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== -->

      <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== -->

      <!-- ==================================== -->
      <div class="sof-manufblog951">
        <div class="row">
         <div class="col-md-10 col-sm-10 col-xs-12">  
          <p class="had-blogtitile956">{{ trans('index/language.blog006') }}</p>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 txt-right965">
          <button class="mainbutton"><p class="buttontext"><i class="fa fa-share-alt"></i></p></button>
          <div class="share-maindata632">
            <ul class="main-lisscoal965">
              <li>
                <a href="#"><i class="fa fa-link"> </i> {{ trans('index/language.blog007') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-facebook"> </i> {{ trans('index/language.blog008') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-twitter"> </i> {{ trans('index/language.blog009') }}</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-pinterest"> </i> {{ trans('index/language.blog0010') }}</a>
              </li>
               <li>
                <a href="#"><i class="fa fa-envelope"> </i> {{ trans('index/language.blog0011') }}</a>
              </li>

            </ul>

</div>
        </div>
        </div>
        <span class="post-timesty">{{ trans('index/language.blog0012') }}</span>
        <div class="blog-datalist6315">
         <img src="{{asset('/img/nuevaWeb/audit-imgblog.png')}}"> 
          <p>{{ trans('index/language.blog0013') }}</p>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-7 post-commentmainsty951">

          <a href="#"> <i class="fa fa-comments" aria-hidden="true"></i>  {{ trans('index/language.blog0014') }}</a>
         </div>
         <div class="col-md-6 col-sm-6 col-xs-5 read-more964data">
           <a href="<?= $lib->nvs0022 ?>">{{ trans('index/language.blog0015') }}</a>
         </div> 
      </div>
      <!-- ==================================== -->

        
      <!-- ==================================== -->
      <div class="ebook-downloadlink956">
      <a style="background-color: #00b0f0 !important;" href="#">{{ trans('index/language.blog0016') }} <i class="fa fa-caret-down" aria-hidden="true"></i>
</a>
    </div>
    </div>
  </div>
</div> 
<!-- ==========================Blog data close =========================== -->
<script>
  $(document).ready(function(){
  $('#search').on("click",(function(e){
  $(".form-group").addClass("sb-search-open");
    e.stopPropagation()
  }));
   $(document).on("click", function(e) {
    if ($(e.target).is("#search") === false && $(".form-control").val().length == 0) {
      $(".form-group").removeClass("sb-search-open");
    }
  });
    $(".form-control-submit").click(function(e){
      $(".form-control").each(function(){
        if($(".form-control").val().length == 0){
          e.preventDefault();
          $(this).css('border', '0px solid red');
        }
    })
  })
})
</script>
<script>
 $(document).ready(function(){
 
   $(".linkbutton").hide(),
   $(".facebookbutton").hide(),
   $(".twitterbutton").hide();
   $('.mainbutton').on('click', function(event) {
      $(this).siblings().find(".main-lisscoal965").slideToggle(),
     $(this).siblings().find('.linkbutton').delay(75).toggle('show'),
     $(this).siblings().find('.facebookbutton').delay(50).toggle('show'),
     $(this).siblings().find('.twitterbutton').delay(25).toggle('show');
   });
 });
 </script>
@endsection