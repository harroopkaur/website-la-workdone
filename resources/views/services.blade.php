@extends('layouts.header')

@section('content')

<style type="text/css">
	.menu_services{
		color: #1FABE2 !important;
	  background-color: transparent;
	}
</style>
<div style="background-color: #fff;">
<p class="subtituloLicensing2 text-center fadeInSmoove about-haddingpdata">
{{ trans('index/language.lg0059') }}
</p>
   <div class="container">
<div class="row">

<p class="tituloLicensing2 fadeInSmoove">{{ trans('index/language.lg0060') }}</p>

<div class="over-spendingcont">
	<div class="row" style="margin: 0px; margin-top: 26px;">
		
		
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="our-software95423 color-redbg123">
				<span class="color-red1"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.sernew0001') }}</h6>
					<p>{{ trans('index/language.sernew0002') }}</p>
				</div> 
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 small-displaynone951">
			<div class="our-software95423" style="text-align: center;">
				<img src="{{asset('/img/nuevaWeb/benefitssam.png')}}"> 
			</div>
		</div>
<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="our-software95423 color-bluebg123">
				<span class="color-yello4"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.sernew0007') }}</h6>
					<p>{{ trans('index/language.sernew0008') }}</p>
				</div> 
			</div>
		</div>
		
	</div>
	<!-- =================================== -->
	 <div class="row" style="margin: 0px; margin-bottom: 30px;">
	   
		
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="our-software95423 color-greenbg123">
				<span class="color-green3"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.sernew0005') }}</h6>
					<p>{{ trans('index/language.sernew0006') }}</p>
				</div> 
			</div>
		</div>

		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="our-software95423 color-yellobg123">
				<span class="color-blue2"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.sernew0003') }}</h6>
					<p>{{ trans('index/language.sernew0004') }}</p>
				</div> 
			</div>
		</div>

		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="our-software95423 color-darkbluebg123">
				<span class="color-darkblue5"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.sernew0009') }}</h6>
					<p>{{ trans('index/language.sernew00010') }}</p>
				</div> 
			</div>
		</div>
	</div>
	<!-- ====================================== -->
</div>

</div>
</div>
</div>
<!-- ============================================= -->
<div class="why-usmainpart">
	<div class="container">
		<div class="row">
		   

			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="why-usmaindatacont fadeInSmoove hover-shoalldata851">
				<div class="samas-sermain">
					<img src="{{asset('/img/nuevaWeb/customer-support.png')}}" class="stanp-rotatesty">
					<div class="data-showhoversam">
						<p>{{ trans('index/language.sernew00011') }}</p>
						<a href="#">{{ trans('index/language.sernew00012') }}</a>
					</div> 
				</div>
				<div class="sams-footertxt">
					<p>{{ trans('index/language.sernew00013') }}</p>
				</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="why-usmaindatacont fadeInSmoove hover-shoalldata851">
				<div class="samas-sermain">
					<img src="{{asset('/img/nuevaWeb/web.png')}}" class="stanp-rotatesty"> 
					 <div class="data-showhoversam">
						<p>{{ trans('index/language.sernew00014') }}</p>
						<a href="#">{{ trans('index/language.sernew00012') }}</a>
					</div> 
				</div>
				<div class="sams-footertxt">
					<p>{{ trans('index/language.sernew00015') }}</p>
				</div>
				</div>

			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="why-usmaindatacont fadeInSmoove hover-shoalldata851">
				<div class="samas-sermain">
					<img src="{{asset('/img/nuevaWeb/graph.png')}}" class="stanp-rotatesty"> 
					 <div class="data-showhoversam">
						<p>{{ trans('index/language.sernew00016') }}</p>
						<a href="#">{{ trans('index/language.sernew00012') }}</a>
					</div> 
				</div>
				<div class="sams-footertxt">
					<p>{{ trans('index/language.sernew00017') }}</p>
				</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="why-usmaindatacont fadeInSmoove hover-shoalldata851">
				<div class="samas-sermain">
					<img src="{{asset('/img/nuevaWeb/interactive.png')}}" class="stanp-rotatesty"> 
					 <div class="data-showhoversam">
						<p>{{ trans('index/language.sernew00018') }}</p>
						<a href="#">{{ trans('index/language.sernew00012') }}</a>
					</div> 
				</div>
				<div class="sams-footertxt">
					<p>{{ trans('index/language.sernew00019') }}</p>
				</div>
				</div>

			</div>

		</div>
	</div>
</div> 
<!-- ================================================ -->

<div class="serv-downloadmain">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				 <img src="{{asset('/img/nuevaWeb/download-serport.jpg')}}" class="ser-downloadimgsty"> 
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="download-portfoliosty">
					<h4><p>{{ trans('index/language.sernew00020') }}</p></h4>
					<a download href="http://45.248.162.197/website-la/public/img/nuevaWeb/download-serport.jpg"><p>{{ trans('index/language.sernew00021') }}</p></a>
				</div>
			</div>
		</div>
	</div>
</div>


@endsection