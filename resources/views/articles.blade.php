@extends('layouts.header')

@section('content')

<p class="subtituloLicensing2 text-center fadeInSmoove"><?= $lib->lg0124 ?></p>
<p class="tituloLicensing2 fadeInSmoove"><?= $lib->lg0125 ?></p>

<div class="col-xs-12 col-sm-12 fadeInSmoove" style="padding-bottom:30px;">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2">
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0">
                    <div class="row">
                        <div class="col-xs-5 col-sm-5">
                            <div class="row">
                                <img src="{{asset('/img/nuevaWeb/laptop.jpg')}}" class="img img-responsive">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <div class="row">
                                <a href="<?= $lib->lg0126 ?>" target="_blank" class="article subtituloLicensing6"><?= $lib->lg0127 ?></a><br class="hidden-xs"><br class="hidden-xs">
                                <p class="subtituloLicensing8 text-justify"><?= $lib->lg0128 ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0">
                    <div class="row">
                        <div class="col-xs-5 col-sm-5 col-sm-offset-1">
                            <div class="row">
                                <img src="{{asset('/img/nuevaWeb/factoryPYME.png')}}" class="img img-responsive">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <div class="row">
                                <a href="<?= $lib->lg0129 ?>" target="_blank" class="article subtituloLicensing6">
                                <?= $lib->lg0130 ?></a><br><br>
                                <p class="subtituloLicensing8 text-justify"><?= $lib->lg0131 ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 fondo1" style="padding-top: 30px; padding-bottom:30px;">
    <div class="row">        
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
            <div class="row">
                <div class="col-xs-4 col-sm-4">
                    <div class="row">
                        <img src="{{asset('/img/nuevaWeb/BLOG.png')}}" class="img img-responsive fadeInSmoove">
                    </div>
                </div>
                
                <div class="col-sm-8 fadeInSmoove">
                    <a href="<?= $lib->lg0132 ?>" target="_blank" class="article subtituloLicensing2">
                    <?= $lib->lg0133 ?></a><br><br>
                    <p class="subtituloLicensing4 text-justify"><?= $lib->lg0134 ?></p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <br>
        <p class="subtituloLicensing3 text-center colorWhite fadeInSmoove"><strong><i><?= $lib->lg0135 ?></i></strong></p>
    </div>
    
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-2 center-block">
            <div class="center-block btn btn-licensing fadeInSmoove">
                <a href="<?= $lib->lg0132 ?>" target="_blank" class="article1"><span><?= $lib->lg0136 ?></span></a>
            </div>
        </div>
    </div>
</div>

@endsection