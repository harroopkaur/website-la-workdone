
@extends('layouts.header')

@section('content')

<style type="text/css">
	.menu_home{
		color: #1FABE2 !important;
	  background-color: transparent;
	}
</style>

<div style="width:100%; min-height:600px; position:relative;">
		<br>
	
<!--<?= $lib->lg0016 ?>-->
<p class="subtituloLicensing2 text-center">{{ trans('index/language.efective_personalized') }}</p>
<p class="tituloLicensing2">{{ trans('index/language.software_control_solutions') }}</p>

<div class="col-xs-12 backcolorGrisClaro">
   <!--  <div class="row visible-xs">
		<div class="col-xs-4">
			<img src="{{asset('/img/'.$lib->lg0018)}}"  class="img img-responsive rotar" data-rotate="360deg">
		</div>
		<div class="col-xs-4">
			<img src="{{asset('/img/'.$lib->lg0019)}}" class="img img-responsive rotar" data-rotate="360deg">
		</div>
		<div class="col-xs-4">
			<img src="{{asset('/img/'.$lib->lg0020)}}" class="img img-responsive rotar" data-rotate="360deg">
		</div>
	</div> -->
	
	<div class="row">
		<div class="container">
		<div class="col-sm-12 center-block">
			<div class="row">
				<div class="col-sm-4 res-smallshoart956">
					<img src="{{asset('/img/'.$lib->lg0018)}}" class="img img-responsive rotar" data-rotate="360deg">
				</div>
				<div class="col-sm-4 res-smallshoart956">
					<img src="{{asset('/img/'.$lib->lg0019)}}" class="img img-responsive rotar" data-rotate="360deg">
				</div>
				<div class="col-sm-4 res-smallshoart956">
					<img src="{{asset('/img/'.$lib->lg0020)}}" class="img img-responsive rotar" data-rotate="360deg">
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<div class="col-xs-12 fondo1" style="padding-top:20px; padding-bottom:20px;">
	<div class="container">
	<div class="row">
		<div class="col-xs-12  col-sm-12 ">
			<div class="row fadeInSmoove">
				<div class="col-xs-12 col-sm-3 res-marauto965">
					<img src="{{asset('img/nuevaWeb/'.$lib->lg0021)}}" class="img img-responsive">
				</div>
				
				<div class="col-xs-12 col-sm-6 text-center">
					<p class="tituloLicensing2 text-center"></p> 
					<p class="subtituloLicensing2 fuenteOpenSan serbutton65">
						<strong>{{ trans('index/language.best_solution_software') }}</strong>
			   
					   {{ trans('index/language.overshoots_quickly') }}
					</p>
					
				</div>
				
				<div class="col-xs-12 col-sm-3 res-marauto965">
					<img src="{{asset('img/nuevaWeb/'.$lib->lg0025)}}" class="img img-responsive">
				</div>
			</div>
		</div>
	</div> 
	
	<div class="row">
	   
		
				<p class="la-servicesty">{{ trans('index/language.la_service_flow') }}</p>
			<!-- <img src="{{asset('img/nuevaWeb/'.$lib->lg0026)}}" class="img img-responsive"> -->
				<div class="col-xs-12 col-sm-3 la-serdataall">
					<p>{{ trans('index/language.inputs_change') }}</p>
					<div class="img-heightsetsty">
					<img src="{{asset('img/nuevaWeb/input-img1.jpg')}}">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 la-serdataall">
					<p>{{ trans('index/language.process_change') }}</p>
					<div class="img-heightsetsty">
					<img src="{{asset('img/nuevaWeb/process-img3.jpg')}}">
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 la-serdataall">
					<p>{{ trans('index/language.outputs_change') }}</p>
					<div class="img-heightsetsty">
					<img src="{{asset('img/nuevaWeb/outputs-img2.jpg')}}">
					</div>
				</div>
	   
		
	</div>
	
	<br>
	<div class="row">
		<div class="col-xs-12 col-sm-5 col-md-4 center-block">
			<div class="center-block btn btn-licensing" onclick="location.href='<?= $lib->nv0004 ?>';">
				<span>{{ trans('index/language.more') }}</span>
			</div>
		</div>
	</div>
</div>
</div>

<div class="col-xs-12" style="padding-top:20px; padding-bottom:20px;">
	<div class="container">
	<div class="row">
		
		<div id="myCarousel1" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
			  <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
			  <li data-target="#myCarousel1" data-slide-to="1"></li>
			  <li data-target="#myCarousel1" data-slide-to="2"></li>
			  <li data-target="#myCarousel1" data-slide-to="3"></li>
			  <li data-target="#myCarousel1" data-slide-to="4"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<div class="col-xs-10 col-sm-10 center-block">
						<div class="row">
							<iframe class="iframeVideo" src="<?= $lib->lg0028 ?>" frameborder="0" allowfullscreen></iframe>
							<p class="tituloLicensing2 color-carousel text-center video-slidertext">{{ trans('index/language.disrupting_sam') }}</p>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="col-xs-10 col-sm-10 center-block">
						<div class="row">
							<iframe class="iframeVideo" src="<?= $lib->lg0029 ?>" frameborder="0" allowfullscreen></iframe>
							<p class="tituloLicensing2 color-carousel text-center video-slidertext">{{ trans('index/language.licensing_assurance') }}</p> 
						</div>
					</div>
				</div>

				<div class="item">
					<div class="col-xs-10 col-sm-10 center-block">
						<div class="row">
							<iframe class="iframeVideo" src="<?= $lib->lg0030 ?>" frameborder="0" allowfullscreen></iframe>
							<p class="tituloLicensing2 color-carousel text-center video-slidertext">{{ trans('index/language.licensing_assurance') }}</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="col-xs-10 col-sm-10 center-block">
						<div class="row">
							<iframe class="iframeVideo" src="<?= $lib->lg0122 ?>" frameborder="0" allowfullscreen></iframe>
							<p class="tituloLicensing2 color-carousel text-center video-slidertext">{{ trans('index/language.customer_testimonial') }}</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="col-xs-10 col-sm-10 center-block">
						<div class="row">
							<iframe class="iframeVideo" src="<?= $lib->lg012523 ?>" frameborder="0" allowfullscreen></iframe>
							<p class="tituloLicensing2 color-carousel text-center video-slidertext">{{ trans('index/language.customer_testimonial') }}</p>
						</div>
					</div>
				</div>

			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control arrow-leftbgsty" href="#myCarousel1" data-slide="prev">
				<span class="fa fa-angle-left arrow-slidersty"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control arrow-leftbgsty" href="#myCarousel1" data-slide="next">
				<span class="fa fa-angle-right arrow-slidersty"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</div>
</div>
<script>
	$(document).ready(function(){
		$(".rotar").smoove({offset:'40%'});
	});
</script>
<?php 
if(isset($_GET["videos"]) && $_GET["videos"] == "true"){
	echo "<script>"
	. "if($('#myCarousel1').length > 0){"
		. "$('html, body').scrollTop((parseInt($('#myCarousel1').offset().top) - 100));"
	. "}"
	. "if($('#myCarousel').length > 0){"
		. "$('html, body').scrollTop((parseInt($('#myCarousel').offset().top) + 200));"
	. "}"
	. "</script>"; 
}
?>
@endsection
