@extends('layouts.header')

@section('content')
<style type="text/css">
  .menu_services,
  .menu_sub_deployment{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>

<p class="subtituloLicensing2 text-center fadeInSmoove">{{ trans('index/language.lg0111') }}  </p>
<p class="tituloLicensing2 fadeInSmoove">{{ trans('index/language.lg0112') }}</p>

<div class="container" style="padding-bottom:30px;">
  <div class="row">        
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="row">                
		<div class="col-xs-12 col-sm-12 col-md-7 fadeInSmoove dv2_1217110519">
		  <p class="subtituloLicensing2 text-justify">{{ trans('index/language.lg0113') }} <br>
		  	{{ trans('index/language.lgs01136') }}
		  </p>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-5 dv1_1217110519">
		  <iframe class="iframeVideoDiagnostic" src="{{ trans('index/language.lgvid22') }}" frameborder="0" allowfullscreen></iframe>
		</div>
	  </div>
	</div>
  </div>
</div>







<div class="fondo1">
  <div class="container dv4_0500100519">
	<div class="row">
	  <div class="col-md-12">
			<h3 class="hd1_0500100519">{{ trans('index/language.deployment01') }}</h3>
	  </div>

		<div class="col-md-4 col-sm-6 col-xs-12 ">
			<div class="our-software95423 color-greenbg123">
				<span class="color-green3"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.deployment02') }}</h6>
					<p>{{ trans('index/language.deployment03') }}</p>
				</div> 
			</div>
		</div>


		<div class="col-md-4 col-sm-6 col-xs-12 ">
			<div class="our-software95423 color-yellobg123">
				<span class="color-darkblue5"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.deployment04') }}</h6>
					<p>{{ trans('index/language.deployment05') }}</p>
				</div> 
			</div>
		</div>

		<div class="col-md-4 col-sm-6 col-xs-12 ">
			<div class="our-software95423 color-redbg123">
				<span class="color-red1"></span>
				<div class="data-leftsoftware951">
					<h6>{{ trans('index/language.deployment06') }}</h6>
					<p>{{ trans('index/language.deployment07') }}</p>
				</div> 
			</div>
		</div>

		<div class="col-md-6 col-sm-12 col-xs-12 dv2_1239110519">
			<h3> {{ trans('index/language.deployment08') }} </h3>
			<p>{{ trans('index/language.deployment09') }}</p>
			<p>{{ trans('index/language.deployment010') }} <a href="<?= $lib->nv0012 ?>" >{{ trans('index/language.deployment011') }}</a></p>

		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 dv2_1239110519">
			<img src="{{asset('/img/nuevaWeb/'.$lib->lg0114)}}" class="img img-responsive">
		</div>


	</div>
  </div>
</div>



<div class="container dv3_1239110519">
  <div class="row">

  	<div class="col-md-12">
  		<p>{{ trans('index/language.deployment012') }}</p>
  	</div>

  	<div class="col-md-5">
			<span>- {{ trans('index/language.deployment013') }}</span>
			<span>- {{ trans('index/language.deployment014') }}</span>
			<span>- {{ trans('index/language.deployment015') }}</span>
			<span>- {{ trans('index/language.deployment016') }}</span>
  	</div>
  	<div class="col-md-7 dv4_1239110519">
  		<h3>{{ trans('index/language.deployment017') }}</h3>
  		<a href="<?= $lib->nv0012 ?>" class="hover_pins">{{ trans('index/language.deployment018') }}</a>
				<img src="{{asset('/img/nuevaWeb/icon_magnify_puls.png')}}">
  	</div>


  </div>
</div>

		<!-- ================================================================================= -->
<div id="demo">
  <div class="container">
    <div class="row">
      <div class=" ">
      <!--   <div class="customNavigation"> <a class="btn prev"><i class="fa fa-caret-left"></i></a> <a class="btn next"><i class="fa fa-caret-right"></i></a> </div> -->
        <div id="owl-demo" class="owl-carousel">
                <div class="item"><img src="{{asset('/img/nuevaWeb/box.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/adobe.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/asty.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/citrex.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/ibm.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/msty.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/oracle.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/salesforce.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/sap.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/vmware.png')}}"></div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- ===================================================================================== -->
	<!-- <marquee>
		<img src="{{asset('/img/nuevaWeb/marcas.png')}}" style="width:1000px;">
	</marquee> -->









<!-- <div class="col-xs-12 col-sm-12 fondo1" style="padding-top: 30px; padding-bottom:30px;">
  <div class="row">
	<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
	  <div class="col-xs-4 col-sm-4">
		<div class="col-xs-8 col-sm-8 pull-right">
		  <img src="{{asset('/img/nuevaWeb/DDEDITADA.png')}}" class="img img-responsive">
		</div>
	  </div>
	  <div class="col-xs-4 col-sm-4">
		<p class="tituloLicensing2 fadeInSmoove"><?= $lib->lg0115 ?></p>
	  </div>
	  <div class="col-xs-3 col-sm-3 col-xs-offset-1 col-sm-offset-1">
		<div class="col-xs-9 col-sm-9">
		  <a href="https://www.licensingassurance.com/Diagnostic" target="_blank"><img src="{{asset('/img/nuevaWeb/'.$lib->lg0116)}}" class="img img-responsive fadeInSmoove" data-move-x="200px"></a>
		</div>
	  </div>
	</div>
	
	<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
	  <div class="row">
		<div class="col-xs-7 col-sm-7 fadeInSmoove">
		  <ul>
			<li class="subtituloLicensing2"><?= $lib->lg0117 ?></li>
			<li class="subtituloLicensing2"><?= $lib->lg0118 ?></li>
			<li class="subtituloLicensing2"><?= $lib->lg0119 ?></li>
			<li class="subtituloLicensing2"><?= $lib->lg0120 ?></li>
		  </ul>
		  
		  <div class="row fadeInSmoove">
			<div class="visible-xs col-xs-12">
			  <div style="float:left; position:relative; overflow:hidden; width:80%; min-height:30px;">
				<p class="subtituloLicensing7 colorWhite" style="position:absolute; bottom:0;"><strong><i><?= $lib->lg0121 ?></i></strong></p>
			  </div>
			  <img src="{{asset('/img/nuevaWeb/CHECK BLUE1.png')}}" style="float:left; margin-left:0px; margin-left:-20px; width:30px; height:auto;" class="img img-responsive">
			</div>
			
			<div class="hidden-xs col-sm-12">
			  <div style="float:left; position:relative; overflow:hidden; width:80%; min-height:70px;">
				<p class="subtituloLicensing7 colorWhite" style="position:absolute; bottom:0;"><strong><i><?= $lib->lg0121 ?></i></strong></p>
			  </div>
			  <img src="{{asset('/img/nuevaWeb/CHECK BLUE1.png')}}" style="float:left; margin-left:-50px; width:80px; height:auto;" class="img img-responsive">
			</div>
		  </div>
		</div>
		
		<div class="col-xs-12 col-sm-5">
		  <iframe class="iframeVideoDiagnostic" src="<?= $lib->lg0122 ?>" frameborder="0" allowfullscreen></iframe>
		</div>
	  </div>
	</div>
  </div>
</div> -->
<script>
	
	    $(document).ready(function() {
		  var owl = $("#owl-demo");
		  owl.owlCarousel({
		  autoPlay: 1500,
		  items : 5, //10 items above 1000px browser width
		  itemsDesktop : [1000,5], //5 items between 1000px and 901px
		  itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
		  itemsTablet: [600,2], //2 items between 600 and 0;
		  itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
		  pagination:false
      });
      $(".next").click(function(){
          owl.trigger('owl.next');
      })
      $(".prev").click(function(){
          owl.trigger('owl.prev');
      })
    });
	</script>
@endsection