@extends('layouts.header')

@section('content')
<style type="text/css">
  .menu_services,
  .menu_sub_spla{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>
<p class="subtituloLicensing2 text-center fadeInSmoove"><?= $lib->lg0104 ?></p>
<p class="tituloLicensing2 fadeInSmoove"><?= $lib->lg0105 ?></p>

<div class="col-xs-12 col-sm-12" style="padding-bottom:30px;">
    <div class="row">        
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
            <div class="row">
                <div class="col-xs-12 fadeInSmoove">
                    <p class="subtituloLicensing2 text-justify"><?= $lib->lg0106 ?></p><br>
                </div>
                
                <div class="col-xs-7 fadeInSmoove">
                    <p class="subtituloLicensing2 text-justify"><?= $lib->lg0107 ?></p>
                </div>
                
                <div class="col-xs-5 col-sm-5">
                    <img src="{{asset('/img/nuevaWeb/SPLA EDIT.png')}}" class="img img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 fondo1" style="padding-top: 30px; padding-bottom:30px;">
    <div class="row">
        <p class="tituloLicensing2 fadeInSmoove"><?= $lib->lg0108 ?></p>
        
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="row">
                <img src="{{asset('/img/nuevaWeb/'.$lib->lg0109)}}" class="img img-responsive">
            </div>
        </div>
    </div>
    <br>
    <p class="subtituloLicensing3 text-center fadeInSmoove"><?= $lib->lg0110 ?></p>
</div>

@endsection