@extends('layouts.header')

@section('content')
<style type="text/css">
  .menu_resourses,
  .menu_sub_news{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>

<div class="container">
    <div class="row news-main">
       <p class="newsmain-hadsty">{{ trans('index/language.news001') }}</p>
       <p class="newsmain-hadsty2">{{ trans('index/language.news002') }}</p>

       <div class="col-md-2 col-sm-2 col-xs-12">
           <img class="click-menunewsimg" src="{{asset('/img/nuevaWeb/menu-togglesty.png')}}"> 
           <div class="menusub-datanews">
            <ul>
                <li><a href="#">{{ trans('index/language.news003') }} </a></li>
                <li><a href="#">{{ trans('index/language.news004') }} </a></li>
                <li><a href="#">{{ trans('index/language.news005') }} </a></li>
                <li><a href="#">{{ trans('index/language.news006') }} </a></li>

             </ul>   
           </div>
       </div>

       <div class="col-md-10 col-sm-10 col-xs-12">

           <div class="news-dataallmeg">
                <!-- <div class="row" style="margin-left: 0px; margin-right: 0px; margin-bottom: 35px;">
                    <div class="col-md-6 col-sm-6 col-xs-6 pdf-imgleft">
                        <img src="{{asset('/img/nuevaWeb/pfticon1.png')}}"> 
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 pdf-imgright">
                        <img src="{{asset('/img/nuevaWeb/pfticon2.png')}}"> 
                    </div>
                 </div>    -->

                  <div class="news-mainimgpost">
                        <img src="{{asset('/img/nuevaWeb/demo-imgsty.png')}}"> 
                    </div>
            <h5>{{ trans('index/language.news007') }}</h5>
            <p>
            {{ trans('index/language.news008') }}</p>

<p>{{ trans('index/language.news009') }}</p>

<p>{{ trans('index/language.news0010') }}</p>

<p>{{ trans('index/language.news0011') }} </p>

            <h6>{{ trans('index/language.news0012') }}</h6>

           <p>{{ trans('index/language.news0013') }}  </p>

<p>{{ trans('index/language.news0014') }} </p>

<p>{{ trans('index/language.news0015') }} </p>




           </div>
       </div>

    </div>
</div> 

<script>

$(".click-menunewsimg").click(function(){
  $(".menusub-datanews").toggle();
}); 
</script>

@endsection