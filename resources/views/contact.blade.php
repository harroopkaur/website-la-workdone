@extends('layouts.header')

@section('content')
<style type="text/css">
	.menu_contact{
		color: #1FABE2 !important;
	  background-color: transparent;
	}
</style>

<input type="hidden" id="domain_root" value="/">
<input type="hidden" id="mensaje" value="{{ trans('index/language.lg0152') }}">


<div class="cont-formmainbgsty">
	<p class="tituloLicensing2 fadeInSmoove contact-mainhadstymar">
		{{ trans('index/language.lg0137') }}</p>
<div class="container">
	<div class="row">
		<div class="contact-datacontsty">
		<div class='col-xs-12 col-sm-5 fadeInSmoove'>
			<div class="contact-leftdata951">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<p class="subtituloLicensing2">
						{{ trans('index/language.lg0138') }}<br>
						{{ trans('index/language.clg0111') }}<br>
						{{ trans('index/language.clg0112') }}<br>
						{{ trans('index/language.clg0113') }}
					</p>
				</div>
			</div>
		</div>
		</div>

		<div class='col-xs-12 col-sm-7 fadeInSmoove'>
			<div class="contact-formdat951">
			<div class="form-bottom contact-form">
			<!-- <? #$GLOBALS["domain_web_root"] ?>/assets/contact.php -->
				<form role="form" action="#" method="post">
					<input type="hidden" id="antispam" name="antispam" value="12">
					<div class="form-group col-md-6 col-sm-6 col-xs-12 cont-inputmain956">
						<label class="sr-only" for="contact-name">{{ trans('index/language.lg0139') }}</label>
						<input type="text" name="name" placeholder="{{ trans('index/language.lg0139') }}" class="contact-name form-control fondo1" id="contact-name">
					</div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 cont-inputmain956">
						<label class="sr-only" for="contact-email">{{ trans('index/language.lg0140') }}</label>
						<input type="text" name="email" placeholder="{{ trans('index/language.lg0140') }}" class="contact-email form-control fondo1" id="contact-email">
					</div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 cont-inputmain956">
						<label class="sr-only" for="contact-phone">{{ trans('index/language.lg0141') }}</label>
						<input type="text" name="phone" placeholder="{{ trans('index/language.lg0141') }}" class="contact-phone form-control fondo1" id="contact-phone">
					</div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 cont-inputmain956">
						<label class="sr-only" for="contact-country">{{ trans('index/language.lg0142') }}</label>
						<input type="text" name="country" placeholder="{{ trans('index/language.lg0142') }}" class="contact-country form-control fondo1" id="contact-country">
					</div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 cont-inputmain956">
						<label class="sr-only" for="contact-company">{{ trans('index/language.lg0143') }}</label>
						<input type="text" name="company" placeholder="{{ trans('index/language.lg0143') }}" class="contact-company form-control fondo1" id="contact-company">
					</div>
					<div class="form-group col-md-6 col-sm-6 col-xs-12 cont-inputmain956">
						<label class="sr-only" for="contact-subject">{{ trans('index/language.lg0144') }}</label>
						<input type="text" name="subject" placeholder="{{ trans('index/language.lg0144') }}" class="contact-subject form-control fondo1" id="contact-subject">
					</div>
					<div class="form-group col-md-12 col-sm-12 col-xs-12 cont-inputmain956">
						<label class="sr-only" for="contact-message">{{ trans('index/language.lg0145') }}</label>
						<textarea name="message" placeholder="{{ trans('index/language.lg0145') }}" class="contact-message form-control fondo1" id="contact-message"></textarea>
					</div>
					<div class="form-group col-md-12 col-sm-12 col-xs-12 cont-inputmain956" style="text-align: center;">
				<button type="submit" class="btn btn-licensing" id="btn">{{ trans('index/language.lg0146') }}</button>
			</div>
				</form>
			</div>
		</div>
		</div>
	</div>
	</div>
</div>

</div>
<div class="flagmaincont-sty">
	<div class="flagmaincont-layer">
	<div class="container">
	<div class="row">
		<div class="col-xs-12  col-sm-12 center-block">
			<p class="subtituloLicensing3 text-center colorWhite"><strong>{{ trans('index/language.lg0147') }}</strong></p>
		</div>
		

			<ul class="countrt-flagmain">
				<li>
					 <img src="{{asset('/img/nuevaWeb/eeuu.png')}}" class="img img-responsive">
					 <span>
						<strong>{{ trans('index/language.lg0148') }}</strong>
						<p>1 (786) 246 2474 / (305) 851 3545</p>
						<p>Dimitri@licensingassurance.com</p>
					 </span>
				</li>

			   

				<li>
					 <img src="{{asset('/img/nuevaWeb/Peru.png')}}" class="img img-responsive">
					 <span>
						<strong>{{ trans('index/language.lg0154') }}</strong>
						<p>51 997 567081</p>
						<p> melisavidal@licensingassurance.com</p>
					 </span>
				</li>

				<li>
					<img src="{{asset('/img/nuevaWeb/mexico.png')}}" class="img img-responsive">
					<span>
							<strong>{{ trans('index/language.lg0149') }}</strong>
							<p> 52 (155) 543 70148</p>
							<p>david.gonzalez@licensingassurance.com </p>
					</span>
				</li>

				<li>
					<img src="{{asset('/img/nuevaWeb/guatemala.png')}}" class="img img-responsive">
				   <span> 
					<strong>Guatemala</strong>
					<p>502 503 69 119</p>
					<p>luisjurado@licensingassurance.com</p>
					</span>
				</li>

				<li>
					 <img src="{{asset('/img/nuevaWeb/el_salvador.png')}}" class="img img-responsive">
					 <span>
							<strong>El Salvador</strong>
							<p>502 405 39 885</p>
							<p>erickesquivel@licensingassurance.com </p>
					</span>
				</li>

				<li>
					 <img src="{{asset('/img/nuevaWeb/costa rica.png')}}" class="img img-responsive">
					 <span>
						<strong>Costa Rica</strong>
							<p> 506 887 01840</p>
							<p>giovannam@licensingassurance.com </p>
					 </span>
				</li>
				
				<li>
					  <img src="{{asset('/img/nuevaWeb/panama.png')}}" class="img img-responsive">
					<span>
						<strong>{{ trans('index/language.lg0150') }}</strong>
							<p> 507 66 75 1020 / 507 6236 4994 </p>
							<p>ianshaw@licensingassurance.com </p>
					</span>
				</li>

				<li>
					 <img src="{{asset('/img/nuevaWeb/colombia.png')}}" class="img img-responsive">
					<span>
						<strong>Colombia</strong>
							<p> 57 315 305 2663</p>
							<p>jaimevasquez@licensingassurance.com </p>
					</span>
				</li>

				<li>
					  <img src="{{asset('/img/nuevaWeb/ecuador.png')}}" class="img img-responsive">
					<span>
						 <strong>Ecuador</strong>
							<p> 5593 99 5043 7520</p>
							<p>guillermoalmeida@licensingassurance.com </p>
					</span>
				</li> 

				<li>
					 <img src="{{asset('/img/nuevaWeb/brasil.png')}}" class="img img-responsive">
					<span>
						 <strong>{{ trans('index/language.lg0151') }}</strong>
							<p> 55 11 968986241 </p>
							 <p>info@licensingassurance.com </p>
					</span>
				</li>

				<li>
					 <img src="{{asset('/img/nuevaWeb/bolivia.png')}}" class="img img-responsive">
					<span>
						 <strong>Bolivia</strong>
							<p> (591) 78181355</p>
							<p> paola@licensingassurance.com </p>
						</span>
				</li>

				<li>
					  <img src="{{asset('/img/nuevaWeb/chile.png')}}" class="img img-responsive">
					<span>
					   <strong>Chile</strong>
							<p> 569 764 83992</p>
							<p>ernestozapata@licensingassurance.com </p>
					</span>
				</li>

			</ul> 
	   
		  
	</div>
</div>
</div>
</div>


<div class="footer-officecontectsty">
	<div class="container">
		<div class="row">
			 <p class="cont-footertxtsty651">{{ trans('index/language.lg0158') }}</p>
			<ul class="countrt-flagmain">
				<li>
						<img src="{{asset('/img/nuevaWeb/location-changesty.png')}}" class="img img-responsive">
					<span>
					  <strong>{{ trans('index/language.lg0159') }}</strong>
							<p> maribelperozo@licensingassurance.com </p>
					</span>
				</li>

				<li>
					<img src="{{asset('/img/nuevaWeb/location-changesty.png')}}" class="img img-responsive">
					<span>
							<strong>{{ trans('index/language.lg0160') }}</strong>
							<p> lucelvimelo@licensingassurance.com </p>
					</span>
				</li>

				<li>
					   <img src="{{asset('/img/nuevaWeb/location-changesty.png')}}" class="img img-responsive">
					<span>
							  <strong>{{ trans('index/language.lg0161') }}</strong>
							<p> 786-853-4926</p>
							<p>info@licensingassurance.com </p>
					</span>
				</li>

				<li>
					 <img src="{{asset('/img/nuevaWeb/location-changesty.png')}}" class="img img-responsive">
					 <span>
						 <strong>{{ trans('index/language.lg0162') }}</strong>
							<p> (506) 8870 1840</p>
							<p>giovannam@licensingassurance.com </p>
					 </span>
				</li>

				<li>
					 <img src="{{asset('/img/nuevaWeb/location-changesty.png')}}" class="img img-responsive">
					<span>
						<strong>{{ trans('index/language.lg0163') }}</strong>
							<p>(591) 78181355</p>
							<p>jaimevasquez@licensingassurance.com </p>
					</span>
				</li>

				<li>
					<img src="{{asset('/img/nuevaWeb/location-changesty.png')}}" class="img img-responsive">
					<span>
						 <strong>{{ trans('index/language.lg0164') }}</strong>
							<p> 1(305) 851 3545 </p>
							<p> anto@licensingassurance.com </p>
					</span>
				</li>
			</ul>
		</div>
	</div>
</div> 

@endsection