@extends('layouts.header')

@section('content')
<style type="text/css">
	.menu_services,
	.menu_sub_sam_as_services{
		color: #1FABE2 !important;
	  background-color: transparent;
	}
</style>

<div class="container dv2_0500100519">
	<div class="row">
		

		<div class="col-md-12">
			<p class="txt1_0458100519">{{ trans('index/language.samasser001') }}</p>
			<p class="txt4_0458100519">{{ trans('index/language.lg0072') }}</p>
		</div>

		<div class="col-md-6">
			
			<p class="txt3_0458100519"> {{ trans('index/language.samasser002') }} </p>
			<div class="row">



				<div class="col-md-5 col-sm-5 text-right">
					<!-- <img src="{{asset('/img/nuevaWeb/samasaservice112.png')}}" class="img1_0500100519"> -->

					<div class="img1_0645130519">
					<img src="{{asset('/img/nuevaWeb/samasaservice_round112.png')}}">
					<img src="{{asset('/img/nuevaWeb/samasaservice_mark112.png')}}">
						
					</div>
				</div>


				

				



				<div class="col-md-7 col-sm-7">
					<p class="txt2_0458100519 text-left">
						<i>
							<b>
								{{ trans('index/language.samasser003') }} <br>
								{{ trans('index/language.samasser004') }} <br>
								{{ trans('index/language.samasser005') }} 
							</b>
						</i>
					</p>
				</div>
			</div>
			<p class="text-center"><i><b>{{ trans('index/language.samasser006') }} </b></i></p>
		</div>

		<div class="col-md-6 dv1_0500100519" style="margin-top: 0px;">
			<p><i><b> {{ trans('index/language.samasser007') }} </b></i></p>
			<img src="{{asset('/img/nuevaWeb/samasaservice111.png')}}" class="img2_0500100519">
			<a class="img3_0500100519" href="https://www.bsa.org/~/media/Files/StudiesDownload/2018_BSA_GSS_Report_eslatam.pdf">
				<img src="{{asset('/img/nuevaWeb/samasaservice113.png')}}">
			</a>
		</div>


	</div>
</div>





<div class="fondo1">
	<div class="container dv4_0500100519">
		<div class="row">

			<div class="col-md-6 dv5_0500100519">
				<h3 class="hd1_0632100519"> {{ trans('index/language.samasser008') }}</h3>
				<ul class="lst1_0632100519">
					<li>{{ trans('index/language.samasser009') }}</li>
					<li>{{ trans('index/language.samasser0010') }}</li>
					<li>{{ trans('index/language.samasser0011') }}</li>
				</ul>
				<div class="dv1_0632100519">
					<img src="{{asset('/img/nuevaWeb/specialist-user.png')}}">
					<p>{{ trans('index/language.samasser0012') }}</p>
				</div>
			</div>

			<div class="col-md-6 dv6_0500100519">
				<h3 class="hd1_0632100519">{{ trans('index/language.samasser0013') }}</h3>
				<ul class="lst1_0632100519">
					<li>{{ trans('index/language.samasser0014') }}</li>
					<li>{{ trans('index/language.samasser0015') }}</li>
					<li>{{ trans('index/language.samasser0016') }}</li>
				</ul>
				<div class="dv1_0632100519">
					<p>{{ trans('index/language.samasser0017') }} <br> {{ trans('index/language.samasser0018') }}</p>
					<img src="{{asset('/img/nuevaWeb/call-center-operator.png')}}">
				</div>
			</div>

			<div class="col-md-12">
				<h3 class="hd3_0632100519">{{ trans('index/language.samasser0019') }}</h3>
			</div>

			<div class="col-md-6">
				<ul class="lst2_0632100519">
					<li>{{ trans('index/language.samasser0020') }}</li>
					<li>{{ trans('index/language.samasser0021') }} </li>
					<li>{{ trans('index/language.samasser0022') }} </li>
					<li>{{ trans('index/language.samasser0023') }}</li>
				</ul>
			</div>

			<div class="col-md-6">
				<ul class="lst2_0632100519">
					<li>{{ trans('index/language.samasser0024') }} </li>
					<li>{{ trans('index/language.samasser0025') }} </li>
					<li>{{ trans('index/language.samasser0026') }} </li>
					<li>{{ trans('index/language.samasser0027') }}</li>
				</ul>
			</div>


		</div>
		
	</div>

<!-- ================================================================================= -->
<div id="demo">
  <div class="container">
    <div class="row">
      <div class=" ">
      <!--   <div class="customNavigation"> <a class="btn prev"><i class="fa fa-caret-left"></i></a> <a class="btn next"><i class="fa fa-caret-right"></i></a> </div> -->
        <div id="owl-demo" class="owl-carousel">
                <div class="item"><img src="{{asset('/img/nuevaWeb/box.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/adobe.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/asty.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/citrex.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/ibm.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/msty.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/oracle.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/salesforce.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/sap.png')}}"></div>
                <div class="item"><img src="{{asset('/img/nuevaWeb/vmware.png')}}"></div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- ===================================================================================== -->
		
<!-- 	<marquee>
		<img src="{{asset('/img/nuevaWeb/marcas.png')}}" style="width:1000px;">
	</marquee> -->
</div>







<div class="container dv7_0500100519">
	<div class="row">
		<div class="col-md-6 dv8_0500100519">
			<h3>{{ trans('index/language.samasser0028') }}</h3>
			<p>{{ trans('index/language.samasser0029') }}</p>
			<a href="<?= $lib->nv0012 ?>" class="hover_pins">{{ trans('index/language.samasser0030') }}</a>
		</div>
		<div class="col-md-6">
			<img src="{{asset('/img/nuevaWeb/tiempo_chart_image.png')}}" class="img4_0500100519">
		</div>
	</div>
</div>



<script>
	
	    $(document).ready(function() {
		  var owl = $("#owl-demo");
		  owl.owlCarousel({
		  autoPlay: 1500,
		  items : 5, //10 items above 1000px browser width
		  itemsDesktop : [1000,5], //5 items between 1000px and 901px
		  itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
		  itemsTablet: [600,2], //2 items between 600 and 0;
		  itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
		  pagination:false
      });
      $(".next").click(function(){
          owl.trigger('owl.next');
      })
      $(".prev").click(function(){
          owl.trigger('owl.prev');
      })
    });
	</script>
@endsection