@extends('layouts.header')

@section('content')

<style>
            html, body{
                font-family: 'Spinnaker', sans-serif;
            }
        
            /*.container-head{
                padding: 40px;
                width: 100%;
                height: 600px;
                background-image: url("<?//= $GLOBALS["domain_web_root"] ?>/img/webinar/webinar-background.png");
            }*/
            
            .container-footer{
                background-color: #0070c0;
                height: 180px;
                padding: 50px;
            }
            
            .label-webinar{
                background-color: #ffffff;
                color: #002060;
                font-size: 25px;
                font-weight: bold;
                line-height: 50px;
                text-align: center;
                width:150px;
                height: 50px;
                border-radius: 10px 10px 10px 10px;
                -moz-border-radius: 10px 10px 10px 10px;
                -webkit-border-radius: 10px 10px 10px 10px;
                border: 0px solid #000000;
            }
            
            .titulo{
                font-family: 'Merriweather Sans', sans-serif;
                font-size: 55px;
                font-weight: bolder;
                text-align: center;
                color: #ffffff;
            }
            
            .titulo-xs{
                font-family: 'Merriweather Sans', sans-serif;
                font-size: 25px;
                font-weight: bolder;
                text-align: center;
                color: #ffffff;
            }
            
            .subtitulo{
                font-size: 20px;
                text-align: center;
                color: #ffffff;
            }
            
            .subtitulo1{
                font-size: 30px;
                text-align: center;
                color: #fdd400;
                font-style: italic;
                font-weight: bold;
            }
            
            .subtitulo2-xs{
                font-size: 25px;
                text-align: center;
                color: #fdd400;
                font-style: italic;
                font-weight: bold;
            }
            
            .subtitulo-xs{
                font-size: 18px;
                text-align: center;
                color: #ffffff;
            }
            
            .subtitulo1-xs{
                font-size: 20px;
                text-align: center;
                color: #fdd400;
                font-style: italic;
                font-weight: bold;
            }
            
            .subtitulo-azul{
                font-size: 25px;
                font-weight: bold;
                color: #002060;
            }
            
            .contenido{
                /*margin-left: 200px;
                margin-right: 200px;*/
                padding: 50px;
                margin-top: -200px;
                background-color: #ffffff;
                -webkit-box-shadow: 2px 3px 22px -2px rgba(0,0,0,0.75);
                -moz-box-shadow: 2px 3px 22px -2px rgba(0,0,0,0.75);
                box-shadow: 2px 3px 22px -2px rgba(0,0,0,0.75);
                overflow: hidden;
                position:relative;
                z-index:250;
            }
            
            .horario{
                font-family: 'Merriweather Sans', sans-serif;
                font-size: 25px;
                font-weight: bolder;
                color: #0070c0;
                text-align: center;
                margin-top: 140px;
            }
            
            .cont-img-calendar{
                height: 250px;
            }
            
            .img-calen-reloj{
                width:130px;
                height: 130px;
                position: absolute;
                left: 50%;
                margin-left: -55px;
                top: 50%;
                margin-top: -75px;
            }
            
            .inputWebinar{
                border: #0070c0 3px solid;
                font-family: 'Merriweather Sans', sans-serif;
                font-size: 25px;
                width: 100%;
            }
            
            .botonBlue{
                background-color: #0070c0;
                color: #ffffff;
                font-size: 18px;
                font-weight: bold;
                line-height: 40px;
                text-align: center;
                width:100px;
                height: 40px;
                cursor: pointer;
            }
            
            .botonFooter{
                background-color: #ffffff;
                color: #000000;
                font-size: 18px;
                font-weight: bold;
                line-height: 40px;
                text-align: center;
                width:200px;
                height: 40px;
                cursor: pointer;
                -webkit-box-shadow: 2px 8px 22px -2px rgba(0,0,0,0.75);
                -moz-box-shadow: 2px 8px 22px -2px rgba(0,0,0,0.75);
                box-shadow: 2px 8px 22px -2px rgba(0,0,0,0.75);
                position: absolute;
                left: 50%;
                margin-left: -100px;
            }
            
            .botonFooter:hover{
                opacity: 0.7;
            }
            
            .parrafo{
                font-size: 18px;
                color: #0070c0;
                text-align: justify;
            }
            
            .listado>li{
                font-size: 22px;
                color: #0070c0;
            }
        </style>
                <div class="fondoOscuro" id="fondo" style="display:none;">
            <img src="{{asset('/img/loading.gif')}}" style="width:50px; height:50px; top:50%; margin-top:-25px; left:50%; margin-left:-25px; position:absolute;">
        </div>

@endsection