@extends('layouts.header')

@section('content')

<div class="container" style="padding-bottom:30px;">
  <div class="row">

	<p class="subtituloLicensing2 text-center">{{ trans('index/language.webcast01') }}</p>
	<p class="hd1_0500100519">{{ trans('index/language.webcast02') }}</p>



	<div class="col-md-7 col-sm-6 dv5_0424110519">
		<p>{{ trans('index/language.webcast03') }}</p>
		<span>{{ trans('index/language.webcast04') }}</span>
	</div>
	<div class="col-md-5 col-sm-6">
		<form class="dv6_0424110519">
			<h3>{{ trans('index/language.webcast05') }}</h3>
		  <input type="text" class="form-control" placeholder="{{ trans('index/language.webcast06') }}*">

		  <input type="email" class="form-control" placeholder="{{ trans('index/language.webcast07') }}*">

		  <input type="number" class="form-control" placeholder="{{ trans('index/language.webcast08') }}*">

		  <input type="text" class="form-control" placeholder="{{ trans('index/language.webcast09') }}*">

		  <input type="text" class="form-control" placeholder="{{ trans('index/language.webcast010') }}">
		  <p>{{ trans('index/language.webcast011') }} <a href="https://www.licensingassurance.com/declaracionPrivacidad.pdf">{{ trans('index/language.webcast012') }}</a></p>
			  
			<button type="button">{{ trans('index/language.webcast013') }}<span></span></button>
		</form>
	</div>


  </div>
</div>














<div class="fondo1">
	<div class="container dv2_0156110519">
	  <div class="row">


			<p class="hd1_0500100519">{{ trans('index/language.webcast014') }}</p>


			<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="our-software95423">
					<span class="color-green3"></span>
					<div class="data-leftsoftware951">
						<h6>{{ trans('index/language.webcast015') }}</h6>
					</div> 
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 dv1_0156110519">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/ngJSa_jzFEA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="our-software95423">
					<span class="color-green3"></span>
					<div class="data-leftsoftware951">
						<h6>{{ trans('index/language.webcast016') }}</h6>
					</div> 
				</div>
			</div>



	  </div>
	</div>
</div>





	<div class="container dv3_0156110519">
	  <div class="row">

			<div class="col-md-12">
				<img src="{{asset('/img/nuevaWeb/webinar_bannr.jpg')}}" class="">
			</div>

		</div>
	</div>






@endsection