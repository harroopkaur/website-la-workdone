@extends('layouts.header')

@section('content')

<div class="container">
	<div class="row site-mapdatasty">
		<a href="<?= $lib->nv0002 ?>"> {{ trans('index/language.sitemap01') }} </a>
		<a href="<?= $lib->nv0003 ?>">{{ trans('index/language.sitemap02') }}</a>
			<ul>
				<li><a href="<?= $lib->nv0004 ?>">{{ trans('index/language.sitemap03') }}</a></li>
				<li><a href="<?= $lib->nv0005 ?>">{{ trans('index/language.sitemap04') }}</a></li>
				<li><a href="<?= $lib->nvs0015 ?>">{{ trans('index/language.sitemap05') }}</a></li>
				<li><a href="<?= $lib->nv0007 ?>">{{ trans('index/language.sitemap06') }}</a></li>

			</ul>
			<a href="<?= $lib->nv0008 ?>">{{ trans('index/language.sitemap07') }}</a>
			<a href="#">{{ trans('index/language.sitemap08') }}</a>
			<ul>
				<li><a href="<?= $lib->nvs0017 ?>">{{ trans('index/language.sitemap09') }} </a></li>
			   <li><a href="<?= $lib->nvs0016 ?>"> {{ trans('index/language.sitemap010') }} </a></li>
				<li><a href="<?= $lib->nvs0019 ?>">{{ trans('index/language.sitemap011') }} </a></li>

			</ul>
			<a href="#">{{ trans('index/language.sitemap012') }}</a>
			<a href="#">{{ trans('index/language.sitemap013') }}</a>
			<a href="#">{{ trans('index/language.sitemap014') }}</a>
			<a href="#">{{ trans('index/language.sitemap015') }}</a>
			<a href="#">{{ trans('index/language.sitemap016') }}</a>
			<a href="#">{{ trans('index/language.sitemap017') }}</a>
			<a href="#">{{ trans('index/language.sitemap018') }}</a>
			<a href="#">{{ trans('index/language.sitemap019') }}</a>
			<a href="#">{{ trans('index/language.sitemap020') }}</a>
			<a href="#">{{ trans('index/language.sitemap021') }}</a>
			<a href="<?= $lib->nvs0020 ?>">{{ trans('index/language.sitemap022') }}</a>
			<a href="<?= $lib->nv0012 ?>">{{ trans('index/language.sitemap023') }}</a>

	</div>
</div> 


@endsection