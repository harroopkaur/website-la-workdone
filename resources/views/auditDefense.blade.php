@extends('layouts.header')

@section('content')
<style type="text/css">
  .menu_services,
  .menu_sub_audit_defense{
    color: #1FABE2 !important;
    background-color: transparent;
  }
</style>

<div class="auditDefensemain-sty">
<div class="container">
<div class="row">
<p class="subtituloLicensing2 text-center fadeInSmoove"> {{ trans('index/language.lg0093') }}</p>
<p class="tituloLicensing2 fadeInSmoove changes-fontsize956">{{ trans('index/language.lg0094') }}</p>

<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="sol-lieshaving">
        <p>{{ trans('index/language.audit0001') }} </p>

            <p>{{ trans('index/language.audit0002') }}</p>
    </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 mar-leftpos951">
           <img src="{{asset('/img/nuevaWeb/auditoria color.png')}}" class="audit-colorsty951">
                <div class="img1_0645130519 cercleimg-sty">
                    <img src="{{asset('/img/nuevaWeb/samasaservice_round112.png')}}">
                    <img src="{{asset('/img/nuevaWeb/samasaservice_mark112.png')}}">
                        
                    </div>
          <p class="we-prepadesty951 fadeInSmoove">{{ trans('index/language.lg0095') }} </p>
    </div>

</div>
</div>
</div>

 <div class="main-benefitsdata">
    <div class="container">
        <div class="row">
            <h6 class="oriented-helpdata">{{ trans('index/language.audit0003') }}</h6>

            <h2 class="benefits-dataall9562">{{ trans('index/language.audit0004') }}</h2>

             <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="benif-circledata">
                  <img src="{{asset('/img/nuevaWeb/medal.png')}}">
                    <p class="circle-hadfirst621">{{ trans('index/language.audit0005') }}</p>
                    <p class="circle-hadsec9632">{{ trans('index/language.audit0006') }}</p>
                </div>
             </div>

              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="benif-circledata">
                  <img src="{{asset('/img/nuevaWeb/evaluation.png')}}" class="img img-responsive">
                    <p class="circle-hadfirst621">{{ trans('index/language.audit0007') }}</p>
                    <p class="circle-hadsec9632">{{ trans('index/language.audit0008') }}</p>
                </div>
             </div>

              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="benif-circledata">
                  <img src="{{asset('/img/nuevaWeb/negotiating.png')}}" class="img img-responsive">
                    <p class="circle-hadfirst621">{{ trans('index/language.audit0009') }}</p>
                    <p class="circle-hadsec9632">{{ trans('index/language.audit00010') }}</p>
                </div>
             </div>

        </div>
    </div>
 </div>
 <!-- ================= benefits close ==================== -->

<!-- ============== Are you in softwate ================= -->

<div class="in-softwareyou">
    <div class="container">
        <div class="row">
            
            <p class="software-audithad846"> {{ trans('index/language.audit00011') }} </p>
            <p class="software-audithad846">{{ trans('index/language.audit00012') }}</p>
            <div class="mar-topauditsoftware">
            <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="software-helpcan856">
                <p>{{ trans('index/language.audit00013') }}</p>
                <p>{{ trans('index/language.audit00014') }}</p>
            </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="audit-defencebuttonsec">
                 <img src="{{asset('/img/nuevaWeb/warning-newsty.png')}}">
                <button class="hover_pins" data-toggle="modal" data-target="#myModal">{{ trans('index/language.audit00015') }}</button>
                </div>
            </div>
        </div>
    </div>
    </div>    
</div>
<!-- ============== Are you in softwate close ================= -->

 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md modalmidsty-862">
      <div class="modal-content">
     
        <div class="modal-body">
          <form>
  <div class="pop-form">
    <input type="text" placeholder="{{ trans('index/language.audit00016') }}">
  </div>
  <div class="pop-form">
    <input type="email" placeholder="{{ trans('index/language.audit00017') }}">
  </div>
  <div class="pop-form">
    <input type="email" placeholder="{{ trans('index/language.audit00018') }}">
  </div>
  <div class="pop-form">
    <button>{{ trans('index/language.audit00019') }}</button>
  </div>
</form> 
        </div>
     
      </div>
    </div>
  </div>


<script>
    $(".evi1-slide").click(function(){
  $(".evil-open").slideToggle();
});
     $(".evi2-slide").click(function(){
  $(".evi2-open").slideToggle();
});

      $(".evi3-slide").click(function(){
  $(".evi3-open").slideToggle();
});
</script>
@endsection