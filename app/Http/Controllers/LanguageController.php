<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;
class LanguageController extends Controller {

    function changeLanguage($language) {
        App::setLocale($language);
        Session::put('locale', $language);
        return back();
    }
}
