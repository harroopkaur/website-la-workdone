<?php

namespace App\Http\Controllers;

use App\User;
use App\clases\idioma_web_en;
use App\clases\idioma_web_es;
use App\clases\General;
use App\clases\clase_visitas;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Session;
use App;



class HomeController extends Controller
{

    public function index(){

        //dejo lib mientras se hacen todas las traducciones
        $lib = new idioma_web_en();
        //echo "<pre>";print_r($lib);die;

        //si nunca ha elegido ni español ni ingles, por defecto es ingles
        if(Session::get('locale') == ""){
            App::setLocale('en');
            Session::put('locale', 'en');
        }

        //dd($lib);
        $this->registervisitas("Inicio");
    	return view('home')->with(['lib'=>$lib]);

    }
    /* no se va usar mas
    public function index_es(){

        $lib = new idioma_web_es();
        //dd($lib);
        $visitas = new clase_visitas();
        $general = new General();

        $ip = $general->getRealIP();
        $geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
        $city = $geo["geoplugin_city"];
        $country = $geo["geoplugin_countryName"];     
        $visitas->insertar($ip, $country, "About Us");
    	return view('home')->with(['lib'=>$lib]);

    }*/
    public function aboutUs(){
        //if($lang=="en")
        $lib = new idioma_web_en();
        //if($lang=="es")        
        //$lib = new idioma_web_es();

        $this->registervisitas("About us");

        //if($lang=="es"||$lang=="en")        
        return view('aboutUs')->with(['lib'=>$lib]);
        //else
        //return redirect("/");

    }
    public function SamAsaServices(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("Sam as a Service");

        return view('samasaservice')->with(['lib'=>$lib]);
    }
    public function auditDefense(){

        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
       ///$lib = new idioma_web_es();
        $this->registervisitas("Audit Defense");

        return view('auditDefense')->with(['lib'=>$lib]);
    }
    public function SPLA(){
        
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("SPLA");

        return view('SPLA')->with(['lib'=>$lib]);
    }
    public function deployment(){
        
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("Deployment");

        return view('deployment')->with(['lib'=>$lib]);
    }
    public function overview(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("Overview");

        return view('overview')->with(['lib'=>$lib]);
    }
    public function articles(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("articles");

        return view('articles')->with(['lib'=>$lib]);
    }
    public function contact(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("articles");

        return view('contact')->with(['lib'=>$lib]);
    }
    public function services(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("services");

        return view('services')->with(['lib'=>$lib]);
    }
     public function datacenters(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("datacenters");

        return view('datacenters')->with(['lib'=>$lib]);
    }

     public function webinar(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("webinar");

        return view('webinar')->with(['lib'=>$lib]);
    }

    public function news(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("news");

        return view('news')->with(['lib'=>$lib]);
    }
 public function webcast(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("webcast");

        return view('webcast')->with(['lib'=>$lib]);
    }
    public function webcast2(){
        $lib = new idioma_web_en();
        $this->registervisitas("webcast2");
        return view('webcast2')->with(['lib'=>$lib]);
    }

    public function sitemap(){
        $lib = new idioma_web_en();
        $this->registervisitas("sitemap");
        return view('sitemap')->with(['lib'=>$lib]);
    }

    public function toolsapps(){
        $lib = new idioma_web_en();
        $this->registervisitas("toolsapps");
        return view('toolsapps')->with(['lib'=>$lib]);
    }

    public function blog(){
        $lib = new idioma_web_en();
        $this->registervisitas("blog");
        return view('blog')->with(['lib'=>$lib]);
    }

     public function blogdetail(){
        $lib = new idioma_web_en();
        $this->registervisitas("blogdetail");
        return view('blogdetail')->with(['lib'=>$lib]);
    }
    
    public function webinar_licenciamiento_SAP(){
        ///if($lang=="en")
        $lib = new idioma_web_en();
        ///if($lang=="es")        
        ///$lib = new idioma_web_es();
        $this->registervisitas("Webinar SAP");

        return view('layouts.webinar')->with(['lib'=>$lib]);    
    }
    public function registervisitas($pages){
        $visitas = new clase_visitas();
        $general = new General();

        $ip = $general->getRealIP();
        $geo = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip));
        $city = $geo["geoplugin_city"];
        $country = $geo["geoplugin_countryName"];     
        $visitas->insertar($ip, $country, $pages);
    }

}
