<?php

namespace App\Clases;

class clase_archivos_increm_fabricantes extends General{
    ########################################  Atributos  ########################################
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $fabricante) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO archivosIncrementoFabricantes (cliente, empleado, fabricante, fechaRegistro) VALUES '
            . '(:cliente, :empleado, :fabricante, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    //Update
    function actualizarDespliegue1($cliente, $empleado, $fabricante, $fecha, $archivo, $tipoDespliegue) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosIncrementoFabricantes SET archivoDespliegue1 = :archivo, tipoDespliegue = :tipoDespliegue '
            . 'WHERE cliente = :cliente AND empleado = :empleado AND fabricante = :fabricante AND fechaRegistro = :fecha');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo, 
            ':tipoDespliegue'=>$tipoDespliegue, ':fecha'=>$fecha));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarDespliegue2($cliente, $empleado, $fabricante, $fecha, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosIncrementoFabricantes SET archivoDespliegue2 = :archivo WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante AND fechaRegistro = :fecha');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo, ':fecha'=>$fecha));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarDespliegue3($cliente, $empleado, $fabricante, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosIncrementoFabricantes SET archivoDespliegue3 = :archivo WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarCompras($cliente, $empleado, $fabricante, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE archivosIncrementoFabricantes SET archivoCompras = :archivo WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':archivo'=>$archivo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function obtenerUltFecha($cliente, $empleado, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(fechaRegistro) AS fechaRegistro FROM archivosIncrementoFabricantes WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante));
            $row = $sql->fetch();
            return $row["fechaRegistro"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return "";
        }
    }
    
    function archivosDespliegue($cliente, $empleado, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM archivosIncrementoFabricantes WHERE cliente = :cliente '
            . 'AND empleado = :empleado AND fabricante = :fabricante AND fechaRegistro = :fecha');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function eliminarIncremFab($cliente, $empleado, $fabricante){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM archivosIncrementoFabricantes WHERE cliente = :cliente '
            . 'AND fabricante = :fabricante');
            $sql->execute(array(':cliente'=>$cliente, ':fabricante'=>$fabricante));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}