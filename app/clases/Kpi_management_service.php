<?php

namespace App\Clases;


// este era el error que dejo el antiguo programador
// asi que comente ese fragmento
// require_once('clase_general.php');

class Kpi_management_service extends General
{
    var $id;
    var $cliente;
    var $equipo;
    var $os;
    var $dias1;
    var $dias2;
    var $dias3;
    var $min;
    var $activo;
    var $tipo;
    var $rango;
    var $errors;
    var $error = NULL;
    var $archivo;
    var $ActivoAD;
    var $LaTool;
    var $usabilidad;
    var $SQLServer;
    var $office;
    var $visio;
    var $project;
    var $visual;

    function compras_isntalaciones($cliente)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT IFNULL(SUM(CASE WHEN compra > 0 THEN compra * precio ELSE 0 END), 0) AS comprasDol,
                IFNULL(SUM(CASE WHEN instalaciones > 0 THEN instalaciones * precio ELSE 0 END), 0) AS instalacionesDol,
                IFNULL(SUM(IFNULL(compra, 0)), 0) AS compras, IFNULL(SUM(IFNULL(instalaciones, 0)), 0) AS instalaciones
            FROM balance_office2
            WHERE cliente = :cliente");
            $sql->execute(array(':cliente' => $cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }


    function software_inactivo()
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(resumen_office2.familia) AS cantidadSoftwareInactivo
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo AND
                    resumen_office2.cliente = resumen_office2.cliente
                WHERE resumen_office2.cliente = 10 AND
                detalles_equipo2.rango NOT IN (1, 2, 3) AND detalles_equipo2.os LIKE '%Windows%'");
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    // OBTENER TODOS LOS REGISTROS
    // function listar_todo1Asignacion($cliente)
    // {
    //     try{
    //         $this->conexion();
    //         $sql = $this->conn->prepare('SELECT IFNULL(SUM(CASE WHEN compra > 0 THEN compra * precio ELSE 0 END), 0) AS comprasDol,
    //         IFNULL(SUM(CASE WHEN instalaciones > 0 THEN instalaciones * precio ELSE 0 END), 0) AS instalacionesDol,
    //         IFNULL(SUM(IFNULL(compra, 0)), 0) AS compras, IFNULL(SUM(IFNULL(instalaciones, 0)), 0) AS instalaciones
    //     FROM balance_office2
    //     WHERE cliente = :cliente ');
    //         $sql->execute(array(':cliente'=>$cliente));
    //         return $sql->fetchAll();
    //     }catch(PDOException $e){
    //         $this->error = $e->getMessage();
    //         return array();
    //     }
    // }

    // OBTENER ID
    // function total($cliente)
    // {
    //     try{
    //         $this->conexion();
    //         $sql = $this->conn->prepare('SELECT COUNT(resumen_office2.familia) AS cantidadSoftwareInactivo
    //         FROM resumen_office2
    //             INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo AND
    //             resumen_office2.cliente = resumen_office2.cliente
    //         WHERE resumen_office2.cliente = :cliente AND
    //         detalles_equipo2.rango NOT IN (1, 2, 3) AND detalles_equipo2.os LIKE '%Windows%'');
    //         $sql->execute(array(':cliente'=>$cliente));
    //         $row = $sql->fetch();
    //         return $row["cantidad"];
    //     }catch(PDOException $e){
    //         $this->error = $e->getMessage();
    //         return 0;
    //     }
    // }


    // Obtener listado de todos los Usuarios
    //     function listar($cliente, $empleado) {
    //     $this->conexion();
    //     $query = "SELECT IFNULL(SUM(CASE WHEN compra > 0 THEN compra * precio ELSE 0 END), 0) AS comprasDol,
    //             IFNULL(SUM(CASE WHEN instalaciones > 0 THEN instalaciones * precio ELSE 0 END), 0) AS instalacionesDol,
    //             IFNULL(SUM(IFNULL(compra, 0)), 0) AS compras, IFNULL(SUM(IFNULL(instalaciones, 0)), 0) AS instalaciones
    //         FROM balance_office2
    //         WHERE cliente = :cliente";
    //     try{
    //         $sql = $this->conn->prepare($query);
    //         $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
    //         return $sql->fetchAll();
    //     }catch(PDOException $e){
    //         $this->error = $e->getMessage();
    //         return array();
    //     }
    // }

    //        function listaraplicaciones($cliente, $empleado) {
    //     $this->conexion();
    //     $query = "SELECT COUNT(resumen_office2.familia) AS cantidadSoftwareInactivo
    //             FROM resumen_office2
    //                 INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo AND
    //                 resumen_office2.cliente = resumen_office2.cliente
    //             WHERE resumen_office2.cliente = 10 AND
    //             detalles_equipo2.rango NOT IN (1, 2, 3) AND detalles_equipo2.os LIKE '%Windows%'";
    //     try{
    //         $sql = $this->conn->prepare($query);
    //         $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
    //         return $sql->fetchAll();
    //     }catch(PDOException $e){
    //         $this->error = $e->getMessage();
    //         return array();
    //     }
    // }

    function listar_todo($cliente)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(resumen_office2.familia) AS cantidadSoftwareInactivo
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo AND
                    resumen_office2.cliente = resumen_office2.cliente
                WHERE resumen_office2.cliente = :cliente AND
                detalles_equipo2.rango NOT IN (1, 2, 3) AND detalles_equipo2.os LIKE '%Windows%'");
            $sql->execute(array(':cliente' => $cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }


    function listar_todoKpi($cliente)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT IFNULL(SUM(CASE WHEN compra > 0 THEN compra * precio ELSE 0 END), 0) AS comprasDol,
                IFNULL(SUM(CASE WHEN instalaciones > 0 THEN instalaciones * precio ELSE 0 END), 0) AS instalacionesDol,
                IFNULL(SUM(IFNULL(compra, 0)), 0) AS compras, IFNULL(SUM(IFNULL(instalaciones, 0)), 0) AS instalaciones
            FROM balance_office2
            WHERE cliente = :cliente");
            $sql->execute(array(':cliente' => $cliente));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array("comprasDol"=>0, "instalacionesDol"=>0, "compras"=>0, "instalaciones"=>0);
        }
    }
}

?>