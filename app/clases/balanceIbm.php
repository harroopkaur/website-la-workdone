<?php

namespace App\Clases;

class balanceIbm extends General{
    ########################################  Atributos  ########################################
    public  $lista = array();
    public  $listaNoIncluir = array();
    public  $error = NULL;
    var $id;
    var $cliente;
    var $officce;
    var $instalaciones;
    var $compra;
    var $balance;
    var $tipo;    
    var $archivo;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $office, $version, $precio, $instalaciones, $compra, $balance, $balancec, $tipo, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_ibm(cliente, empleado, familia, office, version, precio, instalaciones, compra, balance, balancec, tipo, asignacion) VALUES '
            . '(:cliente, :empleado, :familia, :office, :version, :precio, :instalaciones, :compra, :balance, :balancec, :tipo, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':office'=>$office, ':version'=>$version, ':precio'=>$precio, ':instalaciones'=>$instalaciones, ':compra'=>$compra, ':balance'=>$balance, ':balancec'=>$balancec, ':tipo'=>$tipo, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM balance_ibm WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function imprimirTablaExcelBalanza($tabla, $objPHPExcel, $i){
        foreach ($tabla as $reg_equipos) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $reg_equipos["familia"])
                ->setCellValue('B' . $i, $reg_equipos["office"])
                ->setCellValue('C' . $i, $reg_equipos["version"])
                ->setCellValue('D' . $i, $reg_equipos["instalaciones"])
                ->setCellValue('E' . $i, $reg_equipos["compra"])
                ->setCellValue('F' . $i, $reg_equipos["balance"])
                ->setCellValue('G' . $i, $reg_equipos["balancec"]);
            $i++;
        }
    }
    
    function listarBalanzaAgregar($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balanzaGeneral.familia,
                    balanzaGeneral.edicion,
                    balanzaGeneral.version,
                    SUM(balanzaGeneral.instalacion) AS instalacion,
                    SUM(balanzaGeneral.compra) AS compra,
                    balanzaGeneral.asignacion
                FROM (SELECT balanza.familia,
                        balanza.edicion,
                        balanza.version,
                        0 AS instalacion,
                        balanza.compra,
                        balanza.asignacion
                    FROM (SELECT productos.familia,
                            productos.edicion,
                            productos.version,
                            compras_ibm.compra,
                            compras_ibm.asignacion
                        FROM (SELECT productos.nombre AS familia,
                                ediciones.nombre AS edicion,
                                versiones.nombre AS version
                            FROM tabla_equivalencia
                                INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                                INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                                LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                                LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                            WHERE tabla_equivalencia.fabricante = 2
                            ORDER BY familia, edicion, version
                        ) AS productos
                        LEFT JOIN compras_ibm ON productos.familia = compras_ibm.familia AND productos.edicion = compras_ibm.edicion
                        AND productos.version = compras_ibm.version AND compras_ibm.cliente = :cliente AND compras_ibm.empleado = :empleado
                        GROUP BY productos.familia, productos.edicion, productos.version, compras_ibm.asignacion
                    ) balanza

                    UNION

                    SELECT resumen.familia,
                        resumen.edicion,
                        resumen.version,
                        COUNT(resumen.familia) AS instalaciones,
                        0 AS compra,
                        resumen.asignacion
                    FROM (SELECT resumen_ibm.familia,
                            resumen_ibm.edicion,
                            resumen_ibm.version,
                            detalles_equipo_ibm.asignacion
                        FROM resumen_ibm
                            INNER JOIN detalles_equipo_ibm ON resumen_ibm.cliente = detalles_equipo_ibm.cliente AND
                            resumen_ibm.empleado = detalles_equipo_ibm.empleado AND resumen_ibm.equipo = detalles_equipo_ibm.equipo
                       WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado
                       GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version
                    ) AS resumen
                    GROUP BY resumen.familia, resumen.edicion, resumen.version, resumen.asignacion
                ) balanzaGeneral
                GROUP BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion
                ORDER BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function obtenerPrecio($cliente, $empleado, $familia, $edicion, $version){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(compras_ibm.precio, IFNULL(productos.precio, 0)) AS precio
                FROM (SELECT productos.nombre AS familia,
                        ediciones.nombre AS edicion,
                        versiones.nombre AS version,
                        tabla_equivalencia.precio
                    FROM tabla_equivalencia
                        INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                        INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                        LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                        LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                    WHERE tabla_equivalencia.fabricante = 2
                    ORDER BY familia, edicion, version
                    ) AS productos
                    LEFT JOIN compras_ibm ON productos.familia = compras_ibm.familia AND productos.edicion = compras_ibm.edicion
                    AND productos.version = compras_ibm.version AND compras_ibm.cliente = :cliente AND compras_ibm.empleado = :empleado
                WHERE productos.familia = :familia AND productos.edicion = :edicion AND productos.version = :version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia,
            ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            $precio = 0;
            if($row["precio"] != ""){
                $precio = $row["precio"];
            }
            return $precio;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function balanzaAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        $where = "";
        if($edicion == ""){
            $where .= " AND (balance_ibm.office = '' OR balance_ibm.office IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND balance_ibm.office = :edicion ";
            $array[':edicion'] = $edicion;
        }
        
        if($asignacion != ""){
            $where .= " AND balance_ibm.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where .= " AND balance_ibm.asignacion IN (" . $asignacion . ") ";
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_ibm.id,
                    balance_ibm.familia,
                    balance_ibm.office,
                    balance_ibm.version,
                    balance_ibm.asignacion,
                    balance_ibm.instalaciones,
                    (balance_ibm.compra - balance_ibm.instalaciones) AS balance,
                    (balance_ibm.compra - balance_ibm.instalaciones) * balance_ibm.precio AS balancec,
                    balance_ibm.compra,
                    (balance_ibm.compra - balance_ibm.instalaciones) AS disponible,
                    balance_ibm.precio
                FROM balance_ibm
                WHERE balance_ibm.cliente = :cliente AND balance_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ' . $where . '
                GROUP BY balance_ibm.familia, balance_ibm.office, balance_ibm.version, balance_ibm.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function balanzaAsignacion1($cliente, $familia, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        
        if($asignacion != ""){
            $where = " AND balance_ibm.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND balance_ibm.asignacion IN (" . $asignacion . ") ";
        }
               
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_ibm.id,
                    balance_ibm.familia,
                    balance_ibm.office,
                    balance_ibm.version,
                    balance_ibm.asignacion,
                    balance_ibm.instalaciones,
                    (balance_ibm.compra - balance_ibm.instalaciones) AS balance,
                    (balance_ibm.compra - balance_ibm.instalaciones) * balance_ibm.precio AS balancec,
                    balance_ibm.compra,
                    (balance_ibm.compra - balance_ibm.instalaciones) AS disponible,
                    balance_ibm.precio
                FROM balance_ibm
                WHERE balance_ibm.cliente = :cliente AND balance_ibm.familia = :familia ' . $where . '
                GROUP BY balance_ibm.familia, balance_ibm.office, balance_ibm.version, balance_ibm.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    // Obtener listado de todos los Usuarios
    function productosNoIncluir($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia
                FROM balance_ibm
                WHERE cliente = :cliente AND empleado = :empleado AND
                    familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 != "Otros" AND idMaestra = 1)
                GROUP BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaNoIncluir = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_ibm WHERE cliente = :cliente AND familia = :familia AND office = :edicion ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_ibm
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_ibm WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias2($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_ibm WHERE cliente = :cliente AND familia = :familia ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_ibm
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familiasGrafico($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_ibm WHERE cliente = :cliente AND familia = :familia AND office = :edicion AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias1Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_ibmSam WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_familias1SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_ibmSam WHERE archivo = :archivo AND familia = :familia ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familiasSAM($cliente, $empleado, $familia, $edicion) {        
        try{
            $this->conexion();
        //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia AND office = :edicion ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_ibmSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                AND office = :edicion
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_todo_familias2SAM($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            //$sql = $this->conn->prepare('SELECT * FROM balance_adobe WHERE cliente = :cliente AND familia = :familia ORDER BY version');
            $sql = $this->conn->prepare('SELECT *
                FROM balance_ibmSam
                WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csv2/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
    
    function balanceEjecutivo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia,
                    SUM(compra) AS totalCompras,
                    SUM(instalaciones) AS totalInstalaciones,
                    SUM(compra) - SUM(instalaciones) AS neto,
                    precio,
                    (SUM(compra) - SUM(instalaciones)) * precio AS total
                FROM balance_ibm
                WHERE cliente = :cliente AND empleado = :empleado AND precio > 0
                GROUP BY familia
                ORDER BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function sobranteFaltante($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ABS(ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) < 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0)) AS faltante,
                    ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) > 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0) AS sobrante
                FROM balance_ibm
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("faltante"=>0, "sobrante"=>0);
        }
    }
}