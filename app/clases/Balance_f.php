<?php

namespace App\Clases;

class Balance_f extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $officce;
    var $instalaciones;
    var $compra;
    var $balance;
    var $tipo;
    var $error = NULL;
    var $archivo;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $office, $version, $precio, $instalaciones, $compra, 
    $balance, $balancec, $tipo, $tipoCompra, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO balance_office2 (cliente, empleado, familia, office, '
            . 'version, precio, instalaciones, compra, balance, balancec, tipo, tipoCompra, asignacion) '
            . 'VALUES (:cliente, :empleado, :familia, :office, :version, :precio, :instalaciones, :compra, '
            . ':balance, :balancec, :tipo, :tipoCompra, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':office'=>$office, 
            ':version'=>$version, ':precio'=>$precio, ':instalaciones'=>$instalaciones, ':compra'=>$compra, 
            ':balance'=>$balance, ':balancec'=>$balancec, ':tipo'=>$tipo, ':tipoCompra'=>$tipoCompra, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM balance_office2 WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    function listarBalanzaAgregar($cliente, $empleado){
        try{
            $this->conexion();
            /*$sql = $this->conn->prepare('SELECT balanzaGeneral.familia,
                    balanzaGeneral.edicion,
                    balanzaGeneral.version,
                    SUM(balanzaGeneral.instalacion) AS instalacion,
                    SUM(balanzaGeneral.compra) AS compra,
                    balanzaGeneral.tipo,
                    balanzaGeneral.asignacion
                FROM (SELECT balanza.familia,
                        balanza.edicion,
                        balanza.version,
                        0 AS instalacion,
                        balanza.compra,
                        balanza.tipo,
                        balanza.asignacion
                    FROM (SELECT productos.familia,
                            productos.edicion,
                            productos.version,
                            compras2.compra,
                            compras2.tipo,
                            compras2.asignacion
                        FROM (SELECT productos.nombre AS familia,
                                ediciones.nombre AS edicion,
                                versiones.nombre AS version
                            FROM tabla_equivalencia
                                INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                                INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                                LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                                LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                            WHERE tabla_equivalencia.fabricante = 3
                            ORDER BY familia, edicion, version
                        ) AS productos
                        LEFT JOIN compras2 ON productos.familia = compras2.familia AND productos.edicion = compras2.edicion
                        AND productos.version = compras2.version AND compras2.cliente = :cliente AND compras2.empleado = :empleado
                        GROUP BY productos.familia, productos.edicion, productos.version, compras2.asignacion
                    ) balanza

                    UNION

                    SELECT resumen.familia,
                        resumen.edicion,
                        resumen.version,
                        COUNT(resumen.familia) AS instalaciones,
                        0 AS compra,
                        "" AS tipo,
                        resumen.asignacion
                    FROM (SELECT resumen_office2.familia,
                            resumen_office2.edicion,
                            resumen_office2.version,
                            detalles_equipo2.asignacion
                        FROM resumen_office2
                            INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND
                            resumen_office2.empleado = detalles_equipo2.empleado AND resumen_office2.equipo = detalles_equipo2.equipo
                       WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado
                       GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ) AS resumen
                    GROUP BY resumen.familia, resumen.edicion, resumen.version, resumen.asignacion

                    UNION

                    SELECT detalles_equipo2.familia,
                        detalles_equipo2.edicion,
                        detalles_equipo2.version,
                        COUNT(detalles_equipo2.familia) AS instalaciones,
                        0 AS compra,
                        "" AS tipo,
                        detalles_equipo2.asignacion
                    FROM detalles_equipo2
                    WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.empleado = :empleado AND detalles_equipo2.familia LIKE "%Windows%"
                    GROUP BY detalles_equipo2.familia, detalles_equipo2.edicion, detalles_equipo2.version, detalles_equipo2.asignacion
                ) balanzaGeneral
                GROUP BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion
                ORDER BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.asignacion');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));*/
            $sql = $this->conn->prepare('SELECT balanzaGeneral.familia,
                    balanzaGeneral.edicion,
                    balanzaGeneral.version,
                    SUM(balanzaGeneral.instalacion) AS instalacion,
                    SUM(balanzaGeneral.compra) AS compra,
                    balanzaGeneral.tipo,
                    balanzaGeneral.asignacion
                FROM (SELECT balanza.familia,
                        balanza.edicion,
                        balanza.version,
                        0 AS instalacion,
                        balanza.compra,
                        balanza.tipo,
                        balanza.asignacion
                    FROM (SELECT productos.familia,
                            productos.edicion,
                            productos.version,
                            SUM(IFNULL(compras2.compra, 0)) AS compra,
                            IFNULL(compras2.tipo, "") AS tipo,
                            IFNULL(compras2.asignacion, "") AS asignacion
                        FROM (SELECT productos.nombre AS familia,
                                ediciones.nombre AS edicion,
                                versiones.nombre AS version
                            FROM tabla_equivalencia
                                INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                                INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                                LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                                LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                            WHERE tabla_equivalencia.fabricante = 3
                        ) AS productos
                        LEFT JOIN compras2 ON productos.familia = compras2.familia AND productos.edicion = compras2.edicion
                        AND productos.version = compras2.version AND compras2.cliente = :cliente
                        GROUP BY productos.familia, productos.edicion, productos.version, compras2.tipo, compras2.asignacion
                    ) balanza

                    UNION

                    SELECT resumen.familia,
                        resumen.edicion,
                        resumen.version,
                        COUNT(resumen.familia) AS instalaciones,
                        0 AS compra,
                        "" AS tipo,
                        resumen.asignacion
                    FROM (SELECT resumen_office2.familia,
                            resumen_office2.edicion,
                            resumen_office2.version,
                            IFNULL(detalles_equipo2.asignacion, "") AS asignacion
                        FROM resumen_office2
                            INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND resumen_office2.equipo = detalles_equipo2.equipo
                       WHERE resumen_office2.cliente = :cliente
                       GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version, detalles_equipo2.asignacion
                    ) AS resumen
                    GROUP BY resumen.familia, resumen.edicion, resumen.version, resumen.asignacion

                    UNION

                    SELECT detalles_equipo2.familia,
                        detalles_equipo2.edicion,
                        detalles_equipo2.version,
                        COUNT(detalles_equipo2.familia) AS instalaciones,
                        0 AS compra,
                        "" AS tipo,
                        detalles_equipo2.asignacion
                    FROM detalles_equipo2
                    WHERE detalles_equipo2.cliente = :cliente AND detalles_equipo2.familia LIKE "%Windows%"
                    GROUP BY detalles_equipo2.familia, detalles_equipo2.edicion, detalles_equipo2.version, detalles_equipo2.asignacion
                ) balanzaGeneral
                GROUP BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.tipo, balanzaGeneral.asignacion
                ORDER BY balanzaGeneral.familia, balanzaGeneral.edicion, balanzaGeneral.version, balanzaGeneral.tipo, balanzaGeneral.asignacion');
            $sql->execute(array(':cliente'=>$cliente));

            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function obtenerPrecio($cliente, $empleado, $familia, $edicion, $version){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(compras2.precio, IFNULL(productos.precio, 0)) AS precio
                FROM (SELECT productos.nombre AS familia,
                        ediciones.nombre AS edicion,
                        versiones.nombre AS version,
                        tabla_equivalencia.precio
                    FROM tabla_equivalencia
                        INNER JOIN fabricantes ON tabla_equivalencia.fabricante = fabricantes.idFabricante
                        INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto
                        LEFT JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion
                        LEFT JOIN versiones ON tabla_equivalencia.version = versiones.id
                    WHERE tabla_equivalencia.fabricante = 3
                    ORDER BY familia, edicion, version
                    ) AS productos
                    LEFT JOIN compras2 ON productos.familia = compras2.familia AND productos.edicion = compras2.edicion
                    AND productos.version = compras2.version AND compras2.cliente = :cliente
                WHERE productos.familia = :familia AND productos.edicion = :edicion AND productos.version = :version');
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia,
            ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            $precio = 0;
            if($row["precio"] != ""){
                $precio = $row["precio"];
            }
            return $precio;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function balanzaAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%');
        if($familia != "Others"){
            $where1 = " AND balance_office2.familia = :familia ";
            $array[":familia"] = $familia;
        } else{
            $where1 = " AND balance_office2.familia IN ('Visual Studio', 'Exchange Server', 'Sharepoint Server', 'Skype for Business', 'System Center') ";
        }
        
        
        if($asignacion != ""){
            $where = " AND balance_office2.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND balance_office2.asignacion IN (" . $asignacion . ") ";
        }
        
        if ($edicion == "Professional"){
            $edicion = " AND (balance_office2.office LIKE :edicion OR balance_office2.office LIKE '%ProPlus%')";
        }
        else{
            $edicion = " AND balance_office2.office LIKE :edicion ";
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_office2.id,
                    balance_office2.familia,
                    balance_office2.office,
                    balance_office2.version,
                    balance_office2.asignacion,
                    balance_office2.instalaciones,
                    (SUM(IFNULL(balance_office2.compra, 0)) - SUM(IFNULL(balance_office2.instalaciones, 0))) AS balance,
                    (SUM(IFNULL(balance_office2.compra, 0)) - SUM(IFNULL(balance_office2.instalaciones, 0))) * balance_office2.precio AS balancec,
                    SUM(IFNULL(balance_office2.compra, 0)) AS compra,
                    (SUM(IFNULL(balance_office2.compra, 0)) - SUM(IFNULL(balance_office2.instalaciones, 0))) AS disponible,
                    balance_office2.precio,
                    balance_office2.cantidadGAP,
                    balance_office2.totalGAP,
                    balance_office2.balanceGAP
                FROM balance_office2
                WHERE balance_office2.cliente = :cliente ' . $where1 . $edicion . $where . '
                GROUP BY balance_office2.familia, balance_office2.office, balance_office2.version, balance_office2.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function balanzaAsignacionOtros($cliente, $familia, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        
        if($familia == "Windows"){
            $notIn = "'Enterprise', 'Professional'";
        } else if($familia == "Windows Server" || $familia == "SQL Server"){
            $notIn = "'Standard', 'Datacenter', 'Enterprise'";
        } else{
            $notIn = "'Standard', 'Professional', 'Professional Plus', 'Office 365 ProPlus'";
        }
        
        if($asignacion != ""){
            $where = " AND balance_office2.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;            
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND balance_office2.asignacion IN (" . $asignacion . ") ";
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT balance_office2.id,
                    balance_office2.familia,
                    balance_office2.office,
                    balance_office2.version,
                    balance_office2.asignacion,
                    balance_office2.instalaciones,
                    (SUM(IFNULL(balance_office2.compra, 0)) - SUM(IFNULL(balance_office2.instalaciones, 0))) AS balance,
                    (SUM(IFNULL(balance_office2.compra, 0)) - SUM(IFNULL(balance_office2.instalaciones, 0))) * balance_office2.precio AS balancec,
                    SUM(IFNULL(balance_office2.compra, 0)) AS compra,
                    (SUM(IFNULL(balance_office2.compra, 0)) - SUM(IFNULL(balance_office2.instalaciones, 0))) AS disponible,
                    balance_office2.precio,
                    balance_office2.cantidadGAP,
                    balance_office2.totalGAP,
                    balance_office2.balanceGAP
                FROM balance_office2
                WHERE balance_office2.cliente = :cliente AND balance_office2.familia = :familia
                AND balance_office2.office NOT IN (' . $notIn . ') ' . $where . '
                GROUP BY balance_office2.familia, balance_office2.office, balance_office2.version, balance_office2.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_cliente1($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND '
            . '(familia LIKE "%Visio%" OR familia LIKE "%Project%" OR familia LIKE "%Visual%" OR familia LIKE "%System%" ' 
            . 'OR familia LIKE "%Exchange%" OR familia LIKE "%Sharepoint%" OR familia LIKE "%Skype%") ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_cliente2($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Contar el total de Usuarios
    /*function total() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM balance_office2 WHERE id!=0');
            $sql->execute();
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_tipo($tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE tipo = :tipo ORDER BY id');
            $sql->execute(array(':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_office($cliente, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND office LIKE :edicion ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_office3($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND (office LIKE "%Professional%" OR office LIKE "%profesional%") ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Contar el total de Usuarios
    /*function total_office3($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM balance_office2 WHERE cliente = :cliente AND (office LIKE "%Professional%" OR office LIKE "%profesional%") ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/

    // Obtener listado de todos 
    /*function listar_todo_office2($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND '
            . 'id NOT IN (SELECT id FROM balance_office2 WHERE cliente = :cliente AND (office LIKE "%Professional%" '
            . 'OR office LIKE "%Standard%" OR office LIKE "%Profesional%")) ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Verificar si e-mail ya existe
    /*function cliente_existe($cliente, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM balance_office2 WHERE cliente = :cliente AND id != :id');
            $sql->execute(array(':cliente'=>$cliente, ':id'=>$id));
            $row = $sql->fetch();
            if($row["cantidad"] > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND office LIKE :edicion ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familiasGrafico($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND office LIKE :edicion AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=> '%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias1($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias1Sam($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias1SamArchivo($archivo, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE archivo = :archivo AND familia LIKE :familia AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias6($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND familia  NOT LIKE "%Windows Server%" AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias6Sam($cliente, $empleado, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND familia  NOT LIKE "%Windows Server%" AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias6SamArchivo($archivo, $familia, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE archivo = :archivo AND familia LIKE :familia AND familia  NOT LIKE "%Windows Server%" AND office LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias8($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" '
            . 'AND office NOT LIKE "%Datacenter%" AND office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias9($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Datacenter%" AND '
            . 'office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias9Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Datacenter%" AND '
            . 'office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias9SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE archivo = :archivo AND familia LIKE :familia '
            . 'AND office NOT LIKE "%Standard%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Datacenter%" AND '
            . 'office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias7($cliente, $empleado, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia  NOT LIKE "%Windows%" '
            . 'AND familia  NOT LIKE "%Office%" AND familia  NOT LIKE "%Project%" AND familia  NOT LIKE "%visio%" AND '
            . 'familia  NOT LIKE "%SQL%" AND familia LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias7Sam($cliente, $empleado, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE cliente = :cliente AND empleado = :empleado AND familia  NOT LIKE "%Windows%" '
            . 'AND familia  NOT LIKE "%Office%" AND familia  NOT LIKE "%Project%" AND familia  NOT LIKE "%visio%" AND '
            . 'familia  NOT LIKE "%SQL%" AND familia LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias7SamArchivo($archivo, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE archivo = :archivo AND familia  NOT LIKE "%Windows%" '
            . 'AND familia  NOT LIKE "%Office%" AND familia  NOT LIKE "%Project%" AND familia  NOT LIKE "%visio%" AND '
            . 'familia  NOT LIKE "%SQL%" AND familia LIKE :edicion ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias10($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias10Sam($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo_familias10SamArchivo($archivo, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2Sam WHERE archivo = :archivo AND familia LIKE :familia '
            . 'AND familia  NOT LIKE "%Windows Server%" AND office NOT LIKE "%Enterprise%" AND office NOT LIKE "%Professional%" ORDER BY familia, office, version');
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familiasSAM($cliente, $familia, $edicion, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia AND office LIKE :edicion ORDER BY version');
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias2($cliente, $empleado, $familia, $edicion, $edicion2) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND '
            . 'office NOT LIKE :edicion AND  office NOT LIKE :edicion2 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias2Grafico($cliente, $empleado, $familia, $edicion, $edicion2) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND  office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familias2SAM($cliente, $familia, $edicion, $edicion2, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia AND office NOT LIKE :edicion AND  office NOT LIKE :edicion2 ORDER BY version');
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias3($cliente, $empleado, $familia, $edicion, $edicion2, $edicion3) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND  office NOT LIKE :edicion3 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', 
            ':edicion2'=>'%' . $edicion2 . '%', ':edicion3'=>$edicion3));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias3Grafico($cliente, $empleado, $familia, $edicion, $edicion2, $edicion3) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND  office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND  office NOT LIKE :edicion3 AND instalaciones > 0 ORDER BY version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', 
            ':edicion2'=>'%' . $edicion2 . '%', ':edicion3'=>$edicion3));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familias3SAM($cliente, $familia, $edicion, $edicion2, $edicion3, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia AND  office NOT LIKE :edicion AND  office NOT LIKE :edicion2 AND  office NOT LIKE '
            . ':ediicon3 ORDER BY version');
            $sql->execute(array(':arcgivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%', 
            ':edicion2'=>'%' . $edicion2 . '%', ':edicion3'=>$edicion3));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo_familias4($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia ORDER BY office,version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo_familias4Grafico($cliente, $empleado, $familia) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
            . 'FROM balance_office2 '
            . 'WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia '
            . 'AND instalaciones > 0 ORDER BY office,version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todo_familias5($cliente, $familia, $tipo){ 
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balance_office2 WHERE cliente = :cliente AND familia = :familia '
            . 'AND tipo = :tipo ORDER BY office,version');
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    /*function listar_todo_familias4SAM($cliente, $familia, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM balanceOffice2Sam WHERE archivo = :archivo AND cliente = :cliente '
            . 'AND familia = :familia ORDER BY office,version');
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csv2/" . $titulo_archivo;
            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
    
    function verifSoftwareAssurance($cliente, $empleado, $familia, $edicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
                . 'FROM balance_office2 '
                . 'WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia AND office = :edicion '
                . 'AND tipoCompra = "software assurance"');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function actualizarGAP($id, $cantidadGAP, $totalGAP){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE balance_office2 SET cantidadGAP = :cantidadGAP, totalGAP = :totalGAP, balanceGAP = precio * :totalGAP WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':cantidadGAP'=>$cantidadGAP, ':totalGAP'=>$totalGAP));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function cabeceraReporteBalanza($myWorkSheet, $idioma = 1){
        if($idioma == 1){
            $myWorkSheet->setCellValue('A1', 'Producto')
                    ->setCellValue('B1', 'Edición')
                    ->setCellValue('C1', 'Versión')
                    ->setCellValue('D1', 'Asignaciones')
                    ->setCellValue('E1', 'Instalaciones')
                    ->setCellValue('F1', 'Compras')
                    ->setCellValue('G1', 'Neto')
                    ->setCellValue('H1', 'Precio')
                    ->setCellValue('I1', 'Cantidad GAP')
                    ->setCellValue('J1', 'Neto GAP');
        }else{
            $myWorkSheet->setCellValue('A1', 'Product')
                    ->setCellValue('B1', 'Edition')
                    ->setCellValue('C1', 'Version')
                    ->setCellValue('D1', 'Allocations')
                    ->setCellValue('E1', 'Installations')
                    ->setCellValue('F1', 'Purchases')
                    ->setCellValue('G1', 'Available ')
                    ->setCellValue('H1', 'Price')
                    ->setCellValue('I1', 'GAP Amount')
                    ->setCellValue('J1', 'GAP Available');
        }
    }
    
    function detalleReporteBalanza($myWorkSheet, $objPHPExcel, $listar_Of, $indice){
        $i = 2;
        foreach ($listar_Of as $reg_equipos) {
            $myWorkSheet->setCellValue('A' . $i, $reg_equipos["familia"])
                        ->setCellValue('B' . $i, $reg_equipos["office"])
                        ->setCellValue('C' . $i, $reg_equipos["version"])
                        ->setCellValue('D' . $i, $reg_equipos["asignacion"])
                        ->setCellValue('E' . $i, $reg_equipos["instalaciones"])
                        ->setCellValue('F' . $i, round($reg_equipos["compra"], 0))
                        ->setCellValue('G' . $i, round($reg_equipos["balance"], 0))
                        ->setCellValue('H' . $i, round($reg_equipos["balancec"], 0))
                        ->setCellValue('I' . $i, $reg_equipos["cantidadGAP"])
                        ->setCellValue('J' . $i, $reg_equipos["totalGAP"]);
            $i++;
        }
        $objPHPExcel->addSheet($myWorkSheet, $indice);
    }
    
    function balanceEjecutivo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT familia,
                    SUM(compra) AS totalCompras,
                    SUM(instalaciones) AS totalInstalaciones,
                    SUM(compra) - SUM(instalaciones) AS neto,
                    precio,
                    (SUM(compra) - SUM(instalaciones)) * precio AS total
                FROM balance_office2
                WHERE cliente = :cliente AND empleado = :empleado AND precio > 0
                GROUP BY familia
                ORDER BY familia');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function sobranteFaltante($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ABS(ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) < 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0)) AS faltante,
                    ROUND(SUM(IF(IFNULL(compra, 0) - IFNULL(instalaciones, 0) > 0, IFNULL(compra, 0) - IFNULL(instalaciones, 0), 0)), 0) AS sobrante
                FROM balance_office2
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("faltante"=>0, "sobrante"=>0);
        }
    }
}