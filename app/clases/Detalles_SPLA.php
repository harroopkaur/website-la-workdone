<?php

namespace App\Clases;

class Detalles_SPLA extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $equipo;
    var $os;
    var $dias1;
    var $dias2;
    var $dias3;
    var $min;
    var $activo;
    var $tipo;
    var $rango;
    var $errors;
    var $error = NULL;
    var $archivo;
    var $ActivoAD;
    var $LaTool;
    var $usabilidad;
    var $SQLServer;
    var $office;
    var $visio;
    var $project;
    var $visual;
 
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $os, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipo_SPLA (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES (:cliente, :empleado, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo, 
            ':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipo_SPLA (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango, whenCreated) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    /*function insertarSAM($archivo, $cliente, $equipo, $os, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detallesEquipo2Sam (archivo, cliente, equipo, os, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES (:archivo, :cliente, :equipo, :os, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)");
            $sql->execute(array(':archivo'=>$archivo, ':cliente'=>$cliente, ':equipo'=>$equipo, ':os'=>$os, ':dias1'=>$dias1, 
            ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo, ':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Actualizar
    function actualizar($cliente, $empleado, $equipo, $errors) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo_SPLA SET errors = :errors "
            . "WHERE equipo = :equipo AND cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':equipo'=>$equipo, ':cliente'=>$cliente, ':empleado'=>$empleado, ':errors'=>$errors));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function actualizarSAM($cliente, $equipo, $errors, $archivo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detallesEquipo2Sam SET errors = :errors "
            . "WHERE equipo = :equipo AND cliente = :cliente AND archivo = :archivo");
            $sql->execute(array(':equipo'=>$equipo, ':cliente'=>$cliente, ':archivo'=>$archivo, ':errors'=>$errors));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function eliminarSAM($cliente, $archivo) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM detallesEquipo2Sam WHERE cliente = :cliente AND archivo = :archivo");
            $sql->execute(array(':cliente'=>$cliente, ':archivo'=>$archivo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Obtener datos 
    /*function datos($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->equipo = $usuario['equipo'];
            $this->os = $usuario['os'];
            $this->dias1 = $usuario['dias1'];
            $this->dias2 = $usuario['dias2'];
            $this->dias3 = $usuario['dias3'];
            $this->minimo = $usuario['minimo'];
            $this->activo = $usuario['activo'];
            $this->tipo = $usuario['tipo'];
            $this->rango = $usuario['rango'];
            $this->errors = $usuario['errors'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    /*function datosSAM($id) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLASam WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->equipo = $usuario['equipo'];
            $this->os = $usuario['os'];
            $this->dias1 = $usuario['dias1'];
            $this->dias2 = $usuario['dias2'];
            $this->dias3 = $usuario['dias3'];
            $this->minimo = $usuario['minimo'];
            $this->activo = $usuario['activo'];
            $this->tipo = $usuario['tipo'];
            $this->rango = $usuario['rango'];
            $this->errors = $usuario['errors'];
            $this->archivo = $usuario['archivo'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    /*function datos3($cliente, $equipo) {       
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE equipo = :equipo AND cliente = :cliente");
            $sql->execute(array(':equipo'=>$equipo, ':cliente'=>$cliente));
            $usuario = $sql->fetch();
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->equipo = $usuario['equipo'];
            $this->os = $usuario['os'];
            $this->dias1 = $usuario['dias1'];
            $this->dias2 = $usuario['dias2'];
            $this->dias3 = $usuario['dias3'];
            $this->minimo = $usuario['minimo'];
            $this->activo = $usuario['activo'];
            $this->tipo = $usuario['tipo'];
            $this->rango = $usuario['rango'];
            $this->errors = $usuario['errors'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function datosOptimizacion($id, $equipo, $office, $visio, $project, $visual, $usabilidad, $duplicado) {
        $this->id         = $id;
        $this->equipo     = $equipo;
        $this->office     = $office;
        $this->visio      = $visio;
        $this->project    = $project;
        $this->visual     = $visual;
        $this->usabilidad = $usabilidad;
        $this->duplicado  = $duplicado;
    }*/
    
    /*function datosOptimizacionServidor($id, $equipo, $windowsServer, $SQLServer, $usabilidad, $duplicado) {
        $this->id         = $id;
        $this->equipo     = $equipo;
        $this->os         = $windowsServer;
        $this->SQLServer  = $SQLServer;
        $this->usabilidad = $usabilidad;
        $this->duplicado  = $duplicado;
    }*/

    /*function datos4($id, $equipo, $os, $rango, $tipo, $ActivoAD, $LaTool, $usabilidad) {
        $this->id = $id;
        $this->equipo = $equipo;
        $this->os = $os;
        $this->tipo = $tipo;
        $this->rango = $rango;
        $this->ActivoAD = $ActivoAD;
        $this->LaTool = $LaTool;
        $this->usabilidad = $usabilidad;
    }*/

    /*function listar_installWindows($cliente, $edicion, $version) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad FROM detalles_equipo_SPLA WHERE cliente = :cliente AND os LIKE '%Windows%' AND os NOT LIKE '%Windows Server%' "
            . "AND os LIKE :edicion AND os LIKE :version AND activo = '1'");
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':version'=>'%' . $version . '%'));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND id != 0 AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todo1($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND id != 0 ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_todoEdicion($cliente, $empleado, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND id != 0 AND os LIKE '%Windows%' AND os LIKE :edicion ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todoSAM($cliente) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLASam WHERE cliente = :cliente AND id != 0 AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todoSAMArchivo($archivo) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLASam WHERE archivo = :archivo AND id != 0 AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados
    /*function listar_todo_paginado($cliente, $inicio, $fin) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND id != 0 AND os LIKE '%Windows%' ORDER BY  id LIMIT " . $inicio . ", " . $fin);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Contar el total de Usuarios
    /*function total($cliente) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad FROM detalles_equipo_SPLA WHERE cliente = :cliente AND id != 0 AND os LIKE '%Windows%'");
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_tipo($tipo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE tipo = :tipo AND os LIKE '%Windows%' ORDER BY id");
            $sql->execute(array(':tipo'=>$tipo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    /*function listar_todo_office($cliente, $edicion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND edicion = :edicion "
            . "AND os LIKE '%Windows%' ORDER BY version");
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    function listar_WindowsOS($cliente, $empleado, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado "
            . "AND familia = 'Windows' AND edicion = :edicion AND version = :version AND activo = 1");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    function listar_WindowsServer($cliente, $empleado, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado "
            . "AND familia = 'Windows Server' AND edicion = :edicion AND version = :version AND activo = 1");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todog0($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND id != 0 ORDER BY id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todog0Ordenar($cliente, $empleado, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND id != 0 ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todog0SamDetalle($cliente) {
        $this->conexion();
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo_SPLASam WHERE cliente = :cliente AND os LIKE '%windows%' AND id != 0";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todog0SamDetalleArchivo($archivo) {
        $this->conexion();
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo_SPLASam WHERE archivo = :archivo AND os LIKE '%windows%' AND id != 0";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todog1Ordenar($cliente, $empleado, $edicion, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0 ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todog1SamDetalle($cliente, $edicion) {
        $this->conexion();
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo_SPLASam WHERE cliente = :cliente AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0";        
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todog1SamDetalleArchivo($archivo, $edicion) {
        $this->conexion();
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad
            FROM detalles_equipo_SPLASam WHERE archivo = :archivo AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0";        
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todog2Ordenar($cliente, $empleado, $edicion, $edicion2, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad 
            FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id !=0 ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_todog2SamDetalle($cliente, $edicion, $edicion2) {
        $this->conexion();
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS LaTool,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad 
            FROM detalles_equipo_SPLASam WHERE cliente = :cliente AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0"; 
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_todog2SamDetalleArchvio($archivo, $edicion, $edicion2) {
        $this->conexion();
        $query = "SELECT id,
                equipo,
                os,
                rango,
                tipo,
                IF(rango = 1 OR rango = 2 OR rango = 3, 'Si', 'No') AS ActivoAD,
                IF(errors = 'Ninguno', 'Si', 'No') AS errors,
                CASE
                    WHEN rango = 1 THEN
                         'En Uso'
                    WHEN rango = 2 OR rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad 
            FROM detalles_equipo_SPLASam WHERE archivo = :archivo AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0"; 
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_optimizacion($cliente, $empleado, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "detalles_equipo_SPLA.id";
        }
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLA AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo_SPLA
                LEFT JOIN resumen_office2 AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo_SPLA.equipo = visio.equipo AND detalles_equipo_SPLA.cliente = visio.cliente AND detalles_equipo_SPLA.empleado = visio.empleado AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo_SPLA.equipo = project.equipo AND detalles_equipo_SPLA.cliente = project.cliente AND detalles_equipo_SPLA.empleado = project.empleado AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo_SPLA.equipo = visual.equipo AND detalles_equipo_SPLA.cliente = visual.cliente AND detalles_equipo_SPLA.empleado = visual.empleado AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacionServidor($cliente, $empleado, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "detalles_equipo_SPLA.id";
        }
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                detalles_equipo_SPLA.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLA AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo_SPLA
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo_SPLA.equipo = sqlServer.equipo AND detalles_equipo_SPLA.cliente = sqlServer.cliente AND detalles_equipo_SPLA.empleado = sqlServer.empleado AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows Server'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacion1($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLA AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo_SPLA
                LEFT JOIN resumen_office2 AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia = 'Office'
                LEFT JOIN resumen_office2 AS visio ON detalles_equipo_SPLA.equipo = visio.equipo AND detalles_equipo_SPLA.cliente = visio.cliente AND detalles_equipo_SPLA.empleado = visio.empleado AND visio.familia = 'Visio'
                LEFT JOIN resumen_office2 AS project ON detalles_equipo_SPLA.equipo = project.equipo AND detalles_equipo_SPLA.cliente = project.cliente AND detalles_equipo_SPLA.empleado = project.empleado AND project.familia = 'Project'
                LEFT JOIN resumen_office2 AS visual ON detalles_equipo_SPLA.equipo = visual.equipo AND detalles_equipo_SPLA.cliente = visual.cliente AND detalles_equipo_SPLA.empleado = visual.empleado AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY detalles_equipo_SPLA.equipo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacion1Servidor($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                detalles_equipo_SPLA.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLA AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo_SPLA
                LEFT JOIN resumen_office2 AS sqlServer ON detalles_equipo_SPLA.equipo = sqlServer.equipo AND detalles_equipo_SPLA.cliente = sqlServer.cliente AND detalles_equipo_SPLA.empleado = sqlServer.empleado AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows Server'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY detalles_equipo_SPLA.equipo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_optimizacionSam($cliente) {
        $this->conexion();
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLASam AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
             FROM detalles_equipo_SPLASam AS detalles_equipo_SPLA
                LEFT JOIN resumen_SPLASam AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND office.familia = 'Office'
                LEFT JOIN resumen_SPLASam AS visio ON detalles_equipo_SPLA.equipo = visio.equipo AND detalles_equipo_SPLA.cliente = visio.cliente AND visio.familia = 'Visio'
                LEFT JOIN resumen_SPLASam AS project ON detalles_equipo_SPLA.equipo = project.equipo AND detalles_equipo_SPLA.cliente = project.cliente AND project.familia = 'Project'
                LEFT JOIN resumen_SPLASam AS visual ON detalles_equipo_SPLA.equipo = visual.equipo AND detalles_equipo_SPLA.cliente = visual.cliente AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY detalles_equipo_SPLA.equipo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_optimizacionSamArchivo($archivo) {
        $this->conexion();
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                office.edicion AS office,
                visio.edicion AS visio,
                project.edicion AS project,
                visual.edicion AS visual,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLASam AS detEquipo WHERE detEquipo.archivo = :archivo AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
             FROM detalles_equipo_SPLASam AS detalles_equipo_SPLA
                LEFT JOIN resumen_SPLASam AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND office.familia = 'Office'
                LEFT JOIN resumen_SPLASam AS visio ON detalles_equipo_SPLA.equipo = visio.equipo AND detalles_equipo_SPLA.cliente = visio.cliente AND visio.familia = 'Visio'
                LEFT JOIN resumen_SPLASam AS project ON detalles_equipo_SPLA.equipo = project.equipo AND detalles_equipo_SPLA.cliente = project.cliente AND project.familia = 'Project'
                LEFT JOIN resumen_SPLASam AS visual ON detalles_equipo_SPLA.equipo = visual.equipo AND detalles_equipo_SPLA.cliente = visual.cliente AND visual.familia = 'Visual Studio'
            WHERE detalles_equipo_SPLA.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY detalles_equipo_SPLA.equipo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_optimizacionSamServidor($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                detalles_equipo_SPLA.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLASam AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo_SPLASam AS detalles_equipo_SPLA
                LEFT JOIN resumen_SPLASam AS sqlServer ON detalles_equipo_SPLA.equipo = sqlServer.equipo AND detalles_equipo_SPLA.cliente = sqlServer.cliente AND detalles_equipo_SPLA.empleado = sqlServer.empleado AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows Server'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY detalles_equipo_SPLA.equipo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_optimizacionSamServidorArchivo($archivo) {
        $this->conexion();
        $query = "SELECT detalles_equipo_SPLA.id,
                `detalles_equipo_SPLA`.equipo,
                detalles_equipo_SPLA.edicion AS os,
                sqlServer.edicion AS SQLServer,
                CASE
                    WHEN detalles_equipo_SPLA.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_SPLA.rango = 2 OR detalles_equipo_SPLA.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS usabilidad,
                IF((SELECT COUNT(*) FROM detalles_equipo_SPLASam AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo_SPLA.equipo) > 1, 'Duplicado', '') AS duplicado
            FROM detalles_equipo_SPLASam AS detalles_equipo_SPLA
                LEFT JOIN resumen_SPLASam AS sqlServer ON detalles_equipo_SPLA.equipo = sqlServer.equipo AND detalles_equipo_SPLA.cliente = sqlServer.cliente AND sqlServer.familia = 'SQL Server'
            WHERE detalles_equipo_SPLA.archivo = :archivo AND rango NOT IN (1) AND detalles_equipo_SPLA.familia = 'Windows Server'
            GROUP BY detalles_equipo_SPLA.id
            ORDER BY detalles_equipo_SPLA.equipo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    /*function listar_todog0SAM($cliente, $archivo) {
        $this->conexion();
        $query = "SELECT * FROM detallesEquipo2Sam WHERE archivo = :archivo AND cliente = :cliente AND os LIKE '%windows%' AND id != 0 ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todog1($cliente, $empleado, $edicion) {
        $this->conexion();
        $query = "SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0 ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todog1SAM($cliente, $edicion, $archivo) {
        $this->conexion();
        $query = "SELECT * FROM detallesEquipo2Sam WHERE archivo = :archivo AND cliente = :cliente AND os LIKE '%windows%' AND os LIKE :edicion AND id != 0 ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios
    // Obtener listado de todos los Usuarios
    function listar_todog2($cliente, $empleado, $edicion, $edicion2) {
        $this->conexion();
        $query = "SELECT * FROM detalles_equipo_SPLA WHERE cliente = :cliente AND empleado = :empleado AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0 ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todog2SAM($cliente, $edicion, $edicion2, $archivo) {
        $this->conexion();
        $query = "SELECT * FROM detallesEquipo2Sam WHERE archivo = :archivo AND cliente = :cliente AND os  LIKE '%windows%' AND os NOT LIKE :edicion AND os NOT LIKE :edicion2 AND  id != 0 ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%', ':edicion2'=>$edicion2, ':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    /*function fechaSAM($cliente) {
        $this->conexion();
        $fecha = "";
        $query = "SELECT fecha FROM encGrafSPLASam WHERE cliente = :cliente";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }*/
    
    function fechaSAMArchivo($archivo) {
        $this->conexion();
        $fecha = "";
        $query = "SELECT fecha FROM encGrafSPLASam WHERE archivo = :archivo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }
    
    function cantidadProductosOSUso($cliente, $empleado, $familia){
        $query = "SELECT COUNT(id) AS cantidad
            FROM detalles_equipo_SPLA 
            WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function cantidadProductosInstalados($cliente, $empleado, $familia, $os){
        try{
            $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo_SPLA
                INNER JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia = :familia
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = :os AND office.edicion != ''";
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':os'=>$os));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function cantidadProductosOtrosInstalados($cliente, $empleado, $familia, $os){
        $where = "";
        for($i = 0; $i < count($familia); $i++){
            $where .= " AND office.familia != '" . $familia[$i] . "' ";
        }
        $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo_SPLA
                LEFT JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado " . $where . " 
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = :os AND office.edicion != ''";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':os'=>$os));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function productosUsoCliente($cliente, $empleado){
        $query = "SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_SPLA
                INNER JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia = 'Office'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_SPLA
                INNER JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia = 'Visio'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_SPLA
                INNER JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia = 'Project'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_SPLA
                INNER JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia = 'Visual Studio'
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_SPLA
                INNER JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado = office.empleado AND office.familia NOT IN ('Office', 'Visio', 'Project', 'Visual Studio', 'SQL Server')
            WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = 'Windows' AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            ";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
	
    function productosUsoServidor($cliente, $empleado){
        $query = "SELECT tabla.familia AS familia,
                tabla.edicion AS edicion,
                tabla.version AS version,
                COUNT(tabla.familia) AS cantidad
            FROM (SELECT detalles_equipo_SPLA.id,
                    `detalles_equipo_SPLA`.equipo,
                    office.familia AS familia,
                    office.edicion AS edicion,
                    office.version AS version,
                    'Desuso' AS usabilidad
                FROM detalles_equipo_SPLA
                    LEFT JOIN resumen_SPLA AS office ON detalles_equipo_SPLA.equipo = office.equipo AND detalles_equipo_SPLA.cliente = office.cliente AND detalles_equipo_SPLA.empleado =  office.empleado AND office.familia = 'SQL Server'
                WHERE detalles_equipo_SPLA.cliente = :cliente AND detalles_equipo_SPLA.empleado = :empleado AND detalles_equipo_SPLA.familia = 'Windows Server' AND office.familia != ''
                GROUP BY detalles_equipo_SPLA.id) tabla
            GROUP BY tabla.familia, tabla.edicion, tabla.version";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}