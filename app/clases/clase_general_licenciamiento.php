<?php

namespace App\Clases;

class clase_general_licenciamiento{ 
    protected $conex;
    private $usuario;
    private $nombre;
    private $servidor;
    private $contrasena;
    private $configuraciones = 3;
    public  $limit_paginacion = 50;
    
    protected function conexion(){
        try{
            $this->configuracion();
            $this->conex = null;
            $this->conex = new PDO('mysql:host=' . $this->servidor . ';dbname=' . $this->nombre, $this->usuario, $this->contrasena);
            $this->conex->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            print "ERROR: No se pudo realizar la conexión a la base de datos";
        }
    }
    
    private function configuracion(){
        if($this->configuraciones == 1){
            $this->usuario = "vamosclo_admApp";
            $this->nombre = "vamosclo_adminApp";
            $this->servidor = "localhost";
            $this->contrasena = "}@HckH+v2d?{";
        }
        else if($this->configuraciones == 3){
            $this->usuario = "root";
            $this->nombre = "vamosclo_adminApp";
            $this->servidor = "127.0.0.1";
            $this->contrasena = "";
        }
    }
    
    public function generadorCodigo(){
        $codigo = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"), 0, 16);
        $codigoReal = substr($codigo, 0, 4) . "-" . substr($codigo, 4, 4) . "-" . substr($codigo, 8, 4) . "-" . substr($codigo, 12, 4);
        return $codigoReal;
    } 
    
    public function get_escape($valor){
        return htmlspecialchars(addslashes(stripslashes(strip_tags(trim($valor)))));
    }
}