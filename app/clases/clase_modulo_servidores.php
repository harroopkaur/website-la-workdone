<?php
namespace App\Clases;

class moduloServidores extends General{
    public $error;
    
    function windowServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            /*$sql = $this->conn->prepare('SELECT tabla.cluster,
                    tabla.host,
                    tabla.equipo,
                    tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    tabla.MSDN,
                    tabla.tipo,
                    tabla.cpu,
                    tabla.cores,
                    tabla.licSrv,
                    tabla.licProc,
                    tabla.licCore
                FROM (SELECT "" AS cluster,
                        "" AS host,
                        filepcs2.cn AS equipo,
                        "Windows Server" AS familia,
                        ObtenerEdicion(TRIM(REPLACE(filepcs2.os, "Windows Server", ""))) AS edicion,
                        ObtenerVersion(TRIM(REPLACE(TRIM(REPLACE(filepcs2.os, "Windows Server", "")), ObtenerEdicion(TRIM(REPLACE(filepcs2.os, "Windows Server", ""))),""))) AS version,
                        "No" AS MSDN,
                        IF(consolidadoTipoEquipo.modelo LIKE "%Virtual%", "Virtual", "Fisico") AS tipo,
                        IFNULL(consolidadoProcesadores.cpu, 0) AS cpu,
                        IFNULL(consolidadoProcesadores.cores, 0) AS cores,
                        0 AS licSrv,
                        0 As licProc,
                        0 AS licCore
                    FROM filepcs2
                        LEFT JOIN consolidadoTipoEquipo ON filepcs2.cn = substring_index(consolidadoTipoEquipo.host_name,".",1) AND consolidadoTipoEquipo.cliente = :cliente
                        LEFT JOIN consolidadoProcesadores ON consolidadoTipoEquipo.host_name = consolidadoProcesadores.host_name AND consolidadoProcesadores.cpu > 0 AND consolidadoProcesadores.cliente = :clienteProc
                    WHERE filepcs2.os LIKE "%Windows Server%" AND filepcs2.cliente = :clienteFile
                    GROUP BY filepcs2.cn, consolidadoProcesadores.cpu, consolidadoProcesadores.cores) AS tabla
                WHERE tabla.tipo = :tipo');*/
            $sql = $this->conn->prepare('SELECT "" AS cluster,
                        "" AS host,
                        detalles_equipo2.equipo AS equipo,
                        "Windows Server" AS familia,
                        detalles_equipo2.edicion,
                        detalles_equipo2.version,
                        "No" AS MSDN,
                        IF(consolidadoTipoEquipo.modelo LIKE "%Virtual%", "Virtual", "Fisico") AS tipo,
                        "" AS centroCosto,
                        IFNULL(consolidadoProcesadores.cpu, 0) AS cpu,
                        IFNULL(consolidadoProcesadores.cores, 0) AS cores,
                        0 AS licSrv,
                        0 As licProc,
                        0 AS licCore
                    FROM detalles_equipo2
                        LEFT JOIN consolidadoTipoEquipo ON detalles_equipo2.equipo = consolidadoTipoEquipo.host_name AND detalles_equipo2.cliente = consolidadoTipoEquipo.cliente
                        LEFT JOIN consolidadoProcesadores ON consolidadoTipoEquipo.host_name = consolidadoProcesadores.host_name AND consolidadoProcesadores.cpu > 0 AND detalles_equipo2.cliente = consolidadoProcesadores.cliente
                    WHERE detalles_equipo2.os LIKE "%Windows Server%" AND detalles_equipo2.cliente = :cliente AND (IF(consolidadoTipoEquipo.modelo LIKE "%Virtual%", "Virtual", "Fisico")) = :tipo
                    AND detalles_equipo2.errors = "Ninguno" AND detalles_equipo2.rango IN (1, 2, 3)
                    GROUP BY detalles_equipo2.equipo, consolidadoProcesadores.cpu, consolidadoProcesadores.cores');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function SQLServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT "" AS cluster,
                        "" AS host,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version,
                        "No" AS MSDN,
                        IF(consolidadoTipoEquipo.modelo LIKE "%Virtual%", "Virtual", "Fisico") AS tipo,
                        "" AS centroCosto,
                        consolidadoProcesadores.cpu,
                        consolidadoProcesadores.cores,
                        0 AS licSrv,
                        0 As licProc,
                        0 AS licCore
                    FROM resumen_office2 
                        INNER JOIN (SELECT resumen_office2.cliente, resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, MAX(resumen_office2.version) AS version
                                    FROM resumen_office2
                                    WHERE resumen_office2.familia Like "%SQL%"
                                    GROUP BY resumen_office2.cliente, resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion) resumen ON
                        resumen_office2.cliente = resumen.cliente AND resumen_office2.equipo = resumen.equipo AND
                        resumen_office2.familia = resumen.familia AND resumen_office2.edicion = resumen.edicion
                        AND resumen_office2.version = resumen.version
                        LEFT JOIN consolidadoTipoEquipo ON resumen_office2.equipo = consolidadoTipoEquipo.host_name AND consolidadoTipoEquipo.cliente = :cliente
                        LEFT JOIN consolidadoProcesadores ON consolidadoTipoEquipo.host_name = consolidadoProcesadores.host_name AND consolidadoProcesadores.cliente = :clienteProc
                        INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo AND detalles_equipo2.errors = "Ninguno" AND detalles_equipo2.rango IN (1, 2, 3)
                    WHERE resumen_office2.familia LIKE "%SQL%" AND resumen_office2.cliente = :clienteResumen AND (IF(consolidadoTipoEquipo.modelo LIKE "%Virtual%", "Virtual", "Fisico")) = :tipo
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version');
            
            $sql->execute(array(':clienteProc'=>$cliente, ':clienteResumen'=>$cliente, ':cliente'=>$cliente, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeWindowServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM windowServerClientes
                WHERE idCliente = :cliente AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeWindowServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM alineacionWindowsServidores
                WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientes
                WHERE idCliente = :cliente AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerClienteSam($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientesSam
                WHERE idCliente = :cliente AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerClienteSamArchivo($archivo, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientesSam
                WHERE archivo = :archivo AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':archivo'=>$archivo, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerCluster($cliente, $empleado, $tipo, $cluster){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM windowServerClientes
                WHERE idCliente = :cliente AND tipo = :tipo AND cluster = :cluster
                GROUP BY host
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo, ':cluster'=>$cluster));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionWindowsServidores
                WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerAlineacionSam($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionWindowsServidoresSam
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function windowServerAlineacionSamArchivo($archivo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionWindowsServidoresSam
                WHERE archivo = :archivo');
            $sql->execute(array(':archivo'=>$archivo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientes
                WHERE idCliente = :cliente AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerClienteSam($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientesSam
                WHERE idCliente = :cliente AND empleado = :empleado AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerClienteSamArchivo($archivo, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientesSam
                WHERE archivo = :archivo AND tipo = :tipo
                ORDER BY cluster');
            $sql->execute(array(':archivo'=>$archivo, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerCluster($cliente, $empleado, $tipo, $cluster){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM sqlServerClientes
                WHERE idCliente = :cliente AND tipo = :tipo AND cluster = :cluster
                GROUP BY host
                ORDER BY cluster');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo, ':cluster'=>$cluster));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionSqlServidores
                WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerAlineacionSam($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionSqlServidoresSam
                WHERE idCliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function sqlServerAlineacionSamArchivo($archivo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM alineacionSqlServidoresSam
                WHERE archivo = :archivo');
            $sql->execute(array(':archivo'=>$archivo));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeSqlServerCliente($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM sqlServerClientes
                WHERE idCliente = :cliente AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeSqlServerAlineacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM alineacionSqlServidores
                WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            
            $cant = 0;
            foreach($resultado as $row){
                $cant = $row["cantidad"];
            }
            return $cant;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarWindowServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $MSDN, $tipo, $centroCosto, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO windowServerClientes (idCliente, empleado, cluster, host, equipo, familia, edicion, version, MSDN, tipo, centroCosto, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :MSDN, :tipo, :centroCosto, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':MSDN'=>$MSDN, ':tipo'=>$tipo, ':centroCosto'=>$centroCosto, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarAlineacionWindowServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $tipo, $centroCosto, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionWindowsServidores (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, centroCosto, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :tipo, :centroCosto, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':tipo'=>$tipo, ':centroCosto'=>$centroCosto, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarSQLServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $MSDN, $tipo, $centroCosto, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO sqlServerClientes (idCliente, empleado, cluster, host, equipo, familia, edicion, version, MSDN, tipo, centroCosto, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :MSDN, :tipo, :centroCosto, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':MSDN'=>$MSDN, ':tipo'=>$tipo, ':centroCosto'=>$centroCosto, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function agregarAlineacionSQLServer($cliente, $empleado, $cluster, $host, $equipo, $familia, $edicion, $version, $tipo, $centroCosto, $cpu, $cores, $licSrv, $licProc, $licCore){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO alineacionSqlServidores (idCliente, empleado, cluster, host, equipo, familia, edicion, version, tipo, centroCosto, cpu, cores, licSrv, licProc, licCore)
            VALUES (:cliente, :empleado, :cluster, :host, :equipo, :familia, :edicion, :version, :tipo, :centroCosto, :cpu, :cores, :licSrv, :licProc, :licCore)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':cluster'=>$cluster, ':host'=>$host, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':tipo'=>$tipo, ':centroCosto'=>$centroCosto, ':cpu'=>$cpu, ':cores'=>$cores, ':licSrv'=>$licSrv, ':licProc'=>$licProc, ':licCore'=>$licCore));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarWindowServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM windowServerClientes WHERE idCliente = :cliente AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarWindowServer1($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM windowServerClientes WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarAlineaionWindowServer($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM alineacionWindowsServidores WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarSqlServer1($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM sqlServerClientes WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarSqlServer($cliente, $empleado, $tipo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM sqlServerClientes WHERE idCliente = :cliente AND tipo = :tipo');
            $sql->execute(array(':cliente'=>$cliente, ':tipo'=>$tipo));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarAlineaionSqlServer($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM alineacionSqlServidores WHERE idCliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function obtenerEdicProducto($fabricante, $producto){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
                    ediciones.nombre
                FROM `tabla_equivalencia`
                    INNER JOIN ediciones ON `tabla_equivalencia`.`edicion` = ediciones.idEdicion AND ediciones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.`familia` = :producto
                GROUP BY idEdicion');
            $sql->execute(array(':fabricante'=>$fabricante, ':producto' => $producto));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function obtenerEdicProductoNombre($fabricante, $producto){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
                    ediciones.nombre
                FROM (SELECT tabla_equivalencia.fabricante,
                        productos.nombre AS familia,
                        tabla_equivalencia.`edicion` AS idEdicion
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`) AS `tabla_equivalencia`
                    INNER JOIN ediciones ON `tabla_equivalencia`.`idEdicion` = ediciones.idEdicion AND ediciones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.`familia` = :producto
                GROUP BY idEdicion');
            $sql->execute(array(':fabricante'=>$fabricante, ':producto' => $producto));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function obtenerVersionEquivalencia($fabricante, $producto, $edicion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT versiones.id,
                    versiones.nombre
                FROM (SELECT tabla_equivalencia.`fabricante`,
                        productos.`idProducto` AS idProducto,
                        productos.nombre AS familia,
                        ediciones.`idEdicion` AS idEdicion,
                        ediciones.nombre AS edicion,
                        versiones.id AS idVersion,
                        versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.`edicion` = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.`version` = versiones.id) AS tabla_equivalencia
                    INNER JOIN versiones ON `tabla_equivalencia`.`idVersion` = versiones.`id` AND versiones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.`familia` = :producto AND tabla_equivalencia.`edicion` = :edicion');
            $sql->execute(array(':fabricante'=>$fabricante, ':producto' => $producto, ':edicion'=>$edicion));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function balanzaServer($cliente, $empleado, $tipoServidor){
        try{
            // dd('SELECT tabla_equivalencia.familia,
            //         tabla_equivalencia.edicion,
            //         tabla_equivalencia.version,
            //         IFNULL(SUM(IFNULL(balance_office2.instalaciones, 0)), 0) AS instalaciones,
            //         ObtenerLicenciaServidores(' . $cliente . ', ' . $empleado . ',tabla_equivalencia.familia, "Fisico", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Fisico,
            //         ObtenerLicenciaServidores(' . $cliente . ', ' . $empleado . ', tabla_equivalencia.familia, "Virtual", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Virtuals
            //     FROM (SELECT productos.nombre AS familia,
            //         ediciones.nombre AS edicion,
            //         versiones.nombre AS version
            //         FROM tabla_equivalencia
            //             INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
            //             INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.`idEdicion`
            //             INNER JOIN versiones ON tabla_equivalencia.version = versiones.id) AS tabla_equivalencia
            //         LEFT JOIN balance_office2 ON tabla_equivalencia.familia = balance_office2.familia AND
            //         tabla_equivalencia.edicion = balance_office2.office AND
            //         tabla_equivalencia.version = balance_office2.version AND balance_office2.cliente = '.$cliente.'
            //     WHERE tabla_equivalencia.familia = "'.$tipoServidor.'"
            //     GROUP BY tabla_equivalencia.familia, tabla_equivalencia.edicion, tabla_equivalencia.version');
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.familia,
                    tabla_equivalencia.edicion,
                    tabla_equivalencia.version,
                    IFNULL(SUM(IFNULL(balance_office2.instalaciones, 0)), 0) AS instalaciones,
                    ObtenerLicenciaServidores(' . $cliente . ', ' . $empleado . ', "Fisico", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Fisico,
                    ObtenerLicenciaServidores(' . $cliente . ', ' . $empleado . ', "Virtual", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Virtuals, 1 as MSDN
                FROM (SELECT productos.nombre AS familia,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.version = versiones.id) AS tabla_equivalencia
                    LEFT JOIN balance_office2 ON tabla_equivalencia.familia = balance_office2.familia AND
                    tabla_equivalencia.edicion = balance_office2.office AND
                    tabla_equivalencia.version = balance_office2.version AND balance_office2.cliente = :cliente
                WHERE tabla_equivalencia.familia = :tipoServidor
                GROUP BY tabla_equivalencia.familia, tabla_equivalencia.edicion, tabla_equivalencia.version');
            $sql->execute(array(':cliente'=>$cliente, ':tipoServidor'=>$tipoServidor));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return array();
        }
    }
    
    function balanzaServerSam($cliente, $empleado, $tipoServidor){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.familia,
                    tabla_equivalencia.edicion,
                    tabla_equivalencia.version,
                    IFNULL(SUM(IFNULL(balance_office2.instalaciones, 0)), 0) AS instalaciones,
                    ObtenerLicenciaServidoresSam(:idCliente, tabla_equivalencia.familia, "Fisico", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Fisico,
                    ObtenerLicenciaServidoresSam(:cliente, tabla_equivalencia.familia, "Virtual", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Virtual
                FROM (SELECT productos.nombre AS familia,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.version = versiones.id) AS tabla_equivalencia
                    LEFT JOIN balance_office2Sam AS balance_office2 ON tabla_equivalencia.familia = balance_office2.familia AND
                    tabla_equivalencia.edicion = balance_office2.office AND
                    tabla_equivalencia.version = balance_office2.version AND balance_office2.cliente = :clienteBalanza 
                WHERE tabla_equivalencia.familia = :tipoServidor
                GROUP BY tabla_equivalencia.familia, tabla_equivalencia.edicion, tabla_equivalencia.version');
            $sql->execute(array(':idCliente'=>$cliente, ':cliente'=>$cliente, ':clienteBalanza'=>$cliente, ':tipoServidor'=>$tipoServidor));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function balanzaServerSamArchivo($archivo, $tipoServidor){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla_equivalencia.familia,
                    tabla_equivalencia.edicion,
                    tabla_equivalencia.version,
                    IFNULL(SUM(IFNULL(balance_office2.instalaciones, 0)), 0) AS instalaciones,
                    ObtenerLicenciaServidoresSamArchivo(:archivo, tabla_equivalencia.familia, "Fisico", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Fisico,
                    ObtenerLicenciaServidoresSamArchivo(:archivo, tabla_equivalencia.familia, "Virtual", tabla_equivalencia.edicion, tabla_equivalencia.version) AS Virtual
                FROM (SELECT productos.nombre AS familia,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version
                    FROM tabla_equivalencia
                        INNER JOIN productos ON tabla_equivalencia.`familia` = productos.`idProducto`
                        INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.`idEdicion`
                        INNER JOIN versiones ON tabla_equivalencia.version = versiones.id) AS tabla_equivalencia
                    LEFT JOIN balance_office2Sam AS balance_office2 ON tabla_equivalencia.familia = balance_office2.familia AND
                    tabla_equivalencia.edicion = balance_office2.office AND
                    tabla_equivalencia.version = balance_office2.version AND balance_office2.archivo = :archivo
                WHERE tabla_equivalencia.familia = :tipoServidor
                GROUP BY tabla_equivalencia.familia, tabla_equivalencia.edicion, tabla_equivalencia.version');
            $sql->execute(array(':archivo'=>$archivo, ':tipoServidor'=>$tipoServidor));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return array();
        }
    }
    
    function listaDesarrolloPruebaWindowsServer($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT windowServerClientes.equipo,
                    IF(detalles_equipo2.tipo = 1, "Cliente", "Servidor") AS tipo,
                    windowServerClientes.familia,
                    windowServerClientes.edicion,
                    windowServerClientes.version,
                    "" AS fecha_instalacion
                FROM windowServerClientes
                    INNER JOIN detalles_equipo2 ON windowServerClientes.idCliente = detalles_equipo2.cliente AND
                    windowServerClientes.equipo = detalles_equipo2.equipo
                    AND windowServerClientes.familia = detalles_equipo2.familia AND windowServerClientes.edicion = detalles_equipo2.edicion
                    AND windowServerClientes.version = detalles_equipo2.version
                WHERE windowServerClientes.idCliente = :cliente
                GROUP BY windowServerClientes.id');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listaDesarrolloPruebaSQLServer($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT sqlServerClientes.equipo,
                    IF(detalles_equipo2.tipo = 1, "Cliente", "Servidor") AS tipo,
                    sqlServerClientes.familia,
                    sqlServerClientes.edicion,
                    sqlServerClientes.version,
                    IFNULL(resumen_office2.fecha_instalacion, "0000-00-00") AS fecha_instalacion
                FROM sqlServerClientes
                    LEFT JOIN resumen_office2 ON sqlServerClientes.idCliente = resumen_office2.cliente
                    AND sqlServerClientes.equipo = resumen_office2.equipo AND sqlServerClientes.familia =  resumen_office2.familia
                    AND sqlServerClientes.edicion = resumen_office2.edicion AND sqlServerClientes.version = resumen_office2.version
                    LEFT JOIN detalles_equipo2 ON sqlServerClientes.idCliente = detalles_equipo2.cliente
                    AND sqlServerClientes.equipo = detalles_equipo2.equipo AND sqlServerClientes.familia = detalles_equipo2.familia AND sqlServerClientes.edicion = detalles_equipo2.edicion
                    AND sqlServerClientes.version = detalles_equipo2.version
                WHERE sqlServerClientes.idCliente = :cliente
                GROUP BY sqlServerClientes.id');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
}