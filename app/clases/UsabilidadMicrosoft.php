<?php

namespace App\Clases;

class UsabilidadMicrosoft extends General{
    ########################################  Atributos  ########################################
    public  $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $proceso, $PID, $creado, $horaInicio, $finalizado, $horaFin, $duracion) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usabilidadMicrosoft (cliente, empleado, equipo, proceso, PID, creado, horaInicio, finalizado, horaFin, duracion) '
            . 'VALUES (:cliente, :empleado, :equipo, :proceso, :PID, :creado, :horaInicio, :finalizado, :horaFin, :duracion)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':proceso'=>$proceso, ':PID'=>$PID, ':creado'=>$creado, 
            ':horaInicio'=>$horaInicio, ':finalizado'=>$finalizado, ':horaFin'=>$horaFin, ':duracion'=>$duracion));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM usabilidadMicrosoft WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registros";
            return false;
        }
    }
    
    function usabilidadOffice($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version,
                    1 AS instalacion,
                    
                    (IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(usabilidadMicrosoft.finalizado, ' ',usabilidadMicrosoft.horaFin), CONCAT(usabilidadMicrosoft.creado, ' ',usabilidadMicrosoft.horaInicio)))), 0) + 
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(excel.finalizado, ' ',excel.horaFin), CONCAT(excel.creado, ' ',excel.horaInicio)))), 0) + 
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(outlook.finalizado, ' ',outlook.horaFin), CONCAT(outlook.creado, ' ',outlook.horaInicio)))), 0) + 
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(powerpnt.finalizado, ' ',powerpnt.horaFin), CONCAT(powerpnt.creado, ' ',powerpnt.horaInicio)))), 0) +
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(onenote.finalizado, ' ',onenote.horaFin), CONCAT(onenote.creado, ' ',onenote.horaInicio)))), 0) +
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(access.finalizado, ' ',access.horaFin), CONCAT(access.creado, ' ',access.horaInicio)))), 0) +
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(lync.finalizado, ' ',lync.horaFin), CONCAT(lync.creado, ' ',lync.horaInicio)))), 0)) AS usabilidad,
                  
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(usabilidadMicrosoft.finalizado, ' ',usabilidadMicrosoft.horaFin), CONCAT(usabilidadMicrosoft.creado, ' ',usabilidadMicrosoft.horaInicio))))), '0:0:0') AS tiempoWinword,
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(excel.finalizado, ' ',excel.horaFin), CONCAT(excel.creado, ' ',excel.horaInicio))))), '0:0:0') AS tiempoExcel,
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(outlook.finalizado, ' ',outlook.horaFin), CONCAT(outlook.creado, ' ',outlook.horaInicio))))), '0:0:0') AS tiempoOutlook,
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(powerpnt.finalizado, ' ',powerpnt.horaFin), CONCAT(powerpnt.creado, ' ',powerpnt.horaInicio))))), '0:0:0') AS tiempoPowerpnt,
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(onenote.finalizado, ' ',onenote.horaFin), CONCAT(onenote.creado, ' ',onenote.horaInicio))))), '0:0:0') AS tiempoOnenote,
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(access.finalizado, ' ',access.horaFin), CONCAT(access.creado, ' ',access.horaInicio))))), '0:0:0') AS tiempoAccess,
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(lync.finalizado, ' ',lync.horaFin), CONCAT(lync.creado, ' ',lync.horaInicio))))), '0:0:0') AS tiempoLync
                FROM resumen_office2
                    LEFT JOIN usabilidadMicrosoft ON resumen_office2.equipo = usabilidadMicrosoft.equipo AND usabilidadMicrosoft.proceso IN ('winword.exe', 'WINWORD.EXE')
                    AND resumen_office2.cliente = usabilidadMicrosoft.cliente AND resumen_office2.empleado = usabilidadMicrosoft.empleado
                    
                    LEFT JOIN usabilidadMicrosoft excel ON resumen_office2.equipo = excel.equipo AND excel.proceso IN ('excel.exe', 'EXCEL.EXE')
                    AND resumen_office2.cliente = excel.cliente AND resumen_office2.empleado = excel.empleado
                    
                    LEFT JOIN usabilidadMicrosoft outlook ON resumen_office2.equipo = outlook.equipo AND outlook.proceso IN ('outlook.exe', 'OUTLOOK.EXE')
                    AND resumen_office2.cliente = outlook.cliente AND resumen_office2.empleado = outlook.empleado
                    
                    LEFT JOIN usabilidadMicrosoft powerpnt ON resumen_office2.equipo = powerpnt.equipo AND powerpnt.proceso IN ('powerpnt.exe', 'POWERPNT.EXE')
                    AND resumen_office2.cliente = powerpnt.cliente AND resumen_office2.empleado = powerpnt.empleado
                    
                    LEFT JOIN usabilidadMicrosoft onenote ON resumen_office2.equipo = onenote.equipo AND onenote.proceso IN ('onenote.exe', 'ONENOTE.EXE')
                    AND resumen_office2.cliente = onenote.cliente AND resumen_office2.empleado = onenote.empleado
                    
                    LEFT JOIN usabilidadMicrosoft access ON resumen_office2.equipo = access.equipo AND access.proceso IN ('access.exe', 'ACCESS.EXE')
                    AND resumen_office2.cliente = access.cliente AND resumen_office2.empleado = access.empleado
                    
                    LEFT JOIN usabilidadMicrosoft lync ON resumen_office2.equipo = lync.equipo AND lync.proceso IN ('lync.exe', 'LYNC.EXE')
                    AND resumen_office2.cliente = lync.cliente AND resumen_office2.empleado = lync.empleado
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia = 'Office'
                GROUP BY equipo, familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return array();
        }
    }
    
    function usabilidadOfficeVisio($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version,
                    1 AS instalacion,
                    
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(usabilidadMicrosoft.finalizado, ' ',usabilidadMicrosoft.horaFin), CONCAT(usabilidadMicrosoft.creado, ' ',usabilidadMicrosoft.horaInicio)))), 0) AS usabilidad,
                    
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(usabilidadMicrosoft.finalizado, ' ',usabilidadMicrosoft.horaFin), CONCAT(usabilidadMicrosoft.creado, ' ',usabilidadMicrosoft.horaInicio))))), '0:0:0') AS tiempo
                FROM resumen_office2
                    INNER JOIN usabilidadMicrosoft ON resumen_office2.equipo = usabilidadMicrosoft.equipo AND usabilidadMicrosoft.proceso IN ('visio.exe', 'VISIO.EXE')
                    AND resumen_office2.cliente = usabilidadMicrosoft.cliente AND resumen_office2.empleado = usabilidadMcrosoft.empleado
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado AND resumen_office2.familia = 'Office'
                GROUP BY equipo, familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return array();
        }
    }
    
    function usabilidadOfficeProject($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version,
                    1 AS instalacion,
                    
                    IFNULL(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(usabilidadMicrosoft.finalizado, ' ',usabilidadMicrosoft.horaFin), CONCAT(usabilidadMicrosoft.creado, ' ',usabilidadMicrosoft.horaInicio)))), 0) AS usabilidad,
                    
                    IFNULL(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(CONCAT(usabilidadMicrosoft.finalizado, ' ',usabilidadMicrosoft.horaFin), CONCAT(usabilidadMicrosoft.creado, ' ',usabilidadMicrosoft.horaInicio))))), '0:0:0') AS tiempo
                FROM resumen_office2
                    INNER JOIN usabilidadMicrosoft ON resumen_office2.equipo = usabilidadMicrosoft.equipo AND usabilidadMicrosoft.proceso IN ('winproj.exe', 'WINPROJ.EXE')
                    AND resumen_office2.cliente = usabilidadMicrosoft.cliente AND resumen_office2.empleado = usabilidadMicrosoft.empleado
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia = 'Office'
                GROUP BY equipo, familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return array();
        }
    }
}