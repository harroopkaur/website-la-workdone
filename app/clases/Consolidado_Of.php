<?php

namespace App\Clases;

class Consolidado_Of extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $registro, $editor, $version, $fecha_instalacion, $sofware) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_offices (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES (:cliente, :empleado, :dato_control,:host_name, :registro, :editor, :version, :fecha_instalacion, :sofware)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control, ':host_name'=>$host_name, ':registro'=>$registro, 
            ':editor'=>$editor, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion, ':sofware'=>$sofware));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_offices (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_offices WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM consolidado_offices WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo2($cliente, $empleado, $nombre, $nombre3) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM consolidado_offices WHERE cliente = :cliente AND empleado = :empleado AND dato_control LIKE :nombre AND sofware = :nombre3 ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':nombre'=>'%' . $nombre . '%', ':nombre3'=>$nombre3));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo3($cliente, $empleado, $nombre, $nombre3, $nombre4) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM consolidado_offices WHERE cliente = :cliente AND empleado = :empleado AND dato_control LIKE :nombre AND sofware = :nombre3 AND sofware NOT LIKE :nombre4 ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':nombre'=>'%' . $nombre . '%', ':nombre3'=>$nombre3, ':nombre4'=>$nombre4 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_todo4($cliente, $empleado, $nombre, $nombre3, $nombre4, $nombre5) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM consolidado_offices WHERE cliente = :cliente AND dato_control LIKE :nombre AND sofware = :nombre3 AND sofware NOT LIKE :nombre4 AND sofware NOT LIKE :nombre5% ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'>$empleado, ':nombre'=>'%' . $nombre . '%', ':nombre3'=>$nombre3, ':nombre4'=>$nombre4 . '%', ':nombre5'=>$nombre5 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosMicrosoft($cliente, $empleado, $in = null, $out = null) {        
        try{
            $this->conexion();
            
            $agregar = "";
            if($in != null){
                $inAux = explode(",", $in);
            }
            
            if(trim($inAux[0]) == "Office" || trim($inAux[0]) == "Visio" || trim($inAux[0]) == "Visual" || trim($inAux[0]) == "Microsoft Exchange"
            || trim($inAux[0]) == "SharePoint" || trim($inAux[0]) == "SQL"){
                $agregar .= " AND sofware LIKE '%" . trim($inAux[0]) . "%' AND (";
            
                for($i = 1; $i < count($inAux); $i++){
                    if($i > 1){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            } else{
                $agregar .= " AND (";
            
                for($i = 0; $i < count($inAux); $i++){
                    if($i > 0){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            }
                
            $quitar = "";
            if($out != null){
                $outAux = explode(",", $out);
            }
            for($i = 0; $i < count($outAux); $i++){
                if($i > 0){
                    $quitar .= " OR ";
                }
                
                $quitar .= "sofware LIKE '%" . trim($outAux[$i]) . "%'"; 
            }
            
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				FROM consolidado_offices
				WHERE cliente = :cliente AND empleado = :empleado " . $agregar . "
				ORDER BY fecha_instalacion DESC) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_offices
				  WHERE cliente = :cliente AND empleado = :empleado AND (" . $quitar . ")
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {

        if ($titulo_archivo != "") {
            $ruta = storage_path('app/files/archivos_csvf1/') . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}