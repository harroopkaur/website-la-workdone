<?php

namespace App\Clases;

class FilepcOracleAux extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dn;
    var $objectclass;
    var $cn;
    var $useracountcontrol;
    var $lastlogon;
    var $pwdlastset;
    var $os;
    var $lastlogontimes;
    var $error = NULL;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO filepcs_oracleAux (cliente, empleado, dn, objectclass, cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) ";
        $query .= "VALUES " . $bloque;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($bloqueValores);
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        $this->conexion();
        $query = "DELETE FROM filepcs_oracleAux WHERE cliente = :cliente";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function eliminarEquipos($cliente, $empleado, $equipos) {
        $this->conexion();
        $query = "DELETE FROM filepcs_oracleAux WHERE cliente = :cliente AND empleado = :empleado AND (" . $equipos . ")";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Obtener datos 
    /*function datos($id) {
        $this->conexion();
        $query = "SELECT * FROM filepcs2 WHERE id = :id";
        try { 
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->dn = $usuario['dn'];
            $this->objectclass = $usuario['objectclass'];
            $this->cn = $usuario['cn'];
            $this->useracountcontrol = $usuario['useracountcontrol'];
            $this->lastlogon = $usuario['lastlogon'];
            $this->pwdlastset = $usuario['pwdlastset'];
            $this->os = $usuario['os'];
            $this->lastlogontimes = $usuario['lastlogontimes'];
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM filepcs_oracleAux WHERE cliente = :cliente AND empleado = :empleado ORDER BY id";
        try { 
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }

    // Obtener listado de todos los Usuarios
    /*function listar_todo2($cliente, $nombre, $nombre3) {
        $this->conexion();
        $query = "SELECT * FROM filepcs2 WHERE cliente = :cliente AND dato_control LIKE :nombre AND sofware LIKE :nombre3 ORDER BY id";
        try { 
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente, ':nombre'=>'%' . $nombre . '%', ':nombre3'=>$nombre3 . '%'));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }*/

    // Obtener listado de todos los Usuarios paginados
    /*function listar_todo_paginado($cliente, $inicio, $fin) {
        $this->conexion();
        $query = "SELECT * FROM filepcs2 WHERE cliente = :cliente ORDER BY  id LIMIT " . $inicio . ", " . $fin;
        try { 
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }*/

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        $this->conexion();
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf2/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = "Su archivo excede el límite de tamaño de 16MB.  Por favor contacte a un representante <a href='info@licensingassurance.com' target='_blank'>info@licensingassurance.com</a>";
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}