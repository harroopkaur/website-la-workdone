<?php

namespace App\Clases;

class comprasAdobe extends General{
    ########################################  Atributos  ########################################
    public  $listado = array();
    public  $row = array();
    public  $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $edicion, $version, $compra, $precio, $asignacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO compras_adobe (cliente, empleado, familia, edicion, version, compra, precio, asignacion) '
            . 'VALUES (:cliente, :empleado, TRIM(:familia), TRIM(:edicion), TRIM(:version), :compra, :precio, :asignacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version, ':compra'=>$compra, ':precio'=>$precio, ':asignacion'=>$asignacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM compras_adobe WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function listar_asignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT asignacion FROM compras_adobe WHERE cliente = :cliente AND empleado = :empleado GROUP BY asignacion ORDER BY asignacion");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function totalCompras($cliente, $familia, $edicion, $asignacion, $asignaciones) {
        $array = array(':cliente'=>$cliente);
        if($familia != "Otros"){
            $where = " AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
            $array[':familia'] = $familia;
        } else{
            $where = " AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
        }

        if($familia == "Otros" && $edicion != "Todo"){
            $where .= " AND familia = :edicion ";
            $array[':edicion'] = $edicion;
        }
        else if($edicion == ""){
            $where .= " AND (edicion = '' OR edicion IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND edicion = :edicion ";
            $array[':edicion'] = $edicion;
        }

        if($asignacion != ""){
            $where .= " AND asignacion = :asignacion ";
            $array[':asignacion'] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where .= " AND asignacion IN (" . $asignacion . ") ";
        }
        
        
        
        
        /*$where = "";
            $where1 = "";
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
            if($familia != "Otros"){
                $where = " resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
                $array[':familia'] = $familia;
            } else{
                $where = " resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
            }
            
            if($familia == "Otros" && $edicion != "Todo"){
                $where .= " AND resumen_adobe.familia = :familia ";
                $array[':familia'] = $edicion;
            }
            else if($edicion == ""){
                $where .= " AND (resumen_adobe.edicion = '' OR resumen_adobe.edicion IS NULL) ";
            } else if($edicion != "Todo"){
                $where .= " AND resumen_adobe.edicion = :edicion ";
                $array[':edicion'] = $edicion;
            }
            
            if($asignacion != ""){
                $where1 = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            }*/
        
        
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(SUM(IFNULL(compra, 0)), 0) AS compra '
            . 'FROM compras_adobe '
            . 'WHERE cliente = :cliente ' . $where
            . 'ORDER BY id');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["compra"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function datos3($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_adobe WHERE familia = :familia AND edicion = :edicion '
            . 'AND version = :version AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->row = $sql->fetch();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_adobe WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function comprasAsignacion($cliente, $empleado, $familia, $edicion, $asignacion){
        $where = "";
        $where1 = "";
        $array = array(':cliente'=>$cliente, ':empleado'=>$empleado);
        if($familia != "Otros"){
            $where = " AND compras_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
            $where1 = " AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
            $array[':familia'] = $familia;
        } else{
            $where = " AND compras_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
            $where1 = " AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
        }

        if($familia == "Otros" && $edicion != "Todo"){
            $where .= " AND compras_adobe.familia = :edicion ";
            $where1 .= " AND resumen_adobe.familia = :edicion ";
            $array[':edicion'] = $edicion;
        }
        else if($edicion == ""){
            $where .= " AND (compras_adobe.edicion = '' OR compras_adobe.edicion IS NULL) ";
            $where1 .= " AND (resumen_adobe.edicion = '' OR resumen_adobe.edicion IS NULL) ";
        } else if($edicion != "Todo"){
            $where .= " AND compras_adobe.edicion = :edicion ";
            $where1 .= " AND resumen_adobe.edicion = :edicion ";
            $array[':edicion'] = $edicion;
        }

        if($asignacion != ""){
            $where .= " AND compras_adobe.asignacion = :asignacion ";
            $array[':asignacion'] = $asignacion;
        }
        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT compras_adobe.familia,
                    compras_adobe.edicion AS office,
                    compras_adobe.version,
                    compras_adobe.asignacion,
                    IFNULL(COUNT(resumen_adobe.familia), 0) AS instalaciones,
                    (compras_adobe.compra - IFNULL(COUNT(resumen_adobe.familia), 0)) AS balance,
                    (compras_adobe.compra - IFNULL(COUNT(resumen_adobe.familia), 0)) * compras_adobe.precio AS balancec,
                    compras_adobe.compra,
                    (compras_adobe.compra - IFNULL(COUNT(resumen_adobe.familia), 0)) AS disponible,
                    compras_adobe.precio,
                    compras_adobe.cantidadGAP,
                    compras_adobe.balanceGAP
                FROM compras_adobe
                    LEFT JOIN (SELECT  resumen_adobe.cliente,
                            resumen_adobe.empleado,
                            resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version,
                            detalles_equipo_adobe.asignacion
                        FROM resumen_adobe
                            INNER JOIN detalles_equipo_adobe ON resumen_adobe.equipo = detalles_equipo_adobe.equipo
                            AND resumen_adobe.cliente = detalles_equipo_adobe.cliente AND resumen_adobe.empleado = detalles_equipo_adobe.empleado
                        WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado ' . $where1 . '
                        GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version
                        ORDER BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version) AS resumen_adobe ON compras_adobe.cliente = resumen_adobe.cliente AND
                    compras_adobe.empleado = resumen_adobe.empleado AND compras_adobe.familia = resumen_adobe.familia
                    AND compras_adobe.edicion = resumen_adobe.edicion AND compras_adobe.version = resumen_adobe.version
                    AND compras_adobe.asignacion = resumen_adobe.asignacion
                WHERE compras_adobe.cliente = :cliente AND compras_adobe.empleado = :empleado ' . $where . '
                GROUP BY compras_adobe.familia, compras_adobe.edicion, compras_adobe.version, compras_adobe.asignacion
                ORDER BY compras_adobe.familia, compras_adobe.edicion, compras_adobe.version, compras_adobe.asignacion');
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = storage_path('app/files/adobe/archivos_csvf4/') . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}