<?php

namespace App\Clases;

class configuraciones extends General{
   
    //inicio fabricante
    function listar_fabricantes($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idFabricante,
                nombre,
                status
            FROM fabricantes
            WHERE nombre LIKE :nombre AND status = 1');
            $sql->execute(array('nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function existeFabricante($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM fabricantes
            WHERE nombre = :nombre');
            $sql->execute(array('nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function fabricanteEspecifico($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM fabricantes
            WHERE idFabricante = :id');
            $sql->execute(array('id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return array("idFabricante"=>0, "nombre"=>"", "status"=>0);
        }
    }
    
    function insertarFabricante($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO fabricantes (nombre) VALUES (:nombre)');
            $sql->execute(array('nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function modificarFabricante($id, $nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE fabricantes SET nombre = :nombre WHERE idFabricante = :id');
            $sql->execute(array('id'=>$id, 'nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarFabricante($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE fabricantes SET status = 0 WHERE idFabricante = :id');
            $sql->execute(array('id'=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    //fin fabricante
    
    //inicio familia
    function listar_familiaTodoNombre() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT nombre
            FROM productos
            WHERE status = 1');
            $sql->execute();
            //$sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function listar_familia($nombre, $inicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idProducto,
                nombre,
                status
            FROM productos
            WHERE nombre LIKE :nombre AND status = 1
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array('nombre'=>"%" . $nombre . "%"));
            //$sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function totalFamilia($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM productos
            WHERE nombre LIKE :nombre AND status = 1');
            $sql->execute(array('nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function existeFamilia($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM productos
            WHERE nombre = :nombre');
            $sql->execute(array('nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function familiaEspecifico($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM productos
            WHERE idProducto = :id');
            $sql->execute(array('id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return array("idProducto"=>0, "nombre"=>"", "status"=>0);
        }
    }
    
    function insertarFamilia($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO productos (nombre) VALUES (:nombre)');
            $sql->execute(array('nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function modificarFamilia($id, $nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE productos SET nombre = :nombre WHERE idProducto = :id');
            $sql->execute(array('id'=>$id, 'nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarFamilia($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE productos SET status = 0 WHERE idProducto = :id');
            $sql->execute(array('id'=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    //fin familia
    
    //inicio edicion
    function listar_edicionTotal() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idEdicion,
                nombre,
                status
            FROM ediciones
            WHERE status = 1');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function listar_edicion($nombre, $inicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idEdicion,
                nombre,
                status
            FROM ediciones
            WHERE nombre LIKE :nombre AND status = 1
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array('nombre'=>"%" . $nombre . "%"));
            //$sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function totalEdicion($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM ediciones
            WHERE nombre LIKE :nombre AND status = 1');
            $sql->execute(array('nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function existeEdicion($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM ediciones
            WHERE nombre = :nombre');
            $sql->execute(array('nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function edicionEspecifico($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM ediciones
            WHERE idEdicion = :id');
            $sql->execute(array('id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return array("idEdicion"=>0, "nombre"=>"", "status"=>0);
        }
    }
    
    function insertarEdicion($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO ediciones (nombre) VALUES (:nombre)');
            $sql->execute(array('nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function modificarEdicion($id, $nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE ediciones SET nombre = :nombre WHERE idEdicion = :id');
            $sql->execute(array('id'=>$id, 'nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarEdicion($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE ediciones SET status = 0 WHERE idEdicion = :id');
            $sql->execute(array('id'=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    //fin edicion
    
    //inicio version
    function listar_versionTotal() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                nombre,
                status
            FROM versiones
            WHERE status = 1');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function listar_version($nombre, $inicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                nombre,
                status
            FROM versiones
            WHERE nombre LIKE :nombre AND status = 1
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array('nombre'=>"%" . $nombre . "%"));
            //$sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function totalVersion($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM versiones
            WHERE nombre LIKE :nombre AND status = 1');
            $sql->execute(array('nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return array();
        }
    }
    
    function existeVersion($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM versiones
            WHERE nombre = :nombre');
            $sql->execute(array('nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function versionEspecifico($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM versiones
            WHERE id = :id');
            $sql->execute(array('id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return array("id"=>0, "nombre"=>"", "status"=>0);
        }
    }
    
    function insertarVersion($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO versiones (nombre) VALUES (:nombre)');
            $sql->execute(array('nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function modificarVersion($id, $nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE versiones SET nombre = :nombre WHERE id = :id');
            $sql->execute(array('id'=>$id, 'nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarVersion($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE versiones SET status = 0 WHERE id = :id');
            $sql->execute(array('id'=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    //fin version
    
    //inicio conversiones
    function listar_conversiones($codigo, $os) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM tabla_conversion
            WHERE codigo LIKE :codigo AND os LIKE :os');
            $sql->execute(array('codigo'=>"%" . $codigo . "%", 'os'=>"%" . $os . "%"));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return array();
        }
    }
    
    function existeConversion($codigo, $os){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM tabla_conversion
            WHERE codigo = :codigo AND os = :os');
            $sql->execute(array('codigo'=>$codigo, 'os'=>$os));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function conversionEspecifico($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM tabla_conversion
            WHERE id = :id');
            $sql->execute(array('id'=>$id));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return array("id"=>0, "codigo"=>"", "os"=>"");
        }
    }
    
    function insertarConverion($codigo, $os){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO tabla_conversion (codigo, os) VALUES (:codigo, :os)');
            $sql->execute(array('codigo'=>$codigo, 'os'=>$os));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function modificarConversion($id, $codigo, $os){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE tabla_conversion SET codigo = :codigo, os = :os WHERE id = :id');
            $sql->execute(array('id'=>$id, 'codigo'=>$codigo, 'os'=>$os));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function eliminarConversion($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM tabla_conversion WHERE id = :id');
            $sql->execute(array('id'=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    //fin conversiones
    
    function agregarFallas($cliente, $fabricante, $falla, $correo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO reportarFallas (cliente, fabricante, motivo, correo, fecha) '
            . 'VALUES (:cliente, :fabricante, :motivo, :correo, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':fabricante'=>$fabricante, ':motivo'=>$falla, ':correo'=>$correo));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>