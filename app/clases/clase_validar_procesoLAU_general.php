<?php

namespace App\Clases;

class clase_validar_procesoLAU_general extends General{
    private $temp;
    public $nombreArchivo;
    
    //inicio variables LAE
    public $iDN;
    public $iobjectClass;
    public $icn;
    public $ilastLogon;
    public $ipwdLastSet;
    public $isAMAccountName;
    public $ilastLogonTimestamp;
    public $imail;
    //fin variables LAE
    
    private $opcionDespliegue;
    private $opcion;
    public $procesarLAU;
    public $archivoLAE;
    
    function validar_archivo($name, $ext){
        $error = 0;
        $nombre = $name;
     
        // Validaciones
        if($nombre != ""){
            $extension = explode(".", $nombre);  // Obtener tipo de archivo
            $long = count($extension) - 1;
            if(($extension[$long] != $ext) && ($extension[$long] != strtoupper($ext))) { 
                $error = 1; 
            }  
        }else{
            $error = 2;	
        }
        
        return $error;
    }   
   
    function validarLAU($archivoLAU, $opcionDespliegue, $opcion){
        $error = 0;
        $this->opcionDespliegue = $opcionDespliegue;
        $this->opcion = $opcion;

        if($this->obtenerSeparadorUniversal($archivoLAU, 1, 8) === true){
            if (($fichero = fopen($archivoLAU, "r")) !== false) {
                $this->iDN = $this->iobjectClass = $this->icn = $this->ilastLogon = 
                $this->ipwdLastSet = $this->isAMAccountName = $this->ilastLogonTimestamp = 
                $this->imail = -1;
                $this->procesarLAU = true;
                $this->cicloCabecera($fichero, 1, 1);
                fclose($fichero);
                $error = $this->validarResultadosLAU();
            }
            
        } else{
            $error = 10;
            $this->procesarLAU = false;
        }
       
        return $error;
    }
    
    //inicio proceso general de validacion
    function cicloCabecera($fichero, $fila, $opcion){
        $i = 1;
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {

            if($i == $fila){
                $this->ejecutarValidacion($opcion, $datos);
            } else if($i > $fila){
                break;
            }

            $i++;
        }
    }
    
    function ejecutarValidacion($opcion, $datos){ 
        //opcion = 1 AddRemove
        if($opcion == 1){
            $this->validCabLAU($datos);
        }
    }
    
    function validCabLAU($datos){
        //dd($datos);
        for($k = 0; $k < count($datos); $k++){
            $this->iDN = $this->asignarColumnasi($this->iDN, $datos[$k], $k, "DN");
            $this->iobjectClass = $this->asignarColumnasi($this->iobjectClass, $datos[$k], $k, "objectClass");
            $this->icn = $this->asignarColumnasi($this->icn, $datos[$k], $k, "cn");
            $this->ilastLogon = $this->asignarColumnasi($this->ilastLogon, $datos[$k], $k, "lastLogon");
            $this->ipwdLastSet = $this->asignarColumnasi($this->ipwdLastSet, $datos[$k], $k, "pwdLastSet");
            $this->isAMAccountName = $this->asignarColumnasi($this->isAMAccountName, $datos[$k], $k, "sAMAccountName");
            $this->ilastLogonTimestamp = $this->asignarColumnasi($this->ilastLogonTimestamp, $datos[$k], $k, "lastLogonTimestamp");
            $this->imail = $this->asignarColumnasi($this->imail, $datos[$k], $k, "mail");
        }

    }
    
    function validarResultadosLAU(){
        $error = 0;
        if($this->iDN == -1 && $this->iobjectClass == -1 &&  $this->icn == -1 && 
        $this->ilastLogon == -1 && $this->ipwdLastSet == -1 && $this->isAMAccountName == -1  && 
        $this->ilastLogonTimestamp == -1 && $this->imail == -1){
            $this->procesarLAU = false;
        } else if($this->iDN == -1 || $this->iobjectClass == -1 ||  $this->icn == -1  || 
        $this->ilastLogon == -1 || $this->ipwdLastSet == -1 || $this->isAMAccountName == -1  || 
        $this->ilastLogonTimestamp == -1 || $this->imail == -1){
            $error = 3;
        }
        
        return $error;
    }
}
