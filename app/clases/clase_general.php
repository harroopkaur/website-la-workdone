<?php
class General {
    ########################################  Atributos  ########################################

    public  $error = NULL;
    public  $separador;
    public  $limit_paginacion = 50;
    private $configuraciones = 3;
    private $DBusuario;
    private $DBnombre;
    private $DBservidor;
    private $DBcontrasena;
    protected $conn;
    public $registrosBloque = 1000;
    public $tiempoLimite = 600;//el tiempo esta en segundos
    public $ToolAD = "LA_Tool_ADv6.8.rar";
    public $ToolLocal = "LA_Tool_Localv8.0.rar";
    public $ToolOracle = "LATool_Oracle.rar";
    public $ToolUnix = "LA_Tool_Unix_v2.6.rar";
    public $pesoMax = 10240; //peso máximo de los archivos
    #######################################  Operaciones  #######################################
    
    protected function conexion(){
        try{
            $this->configuracion();
            $this->conn = null;
            $this->conn = new PDO('mysql:host=' . $this->DBservidor . ';dbname=' . $this->DBnombre, $this->DBusuario, $this->DBcontrasena);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            print "ERROR: No se pudo realizar la conexión a la base de datos";
        }
    }
    
    private function configuracion(){
        if($this->configuraciones == 1){
            $this->DBusuario = "";
            $this->DBnombre = "";
            $this->DBservidor = "";
            $this->DBcontrasena = "";
        }
        else if($this->configuraciones ==  2){
            $this->DBusuario = "";
            $this->DBnombre = "";
            $this->DBservidor = "";
            $this->DBcontrasena = "";
        }
        else if($this->configuraciones == 3){
            $this->DBusuario = "root";
            $this->DBnombre = "latool";
            $this->DBservidor = "localhost";
            $this->DBcontrasena = "";
        }
    }

    function muestrafecha($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00")) {
            $resultado = '';
        } else {
            $resultado = date("d/m/Y", strtotime($fecha));
        }

        return $resultado;
    }
    
    function muestrafechaIngles($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00")) {
            $resultado = '';
        } else {
            $resultado = date("m/d/Y", strtotime($fecha));
        }

        return $resultado;
    }
    
    function muestraFechaHora($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00 00:00:00")) {
            $resultado = '';
        } else {
            $resultado = date("d/m/Y h:i:s A", strtotime($fecha));
        }

        return $resultado;
    }
    
    function muestraFechaHora1($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00 00:00:00")) {
            $resultado = '';
        } else {
            $f = substr($fecha, 0, 10);
            $resultado = $this->reordenarFecha($f, "-", "/");
        }

        return $resultado;
    }

    function muestrafechas($fecha) {
        if ((empty($fecha)) || ($fecha == "0000-00-00")) {
            $this->error = 'Fecha vac&iacute;a';
            $resultado = 'Fecha vac&iacute;a';
        } else {
            $resultado = date("d-m-Y", strtotime($fecha));
        }

        return $resultado;
    }
    
    function cambiarfechasFormato($fecha, $formato = "Y-m-d") {
        if ((empty($fecha)) || ($fecha == "0000-00-00" || $fecha == "00-0000-00" || $fecha == "00-00-0000" ||
        $fecha == "0000/00/00" || $fecha == "00/0000/00" || $fecha == "00/00/0000")) {
            $this->error = 'Fecha vac&iacute;a';
            $resultado = 'Fecha vac&iacute;a';
        } else {
            $resultado = date($formato, strtotime($fecha));
        }

        return $resultado;
    }
    
    function validateDate($date, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
       
    function reordenarFecha($fecha, $separador, $sustituirSeparador = null, $formato = "dd/mm/YYYY"){
        $fechaAux = explode($separador, $fecha);
        if($sustituirSeparador == null){
            if($formato == "dd/mm/YYYY"){
                $fecha = $fechaAux[2] . $separador . $fechaAux[1] . $separador . $fechaAux[0];
            } else if("mm/dd/YYYY"){
                $fecha = $fechaAux[2] . $separador . $fechaAux[0] . $separador . $fechaAux[1];
            }
        }
        else{
            if($formato == "dd/mm/YYYY"){
                $fecha = $fechaAux[2] . $sustituirSeparador . $fechaAux[1] . $sustituirSeparador . $fechaAux[0];
            } else if("mm/dd/YYYY"){
                $fecha = $fechaAux[2] . $sustituirSeparador . $fechaAux[0] . $sustituirSeparador . $fechaAux[1];
            }
        }
        return $fecha;
    }
    
    function sustituirSeparadorFecha($fecha, $separador, $sustituirSeparador){
        $fechaAux = explode($separador, $fecha);
        $fecha = $fechaAux[0] . $sustituirSeparador . $fechaAux[1] . $sustituirSeparador . $fechaAux[2];
        return $fecha;
    }

    function guardafecha($fecha) {
        $ano = substr($fecha, 6, 4);
        $mes = substr($fecha, 3, 2);
        $dia = substr($fecha, 0, 2);
        $fecha = "$ano-$mes-$dia";

        if ($fecha == '--') {
            $fecha = '';
        }

        return $fecha;
    }

    function daysDiff($dateIni, $dateEnd) {

        $seconds = $dateEnd - $dateIni;
        
        return round($seconds / 60 / 60 / 24, 2);
    }

    function minimo($num1, $num2, $num3) {

        $value = 100000000;

        $value_return = 0;

        if ($num1 == null) {

            $num1 = $value;
        }

        if ($num2 == null) {

            $num2 = $value;
        }

        if ($num3 == null) {

            $num3 = $value;
        }



        if ($value == $num1 && $num1 == $num2 && $num1 == $num3) {

            return $value_return;
        } else {

            if ($num1 <= $num2 && $num1 <= $num3) {

                $value_return = $num1;
            } else if ($num2 <= $num1 && $num2 <= $num3) {

                $value_return = $num2;
            } else if ($num3 <= $num1 && $num3 <= $num2) {

                $value_return = $num3;
            }

            return $value_return;
        }
    }

    function calc_range($num) {

        $result = " d&iacute;as";

        if ($num <= 30) {

            $result = 30 . $result;
        } else if ($num <= 60) {

            $result = 60 . $result;
        } else if ($num <= 90) {

            $result = 90 . $result;
        } else if ($num <= 365) {

            $result = 365 . $result;
        } else {

            $result = "m&aacute;s de 365 d&iacute;s";
        }

        return $result;
    }

    function search_server($string) {

        $searched = strpos($string, "Server");

        $value[0] = 1;

        $value[1] = "Client";

        if ($searched == true) {

            $value[0] = 2;

            $value[1] = "Server";
        }

        return $value;
    }

    function diferencia_entre_fechas($fecha1, $fecha2) {
        // separo la fecha
        $trozos1 = explode("-", $fecha1);
        $trozos2 = explode("-", $fecha2);

        //defino fecha 1
        $ano1 = $trozos1[0];
        $mes1 = $trozos1[1];
        $dia1 = $trozos1[2];

        //defino fecha 2
        $ano2 = $trozos2[0];
        $mes2 = $trozos2[1];
        $dia2 = $trozos2[2];

        //calculo timestam de las dos fechas
        $timestamp1 = mktime(0, 0, 0, $mes1, $dia1, $ano1);
        $timestamp2 = mktime(0, 0, 0, $mes2, $dia2, $ano2);

        //resto a una fecha la otra
        $segundos_diferencia = $timestamp1 - $timestamp2;
        //echo $segundos_diferencia;
        //convierto segundos en d�as
        $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

        //obtengo el valor absoulto de los d�as (quito el posible signo negativo)
        //$dias_diferencia = abs($dias_diferencia);
        //quito los decimales a los d�as de diferencia
        $dias_diferencia = floor($dias_diferencia);

        return $dias_diferencia;
    }

    function dia_semana($fecha) {
        list($ano, $mes, $dia) = explode("-", $fecha);
        $numero_dia_semana = date('w', mktime(0, 0, 0, $mes, $dia, $ano));

        switch ($numero_dia_semana) {
            case 0: return "Domingo";
            case 1: return "Lunes";
            case 2: return "Martes";
            case 3: return "Mi&eacute;rcoles";
            case 4: return "Jueves";
            case 5: return "Viernes";
            case 6: return "S&aacute;bado";
        }
    }

    function generarCodigo($longitud = 5, $caracter = TRUE, $numero = TRUE, $simbolo = FALSE) {
        $source = '';

        if ($caracter) {
            $source .= 'a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';
        }

        if ($numero) {
            $source .= '1 2 3 4 5 6 7 8 9 0';
        }

        if ($simbolo) {
            $source .= '| @ # ~ $ % ( ) = ^ * + [ ] { } - _';
        }

        if ($longitud > 0) {
            $codigo = "";
            $source = explode(" ", $source);

            for ($i = 0; $i < $longitud; $i++) {
                mt_srand((double) microtime() * 1000000);
                $num = mt_rand(1, count($source));
                $codigo .= $source[$num - 1];
            }
        }

        return $codigo;
    }

    function urls_amigables($url) {

        // Tranformamos todo a minusculas

        $url = strtolower($url);

        //Rememplazamos caracteres especiales latinos

        $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');

        $repl = array('a', 'e', 'i', 'o', 'u', 'n');

        $url = str_replace($find, $repl, $url);

        // A�aadimos los guiones

        $find = array(' ', '&', '\r\n', '\n', '+');
        $url = str_replace($find, '-', $url);

        // Eliminamos y Reemplazamos dem�s caracteres especiales

        $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');

        $repl = array('', '-', '');

        $url = preg_replace($find, $repl, $url);

        return $url;
    }

    function obtenerSeparador($archivo){
        $this->separador = "";
        $this->error = "Los separadores v&aacute;lidos para <br>archivo CSV son \",\" o \";\"";
        $bandera = false;
        
        if($this->abrirArchivoSeparador($archivo, $bandera, ',') === false){
            if($this->abrirArchivoSeparador($archivo, $bandera, ';') === true){
                $bandera = true;
                $this->separador = ";";
            }
        }
        else{
            $bandera = true;
            $this->separador = ",";
        }
        return $bandera;
    }
    
    function obtenerSeparadorLAE_Output($archivo){
        $this->separador = "";
        $this->error = "Los separadores v&aacute;lidos para <br>archivo CSV son \",\" o \";\"";
        $bandera = false;
        
        if($this->abrirArchivoSeparadorLAE_Output($archivo, $bandera, ',') === false){
            if($this->abrirArchivoSeparadorLAE_Output($archivo, $bandera, ';') === true){
                $bandera = true;
                $this->separador = ";";
            }
        }
        else{
            $bandera = true;
            $this->separador = ",";
        }
        return $bandera;
    }
    
    function obtenerSeparadorUniversal($archivo, $numFilas, $numColumna){
        $this->separador = "";
        $this->error = "Los separadores v&aacute;lidos para <br>archivo CSV son \",\" o \";\"";
        $bandera = false;
        
        if($this->abrirArchivoSeparadorUniversal($archivo, $bandera, ',', $numFilas, $numColumna) === false){
            if($this->abrirArchivoSeparadorUniversal($archivo, $bandera, ';', $numFilas, $numColumna) === true){
                $bandera = true;
                $this->separador = ";";
            }
        }
        else{
            $bandera = true;
            $this->separador = ",";
        }
        return $bandera;
    }
    
    function abrirArchivoSeparador($archivo, $bandera, $separador){
        if(($fichero = fopen($archivo, "r")) !== FALSE) {
            $i=1;
            while (($datos = fgetcsv($fichero, 1000, $separador)) !== FALSE) {
                if(($i == 1 || $i == 2 || $i == 7) && count($datos) >= 3){
                    $bandera = true;
                    break;
                } 
                if($i > 7){
                    break;
                } 
                $i++;
            }
            fclose($fichero);
        }
        return $bandera;
    }
    
    function abrirArchivoSeparadorLAE_Output($archivo, $bandera, $separador){
        if(($fichero = fopen($archivo, "r")) !== FALSE) {
            $i=1;
            while (($datos = fgetcsv($fichero, 1000, $separador)) !== FALSE) {
                if(($i == 1) && count($datos) >= 3){
                    $bandera = true;
                    break;
                } 
                if($i > 1){
                    break;
                } 
                $i++;
            }
            fclose($fichero);
        }
        return $bandera;
    }
    
    function abrirArchivoSeparadorUniversal($archivo, $bandera, $separador, $numFilas, $numColumna){
        if(($fichero = fopen($archivo, "r")) !== FALSE) {
            $i=1;
            while (($datos = fgetcsv($fichero, 1000, $separador)) !== FALSE) {
                if(($i == $numFilas) && count($datos) >= $numColumna){
                    $bandera = true;
                    break;
                } 
                if($i > $numFilas){
                    break;
                } 
                $i++;
            }
            fclose($fichero);
        }
        return $bandera;
    }
    
    function verifSesion($clienteAutorizado, $tiempoCliente, $TIEMPO_MAXIMO_SESION){
        $result = array();
        if(!$clienteAutorizado) {
            $result[0] = false;
            $result[2] = "¡Usted debe Iniciar Sesión!";
        }
        // Verificar tiempo de sesion
        $time = time();
        $tiempo_sesion = $time - $tiempoCliente;
        if($tiempo_sesion > $TIEMPO_MAXIMO_SESION) {
            $result[0] = false;
            $result[2] = "¡Usted pasó mucho tiempo inactivo!";
        } else {
            $result[0] = true;
            $result[1] = time();
            $result[2] = "";
        }
        return $result;
    }
    
    function obtenerMensaje(){
        return "¡Usted debe Iniciar Sesi&oacute;n!";
    }
    
    function eliminarSesion(){
        session_unset();                 // Vacia las variables de sesion
        session_destroy();               // Destruye la sesion
    }
    
    function salirPorSesion($mensaje){
        echo "<script> "
            . "alert('" . $mensaje . "'); "
            . "location.href='" . $GLOBALS["domain_root"] . "'"
        . "</script>";
    }
    
    function salirPorSesionReporte($mensaje){
        echo "<script> "
            . "alert('" . $mensaje . "'); "
            . "window.close();"
        . "</script>";
    }
    
    public function get_escape($valor){
        return htmlspecialchars(addslashes(stripslashes(strip_tags(trim($valor)))));
    }
    
    public function obtenerClaseColorInicio($fabricante){
        if($fabricante == 1){
            $valor = "colorIncio1";
        } else if($fabricante == 2){
            $valor = "colorIncio2";
        } else if($fabricante == 3){
            $valor = "colorIncio3";
        } else if($fabricante == 4){
            $valor = "colorIncio4";
        } else if($fabricante == 5){
            $valor = "colorIncio5";
        } else if($fabricante == 6){
            $valor = "colorIncio6";
        } else if($fabricante == 7){
            $valor = "colorIncio7";
        } else if($fabricante == 8){
            $valor = "colorIncio8";
        } else if($fabricante == 9){
            $valor = "colorIncio9";
        } else if($fabricante == 10){
            $valor = "colorIncio10";
        }
        return $valor;
    }
    
    public function transformarTipoCompra($valor){
        if($valor === "subscripcion"){
            $valor = "Subscripción";
        } else if($valor === "Subscripción"){
            $valor = "subscripcion";
        } else if($valor === "software assurance"){
            $valor = "Software Assurance";
        } else if($valor === "Software Assurance"){
            $valor = "software assurance";
        } else if($valor === "perpetuo"){
            $valor = "Perpetuo";
        } else if($valor === "Perpetuo"){
            $valor = "perpetuo";
        } else if($valor === "otro"){
            $valor = "Otro";
        } else if($valor === "Otro"){
            $valor = "otro";
        }
        return $valor;
    }
    
    function validarFecha($fecha, $separador, $formato){
        $resultado = false;
        $arrayFecha = explode($separador, $fecha);
        if($formato == "aaaa".$separador."mm".$separador."dd"){
            if(isset($arrayFecha[1]) && isset($arrayFecha[2]) && checkdate($arrayFecha[1] , $arrayFecha[2] , $arrayFecha[0])){
                $resultado = true;
            }
        } else if($formato == "aaaa".$separador."dd".$separador."mm"){
            if(isset($arrayFecha[1]) && isset($arrayFecha[2]) && checkdate($arrayFecha[2] , $arrayFecha[1] , $arrayFecha[0])){
                $resultado = true;
            }
        } else if($formato == "mm".$separador."aaaa".$separador."dd"){
            if(isset($arrayFecha[1]) && isset($arrayFecha[2]) && checkdate($arrayFecha[0] , $arrayFecha[2] , $arrayFecha[1])){
                $resultado = true;
            }
        } else if($formato == "mm".$separador."dd".$separador."aaaa"){
            if(isset($arrayFecha[1]) && isset($arrayFecha[2]) && checkdate($arrayFecha[0] , $arrayFecha[1] , $arrayFecha[2])){
                $resultado = true;
            }
        } else if($formato == "dd".$separador."mm".$separador."aaaa"){
            if(isset($arrayFecha[1]) && isset($arrayFecha[2]) && checkdate($arrayFecha[1] , $arrayFecha[0] , $arrayFecha[2])){
                $resultado = true;
            }
        } else if($formato == "dd".$separador."aaaa".$separador."mm"){
            if(isset($arrayFecha[1]) && isset($arrayFecha[2]) && checkdate($arrayFecha[2] , $arrayFecha[0] , $arrayFecha[1])){
                $resultado = true;
            }
        } 
        return $resultado;
    }
    
    function truncarString($valor, $tamano){
        if($valor != "" && strlen($valor) > $tamano){
            $valor = substr($valor, 0, $tamano);
        }
        return $valor;
    }
    
    function getMensajeExito($idioma){
        if($idioma == 1){
            return "Archivo cargado con éxito";
        } else if($idioma == 2){
            return "File Loaded Successfully";
        }
    }
    
    function getMensajeLAE($idioma){
        if($idioma == 1){
            return "Debe seleccionar el archivo LAE_Output.csv";
        } else if($idioma == 2){
            return "You must select the LAE_Output file";
        }
    }
    
    function getMensajeEquipos($idioma){
        if($idioma == 1){
            return "No existen equipos";
        } else if($idioma == 2){
            return "There are no equipment";
        }
    }
    
    function getMensajeRar($idioma){
        if($idioma == 1){
            return "Debe ser un archivo con extensión rar";
        } else if($idioma == 2){
            return "Must be a rar file";
        }
    }
    
    function getMensajeCarpNoCreada($idioma){
        if($idioma == 1){
            return "No se pudo crear la carpeta del cliente";
        } else if($idioma == 2){
            return "Could not create Client folder";
        }
    }
    
    function getMensajeCsv($idioma){
        if($idioma == 1){
            return "Debe ser un archivo con extensión csv";
        } else if($idioma == 2){
            return "Must be a CSV file";
        }
    }
    
    function getMensajeTxt($idioma){
        if($idioma == 1){
            return "Debe ser un archivo con extensión txt";
        } else if($idioma == 2){
            return "Must be a txt file";
        }
    }
    
    function getMensajeSeleccionar($idioma){
        if($idioma == 1){
            return "Debe seleccionar un archivo";
        } else if($idioma == 2){
            return "You must select a file";
        }
    }
    
    function getAdverCompras($idioma){
        if($idioma == 1){
            return "Estimado cliente, por favor recuerde cargar las compras en la pestaña Compras";
        } else if($idioma == 2){
            return "please remember to load purchases on the purchases tab";
        }
    }
    
    function getMensajeCabAddRemove($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Consolidado Addremove.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the Consolidado Addremove.csv file is not supported";
        }
    }
    
    function getMensajeCabEscaneo($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Resultados_Escaneo.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the Resultados_Escaneo.csv file is not supported";
        }
    }
    
    function getMensajeCabLAE($idioma){
        if($idioma == 1){
            return "La cabecera del archivo LAE_Output no es compatible";
        } else if($idioma == 2){
            return "The header of the LAE_Output file is not supported";
        }
    }
    
    function getMensajeCabProcesadores($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Consolidado Procesadores.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the Consolidado Procesadores.csv file is not supported";
        }
    }
    
    function getMensajeCabTipoEquipo($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Consolidado Tipo de Equipo.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the Consolidado Tipo de Equipo.CSV file is not supported";
        }
    }
    
    function getMensajeCabUsuarioEquipo($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Consolidado Usuario-Equipo.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the Consolidado Usuario-Equipo.csv file is not supported";
        }
    }
    
    function getMensajeCabCompSistema($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Componentes Sistema no es compatible";
        } else if($idioma == 2){
            return "The header of the system components file is not supported";
        }
    }
    
    function getMensajeCabUsuModulo($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Usuario x Módulo no es compatible";
        } else if($idioma == 2){
            return "The header of the user X module file is not supported";
        }
    }
    
    function getMensajeCabAging($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Aging Usuarios no es compatible";
        } else if($idioma == 2){
            return "The header of the aging users file is not supported";
        }
    }
    
    function getMensajeCabVMWare($idioma){
        if($idioma == 1){
            return "La cabecera del archivo VMWare no es compatible";
        } else if($idioma == 2){
            return "The header of the VMware file is not supported";
        }
    }
    
    function getMensajeCab($idioma){
        if($idioma == 1){
            return "La cabecera del archivo no es compatible";
        } else if($idioma == 2){
            return "The header of the file is not supported";
        }
    }
    
    function getMensajeFaltanArchivos($idioma){
        if($idioma == 1){
            return "El archivo .rar no posee los archivos necesarios";
        } else if($idioma == 2){
            return "The .rar file does not have the necessary files";
        }
    }
    
    function getMensajeNoDescRar($idioma){
        if($idioma == 1){
            return "No se descomprimió el archivo .rar";
        } else if($idioma == 2){
            return "The .rar file was not unzipped";
        }
    }
    
    function getAlertArchCargado($idioma, $archivo){
        if($idioma == 1){
            return "El archivo " . $archivo . " ya fue cargado";
        } else if($idioma == 2){
            return "The " . $archivo . " file already loaded";
        }
    }
    
    function getAlertNoCargado($idioma){
        if($idioma == 1){
            return "El archivo no fue cargado";
        } else if($idioma == 2){
            return "File was not loaded";
        }
    }
    
    function getAlertPassword($idioma){
        if($idioma == 1){
            return "Contraseña Incorrecta";
        } else if($idioma == 2){
            return "Wrong Password";
        }
    }
    
    function getMensajeReporteFinal($idioma){
        if($idioma == 1){
            return "La cabecera del archivo ReporteFinal.txt no es compatible";
        } else if($idioma == 2){
            return "The header of the ReporteFinal.txt file is not supported";
        }
    }
    
    function getMensajevCPU($idioma){
        if($idioma == 1){
            return "La cabecera del archivo vCPU.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the vCPU.csv file is not supported";
        }
    }
    
    function getMensajeSPLAClientes($idioma){
        if($idioma == 1){
            return "La cabecera del archivo clientes.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the clientes.csv file is not supported";
        }
    }
    
    function getMensajeSPLASKU($idioma){
        if($idioma == 1){
            return "La cabecera del archivo SKU.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the SKU.csv file is not supported";
        }
    }
    
    function getMensajeSQL($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Consolidado SQL.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the Consolidado SQL.csv file is not supported";
        }
    }
    
    function getMensajeAsignacion($idioma){
        if($idioma == 1){
            return "La cabecera del archivo asignacion.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the asignacion.csv file is not supported";
        }
    }
    
    function getMensajeCompras($idioma){
        if($idioma == 1){
            return "La cabecera del archivo Formulario de Compras.csv no es compatible";
        } else if($idioma == 2){
            return "The header of the Formulario de Compras.csv file is not supported";
        }
    }
    
    function getSeparadorAddRemove($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo Consolidado Addremove.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the Consolidado Addremove.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorEscaneo($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo Resultados_Escaneo.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the Resultados_Escaneo.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorProcesadores($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo Consolidado Procesadores.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the Consolidado Procesadores.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorTipoEquipo($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo Consolidado Tipo de Equipo.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the Consolidado Tipo de Equipo.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorUsuarioEquipo($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo Consolidado Usuario-Equipo.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the Consolidado Usuario-Equipo.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorLAE($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo LAE_output.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the LAE_output.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorvCPU($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo vCPU.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the vCPU.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorSPLAClientes($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo clientes.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the clientes.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorSPLASKU($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo SKU.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the SKU.csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorGeneral($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the csv file are \",\" or \";\"";
        }
    }
    
    function getSeparadorSQL($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo Consolidado SQL.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the Consolidado SQL.csv file are \",\" or \";\"";
        }
    }
    function getSeparadorAsignacion($idioma){
        if($idioma == 1){
            return "Los separadores válidos para el archivo asignacion.csv son \",\" o \";\"";
        } else if($idioma == 2){
            return "The valid separators for the asignacion.csv file are \",\" or \";\"";
        }
    }
    
    
    function getAlertFinSesionInactivo($idioma){
        if($idioma == 1){
            return "Estimado cliente ha pasado mucho tiempo inactivo.  ¿Desea renovar sesión?";
        } else{
            return "Dear customer has spent a lot of time idle. Do you want to renew session?";
        }
    }
    
    function getAlertSesionRenovada($idioma){
        if($idioma == 1){
            return "Sesión renovada";
        } else{
            return "Renewed session";
        }
    }
    
    function getDebeIniSesion($idioma){
        if($idioma == 1){
            return "¡Usted debe Iniciar Sesión!";
        } else{
            return "You must Log In!";
        }
    }
    
    function getInactivo($idioma){
        if($idioma == 1){
            return "¡Usted pasó mucho tiempo inactivo!";
        } else if($idioma == 2){
            return "You spent a lot of time inactive!";
        }
    }
    
    function obtenerAsignacionClienteEmpleado($cliente, $empleado){
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT asignacion.id,
                    asignacion.nombre AS asignacion
                FROM asigClienteEmple
                    INNER JOIN asignacion ON asigClienteEmple.idAsignacion = asignacion.id
                    INNER JOIN asigCliente ON asigClienteEmple.cliente = asigCliente.cliente AND asigCliente.estado = 1
                    AND asigCliente.idAsignacion = asigClienteEmple.idAsignacion
                WHERE asigClienteEmple.cliente = :cliente AND asigClienteEmple.empleado = :empleado AND asigClienteEmple.estado = 1
                GROUP BY asignacion.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function validarAsignacion($cliente, $empleado, $asignacion){
        try{   
            $result = false;
            $this->conexion();
            $sql = $this->conn->prepare("SELECT asignacion.id
                FROM asigClienteEmple
                    INNER JOIN asignacion ON asigClienteEmple.idAsignacion = asignacion.id AND
                    asignacion.nombre = :asignacion
                WHERE asigClienteEmple.cliente = :cliente AND asigClienteEmple.empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':asignacion'=>$asignacion));
            if(count($sql->fetchAll()) > 0){
                $result = true;
            }
            return $result;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function stringConsulta($array, $campo){
        $valor = "";
        foreach($array as $row){
            if($valor != ""){
                $valor .= ", ";
            }
            $valor .= "'" . $row[$campo] . "'";
        }
        return $valor;
    }
    
    function getRealIP() {
        $ip = $_SERVER["REMOTE_ADDR"];
        if (!empty($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        return $ip;
    }
    
    function completarCeroNumeros($valor){
        if($valor > 12){
            $valor -= 12;
        }
        
        if($valor < 10){
            $valor = "0" . $valor;
        }
        return $valor;
    } 
    
    function extraerEquipo($hostEquipo){
        $host = explode('.', $hostEquipo);
        if(filter_var($hostEquipo, FILTER_VALIDATE_IP)!== false){
            $host[0] = $hostEquipo;
        }
        return $host[0];
    }
    
    function asignarColumnasi($ivalor, $nombColumna, $indiceColumna, $valorComparar){
        $result = $ivalor;
        
        if($ivalor == -1 && trim(utf8_encode($nombColumna)) == $valorComparar){ 
            $result = $indiceColumna; 
        }
        return $result;
    }
    
    function asignarColumnasi1($ivalor, $nombColumna, $indiceColumna, $valorComparar, $valorComparar1){
        $result = $ivalor;
        if($ivalor == -1 && (trim(utf8_encode($nombColumna)) == $valorComparar || trim(utf8_encode($nombColumna)) == $valorComparar1)){ 
            $result = $indiceColumna; 
        }
        return $result;
    }
    
    function asignarColumnasi2($ivalor, $nombColumna, $indiceColumna, $valorComparar){
        $result = $ivalor;
        if($ivalor == -1 && strpos(trim(utf8_encode($nombColumna)), $valorComparar) !== false){ 
            $result = $indiceColumna; 
        }
        return $result;
    }
    
    function validarArchivosCargados($opcionDespliegue, $archivosCargados, $nombArchivo){
        $error = 0;
        if($opcionDespliegue != "completo" && $opcionDespliegue != "segmentado"){
            $error = $this->compararArchivos($archivosCargados, $nombArchivo);
        }
        
        return $error;
    }
    
    function compararArchivos($archivosCargados, $nombArchivo){
        $error = 0;
        for($i = 0; $i < count($archivosCargados); $i++){
            if($archivosCargados[$i] == $nombArchivo){
                $error = 15;
                break;
            }
        }
        
        return $error;
    }
    
    function obtenerCampoResumenProfesional($tabla){
        $valor = " AND (" . $tabla . ".edicion LIKE :edicion OR " . $tabla . ".edicion LIKE '%ProPlus%' OR " . $tabla . ".edicion LIKE '%Profesional%') ";
        return $valor;
    }
    
    function noIncluirResumenProfesional($tabla){
        $valor = " AND NOT " . $tabla . ".edicion LIKE :edicion AND NOT " . $tabla . ".edicion LIKE '%ProPlus%' AND NOT " . $tabla . ".edicion LIKE '%Profesional%' ";
        return $valor;
    }
    
    public function generadorCodigo(){
        $codigo = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"), 0, 16);
        $codigoReal = substr($codigo, 0, 4) . "-" . substr($codigo, 4, 4) . "-" . substr($codigo, 8, 4) . "-" . substr($codigo, 12, 4);
        return $codigoReal;
    } 
    
    public function letraExcel($indice){
        $letra = "A";
        switch ($indice) {
            case 1:
                $letra = "A";
                break;
            case 2:
                $letra = "B";
                break;
            case 3:
                $letra = "C";
                break;
            case 4:
                $letra = "D";
                break;
            case 5:
                $letra = "E";
                break;
            case 6:
                $letra = "F";
                break;
            case 7:
                $letra = "G";
                break;
            case 8:
                $letra = "H";
                break;
            case 9:
                $letra = "I";
                break;
            case 10:
                $letra = "J";
                break;
            case 11:
                $letra = "K";
                break;
            case 12:
                $letra = "L";
                break;
            case 13:
                $letra = "M";
                break;
            case 14:
                $letra = "N";
                break;
            case 15:
                $letra = "O";
                break;
            case 16:
                $letra = "P";
                break;
            case 17:
                $letra = "Q";
                break;
            case 18:
                $letra = "R";
                break;
            case 19:
                $letra = "S";
                break;
            case 20:
                $letra = "T";
                break;
            case 21:
                $letra = "U";
                break;
            case 22:
                $letra = "V";
                break;
            case 23:
                $letra = "W";
                break;
            case 24:
                $letra = "X";
                break;
            case 25:
                $letra = "Y";
                break;
            case 26:
                $letra = "Z";
                break;
            case 27:
                $letra = "AA";
                break;
            case 28:
                $letra = "AB";
                break;
            case 29:
                $letra = "AC";
                break;
            case 30:
                $letra = "AD";
                break;
        }
        
        return $letra;
    }
}