<?php

namespace App\Clases;

class DetallesIbm extends General{
    ########################################  Atributos  ########################################
    public $lista;
    public $listaCliente;
    public $listaServidor;
    public $error;
    public $lg0000 = "En Uso";
    public $lg0001 = "Uso Probable";
    public $lg0002 = "Obsoleto";
    
    function __construct() {
       // if($_SESSION["idioma"] == 2){
       //     $this->lg0000 = "In Use";
       //     $this->lg0001 = "Probably in Use";
       //     $this->lg0002 = "Obsolete";
       // }
    }
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $os, $familia, $edicion, $version, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_ibm (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) '
            . 'VALUES (:cliente, :empleado, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo,':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO detalles_equipo_ibm (cliente, empleado, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    /*function insertarSAM($archivo, $cliente, $equipo, $os, $dias1, $dias2, $dias3, $minimo, $activo, $tipo, $rango) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalles_equipo_ibmSam (cliente, equipo, os, familia, edicion, version, dias1, dias2,dias3, minimo, activo,tipo, rango) VALUES '
            . 'VALUES (:cliente, :equipo, :os, :familia, :edicion, :version, :dias1, :dias2, :dias3, :minimo, :activo,:tipo, :rango)');        
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':os'=>$os, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':dias1'=>$dias1, ':dias2'=>$dias2, ':dias3'=>$dias3, ':minimo'=>$minimo, ':activo'=>$activo,':tipo'=>$tipo, ':rango'=>$rango));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/

    // Actualizar
    function actualizar($cliente, $empleado, $equipo, $errors) {       
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalles_equipo_ibm SET errors = :errors WHERE equipo = :equipo AND cliente = :cliente AND empleado = :empleado');        
            $sql->execute(array(':errors'=>$errors, ':equipo'=>$equipo, ':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo actualizar el registro";
            return false;
        }
    }
    
    function actualizarAsignacion($cliente, $empleado, $equipo, $asignacion){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo_ibm SET asignacion = :asignacion WHERE cliente = :cliente AND empleado = :empleado "
            .  "AND equipo = :equipo");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':asignacion'=>$asignacion, ':equipo'=>$equipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function limpiarAsignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE detalles_equipo_ibm SET asignacion = null WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalles_equipo_ibm WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_ibm WHERE cliente = :cliente AND empleado = :empleado AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo1($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_ibm WHERE cliente = :cliente ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo1Asignacion($cliente, $asignacion, $asignaciones) {        
        try {
            $array = array(':cliente'=>$cliente);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_ibm WHERE cliente = :cliente ' . $where . ' ORDER BY id');
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }

    /*function listar_todoSAM($cliente) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_ibmSam WHERE cliente = :cliente AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }*/
    
    function listar_todoSAMArchivo($archivo) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_ibmSam WHERE archivo = :archivo AND os LIKE "%Windows%" ORDER BY id');
            $sql->execute(array(':archivo'=>$archivo));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listar_todoSAMArchivo1($archivo) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM detalles_equipo_ibmSam WHERE archivo = :archivo ORDER BY id');
            $sql->execute(array(':archivo'=>$archivo));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }

    function listar_optimizacion($cliente, $empleado, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(IBMDSStorage.edicion) AS IBMDSStorage,
                        GROUP_CONCAT(IBMHigh.edicion) AS IBMHigh,
                        GROUP_CONCAT(IBMInformix.edicion) AS IBMInformix,
                        GROUP_CONCAT(IBMSPSS.edicion) AS IBMSPSS,
                        GROUP_CONCAT(IBMIntegration.edicion) AS IBMIntegration,
                        GROUP_CONCAT(IBMSecurityAppScan.edicion) AS IBMSecurityAppScan,
                        GROUP_CONCAT(IBMSterlingCertificate.edicion) AS IBMSterlingCertificate,
                        GROUP_CONCAT(IBMSterlingConnect.edicion) AS IBMSterlingConnect,
                        GROUP_CONCAT(IBMSterlingExternal.edicion) AS IBMSterlingExternal,
                        GROUP_CONCAT(IBMSterlingSecureProxy.edicion) AS IBMSterlingSecureProxy,
                        GROUP_CONCAT(IBMTivoliStorage.edicion) AS IBMTivoliStorage,
                        GROUP_CONCAT(IBMWebservice.edicion) AS IBMWebservice,
                        GROUP_CONCAT(IBMWebSphere.edicion) AS IBMWebSphere,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_ibm AS detEquipo WHERE detEquipo.cliente = :cliente1 AND detEquipo.empleado = :empleado AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_ibm AS detalles_equipo2
                      LEFT JOIN resumen_ibm AS IBMDSStorage ON detalles_equipo2.equipo = IBMDSStorage.equipo AND detalles_equipo2.cliente = IBMDSStorage.cliente AND detalles_equipo2.empleado = IBMDSStorage.empleado AND IBMDSStorage.familia = 'IBM DS Storage'
                      LEFT JOIN resumen_ibm AS IBMHigh ON detalles_equipo2.equipo = IBMHigh.equipo AND detalles_equipo2.cliente = IBMHigh.cliente AND detalles_equipo2.empleado = IBMHigh.empleado AND IBMHigh.familia = 'IBM High'
                      LEFT JOIN resumen_ibm AS IBMInformix ON detalles_equipo2.equipo = IBMInformix.equipo AND detalles_equipo2.cliente = IBMInformix.cliente AND detalles_equipo2.empleado = IBMInformix.empleado AND IBMInformix.familia = 'IBM Informix'
                      LEFT JOIN resumen_ibm AS IBMSPSS ON detalles_equipo2.equipo = IBMSPSS.equipo AND detalles_equipo2.cliente = IBMSPSS.cliente AND detalles_equipo2.empleado = IBMSPSS.empleado AND IBMSPSS.familia = 'IBM SPSS'
                      LEFT JOIN resumen_ibm AS IBMIntegration ON detalles_equipo2.equipo = IBMIntegration.equipo AND detalles_equipo2.cliente = IBMIntegration.cliente AND detalles_equipo2.empleado = IBMIntegration.empleado AND IBMIntegration.familia = 'IBM Integration'
                      LEFT JOIN resumen_ibm AS IBMSecurityAppScan ON detalles_equipo2.equipo = IBMSecurityAppScan.equipo AND detalles_equipo2.cliente = IBMSecurityAppScan.cliente AND detalles_equipo2.empleado = IBMSecurityAppScan.empleado AND IBMSecurityAppScan.familia = 'IBM Security AppScan'
                      LEFT JOIN resumen_ibm AS IBMSterlingCertificate ON detalles_equipo2.equipo = IBMSterlingCertificate.equipo AND detalles_equipo2.cliente = IBMSterlingCertificate.cliente AND detalles_equipo2.empleado = IBMSterlingCertificate.empleado AND IBMSterlingCertificate.familia = 'IBM Sterling Certificate'
                      LEFT JOIN resumen_ibm AS IBMSterlingConnect ON detalles_equipo2.equipo = IBMSterlingConnect.equipo AND detalles_equipo2.cliente = IBMSterlingConnect.cliente AND detalles_equipo2.empleado = IBMSterlingConnect.empleado AND IBMSterlingConnect.familia = 'IBM Sterling Connect'
                      LEFT JOIN resumen_ibm AS IBMSterlingExternal ON detalles_equipo2.equipo = IBMSterlingExternal.equipo AND detalles_equipo2.cliente = IBMSterlingExternal.cliente AND detalles_equipo2.empleado = IBMSterlingExternal.empleado AND IBMSterlingExternal.familia = 'IBM Sterling External'
                      LEFT JOIN resumen_ibm AS IBMSterlingSecureProxy ON detalles_equipo2.equipo = IBMSterlingSecureProxy.equipo AND detalles_equipo2.cliente = IBMSterlingSecureProxy.cliente AND detalles_equipo2.empleado = IBMSterlingSecureProxy.empleado AND IBMSterlingSecureProxy.familia = 'IBM Sterling Secure Proxy'
                      LEFT JOIN resumen_ibm AS IBMTivoliStorage ON detalles_equipo2.equipo = IBMTivoliStorage.equipo AND detalles_equipo2.cliente = IBMTivoliStorage.cliente AND detalles_equipo2.empleado = IBMTivoliStorage.empleado AND IBMTivoliStorage.familia = 'IBM Tivoli Storage'
                      LEFT JOIN resumen_ibm AS IBMWebservice ON detalles_equipo2.equipo = IBMWebservice.equipo AND detalles_equipo2.cliente = IBMWebservice.cliente AND detalles_equipo2.empleado = IBMWebservice.empleado AND IBMWebservice.familia = 'IBM Web service'
                      LEFT JOIN resumen_ibm AS IBMWebSphere ON detalles_equipo2.equipo = IBMWebSphere.equipo AND detalles_equipo2.cliente = IBMWebSphere.cliente AND detalles_equipo2.empleado = IBMWebSphere.empleado AND IBMWebSphere.familia = 'IBM WebSphere'
                 WHERE detalles_equipo2.cliente=:cliente AND detalles_equipo2.empleado = :empleado AND rango NOT IN (1) AND detalles_equipo2.familia = :os
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute(array(':cliente'=>$cliente, ':cliente1'=>$cliente, ':empleado'=>$empleado, ':os'=>$os));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function listar_optimizacionAsignacion($cliente, $os, $asignacion, $asignaciones) {
        try {
            $array = array(':cliente'=>$cliente, ':os'=>$os);
            if($asignacion != ""){
                $array[':asignacion'] = $asignacion;
                $where = " AND detalles_equipo2.asignacion = :asignacion ";
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(IBMDSStorage.edicion) AS IBMDSStorage,
                        GROUP_CONCAT(IBMHigh.edicion) AS IBMHigh,
                        GROUP_CONCAT(IBMInformix.edicion) AS IBMInformix,
                        GROUP_CONCAT(IBMSPSS.edicion) AS IBMSPSS,
                        GROUP_CONCAT(IBMIntegration.edicion) AS IBMIntegration,
                        GROUP_CONCAT(IBMSecurityAppScan.edicion) AS IBMSecurityAppScan,
                        GROUP_CONCAT(IBMSterlingCertificate.edicion) AS IBMSterlingCertificate,
                        GROUP_CONCAT(IBMSterlingConnect.edicion) AS IBMSterlingConnect,
                        GROUP_CONCAT(IBMSterlingExternal.edicion) AS IBMSterlingExternal,
                        GROUP_CONCAT(IBMSterlingSecureProxy.edicion) AS IBMSterlingSecureProxy,
                        GROUP_CONCAT(IBMTivoliStorage.edicion) AS IBMTivoliStorage,
                        GROUP_CONCAT(IBMWebservice.edicion) AS IBMWebservice,
                        GROUP_CONCAT(IBMWebSphere.edicion) AS IBMWebSphere,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 '" . $this->lg0000 . "'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 '" . $this->lg0001 . "'
                            ELSE
                                 '" . $this->lg0002 . "'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_ibm AS detEquipo WHERE detEquipo.cliente = :cliente AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_ibm AS detalles_equipo2
                      LEFT JOIN resumen_ibm AS IBMDSStorage ON detalles_equipo2.equipo = IBMDSStorage.equipo AND detalles_equipo2.cliente = IBMDSStorage.cliente AND IBMDSStorage.familia = 'IBM DS Storage'
                      LEFT JOIN resumen_ibm AS IBMHigh ON detalles_equipo2.equipo = IBMHigh.equipo AND detalles_equipo2.cliente = IBMHigh.cliente AND IBMHigh.familia = 'IBM High'
                      LEFT JOIN resumen_ibm AS IBMInformix ON detalles_equipo2.equipo = IBMInformix.equipo AND detalles_equipo2.cliente = IBMInformix.cliente AND IBMInformix.familia = 'IBM Informix'
                      LEFT JOIN resumen_ibm AS IBMSPSS ON detalles_equipo2.equipo = IBMSPSS.equipo AND detalles_equipo2.cliente = IBMSPSS.cliente AND IBMSPSS.familia = 'IBM SPSS'
                      LEFT JOIN resumen_ibm AS IBMIntegration ON detalles_equipo2.equipo = IBMIntegration.equipo AND detalles_equipo2.cliente = IBMIntegration.cliente AND IBMIntegration.familia = 'IBM Integration'
                      LEFT JOIN resumen_ibm AS IBMSecurityAppScan ON detalles_equipo2.equipo = IBMSecurityAppScan.equipo AND detalles_equipo2.cliente = IBMSecurityAppScan.cliente AND IBMSecurityAppScan.familia = 'IBM Security AppScan'
                      LEFT JOIN resumen_ibm AS IBMSterlingCertificate ON detalles_equipo2.equipo = IBMSterlingCertificate.equipo AND detalles_equipo2.cliente = IBMSterlingCertificate.cliente AND IBMSterlingCertificate.familia = 'IBM Sterling Certificate'
                      LEFT JOIN resumen_ibm AS IBMSterlingConnect ON detalles_equipo2.equipo = IBMSterlingConnect.equipo AND detalles_equipo2.cliente = IBMSterlingConnect.cliente AND IBMSterlingConnect.familia = 'IBM Sterling Connect'
                      LEFT JOIN resumen_ibm AS IBMSterlingExternal ON detalles_equipo2.equipo = IBMSterlingExternal.equipo AND detalles_equipo2.cliente = IBMSterlingExternal.cliente AND IBMSterlingExternal.familia = 'IBM Sterling External'
                      LEFT JOIN resumen_ibm AS IBMSterlingSecureProxy ON detalles_equipo2.equipo = IBMSterlingSecureProxy.equipo AND detalles_equipo2.cliente = IBMSterlingSecureProxy.cliente AND IBMSterlingSecureProxy.familia = 'IBM Sterling Secure Proxy'
                      LEFT JOIN resumen_ibm AS IBMTivoliStorage ON detalles_equipo2.equipo = IBMTivoliStorage.equipo AND detalles_equipo2.cliente = IBMTivoliStorage.cliente AND IBMTivoliStorage.familia = 'IBM Tivoli Storage'
                      LEFT JOIN resumen_ibm AS IBMWebservice ON detalles_equipo2.equipo = IBMWebservice.equipo AND detalles_equipo2.cliente = IBMWebservice.cliente AND IBMWebservice.familia = 'IBM Web service'
                      LEFT JOIN resumen_ibm AS IBMWebSphere ON detalles_equipo2.equipo = IBMWebSphere.equipo AND detalles_equipo2.cliente = IBMWebSphere.cliente AND IBMWebSphere.familia = 'IBM WebSphere'
                 WHERE detalles_equipo2.cliente=:cliente AND rango NOT IN (1) AND detalles_equipo2.familia = :os " . $where . "
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    }
    
    /*function listar_optimizacionSam($cliente, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(IBMDSStorage.edicion) AS IBMDSStorage,
                        GROUP_CONCAT(IBMHigh.edicion) AS IBMHigh,
                        GROUP_CONCAT(IBMInformix.edicion) AS IBMInformix,
                        GROUP_CONCAT(IBMSPSS.edicion) AS IBMSPSS,
                        GROUP_CONCAT(IBMIntegration.edicion) AS IBMIntegration,
                        GROUP_CONCAT(IBMSecurityAppScan.edicion) AS IBMSecurityAppScan,
                        GROUP_CONCAT(IBMSterlingCertificate.edicion) AS IBMSterlingCertificate,
                        GROUP_CONCAT(IBMSterlingConnect.edicion) AS IBMSterlingConnect,
                        GROUP_CONCAT(IBMSterlingExternal.edicion) AS IBMSterlingExternal,
                        GROUP_CONCAT(IBMSterlingSecureProxy.edicion) AS IBMSterlingSecureProxy,
                        GROUP_CONCAT(IBMTivoliStorage.edicion) AS IBMTivoliStorage,
                        GROUP_CONCAT(IBMWebservice.edicion) AS IBMWebservice,
                        GROUP_CONCAT(IBMWebSphere.edicion) AS IBMWebSphere,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_ibmSam AS detEquipo WHERE detEquipo.cliente = :cliente1 AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_ibmSam AS detalles_equipo2
                      LEFT JOIN resumen_ibmSam AS IBMDSStorage ON detalles_equipo2.equipo = IBMDSStorage.equipo AND detalles_equipo2.cliente = IBMDSStorage.cliente AND IBMDSStorage.familia = 'IBM DS Storage'
                      LEFT JOIN resumen_ibmSam AS IBMHigh ON detalles_equipo2.equipo = IBMHigh.equipo AND detalles_equipo2.cliente = IBMHigh.cliente AND IBMHigh.familia = 'IBM High'
                      LEFT JOIN resumen_ibmSam AS IBMInformix ON detalles_equipo2.equipo = IBMInformix.equipo AND detalles_equipo2.cliente = IBMInformix.cliente AND IBMInformix.familia = 'IBM Informix'
                      LEFT JOIN resumen_ibmSam AS IBMSPSS ON detalles_equipo2.equipo = IBMSPSS.equipo AND detalles_equipo2.cliente = IBMSPSS.cliente AND IBMSPSS.familia = 'IBM SPSS'
                      LEFT JOIN resumen_ibmSam AS IBMIntegration ON detalles_equipo2.equipo = IBMIntegration.equipo AND detalles_equipo2.cliente = IBMIntegration.cliente AND IBMIntegration.familia = 'IBM Integration'
                      LEFT JOIN resumen_ibmSam AS IBMSecurityAppScan ON detalles_equipo2.equipo = IBMSecurityAppScan.equipo AND detalles_equipo2.cliente = IBMSecurityAppScan.cliente AND IBMSecurityAppScan.familia = 'IBM Security AppScan'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingCertificate ON detalles_equipo2.equipo = IBMSterlingCertificate.equipo AND detalles_equipo2.cliente = IBMSterlingCertificate.cliente AND IBMSterlingCertificate.familia = 'IBM Sterling Certificate'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingConnect ON detalles_equipo2.equipo = IBMSterlingConnect.equipo AND detalles_equipo2.cliente = IBMSterlingConnect.cliente AND IBMSterlingConnect.familia = 'IBM Sterling Connect'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingExternal ON detalles_equipo2.equipo = IBMSterlingExternal.equipo AND detalles_equipo2.cliente = IBMSterlingExternal.cliente AND IBMSterlingExternal.familia = 'IBM Sterling External'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingSecureProxy ON detalles_equipo2.equipo = IBMSterlingSecureProxy.equipo AND detalles_equipo2.cliente = IBMSterlingSecureProxy.cliente AND IBMSterlingSecureProxy.familia = 'IBM Sterling Secure Proxy'
                      LEFT JOIN resumen_ibmSam AS IBMTivoliStorage ON detalles_equipo2.equipo = IBMTivoliStorage.equipo AND detalles_equipo2.cliente = IBMTivoliStorage.cliente AND IBMTivoliStorage.familia = 'IBM Tivoli Storage'
                      LEFT JOIN resumen_ibmSam AS IBMWebservice ON detalles_equipo2.equipo = IBMWebservice.equipo AND detalles_equipo2.cliente = IBMWebservice.cliente AND IBMWebservice.familia = 'IBM Web service'
                      LEFT JOIN resumen_ibmSam AS IBMWebSphere ON detalles_equipo2.equipo = IBMWebSphere.equipo AND detalles_equipo2.cliente = IBMWebSphere.cliente AND IBMWebSphere.familia = 'IBM WebSphere'
                 WHERE detalles_equipo2.cliente=:cliente AND rango NOT IN (1) AND detalles_equipo2.familia = :os
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute(array(':cliente'=>$cliente, ':cliente1'=>$cliente, ':os'=>$os));
            if($os == "Windows"){
                $this->listaCliente = $sql->fetchAll();
            }
            else{
                $this->listaServidor = $sql->fetchAll();
            }
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }*/
    
    function listar_optimizacionSamArchivo($archivo, $os) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo2.id,
                        `detalles_equipo2`.equipo,
                        GROUP_CONCAT(IBMDSStorage.edicion) AS IBMDSStorage,
                        GROUP_CONCAT(IBMHigh.edicion) AS IBMHigh,
                        GROUP_CONCAT(IBMInformix.edicion) AS IBMInformix,
                        GROUP_CONCAT(IBMSPSS.edicion) AS IBMSPSS,
                        GROUP_CONCAT(IBMIntegration.edicion) AS IBMIntegration,
                        GROUP_CONCAT(IBMSecurityAppScan.edicion) AS IBMSecurityAppScan,
                        GROUP_CONCAT(IBMSterlingCertificate.edicion) AS IBMSterlingCertificate,
                        GROUP_CONCAT(IBMSterlingConnect.edicion) AS IBMSterlingConnect,
                        GROUP_CONCAT(IBMSterlingExternal.edicion) AS IBMSterlingExternal,
                        GROUP_CONCAT(IBMSterlingSecureProxy.edicion) AS IBMSterlingSecureProxy,
                        GROUP_CONCAT(IBMTivoliStorage.edicion) AS IBMTivoliStorage,
                        GROUP_CONCAT(IBMWebservice.edicion) AS IBMWebservice,
                        GROUP_CONCAT(IBMWebSphere.edicion) AS IBMWebSphere,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS usabilidad,
                        IF((SELECT COUNT(*) FROM detalles_equipo_ibmSam AS detEquipo WHERE detEquipo.archivo = :archivo AND detEquipo.rango NOT IN (1) AND detEquipo.equipo = detalles_equipo2.equipo) > 1, 'Duplicado', '') AS duplicado
                 FROM detalles_equipo_ibmSam AS detalles_equipo2
                      LEFT JOIN resumen_ibmSam AS IBMDSStorage ON detalles_equipo2.equipo = IBMDSStorage.equipo AND detalles_equipo2.cliente = IBMDSStorage.cliente AND IBMDSStorage.familia = 'IBM DS Storage'
                      LEFT JOIN resumen_ibmSam AS IBMHigh ON detalles_equipo2.equipo = IBMHigh.equipo AND detalles_equipo2.cliente = IBMHigh.cliente AND IBMHigh.familia = 'IBM High'
                      LEFT JOIN resumen_ibmSam AS IBMInformix ON detalles_equipo2.equipo = IBMInformix.equipo AND detalles_equipo2.cliente = IBMInformix.cliente AND IBMInformix.familia = 'IBM Informix'
                      LEFT JOIN resumen_ibmSam AS IBMSPSS ON detalles_equipo2.equipo = IBMSPSS.equipo AND detalles_equipo2.cliente = IBMSPSS.cliente AND IBMSPSS.familia = 'IBM SPSS'
                      LEFT JOIN resumen_ibmSam AS IBMIntegration ON detalles_equipo2.equipo = IBMIntegration.equipo AND detalles_equipo2.cliente = IBMIntegration.cliente AND IBMIntegration.familia = 'IBM Integration'
                      LEFT JOIN resumen_ibmSam AS IBMSecurityAppScan ON detalles_equipo2.equipo = IBMSecurityAppScan.equipo AND detalles_equipo2.cliente = IBMSecurityAppScan.cliente AND IBMSecurityAppScan.familia = 'IBM Security AppScan'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingCertificate ON detalles_equipo2.equipo = IBMSterlingCertificate.equipo AND detalles_equipo2.cliente = IBMSterlingCertificate.cliente AND IBMSterlingCertificate.familia = 'IBM Sterling Certificate'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingConnect ON detalles_equipo2.equipo = IBMSterlingConnect.equipo AND detalles_equipo2.cliente = IBMSterlingConnect.cliente AND IBMSterlingConnect.familia = 'IBM Sterling Connect'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingExternal ON detalles_equipo2.equipo = IBMSterlingExternal.equipo AND detalles_equipo2.cliente = IBMSterlingExternal.cliente AND IBMSterlingExternal.familia = 'IBM Sterling External'
                      LEFT JOIN resumen_ibmSam AS IBMSterlingSecureProxy ON detalles_equipo2.equipo = IBMSterlingSecureProxy.equipo AND detalles_equipo2.cliente = IBMSterlingSecureProxy.cliente AND IBMSterlingSecureProxy.familia = 'IBM Sterling Secure Proxy'
                      LEFT JOIN resumen_ibmSam AS IBMTivoliStorage ON detalles_equipo2.equipo = IBMTivoliStorage.equipo AND detalles_equipo2.cliente = IBMTivoliStorage.cliente AND IBMTivoliStorage.familia = 'IBM Tivoli Storage'
                      LEFT JOIN resumen_ibmSam AS IBMWebservice ON detalles_equipo2.equipo = IBMWebservice.equipo AND detalles_equipo2.cliente = IBMWebservice.cliente AND IBMWebservice.familia = 'IBM Web service'
                      LEFT JOIN resumen_ibmSam AS IBMWebSphere ON detalles_equipo2.equipo = IBMWebSphere.equipo AND detalles_equipo2.cliente = IBMWebSphere.cliente AND IBMWebSphere.familia = 'IBM WebSphere'
                 WHERE detalles_equipo2.archivo=:archivo AND rango NOT IN (1) AND detalles_equipo2.familia = :os
                 GROUP BY detalles_equipo2.id
                 ORDER BY detalles_equipo2.equipo");        
            $sql->execute(array(':archivo'=>$archivo, ':os'=>$os));
            if($os == "Windows"){
                $this->listaCliente = $sql->fetchAll();
            }
            else{
                $this->listaServidor = $sql->fetchAll();
            }
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    /*function fechaSAM($cliente) {
        $this->conexion();
        $fecha = "";
        $query = "SELECT fecha FROM encGrafIbmSam WHERE cliente = :cliente";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }*/
    
    function fechaSAMArchivo($archivo) {
        $this->conexion();
        $fecha = "";
        $query = "SELECT fecha FROM encGrafIbmSam WHERE archivo = :archivo";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo));
            $row = $sql->fetch();
            return $row["fecha"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return $fecha;
        }
    }
    
    function cantidadProductosOSUso($cliente, $empleado, $familia){
        $query = "SELECT COUNT(id) AS cantidad
            FROM detalles_equipo_ibm 
            WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%'));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function cantidadProductosInstalados($cliente, $empleado, $familia){
        try{
            $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = :familia
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.edicion != ''";
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function cantidadProductosOtrosInstalados($cliente, $empleado, $familia){
        $where = "";
        for($i = 0; $i < count($familia); $i++){
            $where .= " AND office.familia != '" . $familia[$i] . "' ";
        }
        $query = "SELECT COUNT(office.id) AS cantidad
            FROM detalles_equipo_ibm
                LEFT JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado " . $where . " 
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.edicion != ''";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cantidad'=>0);
        }
    }
	
    function productosUsoCliente($cliente, $empleado){
        $query = "SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM DS Storage'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM High'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Informix'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version

            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM SPSS'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Integration'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Security AppScan'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Sterling Certificate'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Sterling Connect'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Sterling External'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Sterling Secure Proxy'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Tivoli Storage'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM Web service'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            
            UNION 
            
            SELECT office.familia AS familia,
                office.edicion AS edicion,
                office.version AS version,
                COUNT(office.familia) AS cantidad
            FROM detalles_equipo_ibm
                INNER JOIN resumen_ibm AS office ON detalles_equipo_ibm.equipo = office.equipo AND detalles_equipo_ibm.cliente = office.cliente AND detalles_equipo_ibm.empleado = office.empleado AND office.familia = 'IBM WebSphere'
            WHERE detalles_equipo_ibm.cliente = :cliente AND detalles_equipo_ibm.empleado = :empleado AND office.familia != ''
            GROUP BY office.familia, office.edicion, office.version
            ";
        try{
            $this->conexion();
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_equipos($cliente){
        try{
            $this->conexion();
            
            $sql = $this->conn->prepare("SELECT equipo
                FROM detalles_equipo_ibm
                WHERE cliente = :cliente
                ORDER BY equipo");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}