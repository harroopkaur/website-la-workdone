<?php

namespace App\Clases;

class Compras_SPLA extends General{
    ########################################  Atributos  ########################################
    var $id;
    var $cliente;
    var $officce;
    var $instalaciones;
    var $compra;
    var $balance;
    var $tipo;
    var $precio;
    var $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $familia, $edicion, $version, $compra, $precio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO compras_SPLA (cliente, empleado, familia, edicion, version, compra, precio) '
            . 'VALUES (:cliente, :empleado, :familia, :edicion, :version, :compra, :precio)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, 
            ':compra'=>$compra, ':precio'=>$precio));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM compras_SPLA WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    /*function datos($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_SPLA WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id      = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            $this->compra  = $usuario['compra'];
            $this->precio  = $usuario['precio'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    function datos3($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_SPLA WHERE familia = :familia AND edicion = :edicion AND '
            . 'version = :version AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $usuario = $sql->fetch();
            
            $this->id      = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            if($usuario['compra'] == ""){
                $this->compra  = 0;
            }
            else{
                $this->compra  = $usuario['compra'];
            }
            $this->precio = $usuario['precio'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function datos2($cliente, $empleado, $familia, $edicion) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_SPLA WHERE familia = :familia AND edicion = :edicion AND cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $usuario = $sql->fetch();
            
            $this->id      = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            $this->compra  = $usuario['compra'];
            $this->precio  = $usuario['precio'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_SPLA WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Contar el total de Usuarios
    function total($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM compras_SPLA WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    // Verificar si e-mail ya existe
    /*function cliente_existe($cliente, $id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM compras_SPLA WHERE cliente = :cliente AND id != :id');
            $sql->execute(array(':cliente'=>$cliente, ':id'=>$id));
            $row = $sql->fetch();
            if(count($row["cantidad"]) > 0){
                return true;
            }
            else{
                return false;
            }
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf4/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }

    function buscarPrecioRepo($cliente, $empleado, $fabricante, $familia, $edicion, $version) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT  detalleContrato.precio 
                FROM detalleContrato 
                     INNER JOIN contrato ON detalleContrato.`idContrato` = contrato.`idContrato`
                         INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                         INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                         INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE contrato.idCliente = :cliente AND contrato.empleado AND contrato.idFabricante = :fabricante AND productos.`nombre` LIKE :familia
                AND ediciones.nombre LIKE :edicion AND versiones.nombre LIKE :version
                GROUP BY productos.nombre, ediciones.nombre, versiones.nombre');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':fabricante'=>$fabricante, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':version'=>'%' . $version . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}