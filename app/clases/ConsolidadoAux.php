<?php

namespace App\Clases;

class ConsolidadoAux extends General{
    ########################################  Atributos  ########################################
    var $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $registro, $editor, $version, $fecha_instalacion, $sofware) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_officesAux (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES (:cliente, :empleado, :dato_control,:host_name, :registro, :editor, :version, :fecha_instalacion, :sofware)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control, ':host_name'=>$host_name, ':registro'=>$registro, 
            ':editor'=>$editor, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion, ':sofware'=>$sofware));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_officesAux (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_officesAux WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listarProductosMicrosoft($cliente, $empleado, $in = null, $out = null) {        
        try{
            $this->conexion();
            
            $agregar = "";
            if($in != null){
                $inAux = explode(",", $in);
            }
            
            if(trim($inAux[0]) == "Office" || trim($inAux[0]) == "Visio" || trim($inAux[0]) == "Visual" || trim($inAux[0]) == "Microsoft Exchange"
            || trim($inAux[0]) == "SharePoint" || trim($inAux[0]) == "SQL"){
                $agregar .= " AND sofware LIKE '%" . trim($inAux[0]) . "%' AND (";
            
                for($i = 1; $i < count($inAux); $i++){
                    if($i > 1){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            } else{
                $agregar .= " AND (";
            
                for($i = 0; $i < count($inAux); $i++){
                    if($i > 0){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            }
                
            $quitar = "";
            if($out != null){
                $outAux = explode(",", $out);
            }
            for($i = 0; $i < count($outAux); $i++){
                if($i > 0){
                    $quitar .= " OR ";
                }
                
                $quitar .= "sofware LIKE '%" . trim($outAux[$i]) . "%'"; 
            }
            
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				FROM consolidado_officesAux
				WHERE cliente = :cliente AND empleado = :empleado " . $agregar . "
				ORDER BY fecha_instalacion DESC) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (" . $quitar . ")
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listarProductosOffice($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				FROM consolidado_officesAux
				WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE '%Office%' AND (sofware LIKE '%Office Small Business%' OR
				sofware LIKE '%Standard%' OR sofware LIKE '%Professional%' OR sofware LIKE '%Profesional%' OR
				sofware LIKE '%Home%' OR sofware LIKE '%Hogar%' OR sofware LIKE '%Enterprise%' OR sofware LIKE '%Office 365%' 
                                OR sofware LIKE '%Office Starter%' OR sofware LIKE '%Office Basic%')
				ORDER BY fecha_instalacion DESC) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = ':cliente' AND empleado = :empleado AND (sofware LIKE '%Visio%' OR sofware LIKE '%Project%')
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosProject($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				FROM consolidado_officesAux
				WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE 'Microsoft Project 32-Bit Edition%' OR
				sofware LIKE 'Microsoft Project 32-Bit Edition%' OR sofware LIKE 'Microsoft Office Project Professional%' OR
				sofware LIKE 'Microsoft Project Professional%' OR sofware LIKE 'Microsoft Project Server%' OR
				sofware LIKE 'Microsoft Project Web Access%' OR /*sofware LIKE 'Microsoft Project Standard%' OR*/
				sofware LIKE 'Microsoft Office Project Standard%' OR sofware LIKE 'Microsoft Project Developer%') ORDER BY fecha_instalacion DESC) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%Language%' OR sofware LIKE '%Update%')
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosVisio($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				 FROM consolidado_officesAux
				 WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE '%Visio%' AND (sofware LIKE '%Visio Office 365%' OR
				 sofware LIKE '%Visio Pro Office 365%' OR sofware LIKE '%Standard%' OR sofware LIKE '%Professional%' OR
				 sofware LIKE '%Profesional%' OR sofware LIKE '%Enterprise%' OR sofware LIKE '%Visio Premium%' OR
				 sofware LIKE '%Office Visio%' OR sofware LIKE '%Visio v6.0 Technical%') ORDER BY fecha_instalacion DESC
			) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%MUI%' OR sofware LIKE '%Update%'
				  OR sofware LIKE '%Service Pack%' OR sofware LIKE '%Viewer%' OR sofware LIKE '%Instalación%')
                        )");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosVisualStudio($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				 FROM consolidado_officesAux
				 WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE '%Visual%' AND (sofware LIKE '%Visual Studio Team System%' OR
				 sofware LIKE '%Premier Partner Edition%' OR
				 sofware LIKE '%Visual Studio Express%' OR
				 sofware LIKE '%Visual Studio Premium%' OR
				 sofware LIKE '%Visual Studio Ultimate%' OR
				 sofware LIKE '%Standard%' OR
				 sofware LIKE '%Professional%' OR
				 sofware LIKE '%Profesional%' OR
				 sofware LIKE '%Enterprise%' OR
                 sofware LIKE '%Studio Office Developer Tools%' OR
                 sofware LIKE '%Office Visio%' OR
                 sofware LIKE '%Visio v6.0 Technical%')
				 ORDER BY fecha_instalacion DESC
			) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%para%' OR sofware LIKE '%for%'
				  OR sofware LIKE '%Service Pack%' OR sofware LIKE '%C++%' OR sofware LIKE '%enu%'
                  OR sofware LIKE '%Ultimate%' OR sofware LIKE '%Visual C++ Professional Windows 8.1%'
                  OR sofware LIKE '%Visual C++ IDE Professional PlusPackage%' OR sofware LIKE '%SAP%' OR sofware LIKE '%Visual Basic%')
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosExchangeServer($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				 FROM consolidado_officesAux
				 WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE '%Microsoft Exchange%' AND (sofware LIKE 'Microsoft Exchange Server%' OR
				 sofware LIKE 'Microsoft Exchange Standard%' OR
				 sofware LIKE 'Microsoft Exchange Enterprise%' OR
				 sofware LIKE 'Microsoft Exchange Server Enterprise%' OR
				 sofware LIKE 'Microsoft Exchange Server Standard%' OR
				 sofware LIKE '%Profesional%' OR
				 sofware LIKE 'Microsoft Exchange Server Platinum%' OR
				 sofware LIKE '%Web Services Managed%')
				 ORDER BY fecha_instalacion DESC
			) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%Pack%' OR sofware LIKE '%Update%'
				  OR sofware LIKE '%Web Services%')
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosSharepoint($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				 FROM consolidado_officesAux
				 WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE '%SharePoint%' AND (sofware LIKE '%Portal Server%' OR
				 sofware LIKE '%Designer%' OR
				 sofware LIKE '%Server%' OR
				 sofware LIKE '%Team Services%' OR
				 sofware LIKE '%SharePoint Services%' OR
				 sofware LIKE '%Enterprise%' OR
				 sofware LIKE '%Foundation%' OR
				 sofware LIKE '%MOSS Enterprise%' OR
				 sofware LIKE '%MOSS Standard%' OR
				 sofware LIKE '%Online Enterprise%' OR
				 sofware LIKE '%Standard Edition%')
				 ORDER BY fecha_instalacion DESC
			) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente empleado = :empleado AND sofware LIKE '%Pack%' OR sofware LIKE '%Language%'
				  OR sofware LIKE '%Update%' OR sofware LIKE '%xtensions%' OR sofware LIKE '%Extensiones%' OR sofware LIKE '%Lang Pack%' OR sofware LIKE '%Hotfix%' OR sofware LIKE '%Services%' OR sofware LIKE '%1033%' OR sofware LIKE '%3082%' OR sofware LIKE '%SQL%' OR sofware LIKE '%MUI%' OR sofware LIKE '%Office Server%' 
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosLync($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				 FROM consolidado_officesAux
				 WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%Lync%' OR sofware LIKE '%Front End Server%') AND (sofware LIKE '%Skype for Business Server%' OR sofware LIKE '%Office Communications Server%' OR sofware LIKE '%Live Communications Server%' OR sofware LIKE '%Lync Server%')
				 ORDER BY fecha_instalacion DESC
			) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%QuestMessageStats%' OR sofware LIKE '%Bootstrapper%' OR sofware LIKE '%Tool%' OR sofware LIKE '%Conferencing%' OR sofware LIKE '%Web%' OR sofware LIKE '%Bandwidth%' OR sofware LIKE '%Tools%' OR sofware LIKE '%Core Components%' OR sofware LIKE '%Mediation%' OR sofware LIKE '%Management%' OR sofware LIKE '%Application Host%' OR sofware LIKE '%Service%' OR sofware LIKE '%Reach%' OR sofware LIKE '%Backwards%'))");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosSystemCenter($cliente, $empleado){ 
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
				 FROM consolidado_officesAux
				 WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE '%System Center%'
				 ORDER BY fecha_instalacion DESC
			) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%Virtual%' OR sofware LIKE '%Agent%' OR sofware LIKE '%Setup%' OR sofware LIKE '%Pack%')
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listarProductosSQL($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
			FROM (SELECT *
			FROM consolidado_officesAux
			WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE '%SQL%' AND (sofware LIKE 'Microsoft SQL Server%' OR sofware LIKE '%Azure%' OR sofware = 'Microsoft SQL Server 2005 Express Edition')
			ORDER BY fecha_instalacion DESC
			) AS tabla
			WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_officesAux
				  WHERE cliente = :cliente AND empleado = :empleado AND (sofware LIKE '%Search%' OR sofware LIKE '%Tools%' OR sofware LIKE '%Language%' OR sofware LIKE '%Management%' OR sofware LIKE '%ScriptDom%' OR sofware LIKE '%Setup%' OR sofware LIKE '%Policies%' OR sofware LIKE '%Transact-SQL%' OR sofware LIKE '%RsFx Driver%' OR sofware LIKE '%Framework%' OR sofware LIKE '%Native%' OR sofware LIKE '%Command%' OR sofware LIKE '%LocalDB%' OR sofware LIKE '%Engine%' OR sofware LIKE '%Common%' OR sofware LIKE '%Browser%' OR sofware LIKE '%Books%' OR sofware LIKE '%Analysis%' OR sofware LIKE '%Compact%' OR sofware LIKE '%Backward%' OR sofware LIKE '%Services%' OR sofware LIKE '%NETMONITOR%' OR sofware LIKE '%SPOINT%' OR sofware LIKE '%System%' OR sofware LIKE '%VSS Writer%' OR sofware LIKE '%PowerPivot%' OR sofware LIKE '%Express Edition (MICROSO%' OR sofware LIKE '%Express Edition (ACRONIS%' OR sofware LIKE '%Express Edition (WHATSUP%' OR sofware LIKE '%SharePoint%') 
			)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}