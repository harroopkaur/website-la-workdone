<?php

namespace App\Clases;

class cabTrasladoLicencia extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;

    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $descripcion, $observacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO trasladoCabLicencias (cliente, empleado, fecha, descripcion, observacion) '
            . 'VALUES (:cliente, :empleado, CURDATE(), :descripcion, :observacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':descripcion'=>$descripcion, ':observacion'=>$observacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function actualizar($id, $descripcion, $observacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE trasladoCabLicencias SET descripcion = :descripcion, observacion = :observacion WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':descripcion'=>$descripcion, ':observacion'=>$observacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function actualizarEstado($id, $estado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE trasladoCabLicencias SET estado = :estado WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':estado'=>$estado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function obtenerUltId() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(MAX(id), 0) AS id FROM trasladoCabLicencias');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function historialPaginado($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                    DATE_FORMAT(fecha, "%d/%m/%Y") AS fecha,
                    descripcion,
                    observacion,
                    IF(estado = 0, "Eliminado", IF(estado = 1, "Por Aprobar", "Aprobado")) AS estado
                FROM trasladoCabLicencias
                WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function historialPaginadoPorAprobar($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                    DATE_FORMAT(fecha, "%d/%m/%Y") AS fecha,
                    descripcion,
                    observacion,
                    IF(estado = 0, "Eliminado", IF(estado = 1, "Por Aprobar", "Aprobado")) AS estado
                FROM trasladoCabLicencias
                WHERE cliente = :cliente AND estado = 1');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    
    function dataTraslado($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id,
                    DATE_FORMAT(fecha, "%d/%m/%Y") AS fecha,
                    descripcion,
                    observacion
                FROM trasladoCabLicencias
                WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('id'=>'', 'fecha'=>'00/00/0000', 'descripcion'=>'', 'observacion'=>'');
        }
    }
}