<?php

namespace App\Clases;

class desarrolloPruebaWindows extends General{
    ########################################  Atributos  ########################################
    public $error;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $tipo, $familia, $edicion, $version, $fechaInstalacion, 
    $usuario = null, $equipoUsuario = null) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO desarrolloPruebaWindows (cliente, empleado, equipo, tipo, "
            . "familia, edicion, version, fechaInstalacion, usuario, equipoUsuario) VALUES (:cliente, :empleado, "
            . ":equipo, :tipo, :familia, :edicion, :version, :fechaInstalacion, :usuario, :equipoUsuario)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':tipo'=>$tipo, 
            ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':fechaInstalacion'=>$fechaInstalacion, 
            ':usuario'=>$usuario, ':equipoUsuario'=>$equipoUsuario));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $usuario, $equipoUsuario){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE desarrolloPruebaWindows SET usuario = :usuario, equipoUsuario = :equipoUsuario "
            . "WHERE id = :id");
            $sql->execute(array(':id'=>$id, ':usuario'=>$usuario, ':equipoUsuario'=>$equipoUsuario));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM desarrolloPruebaWindows WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarId($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM desarrolloPruebaWindows WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existeDesarrolloPruebaWindows($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad "
            . "FROM desarrolloPruebaWindows WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listadoDesarrolloPrueba($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM desarrolloPruebaWindows WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function desarrolloPruebaEspecifica($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM desarrolloPruebaWindows WHERE id = :id");
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('cliente'=>'', 'empleado'=>'', 'equipo'=>'', 'tipo'=>'', 'familia'=>'', 'edicion'=>'',
            'version'=>'', 'fechaInstalacion'=>'', 'usuario'=>'', 'equipoUsuario'=>'');
        }
    }
    
    function obtenerUltId(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT MAX(id) AS id "
            . "FROM desarrolloPruebaWindows");
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    //inicio reporte SAM
    function listadoDesarrolloPruebaSam($archivo){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
            . "FROM desarrolloPruebaWindowsSam WHERE archivo = :archivo");
            $sql->execute(array(':archivo'=>$archivo));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    //fin reporte SAM
}