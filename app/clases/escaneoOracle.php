<?php

namespace App\Clases;

class escaneoOracle extends General{
    ########################################  Atributos  ########################################
    public  $error;
    public  $listado;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $errors) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO escaneo_equipos_oracle (cliente, empleado, equipo, status, errors) VALUES '
            . '(:cliente, :empleado, :equipo, null, :errors)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':errors'=>$errors));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM escaneo_equipos_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo2($cliente, $empleado) {        
        try {
            $this->conexion();
            $query = "SELECT id,
                    cliente,
                    empleado,
                    equipo,
                    status,
                    errors
                FROM escaneo_equipos_oracle
                WHERE cliente = :cliente AND empleado = :empleado AND errors = 'Ninguno'
                GROUP BY equipo

                UNION

                SELECT id,
                    cliente,
                    empleado,
                    equipo,
                    status,
                    errors
                FROM escaneo_equipos_oracle
                WHERE cliente = :cliente AND empleado = :empleado AND errors != '' AND NOT errors IS NULL
                AND equipo NOT IN (SELECT equipo FROM escaneo_equipos_oracle WHERE cliente = :cliente AND empleado = :empleado AND errors = 'Ninguno' GROUP BY equipo)
                GROUP BY equipo";
            //$query = 'SELECT * FROM escaneo_equipos_oracle WHERE cliente = :cliente AND empleado = :empleado GROUP BY equipo';
            $sql = $this->conn->prepare($query);
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = "archivos_csvf3/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}