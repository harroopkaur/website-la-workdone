<?php

namespace App\Clases;

class Productos extends General{
    function getProductoNombre($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
                FROM productos
                WHERE nombre = :nombre');
            $sql->execute(array('nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
}