<?php

namespace App\Clases;

class certificaciones extends General{
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $descripcion, $ruta) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO certificaciones (cliente, descripcion, ruta, fecha) VALUES '
            . '(:cliente, :descripcion, :ruta, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':descripcion'=>$descripcion, ':ruta'=>$ruta));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE certificaciones SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function certificadosCliente($cliente, $inicio) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
            . 'FROM certificaciones '
            . 'WHERE cliente = :cliente AND estado = 1 '
            . 'ORDER BY fecha DESC '
            . 'LIMIT ' . $inicio . ", " . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
            . 'FROM certificaciones '
            . 'WHERE cliente = :cliente AND estado = 1');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
}