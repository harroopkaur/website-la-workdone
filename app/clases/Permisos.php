<?php

namespace App\Clases;

class Permisos extends General{
    public  $lista;
    public  $result;
    public  $permisoEspecifico;
    public  $error;
    
    function insertar($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO permisosGenerales (nombre) VALUES '
            . '(:nombre)');
            $sql->execute(array(':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    //verify module
    function existe_permiso($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM permisosGenerales WHERE nombre = :nombre');
            $sql->execute(array(':nombre'=>$nombre));
            $this->result = $sql->fetch(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // delete
    function eliminar($idPermisos) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE permisosGenerales SET status = 0 WHERE idPermisos = :idPermisos');
            $sql->execute(array(':idPermisos'=>$idPermisos));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function activar($idPermisos) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE permisosGenerales SET status = 1 WHERE idPermisos = :idPermisos');
            $sql->execute(array(':idPermisos'=>$idPermisos));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function modificar($idPermisos, $nombre, $status) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE permisosGenerales SET nombre = :nombre, status = :status WHERE idPermisos = :idPermisos');
            $sql->execute(array(':idPermisos'=>$idPermisos, ':nombre'=>$nombre, ':status'=>$status));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listado(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM permisosGenerales '
                . 'WHERE status = 1');
            $sql->execute();
            $this->lista = $sql->fetchAll(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_todo_paginado($nombre, $inicio){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idPermisos, '
                    . 'nombre, '
                    . 'IF(status = 1, "Activo", "Inactivo") status '
                . 'FROM permisosGenerales '
                . 'WHERE nombre LIKE :nombre '
                . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':nombre'=>'%' . $nombre . '%'));
            $this->lista = $sql->fetchAll(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function total($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad FROM permisosGenerales WHERE nombre like :nombre');
            $sql->execute(array(':nombre'=>'%' . $nombre . '%'));
            $this->result = $sql->fetch(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function permisosEspecifico($idPermisos){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM permisosGenerales WHERE idPermisos = :idPermisos');
            $sql->execute(array(':idPermisos'=>$idPermisos));
            $this->permisoEspecifico = $sql->fetch(); 
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}