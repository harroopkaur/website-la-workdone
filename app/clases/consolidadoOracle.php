<?php

namespace App\Clases;

class consolidadoOracle extends General{
    public  $lista        = array();
    public  $listaEdicion = array();
    public  $listaVersion = array();
    public  $edicion      = "";
    public  $version      = "";
    public  $error        = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $host_name, $product, $type, $description, $file) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_oracle(cliente, empleado, host_name, product, type, description, file) VALUES '
            . '(:cliente, :empleado, :host_name, :product, :type, :description, :file)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':host_name'=>$host_name, ':product'=>$product, ':type'=>$type, ':description'=>$description, ':file'=>$file));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    function insertar2($cliente, $empleado, $dato_control, $host_name, $registro, $editor, $version, $fecha_instalacion, $software) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_oracle2(cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, software) VALUES '
            . '(:cliente, :empleado, :dato_control, :host_name, :registro, :editor, :version, :fecha_instalacion, :software)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control,':host_name'=>$host_name, ':registro'=>$registro, ':editor'=>$editor, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion, ':software'=>$software));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    function eliminar2($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_oracle2 WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductos($cliente, $empleado, $software) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_oracle
            WHERE cliente = :cliente AND empleado = :empleado AND product LIKE :product
            GROUP BY cliente, empleado, host_name, product
            ORDER BY host_name DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':product'=>"%" . $software . "%"));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    function listarProductos2($cliente, $empleado, $software) {
       
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_oracle2
            WHERE cliente = :cliente AND empleado = :empleado AND software LIKE :software
            GROUP BY cliente, empleado, host_name, software
            ORDER BY host_name DESC');
            //dd(   $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':software'=>"%" . $software . "%")));
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':software'=>"%" . $software . "%"));
            $this->lista = $sql->fetchAll();            
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}
?>