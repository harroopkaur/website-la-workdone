<?php

namespace App\Clases;

class pvuClienteAIX extends General {
    public $error;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $tipoProcesador, $producto, $conteoProcesadores, $conteoCore, 
    $usaProcesadores, $usaCore, $pvu, $conteo, $totalPvu) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO pvuClienteAIX (cliente, empleado, servidor, tipoProcesador, "
            . "producto, conteoProcesadores, conteoCore, usaProcesadores, usaCore, pvu, conteo, totalPvu) "
            . "VALUES (:cliente, :empleado, :equipo, :tipoProcesador, :producto, :conteoProcesadores, :conteoCore, :usaProcesadores, "
            . ":usaCore, :pvu, :conteo, :totalPvu)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':tipoProcesador'=>$tipoProcesador, 
            ':producto'=>$producto, ':conteoProcesadores'=>$conteoProcesadores, ':conteoCore'=>$conteoCore, ':usaProcesadores'=>$usaProcesadores, 
            ':usaCore'=>$usaCore, ':pvu'=>$pvu, ':conteo'=>$conteo, ':totalPvu'=>$totalPvu));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO pvuClienteAIX (cliente, empleado, servidor, tipoProcesador, "
            . "producto, conteoProcesadores, conteoCore, usaProcesadores, usaCore, pvu, conteo, totalPvu) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente,$empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM pvuClienteAIX WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $usaProcesadores, $usaCore) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE pvuClienteAIX SET usaProcesadores = :usaProcesadores, usaCore = :usaCore "
            . "WHERE id = :id");
            $sql->execute(array(':id'=>$id,':usaProcesadores'=>$usaProcesadores, ':usaCore'=>$usaCore));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function procesarConsolidadoProcesadores($procesador){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT procesador.descripcion AS procesador,
                    modelo.descripcion AS modelo,
                    tablaPVU.pvu
                FROM tablaPVU
                    INNER JOIN detalleMaestra procesador ON tablaPVU.idProcesador = procesador.idDetalle AND
                    tablaPVU.idMaestraProcesador = procesador.idMaestra AND procesador.idMaestra = 8
                    INNER JOIN detalleMaestra modelo ON tablaPVU.idModeloServidor = modelo.idDetalle AND
                    tablaPVU.idMaestraModelo = modelo.idMaestra AND modelo.idMaestra = 9
                WHERE :procesador LIKE CONCAT('%', procesador.descripcion ,'%')
                AND (:procesador LIKE CONCAT('%', modelo.descripcion ,'%') OR modelo.descripcion = 'todos')");
            $sql->execute(array(':procesador'=>$procesador));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('procesador'=>'', 'modelo'=>'', 'pvu'=>'');
        }
    }
    
    function listadoPVUCliente($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
                . "FROM pvuClienteAIX "
                . "WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoProcesadoresUnixAIX($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo_UnixAIX.cliente,
                    detalles_equipo_UnixAIX.empleado,
                    detalles_equipo_UnixAIX.equipo,
                    detalles_equipo_UnixAIX.versionCpu AS tipo_CPU,
                    CONCAT(IFNULL(resumen_UnixAIX.familia, ''), ' ', IFNULL(resumen_UnixAIX.edicion, ''), ' ', IFNULL(resumen_UnixAIX.version, '')) AS producto,
                    detalles_equipo_UnixAIX.cpu,
                    detalles_equipo_UnixAIX.core
                FROM detalles_equipo_UnixAIX
                     INNER JOIN resumen_UnixAIX ON detalles_equipo_UnixAIX.cliente = resumen_UnixAIX.cliente AND
                     detalles_equipo_UnixAIX.empleado = resumen_UnixAIX.empleado AND detalles_equipo_UnixAIX.equipo = resumen_UnixAIX.equipo
                WHERE detalles_equipo_UnixAIX.cliente = :cliente AND detalles_equipo_UnixAIX.empleado = :empleado AND detalles_equipo_UnixAIX.cpu > 0
                GROUP BY detalles_equipo_UnixAIX.equipo, detalles_equipo_UnixAIX.cpu, detalles_equipo_UnixAIX.core");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}