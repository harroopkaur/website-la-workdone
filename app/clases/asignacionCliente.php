<?php

namespace App\Clases;

class asignacionCliente extends General{
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($asignacion, $cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO asigCliente (idAsignacion, cliente) VALUES (:asignacion, :cliente)');
            $sql->execute(array(':asignacion'=>$asignacion, ':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function actualizar($asignacion, $cliente, $estado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE asigCliente SET estado = :estado WHERE idAsignacion = :asignacion AND cliente = :cliente');
            $sql->execute(array(':asignacion'=>$asignacion, ':cliente'=>$cliente, ':estado'=>$estado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function anularAsignaciones($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE asigCliente SET estado = 0 WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_asignacion($cliente) {
        try{
            $this->conexion();           
            $sql = $this->conn->prepare('SELECT asigCliente.idAsignacion,
                asignacion.nombre,
                asigCliente.estado
            FROM asigCliente
                INNER JOIN asignacion ON asigCliente.idAsignacion = asignacion.id AND asignacion.estado = 1
            WHERE cliente = :cliente AND asigCliente.estado = 1 
            ORDER BY asignacion.nombre');
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeAsignacion($asignacion, $cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM asigCliente
            WHERE idAsignacion = :asignacion AND cliente = :cliente');
            $sql->execute(array(':asignacion'=>$asignacion, ':cliente'=>$cliente));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
}