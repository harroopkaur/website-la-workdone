<?php

namespace App\Clases;

class usuarioEquipoAdobe extends General{
    ########################################  Atributos  ########################################
    public $error = NULL;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $dominio, $usuario, $ip) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usuarioEquipoAdobe (cliente, empleado, dato_control, host_name, dominio, usuario, ip) '
            . 'VALUES (:cliente, :empleado, :dato_control, :host_name, :dominio, :usuario, :ip)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control, ':host_name'=>$host_name, ':dominio'=>$dominio, ':usuario'=>$usuario, ':ip'=>$ip));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usuarioEquipoAdobe (cliente, empleado, dato_control, host_name, dominio, usuario, ip) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $centroCosto) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE usuarioEquipoAdobe SET centroCosto = :centroCosto WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':centroCosto'=>$centroCosto));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM usuarioEquipoAdobe WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarUsuarioEquipo($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla1.id,
                    tabla1.host_name,
                    tabla1.usuario,
                    tabla1.centroCosto,
                    SUM(IFNULL(compras2.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                FROM (SELECT resumen_office2.cliente,
                        resumen_office2.empleado,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM resumen_office2
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY resumen_office2.equipo
                    

                    /*SELECT resumen_office2.cliente,
                        resumen_office2.empleado,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM resumen_office2
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version

                    UNION

                    SELECT resumen_adobe.cliente,
                        resumen_adobe.empleado,
                        resumen_adobe.equipo,
                        resumen_adobe.familia,
                        resumen_adobe.edicion,
                        resumen_adobe.version
                    FROM resumen_adobe
                    WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado
                    GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version

                    UNION

                    SELECT resumen_ibm.cliente,
                        resumen_ibm.empleado,
                        resumen_ibm.equipo,
                        resumen_ibm.familia,
                        resumen_ibm.edicion,
                        resumen_ibm.version
                    FROM resumen_ibm
                    WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado
                    GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version

                    UNION

                    SELECT resumen_oracle.cliente,
                        resumen_oracle.empleado,
                        resumen_oracle.equipo,
                        resumen_oracle.familia,
                        resumen_oracle.edicion,
                        resumen_oracle.version
                    FROM resumen_oracle
                    WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.empleado = :empleado
                    GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version

                    UNION

                    SELECT resumen_UnixAIX.cliente,
                        resumen_UnixAIX.empleado,
                        resumen_UnixAIX.equipo,
                        resumen_UnixAIX.familia,
                        resumen_UnixAIX.edicion,
                        resumen_UnixAIX.version
                    FROM resumen_UnixAIX
                    WHERE resumen_UnixAIX.cliente = :cliente AND resumen_UnixAIX.empleado = :empleado
                    GROUP BY resumen_UnixAIX.equipo, resumen_UnixAIX.familia, resumen_UnixAIX.edicion, resumen_UnixAIX.version
                    
                    UNION

                    SELECT resumen_UnixLinux.cliente,
                        resumen_UnixLinux.empleado,
                        resumen_UnixLinux.equipo,
                        resumen_UnixLinux.familia,
                        resumen_UnixLinux.edicion,
                        resumen_UnixLinux.version
                    FROM resumen_UnixLinux
                    WHERE resumen_UnixLinux.cliente = :cliente AND resumen_UnixLinux.empleado = :empleado
                    GROUP BY resumen_UnixLinux.equipo, resumen_UnixLinux.familia, resumen_UnixLinux.edicion, resumen_UnixLinux.version

                    UNION

                    SELECT resumen_UnixSolaris.cliente,
                        resumen_UnixSolaris.empleado,
                        resumen_UnixSolaris.equipo,
                        resumen_UnixSolaris.familia,
                        resumen_UnixSolaris.edicion,
                        resumen_UnixSolaris.version
                    FROM resumen_UnixSolaris
                    WHERE resumen_UnixSolaris.cliente = :cliente AND resumen_UnixSolaris.empleado = :empleado
                    GROUP BY resumen_UnixSolaris.equipo, resumen_UnixSolaris.familia, resumen_UnixSolaris.edicion, resumen_UnixSolaris.version

                    UNION

                    SELECT resumen_OracleAIX.cliente,
                        resumen_OracleAIX.empleado,
                        resumen_OracleAIX.equipo,
                        resumen_OracleAIX.familia,
                        resumen_OracleAIX.edicion,
                        resumen_OracleAIX.version
                    FROM resumen_OracleAIX
                    WHERE resumen_OracleAIX.cliente = :cliente AND resumen_OracleAIX.empleado = :empleado
                    GROUP BY resumen_OracleAIX.equipo, resumen_OracleAIX.familia, resumen_OracleAIX.edicion, resumen_OracleAIX.version

                    UNION

                    SELECT resumen_OracleLinux.cliente,
                        resumen_OracleLinux.empleado,
                        resumen_OracleLinux.equipo,
                        resumen_OracleLinux.familia,
                        resumen_OracleLinux.edicion,
                        resumen_OracleLinux.version
                    FROM resumen_OracleLinux
                    WHERE resumen_OracleLinux.cliente = :cliente AND resumen_OracleLinux.empleado = :empleado
                    GROUP BY resumen_OracleLinux.equipo, resumen_OracleLinux.familia, resumen_OracleLinux.edicion, resumen_OracleLinux.version
                    
                    UNION

                    SELECT resumen_OracleSolaris.cliente,
                        resumen_OracleSolaris.empleado,
                        resumen_OracleSolaris.equipo,
                        resumen_OracleSolaris.familia,
                        resumen_OracleSolaris.edicion,
                        resumen_OracleSolaris.version
                    FROM resumen_OracleSolaris
                    WHERE resumen_OracleSolaris.cliente = :cliente AND resumen_OracleSolaris.empleado = :empleado
                    GROUP BY resumen_OracleSolaris.equipo, resumen_OracleSolaris.familia, resumen_OracleSolaris.edicion, resumen_OracleSolaris.version*/
                    
                    ) AS tabla
                    LEFT JOIN compras2 ON tabla.cliente = compras2.cliente AND tabla.empleado = compras2.empleado
                    AND tabla.familia = compras2.familia AND tabla.edicion = compras2.edicion AND tabla.version = compras2.version
                    INNER JOIN versiones ON tabla.version = versiones.nombre
                    INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                    INNER JOIN productos ON tabla.familia = productos.nombre
                    INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                    INNER JOIN (SELECT usuarioEquipoMicrosoft.id,
                                    usuarioEquipoMicrosoft.cliente,
                                    usuarioEquipoMicrosoft.empleado,
                                    usuarioEquipoMicrosoft.host_name,
                                    usuarioEquipoMicrosoft.usuario,
                                    usuarioEquipoMicrosoft.centroCosto
                                FROM usuarioEquipoMicrosoft
                                WHERE usuarioEquipoMicrosoft.cliente = :cliente AND usuarioEquipoMicrosoft.empleado = :empleado
                                GROUP BY usuarioEquipoMicrosoft.host_name, usuarioEquipoMicrosoft.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente AND tabla.empleado = tabla1.empleado
                GROUP BY tabla1.host_name, tabla1.usuario
                ORDER BY tabla1.host_name');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function UsuarioXEquipo($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT host_name, usuario, centroCosto
                FROM usuarioEquipoAdobe
                WHERE cliente = :cliente AND empleado = :empleado AND centroCosto != ""
                GROUP BY host_name, usuario
                ORDER BY centroCosto, host_name, usuario');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoCentroCostoAsignacion($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT tabla1.centroCosto,
                    SUM(IFNULL(compras2.precio, IFNULL(tabla_equivalencia.precio, 0))) AS costo
                FROM (SELECT resumen_office2.cliente,
                        resumen_office2.empleado,
                        resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM resumen_office2
                    WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                    ORDER BY resumen_office2.equipo) AS tabla
                    LEFT JOIN compras2 ON tabla.cliente = compras2.cliente AND tabla.empleado = compras2.empleado
                    AND tabla.familia = compras2.familia AND tabla.edicion = compras2.edicion AND tabla.version = compras2.version
                    INNER JOIN versiones ON tabla.version = versiones.nombre
                    INNER JOIN ediciones ON tabla.edicion = ediciones.nombre
                    INNER JOIN productos ON tabla.familia = productos.nombre
                    INNER JOIN tabla_equivalencia ON productos.idProducto = tabla_equivalencia.familia AND ediciones.idEdicion = tabla_equivalencia.edicion AND versiones.id = tabla_equivalencia.version
                    INNER JOIN (SELECT usuarioEquipoMicrosoft.id,
                                    usuarioEquipoMicrosoft.cliente,
                                    usuarioEquipoMicrosoft.empleado,
                                    usuarioEquipoMicrosoft.host_name,
                                    usuarioEquipoMicrosoft.usuario,
                                    usuarioEquipoMicrosoft.centroCosto
                                FROM usuarioEquipoMicrosoft
                                WHERE usuarioEquipoMicrosoft.cliente = :cliente AND usuarioEquipoMicrosoft.empleado = :empleado
                                GROUP BY usuarioEquipoMicrosoft.host_name, usuarioEquipoMicrosoft.usuario) AS tabla1 ON tabla.equipo = tabla1.host_name AND tabla.cliente = tabla1.cliente AND tabla.empleado = tabla1.empleado
                GROUP BY tabla1.centroCosto
                ORDER BY tabla1.host_name');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}