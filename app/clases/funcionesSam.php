<?php

namespace App\Clases;

class funcionesSam extends General{
    public $error = "ERROR: No se pudo realizar la consulta";

    function existe_permiso($idEmpleado, $idPermiso) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT * FROM permisoSam WHERE idEmpleado = :idEmpleado AND idPermiso = :idPermiso');
            $sql->execute(array('idEmpleado' => $idEmpleado, 'idPermiso' => $idPermiso));
            $resultado = $sql->fetchAll();
            if (count($resultado) > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            return false;
        }
    }

    function menu2($id_user, $opcion = null) {
        $menu = '';
        $activo = "";
        if ($this->existe_permiso($id_user, 3)) {
            if ($opcion == "sam" || $opcion == null) {
                $opcion = "sam";
                $activo = "activado1";
            }
            $menu .= '<div class="botones_m3Sam boton2 ' . $activo . '" id="SamRepositorio">
                Repositorios
            </div>';
            $activo = "";
        }

        if ($this->existe_permiso($id_user, 2)) {
            if ($opcion == "procesos" || $opcion == null) {
                $opcion = "procesos";
                $activo = "activado1";
            }
            $menu .= '<div class="botones_m3Sam boton2 ' . $activo . '" id="SamCicloVida">
                Procesos SAM
            </div>';
            $activo = "";
        }

        if ($this->existe_permiso($id_user, 4)) {
            if ($opcion == "acceso" || $opcion == null) {
                $opcion = "acceso";
                $activo = "activado1";
            }
            $menu .= '<div class="botones_m3Sam boton2 ' . $activo . '" id="SamAcceso">
                Accesos SAM
            </div>';
        }
        return $menu;
    }

    function menu3($opcion, $id_user) {
        $menu = '';
        if ($this->existe_permiso($id_user, 2) && $opcion == "procesos") {
            $menu .= '
            <div class="botones_m2 boton2 activado1" id="generalProcesos">General</div>
            <div class="botones_m2 boton2" id="PC13">PC 1-3</div>
            <div class="botones_m2 boton2" id="PC46">PC 4-6</div>
            <div class="botones_m2 boton2" id="PC79">PC 7-9</div>
            <div class="botones_m2 boton2" id="PC1012">PC 10-12</div>';
        } else {
            if ($this->existe_permiso($id_user, 3) && ($opcion == "sam" || $opcion == "despliegue" || $opcion == "centroCosto")) {
                $menu .= '
                <div class="botones_m2 boton2';
                if ($opcion == "sam") {
                    $menu .= ' activado1';
                }
                $menu .= '" id="SamGeneral">General</div>
                <!--<div class="botones_m2 boton2" id="SamCompras">Contratos</div>-->
                                <div class="botones_m2 boton2" onclick="location.href=\'contratos.php\';">Contratos</div>
                <div class="botones_m2 boton2" id="SamBalance">Licencias</div>
                <div class="botones_m2 boton2';
                if ($opcion == "despliegue") {
                    $menu .= ' activado1';
                }
                $menu .= '" id="SamDespliegue">Despliegue</div>
                    <div class="botones_m2 boton2';
                if ($opcion == "centroCosto") {
                    $menu .= ' activado1';
                }
                $menu .= '" id="SamCentroCosto">Centro de Costos</div>';
            } else {
                if ($this->existe_permiso($id_user, 4) && $opcion == "acceso") {
                    $menu .= '<div class="botones_m2 boton2 activado1" id="generalAcceso">General</div>';
                }
            }
        }
        return $menu;
    }

    //inicio despliegue Microsoft
    function despliegueSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    //fin despliegue Microsoft
    //inicio despliegue Adobe
    function despliegueAdobeSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafAdobeSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueAdobeSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchAdobeSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue Adobe
    
    //inicio despliegue Ibm
    function despliegueIbmSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafIbmSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueIbmSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchIbmSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue IBM
    
    //inicio despliegue Oracle
    function despliegueOracleSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafOracleSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueOracleSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchOracleSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue Oracle
    
    //inicio despliegue VMWare
    function despliegueVMWareSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafVMWareSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueVMWareSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchVMWareSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue VMWare
    
    //inicio despliegue Unix-IBM
    function despliegueUnixIBMSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafUnixIBMSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueUnixIBMSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchUnixIBMSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue Unix-IBM
    
    //inicio despliegue Unix-Oracle
    function despliegueUnixOracleSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafUnixOracleSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueUnixOracleSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchUnixOracleSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue Unix-Oracle
    
    //inicio despliegue SAP
    function despliegueSAPSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafSapSam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueSAPSam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchSapSam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue SAP
    
    //inicio despliegue SPLA
    function despliegueSPLASam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo,
                            cliente,
                            DATE_FORMAT(fecha, "%d/%m/%Y %H:%i:%s %p") AS fecha
                        FROM encGrafSPLASam
                        WHERE cliente = :cliente AND empleado = :empleado
                        ORDER BY fecha');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            //$resultado = $sql->fetch();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function archDespliegueSPLASam($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT 
                        cliente,
                        custom,
                        custom1,
                        DATE_FORMAT(fechaCustom, "%d/%m/%Y %H:%i:%s") AS fechaCustom,
                        DATE_FORMAT(fechaCustom1, "%d/%m/%Y %H:%i:%s") AS fechaCustom1,
                        otros,
                        otros1,
                        DATE_FORMAT(fechaOtros, "%d/%m/%Y %H:%i:%s") AS fechaOtros,
                        DATE_FORMAT(fechaOtros1, "%d/%m/%Y %H:%i:%s") AS fechaOtros1
                    FROM encArchSPLASam
                    WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetch();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }
    //fin despliegue SPLA

    function verCabeceraContrato($contrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT contrato.idFabricante,
                       contrato.numero,
                       contrato.tipo,
                       DATE_FORMAT(contrato.fechaEfectiva, "%d/%m/%Y") AS fechaEfectiva,
                       DATE_FORMAT(contrato.fechaExpiracion, "%d/%m/%Y") AS fechaExpiracion,
                       DATE_FORMAT(contrato.fechaProxima, "%d/%m/%Y") AS fechaProxima,
                       contrato.archivo1,
                       contrato.archivo2,
                       contrato.archivo3,
                       contrato.archivo4,
                       contrato.archivo5,
                       contrato.comentarios,
                       SUM(detalleContrato.cantidad) AS cantidad,
                       SUM(detalleContrato.cantidad * detalleContrato.precio) AS precio,
                                           subsidiaria,
                                           proveedor
                FROM contrato
                    INNER JOIN detalleContrato ON contrato.idContrato = detalleContrato.idContrato
                WHERE contrato.idContrato = :contrato');
            $sql->execute(array('contrato' => $contrato));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function verDetalleContrato($contrato, $asignaciones) {
        if(count($asignaciones) > 0){
            $array = array('contrato' => $contrato);
            $where = " AND asignacion IN (";
            
            $i = 0;
            foreach($asignaciones as $row){
                if($i > 0){
                    $where .= ", ";
                }
                $where .= "'" . $row["asignacion"] . "'";
                $i++;
            }
            
            $where .= ")";

            $this->conexion();
            try {
                $sql = $this->conn->prepare('SELECT detalleContrato.idDetalleContrato,
                        detalleContrato.idProducto,
                        detalleContrato.idEdicion,
                        detalleContrato.version,
                        detalleContrato.sku,
                        detalleContrato.tipo,
                        detalleContrato.cantidad,
                        detalleContrato.precio,
                        detalleContrato.asignacion
                    FROM detalleContrato
                    WHERE detalleContrato.idContrato = :contrato AND status = 1 ' . $where);
                $sql->execute($array);
                $resultado = $sql->fetchAll();
                return $resultado;
            } catch (PDOException $e) {
                return array();
            }
        } else{
            return array();
        }
    }

    function guardarContrato($cliente, $empleado, $idFabricante, $numero, $tipo, $fechaEfectiva, $fechaExpiracion, 
    $fechaProxima, $comentarios, $archivo1, $archivo2, $archivo3, $archivo4, $archivo5, $subsidiaria, $proveedor) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('INSERT INTO contrato (idCliente, empleado, idFabricante, numero, '
            . 'tipo, fechaEfectiva, fechaExpiracion, fechaProxima, comentarios, archivo1, archivo2, archivo3, '
            . 'archivo4, archivo5, subsidiaria, proveedor) VALUES (:idCliente, :empleado, :idFabricante, :numero, :tipo, '
            . ':fechaEfectiva, :fechaExpiracion, :fechaProxima, :comentarios, :archivo1, :archivo2, :archivo3, '
            . ':archivo4, :archivo5, :subsidiaria, :proveedor)');
            $sql->execute(array(':idCliente' => $cliente, ':empleado'=>$empleado, ':idFabricante' => $idFabricante, 
            ':numero' => $numero, ':tipo' => $tipo, ':fechaEfectiva' => $fechaEfectiva, ':fechaExpiracion' => $fechaExpiracion, 
            ':fechaProxima' => $fechaProxima, ':comentarios' => $comentarios, ':archivo1' => $archivo1, ':archivo2' => $archivo2, 
            ':archivo3' => $archivo3, ':archivo4' => $archivo4, ':archivo5' => $archivo5, ':subsidiaria'=>$subsidiaria, 
            ':proveedor'=>$proveedor));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function editarContrato($contrato, $idFabricante, $numero, $tipo, $fechaEfectiva, $fechaExpiracion, 
    $fechaProxima, $comentarios, $archivo1, $archivo2, $archivo3, $archivo4, $archivo5, $subsidiaria, $proveedor) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET idFabricante = :idFabricante, numero = :numero, '
            . 'tipo = :tipo, fechaEfectiva = :fechaEfectiva, fechaExpiracion = :fechaExpiracion, fechaProxima = :fechaProxima, '
            . 'comentarios = :comentarios, archivo1 = :archivo1, archivo2 = :archivo2, archivo3 = :archivo3, '
            . 'archivo4 = :archivo4, archivo5 = :archivo5, subsidiaria = :subsidiaria, proveedor = :proveedor WHERE idContrato = :contrato');
            $sql->execute(array(':idFabricante' => $idFabricante, ':numero' => $numero, ':tipo' => $tipo, ':fechaEfectiva' => $fechaEfectiva, 
            ':fechaExpiracion' => $fechaExpiracion, ':fechaProxima' => $fechaProxima, ':comentarios' => $comentarios, 
            ':archivo1' => $archivo1, ':archivo2' => $archivo2, ':archivo3' => $archivo3, ':archivo4' => $archivo4, 
            ':archivo5' => $archivo5, ':subsidiaria'=>$subsidiaria, ':proveedor'=>$proveedor, ':contrato' => $contrato));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function eliminarContrato($idContrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET status = 0 WHERE idContrato = :idContrato; '
            . 'UPDATE detalleContrato SET status = 0 WHERE idContrato = :idContrato');
            $sql->execute(array(':idContrato' => $idContrato));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function guardarDetalleContrato($idContrato, $idProducto, $idEdicion, $version, $sku, $tipo, $cantidad, $precio, $asignacion) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('INSERT INTO detalleContrato (idContrato, idProducto, idEdicion, version, sku, tipo, cantidad, precio, asignacion) VALUES (:idContrato, :idProducto, :idEdicion, :version, :sku, :tipo, :cantidad, :precio, :asignacion)');
            $sql->execute(array(':idContrato' => $idContrato, ':idProducto' => $idProducto, ':idEdicion' => $idEdicion, ':version' => $version, ':sku' => $sku, ':tipo' => $tipo, ':cantidad' => $cantidad, ':precio' => $precio, ':asignacion' => $asignacion));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }
    
    function existeDetalleContrato($idContrato, $idProducto, $idEdicion, $version, $asignacion) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT COUNT(idContrato) AS cantidad '
                . 'FROM detalleContrato '
                . 'WHERE idContrato = :idContrato AND idProducto = :idProducto AND idEdicion = :idEdicion AND '
                . 'version = :version AND asignacion = :asignacion');
            $sql->execute(array(':idContrato' => $idContrato, ':idProducto' => $idProducto, ':idEdicion' => $idEdicion, ':version' => $version, ':asignacion' => $asignacion));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            return 0;
        }
    }
    
    function actualizarDetalleContrato($idContrato, $idProducto, $idEdicion, $version, $sku, $tipo, $cantidad, $precio, $asignacion) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE detalleContrato SET sku = :sku, tipo = :tipo, cantidad = :cantidad, precio = :precio, status = 1 '
            . 'WHERE idContrato = :idContrato AND idProducto = :idProducto AND idEdicion = :idEdicion AND version = :version AND asignacion = :asignacion');
            $sql->execute(array(':idContrato' => $idContrato, ':idProducto' => $idProducto, ':idEdicion' => $idEdicion, ':version' => $version, ':sku' => $sku, ':tipo' => $tipo, ':cantidad' => $cantidad, ':precio' => $precio, ':asignacion' => $asignacion));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }
    
    function sumarCantidadDetalleContrato($idDetalle, $cantidad) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE detalleContrato SET cantidad = cantidad + :cantidad WHERE idDetalleContrato = :idDetalle');
            $sql->execute(array(':idDetalle' => $idDetalle, ':cantidad' => $cantidad));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function restarCantidadDetalleContrato($idDetalle, $cantidad) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE detalleContrato SET cantidad = cantidad - :cantidad WHERE idDetalleContrato = :idDetalle');
            $sql->execute(array(':idDetalle' => $idDetalle, ':cantidad' => $cantidad));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    function eliminarDetalleContrato($idContrato, $asignaciones) {
        if(count($asignaciones) > 0){
            $array = array('idContrato' => $idContrato);
            $where = " AND asignacion IN (";
            
            $i = 0;
            foreach($asignaciones as $row){
                if($i > 0){
                    $where .= ", ";
                }
                $where .= "'" . $row["asignacion"] . "'";
                $i++;
            }
            
            $where .= ")";
            
            $this->conexion();
            try {
                $sql = $this->conn->prepare('UPDATE detalleContrato SET status = 0 WHERE idContrato = :idContrato ' . $where);
                $sql->execute($array);
                return 1;
            } catch (PDOException $e) {
                return 0;
            }
        } else{
            return 0;
        }
    }

    function actualizarArchivosContrato($idContrato, $archivo1, $archivo2, $archivo3, $archivo4, $archivo5) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET archivo1 = :archivo1, archivo2 = :archivo2, archivo3 = :archivo3, archivo4 = :archivo4, archivo5 = :archivo5 WHERE idContrato = :idContrato');
            $sql->execute(array('idContrato' => $idContrato, 'archivo1' => $archivo1, 'archivo2' => $archivo2, 'archivo3' => $archivo3, 'archivo4' => $archivo4, 'archivo5' => $archivo5));
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function getUltimoContrato() {
        $result = 0;
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT MAX(idContrato) idContrato FROM contrato');
            $sql->execute();
            $resultado = $sql->fetchAll();
            foreach ($resultado as $row) {
                $result = $row["idContrato"];
            }
            return $result;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function obtenerFabricantes() {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT * FROM fabricantes WHERE status = 1 ORDER BY nombre');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }
    
    function obtenerFabricantesWindows() {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT * FROM fabricantes WHERE status = 1 AND idFabricante IN (1, 2, 3, 4) ORDER BY nombre');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }
    
    function obtenerFabricantesEmpleado($empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT fabricantes.idFabricante,
                    fabricantes.nombre
                FROM fabricantes
                    INNER JOIN permisosGenerales ON permisosGenerales.nombre LIKE CONCAT("%", fabricantes.nombre, "%") AND permisosGenerales.status = 1
                    INNER JOIN permisoSam ON permisosGenerales.idPermisos = permisoSam.idPermiso AND permisoSam.idEmpleado = :empleado
                WHERE fabricantes.status = 1
                GROUP BY fabricantes.idFabricante
                ORDER BY fabricantes.nombre, fabricantes.idFabricante DESC');
            $sql->execute(array(':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function obtenerProductos() {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT * FROM productos WHERE status = 1 ORDER BY nombre');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function listadoAsignaciones($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT detalleContrato.asignacion
                FROM detalleContrato
                                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :cliente AND contrato.empleado = :empleado
                WHERE detalleContrato.status = 1
                GROUP BY detalleContrato.asignacion');
            $sql->execute(array('cliente' => $cliente, ':empleado'=>$empleado));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function totalRegistrosContratos($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT COUNT(contrato.idContrato) AS total
                FROM contrato
                                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                WHERE contrato.idCliente = :cliente AND contrato.status = 1');
            $sql->execute(array(':cliente' => $cliente));
            $resultado = $sql->fetchAll();
            $total = 0;
            foreach ($resultado as $row) {
                $total = $row["total"];
            }
            return $total;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function totalRegistrosLicencias($cliente, $empleado, $fabricante, $asignacion, $asignaciones) {
        $this->conexion();
        try {
            $where = "";
            if($fabricante != ""){
                $where .= " AND contrato.idFabricante = " . $fabricante;
            }
            if($asignacion != ""){
                $where .= " AND detalleContrato.asignacion = '" . $asignacion . "'";
            } else{
                $where .= " AND detalleContrato.asignacion IN (";
                
                $i = 0;
                foreach($asignaciones as $row){
                    if($i > 0){
                        $where .= ", ";
                    }
                    $where .= "'" . $row["asignacion"] . "'";
                    $i++;
                }
                
                $where .= ") ";
            }
            
            $sql = $this->conn->prepare('SELECT COUNT(detalleContrato.idDetalleContrato) AS total
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :cliente
                WHERE detalleContrato.status = 1 ' . $where);
            $sql->execute(array(':cliente'=>$cliente));
            $resultado = $sql->fetchAll();
            $total = 0;
            foreach ($resultado as $row) {
                $total = $row["total"];
            }
            return $total;
        } catch (PDOException $e) {
            echo $this->serror;
        }
    }

    function ultimaPaginaLicencias($cliente, $empleado, $fabricante, $asignacion, $asignaciones, $limite) {
        return ceil($this->totalRegistrosLicencias($cliente,$empleado, $fabricante, $asignacion, $asignaciones) / $limite);
    }
    
    function ultimaPaginaLicenciasAsignacion($cliente, $fabricante, $asignacion, $asignaciones, $limite) {
        return ceil(count($this->balanzaAsignacionSAM($cliente, $fabricante, $asignacion, $asignaciones)) / $limite);
    }

    function listadoLicencias($cliente, $empleado, $asignacion, $asignaciones, $fabricante, $pagina, $limite, $orden, $direccion) {
        $this->conexion();
        try {
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaLicencias($cliente, $empleado, $fabricante, $asignacion, $asignaciones, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }

            $campo = "";
            $valor = "";
            $array = array();
            if ($asignacion != "") {
                $campo .= " AND detalleContrato.asignacion = :asignacion";
                $array["asignacion"] = $asignacion;
            } else{
                $campo .= " AND detalleContrato.asignacion IN (";
                
                $i = 0;
                foreach($asignaciones as $row){
                    if($i > 0){
                        $campo .= ", ";
                    }
                    $campo .= "'" . $row["asignacion"] . "'";
                    $i++;
                }
                
                $campo .= ") ";
            }

            if ($fabricante != "") {
                $campo .= " AND fabricantes.idFabricante = :idFabricante";
                if ($asignacion != "") {
                    $valor .= ",";
                }
                $array["idFabricante"] = $fabricante;
            }

            $sql = $this->conn->prepare('SELECT detalleContrato.idDetalleContrato,
                    fabricantes.idFabricante,
                    fabricantes.nombre,
                    contrato.numero,
                                        contrato.proveedor,
                                        contrato.fechaEfectiva,
                                        contrato.fechaExpiracion,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    detalleContrato.cantidad,
                                        detalleContrato.precio,
                    detalleContrato.asignacion,
                    if(contrato.fechaExpiracion > NOW(), "Activo", "Vencido") AS estado
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :idCliente
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto 
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE detalleContrato.status = 1' . $campo . ' 
                ORDER BY ' . $orden . ' ' . $direccion . '
                LIMIT ' . $pagina . ', ' . $limite);
            /* if($asignacion != "" || $fabricante != ""){
              $sql->execute($array);
              }
              else{
              $sql->execute();
              } */
            // dd($sq)l;
            $array[":idCliente"] = $cliente;
            $sql->execute($array);

            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    function listadoLicencias1($cliente, $empleado, $asignacion, $asignaciones, $fabricante) {
        $this->conexion();
        try {
            $campo = "";
            $valor = "";
            $array = array();
            if ($asignacion != "") {
                $campo .= " AND detalleContrato.asignacion = :asignacion";
                $array["asignacion"] = $asignacion;
            } else{
                $campo .= " AND detalleContrato.asignacion IN (";
                
                $i = 0;
                foreach($asignaciones as $row){
                    if($i > 0){
                        $campo .= ", ";
                    }
                    $campo .= "'" . $row["asignacion"] . "'";
                    $i++;
                }
                
                $campo .= ") ";
            }

            if ($fabricante != "") {
                $campo .= " AND fabricantes.idFabricante = :idFabricante";
                if ($asignacion != "") {
                    $valor .= ",";
                }
                $array["idFabricante"] = $fabricante;
            }

            $sql = $this->conn->prepare('SELECT detalleContrato.idDetalleContrato,
                    fabricantes.idFabricante,
                    fabricantes.nombre,
                    contrato.numero,
                                        contrato.proveedor,
                                        contrato.fechaEfectiva,
                                        contrato.fechaExpiracion,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    detalleContrato.cantidad,
                                        detalleContrato.precio,
                    detalleContrato.asignacion,
                    if(contrato.fechaExpiracion > NOW(), "Activo", "Vencido") AS estado
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :idCliente
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto 
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE detalleContrato.status = 1' . $campo . ' 
                ORDER BY fabricantes.nombre, detalleContrato.idDetalleContrato asc');
            $array[":idCliente"] = $cliente;
            $sql->execute($array);

            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    function listadoContratosCliente($cliente, $empleado, $fabricante){
        $this->conexion();
        try{
            $sql = $this->conn->prepare('SELECT idContrato,
                    numero,
                    subsidiaria,
                    if(fechaExpiracion > NOW(), "Activo", "Vencido") AS estado
                FROM contrato
                WHERE idCliente = :idCliente AND idFabricante = :fabricante AND status = 1');
            $sql->execute(array(':idCliente' => $cliente, ':fabricante' => $fabricante));

            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return array();
        }
    }
    
    function listadoContratosClienteGantt($cliente, $empleado){
        $this->conexion();
        try{
            $sql = $this->conn->prepare('SELECT contrato.idContrato,
                    fabricantes.nombre,
                    contrato.numero,
                    contrato.fechaEfectiva,
                    contrato.fechaExpiracion,
                    SUM(detalleContrato.precio * detalleContrato.cantidad) total
                FROM contrato
                     INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                     INNER JOIN detalleContrato ON contrato.idContrato = detalleContrato.idContrato
                WHERE contrato.idCliente = :cliente AND contrato.empleado = :empleado AND contrato.status = 1
                GROUP BY contrato.idContrato

                UNION

                SELECT "" AS idContrato,
                    "" AS nombre,
                    "" AS numero,
                    "" AS fechaEfectiva,
                    "Total" AS fechaExpiracion,
                    SUM(detalleContrato.precio * detalleContrato.cantidad) total
                FROM contrato
                     INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                     INNER JOIN detalleContrato ON contrato.idContrato = detalleContrato.idContrato
                WHERE contrato.idCliente = :cliente AND contrato.empleado = :empleado AND contrato.status = 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));

            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return array();
        }
    }
    
    function listadoLicenciasClienteGantt($cliente, $empleado){
        $this->conexion();
        try{
            $sql = $this->conn->prepare('SELECT contrato.idContrato,
                    fabricantes.nombre,
                    contrato.numero,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    detalleContrato.asignacion,
                    detalleContrato.cantidad,
                    SUM(detalleContrato.precio * detalleContrato.cantidad) total
                FROM contrato
                     INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                     INNER JOIN detalleContrato ON contrato.idContrato = detalleContrato.idContrato
                     INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                     INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                     INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE contrato.idCliente = :cliente AND contrato.empleado = :empleado AND contrato.status = 1
                GROUP BY productos.nombre, ediciones.nombre, versiones.nombre, detalleContrato.asignacion

                UNION

                SELECT "" AS idContrato,
                    "" AS nombre,
                    "" AS numero,
                    "" AS producto,
                    "" AS edicion,
                    "" AS version,
                    "" AS asignacion,
                    "Total" AS cantidad,
                    SUM(detalleContrato.precio * detalleContrato.cantidad) total
                FROM contrato
                     INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                     INNER JOIN detalleContrato ON contrato.idContrato = detalleContrato.idContrato
                WHERE contrato.idCliente = :cliente AND contrato.empleado = :empleado AND contrato.status = 1');
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));

            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return array();
        }
    }
    
    function listadoContratoLicenciasCliente($cliente, $fabricante, $contratos){
        $this->conexion();
        try {
            $where = "";
            for($i = 0; $i < count($contratos); $i++){
                if($i == 0){
                    $where .= " AND (";
                }
                else{
                    $where .= " OR ";
                }
                $where .= "contrato.idContrato = " . $contratos[$i];
            }
            $where .= ")";
            $sql = $this->conn->prepare('SELECT detalleContrato.idDetalleContrato,
                    fabricantes.idFabricante,
                    fabricantes.nombre,
                    contrato.numero,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                                        detalleContrato.tipo,
                    detalleContrato.cantidad,
                    detalleContrato.asignacion,
                                        detalleContrato.precio,
                    if(contrato.fechaExpiracion > NOW(), "Activo", "Vencido") AS estado
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :idCliente
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante AND fabricantes.idFabricante = :fabricante
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto 
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE detalleContrato.status = 1 ' . $where);
            $sql->execute(array(':idCliente' => $cliente, ':fabricante' => $fabricante));
            
            $resultado = $sql->fetchAll();  
            return $resultado;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    function listadoLicenciasCliente($cliente, $empleado, $fabricante) {
        $this->conexion();
        try {

            $sql = $this->conn->prepare('SELECT detalleContrato.idDetalleContrato,
                    fabricantes.idFabricante,
                    fabricantes.nombre,
                    contrato.numero,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    detalleContrato.cantidad,
                    detalleContrato.asignacion,
                                        detalleContrato.precio,
                    if(contrato.fechaExpiracion > NOW(), "Activo", "Vencido") AS estado
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :idCliente AND contrato.empleado = :empleado
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante AND fabricantes.idFabricante = :fabricante
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto 
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE detalleContrato.status = 1');
            $sql->execute(array(':idCliente' => $cliente, ':empleado'=>$empleado, ':fabricante' => $fabricante));

            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    function eliminarArchivosContrato($contrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT archivo1,
                    archivo2,
                    archivo3,
                    archivo4,
                    archivo5
                FROM contrato
                WHERE idContrato = :contrato');
            $sql->execute(array('contrato' => $contrato));
            $resultado = $sql->fetchAll();
            foreach ($resultado as $row) {
                if ($row["archivo1"] != "" && file_exists($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo1"])) {
                    unlink($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo1"]);
                }
                if ($row["archivo2"] != "" && file_exists($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo2"])) {
                    unlink($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo2"]);
                }
                if ($row["archivo3"] != "" && file_exists($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo3"])) {
                    unlink($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo3"]);
                }
                if ($row["archivo4"] != "" && file_exists($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo4"])) {
                    unlink($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo4"]);
                }
                if ($row["archivo5"] != "" && file_exists($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo5"])) {
                    unlink($GLOBALS["app_root"] . "/sam/contratos/" . $row["archivo5"]);
                }
            }
            return 1;
        } catch (PDOException $e) {
            return 0;
        }
    }

    function ultimaPaginaContratos($cliente, $empleado, $limite) {
        return ceil($this->totalRegistrosContratos($cliente, $empleado) / $limite);
    }

    function alertaContratosActivos($cliente, $empleado) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT contrato.idContrato,
                    contrato.numero,
                    clientes.correo,
                    clientes.nombre,
                    clientes.apellido,
                    DATEDIFF(contrato.fechaExpiracion, CURDATE()) AS tiempoExpiracion,
                    DATEDIFF(contrato.fechaProxima, CURDATE()) AS tiempoProxima,
                    contrato.correoExpiracion90,
                    contrato.correoExpiracion60,
                    contrato.correoExpiracion30,
                    contrato.correoProxima90,
                    contrato.correoProxima60,
                    contrato.correoProxima30
                FROM contrato
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                    INNER JOIN clientes ON contrato.idCliente = clientes.id
                WHERE contrato.idCliente = :cliente AND contrato.status = 1 AND ((DATEDIFF(contrato.fechaProxima, CURDATE()) <= 90 
                AND DATEDIFF(contrato.fechaProxima, CURDATE()) > 0) OR (DATEDIFF(contrato.fechaExpiracion, CURDATE()) <= 90 
                AND DATEDIFF(contrato.fechaExpiracion, CURDATE()) > 0))');
            $sql->execute(array('cliente' => $cliente));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            echo $this->error;
        }
    }
    
    function actulizarCorreoExpiracion90($idContrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET correoExpiracion90 = NOW() WHERE idContrato = :idContrato');
            $sql->execute(array(':idContrato' => $idContrato));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actulizarCorreoExpiracion60($idContrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET correoExpiracion60 = NOW() WHERE idContrato = :idContrato');
            $sql->execute(array(':idContrato' => $idContrato));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actulizarCorreoExpiracion30($idContrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET correoExpiracion30 = NOW() WHERE idContrato = :idContrato');
            $sql->execute(array(':idContrato' => $idContrato));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actulizarCorreoProxima90($idContrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET correoProxima90 = NOW() WHERE idContrato = :idContrato');
            $sql->execute(array(':idContrato' => $idContrato));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actulizarCorreoProxima60($idContrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET correoProxima60 = NOW() WHERE idContrato = :idContrato');
            $sql->execute(array(':idContrato' => $idContrato));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function actulizarCorreoProxima30($idContrato) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('UPDATE contrato SET correoProxima30 = NOW() WHERE idContrato = :idContrato');
            $sql->execute(array(':idContrato' => $idContrato));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
    
    function listadoContratos($cliente, $empleado, $pagina, $limite, $orden, $direccion) {
        $this->conexion();
        try {
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaContratos($cliente, $empleado, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }

            $sql = $this->conn->prepare('SELECT contrato.idContrato,
                    contrato.numero,
                    fabricantes.nombre,
                    DATE_FORMAT(contrato.fechaEfectiva, "%d/%m/%Y") AS fechaEfectiva,
                    DATE_FORMAT(contrato.fechaExpiracion, "%d/%m/%Y") AS fechaExpiracion,
                    DATE_FORMAT(contrato.fechaProxima, "%d/%m/%Y") AS fechaProxima,
                    if(contrato.fechaExpiracion > NOW(), "Activo", "Vencido") AS estado,
                    contrato.comentarios
                FROM contrato
                     INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                WHERE contrato.idCliente = :cliente AND contrato.status = 1
                ORDER BY ' . $orden . ' ' . $direccion . '
                LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute(array(':cliente' => $cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function obtenerProdFabricante($fabricante) {
        $this->conexion();
        try {
            /* $sql = $this->conn->prepare('SELECT productos.idProducto,
              productos.nombre
              FROM prodFabricante
              INNER JOIN productos ON prodFabricante.idProducto = productos.idProducto AND productos.status = 1
              WHERE prodFabricante.idFabricante = :fabricante AND prodFabricante.status = 1
              ORDER BY nombre'); */
            $sql = $this->conn->prepare('SELECT productos.idProducto,
                       productos.nombre
                FROM tabla_equivalencia
                     INNER JOIN productos ON tabla_equivalencia.familia = productos.idProducto AND productos.status = 1
                WHERE tabla_equivalencia.fabricante = :fabricante
                GROUP BY productos.idProducto
                ORDER BY nombre');
            $sql->execute(array('fabricante' => $fabricante));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function obtenerEdicProducto($fabricante, $producto) {
        $this->conexion();
        try {
            /* $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
              ediciones.nombre
              FROM edicproducto
              INNER JOIN ediciones ON edicproducto.idEdicion = ediciones.idEdicion AND ediciones.status = 1
              WHERE edicproducto.idProducto = :producto AND edicproducto.status = 1'); */
            $sql = $this->conn->prepare('SELECT ediciones.idEdicion,
                       ediciones.nombre
                FROM tabla_equivalencia
                     INNER JOIN ediciones ON tabla_equivalencia.edicion = ediciones.idEdicion AND ediciones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.familia = :producto
                                GROUP BY ediciones.idEdicion
                                ORDER BY nombre');
            $sql->execute(array('fabricante' => $fabricante, 'producto' => $producto));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function obtenerEdicProductoVersion($fabricante, $producto, $edicion) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT versiones.id,
                       versiones.nombre
                FROM tabla_equivalencia
                     INNER JOIN versiones ON tabla_equivalencia.version = versiones.id AND versiones.status = 1
                WHERE tabla_equivalencia.`fabricante` = :fabricante AND tabla_equivalencia.familia = :producto AND tabla_equivalencia.edicion = :edicion
                                GROUP BY versiones.id
                                ORDER BY nombre');
            $sql->execute(array('fabricante' => $fabricante, 'producto' => $producto, 'edicion' => $edicion));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function obtenerVersiones() {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT id,
                                nombre
                     FROM versiones
                     WHERE status = 1');
            $sql->execute();
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }

    function renombrarArchivo($archivo) {
        $array = explode(".", $archivo);
        for ($i = 0; $i < count($array); $i++) {
            if ($i == 1) {
                $nombre = $array[$i];
            } else if ($i > 1) {
                $nombre .= "." . $array[$i];
            }
        }
        return $nombre;
    }

    function transformarFecha($fecha) {
        $result = "";
        if ($fecha != "") {
            $valor1 = explode("/", $fecha);
            $result = $valor1[2] . "/" . $valor1[1] . "/" . $valor1[0];
        }
        return $result;
    }

    function fechaReporte($archivo, $cliente) {
        $this->conexion();
        try {
            $sql = $this->conn->prepare('SELECT DATE_FORMAT(fecha, "%d/%m/%Y") AS fecha
                FROM encGrafSam
                WHERE archivo = :archivo AND cliente = :cliente');
            $sql->execute(array('archivo' => $archivo, 'cliente' => $cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }
    
    function fechaReporteSamGantt($cliente, $empleado, $idFabricante) {
        $this->conexion();
        try {
            $tabla = "";
            if($idFabricante == 1){
                $tabla = "encGrafAdobeSam";
            } else if($idFabricante == 2){
                $tabla = "encGrafIbmSam";
            } else if($idFabricante == 3){
                $tabla = "encGrafSam";
            } else if($idFabricante == 4){
                $tabla = "encGrafOracleSam";
            } else if($idFabricante == 5){
                $tabla = "encGrafSapSam";
            } else if($idFabricante == 6){
                $tabla = "encGrafVMWareSam";
            } else if($idFabricante == 7){
                $tabla = "encGrafUnixIBMSam";
            } else if($idFabricante == 8){
                $tabla = "encGrafUnixOracleSam";
            } else if($idFabricante == 10){
                $tabla = "encGrafSPLASam";
            }
            
            $sql = $this->conn->prepare('SELECT DATE_FORMAT(fecha, "%d/%m/%Y %h:%i:%s %p") AS fecha,
                    archivo
                FROM ' . $tabla . '
                WHERE cliente = :cliente');
            $sql->execute(array('cliente' => $cliente));
            $resultado = $sql->fetchAll();
            return $resultado;
        } catch (PDOException $e) {
            echo $this->error;
        }
    }
    
    /*inicio metodos reporte licencias*/
    function reporteLicencias($cliente, $empleado, $where){
        $this->conexion();
        try {
            $sql   = "SELECT COUNT(detalleContrato.idDetalleContrato) AS total
                    FROM detalleContrato
                        INNER JOIN contrato ON detalleContrato.idContrato = contrato.`idContrato` 
                        AND contrato.idCliente = :cliente AND contrato.empleado = :empleado
                    WHERE detalleContrato.status = 1" . $where;
            $query = $this->conn->prepare($sql);
            $query->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $row = $query->fetch();
            return $row["total"];
        } catch (PDOException $e) {
            return 0;
        }
    }
    
    function reporteLicencias1($cliente, $empleado, $where, $inicio, $total){
        $this->conexion();
        try {
            $sql   = "SELECT detalleContrato.idDetalleContrato,
                        fabricantes.idFabricante,
                        fabricantes.nombre,
                        contrato.numero,
                        productos.nombre AS producto,
                        ediciones.nombre AS edicion,
                        versiones.nombre AS version,
                        detalleContrato.cantidad,
                        detalleContrato.precio,
                        detalleContrato.asignacion,
                        if(contrato.fechaExpiracion > NOW(), 'Activo', 'Vencido') AS estado
                FROM detalleContrato
                        INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :cliente AND contrato.empleado = :empleado
                        INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                        INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto 
                        INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                        INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE detalleContrato.status = 1 " . $where . " 
                ORDER BY nombre asc
                LIMIT " . $inicio . ", ".$total;

            $query = $this->conn->prepare($sql);
            $query->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $query->fetchAll();
        } catch (PDOException $e) {
            return array();
        }
    }
    /*fin metodos reportes licencias*/

    function getFuncionesMenuLateral() {
        $funciones = '<script>
            $(document).ready(function(){
                $("#SamCicloVida").click(function(){
                    $("#fondo").show();
                    $(this).addClass("activado1"); 
                    $("#SamRepositorio").removeClass("activado1");
                    $("#SamAcceso").removeClass("activado1");

                    $.post("' . $GLOBALS["domain_root"] . '/sam/procesoGeneral.php", { opcion : "true", token : localStorage.licensingassuranceToken }, function(data){
                        if(data[0].resultado === false){
                            location.href = "' . $GLOBALS["domain_root"] . '";
                            return false;
                        }
                        if(data[0].sesion === "false"){
                            $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                location.href = "' . $GLOBALS['domain_root'] . '";
                                return false;
                            });
                        }
                        $("#contenedor_ver2").empty();
                        $("#contenedor_ver2").append(data[0].menuSuperior);
                        $("#contenedorCentral").empty();
                        $("#contenedorCentral").append(data[0].div);
                        $("#fondo").hide();
                    }, "json")
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                }); 

                $("#SamRepositorio").click(function(){
                    $("#fondo").show();
                    $(this).addClass("activado1"); 
                    $("#SamCicloVida").removeClass("activado1");
                    $("#SamAcceso").removeClass("activado1");

                    $.post("' . $GLOBALS["domain_root"] . '/sam/general.php", { opcion : "true", token : localStorage.licensingassuranceToken }, function(data){
                        if(data[0].resultado === false){
                            location.href = "' . $GLOBALS["domain_root"] . '";
                            return false;
                        }
                        if(data[0].sesion === "false"){
                            $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                location.href = "' . $GLOBALS['domain_root'] . '";
                                return false;
                            });
                        }
                        $("#contenedor_ver2").empty();
                        $("#contenedor_ver2").append(data[0].menuSuperior);
                        $("#contenedorCentral").empty();
                        $("#contenedorCentral").append(data[0].div);
                        $("#fondo").hide();
                    }, "json")
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                });

                $("#SamAcceso").click(function(){
                    $("#fondo").show();
                    $(this).addClass("activado1"); 
                    $("#SamCicloVida").removeClass("activado1");
                    $("#SamRepositorio").removeClass("activado1");

                    $.post("' . $GLOBALS["domain_root"] . '/sam/accesoGeneral.php", { token : localStorage.licensingassuranceToken }, function(data){
                        if(data[0].resultado === false){
                            location.href = "' . $GLOBALS["domain_root"] . '";
                            return false;
                        }
                        if(data[0].sesion === "false"){
                            $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                location.href = "' . $GLOBALS['domain_root'] . '";
                                return false;
                            });
                        }
                        $("#contenedor_ver2").empty();
                        $("#contenedor_ver2").append(data[0].menuSuperior);
                        $("#contenedorCentral").empty();
                        $("#contenedorCentral").append(data[0].div);
                        $("#fondo").hide();
                    }, "json")
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                }); 
            });
            </script>';
        return $funciones;
    }

    function getFuncionesMenuSuperior() {
        $funciones = '<script>  
            var click = 0;
            //inicio menu procesos
            $(document).ready(function(){
                $("#generalProcesos").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#PC13").removeClass("activado1");
                        $("#PC46").removeClass("activado1");
                        $("#PC79").removeClass("activado1");
                        $("#PC1012").removeClass("activado1");
                        $(this).addClass("activado1");
                        $.post("' . $GLOBALS["domain_root"] . '/sam/procesoGeneral.php", { opcion : "false", token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                            location.href = "' . $GLOBALS["domain_root"] . '";
                            return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });

                $("#PC13").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#generalProcesos").removeClass("activado1");
                        $("#PC46").removeClass("activado1");
                        $("#PC79").removeClass("activado1");
                        $("#PC1012").removeClass("activado1");
                        $(this).addClass("activado1");
                        $.post("' . $GLOBALS["domain_root"] . '/sam/pc1-3.php", { token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });

                $("#PC46").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#generalProcesos").removeClass("activado1");
                        $("#PC13").removeClass("activado1");
                        $("#PC79").removeClass("activado1");
                        $("#PC1012").removeClass("activado1");
                        $(this).addClass("activado1");
                        $.post("' . $GLOBALS["domain_root"] . '/sam/pc4-6.php", { token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });

                $("#PC79").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#generalProcesos").removeClass("activado1");
                        $("#PC13").removeClass("activado1");
                        $("#PC46").removeClass("activado1");
                        $("#PC1012").removeClass("activado1");
                        $(this).addClass("activado1");
                        $.post("' . $GLOBALS["domain_root"] . '/sam/pc7-9.php", { token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });

                $("#PC1012").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#generalProcesos").removeClass("activado1");
                        $("#PC13").removeClass("activado1");
                        $("#PC46").removeClass("activado1");
                        $("#PC79").removeClass("activado1");
                        $(this).addClass("activado1");
                        $.post("' . $GLOBALS["domain_root"] . '/sam/pc10-12.php", { token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });
                //fin menu procesos

                //inicio menu repositorio
                $("#SamGeneral").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#SamGeneral").addClass("activado1");
                        $("#SamCompras").removeClass("activado1");
                        $("#SamBalance").removeClass("activado1");
                        $("#SamDespliegue").removeClass("activado1");
                        $("#SamCentroCosto").removeClass("activado1");
                        $(this).addClass("activado1");
                        $.post("' . $GLOBALS["domain_root"] . '/sam/general.php", { opcion : "false", token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });

                $("#SamCompras").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#SamGeneral").removeClass("activado1");
                        $("#SamCompras").addClass("activado1");
                        $("#SamBalance").removeClass("activado1");
                        $("#SamDespliegue").removeClass("activado1");
                        $("#SamCentroCosto").removeClass("activado1");
                        $.post("' . $GLOBALS['domain_root'] . '/sam/compras.php", { token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });

                $("#SamBalance").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#SamGeneral").removeClass("activado1");
                        $("#SamCompras").removeClass("activado1");
                        $("#SamBalance").addClass("activado1");
                        $("#SamDespliegue").removeClass("activado1");
                        $("#SamCentroCosto").removeClass("activado1");
                        $.post("' . $GLOBALS['domain_root'] . '/sam/licencias.php", { token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });

                $("#SamDespliegue").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#SamGeneral").removeClass("activado1");
                        $("#SamCompras").removeClass("activado1");
                        $("#SamBalance").removeClass("activado1");
                        $("#SamDespliegue").addClass("activado1");
                        $("#SamCentroCosto").removeClass("activado1");
                        $.post("' . $GLOBALS['domain_root'] . '/sam/despliegue.php", { vert1 : $("#vert1").val(), token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });
                
                $("#SamCentroCosto").click(function(){
                    click++;
                    if(click === 1){
                        $("#fondo").show();
                        $("#SamGeneral").removeClass("activado1");
                        $("#SamCompras").removeClass("activado1");
                        $("#SamBalance").removeClass("activado1");
                        $("#SamDespliegue").removeClass("activado1");
                        $("#SamCentroCosto").addClass("activado1");
                        $.post("' . $GLOBALS['domain_root'] . '/sam/centroCosto.php", { vert1 : $("#vert1").val(), token : localStorage.licensingassuranceToken }, function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            $("#contenedorCentral").empty();
                            $("#contenedorCentral").append(data[0].div);
                            click = 0;
                            $("#fondo").hide();
                        }, "json")
                        .fail(function( jqXHR ){
                            $("#fondo").hide();
                            $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                                click = 0;
                            });
                        });
                    }
                });
                //fin menu repositorio
            });
            </script>';
        return $funciones;
    }

    function agrLicencias($filaLicencia) {
        $funciones = '<script>
                $("#cantidad' . $filaLicencia . '").numeric(false);
                $("#precio' . $filaLicencia . '").numeric(); 
            </script>';
        return $funciones;
    }

    function mostrarDespliegueSam($fabricante) {
        $div = '<div style="border:2px solid #000000;">'
        . '<table style="width:100%">';
        
        if($fabricante == 1){            
            $tabla = $this->despliegueAdobeSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("adobe", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
  
                $i++;
            }
           
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 2){            
            $tabla       = $this->despliegueIbmSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("ibm", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
                
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 3){           
            $tabla       = $this->despliegueSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("microsoft", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
               
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 4){
            $tabla       = $this->despliegueOracleSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("oracle", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
                
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 5){
            $tabla       = $this->despliegueSAPSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("sap", $fecha, $row["archivo"]);
               
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
           
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 6){            
            $tabla       = $this->despliegueVMWareSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("vmware", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
            
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 7){
            $tabla       = $this->despliegueUnixIBMSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("unixibm", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
            
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 8){
            $tabla       = $this->despliegueUnixOracleSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("unixoracle", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
            
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
        else if($fabricante == 10){            
            $tabla       = $this->despliegueSPLASam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            
            $i = 0;
            foreach($tabla as $row){
                if(count($tabla) > 0){
                    $fecha       = $row["fecha"];
                }

                $div .= $this->tablaDespliegue("spla", $fecha, $row["archivo"]);
                
                $div .= '<tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
            
                $i++;
            }
            
            $div .= $this->mostrarArchivoSam($fabricante);
        }
            
        $div .= '</tbody>
        </table>'
        . '</div>';
        return $div."*".count($tabla);
    }
    
    function mostrarArchivoSam($fabricante) {
        $div = '<div style="border:0px solid #000000;">'
        . '<table style="width:100%">';
        
        if($fabricante == 1){            
            $tabla1 = $this->archDespliegueAdobeSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1["cliente"]) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('adobe', 1, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 2){
            $tabla1 = $this->archDespliegueIbmSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1["cliente"]) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('ibm', 2, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 3){
            $tabla1 = $this->archDespliegueSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('microsoft', 3, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 4){
            $tabla1      = $this->archDespliegueOracleSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('oracle', 4, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 5){
            $tabla1      = $this->archDespliegueSAPSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1) > 0){
               $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('sap', 5, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 6){
            $tabla1      = $this->archDespliegueVMWareSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";
            
            if(count($tabla1) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('vmware', 6, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 7){
            $tabla1      = $this->archDespliegueUnixIBMSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('unixibm', 7, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 8){
            $tabla1      = $this->archDespliegueUnixOracleSam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('unixoracle', 8, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
        else if($fabricante == 10){
            $tabla1      = $this->archDespliegueSPLASam($_SESSION["client_id"], $_SESSION["client_empleado"]);
            $custom       = "";
            $custom1      = "";
            $fechaCustom  = "";
            $fechaCustom1 = "";
            $otros        = "";
            $otros1       = "";
            $fechaOtros   = "";
            $fechaOtros1  = "";

            if(count($tabla1) > 0){
                $custom       = $tabla1["custom"];
                $fechaCustom  = $tabla1["fechaCustom"];
                $custom1      = $tabla1["custom1"];
                $fechaCustom1 = $tabla1["fechaCustom1"];
                $otros        = $tabla1["otros"];
                $fechaOtros   = $tabla1["fechaOtros"];
                $otros1       = $tabla1["otros1"];
                $fechaOtros1  = $tabla1["fechaOtros1"];
            }
            $div .= $this->tablaArchivoDespliegue('spla', 10, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1);
        }
            
        $div .= '</tbody>
        </table>'
        . '</div>'
        . '<script>
            $(document).ready(function(){
                $("#buscarCustom").click(function(){
                    $("#custom").click();
                });
                
                $("#buscarCustom1").click(function(){
                    $("#custom1").click();
                });

                $("#buscarOtros").click(function(){
                    $("#otros").click();
                });
                
                $("#buscarOtros1").click(function(){
                    $("#otros1").click();
                });

                $("#custom").change(function(){
                    $("#fondo").show();
                    $("#tokenCustom").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#subirCustom")[0]);  
                    $.ajax({
                        type: "POST",
                        url: "' . $GLOBALS['domain_root'] . '/sam/subirCustom.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            result = data[0].result;
                            if(result == 0){
                                alert("No se cargó el archivo");
                            }
                            else if(result == 1){
                                alert("Se guardó el archivo con éxito");
                                $("#fechaCustom").empty();
                                $("#fechaCustom").append(data[0].archivo + " " + data[0].fecha);
                                $("#archivoCustom").val(data[0].archivo);
                                $("#mostrarCustom").show();
                                $("#eliminarCustom").show();
                            }
                            else if(result == 2){
                                alert("No se creo la carpeta del usuario");
                            }
                            $("#fondo").hide();
                        }
                    })
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                });
                
                $("#custom1").change(function(){
                    $("#fondo").show();
                    $("#tokenCustom1").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#subirCustom1")[0]); 
                    $.ajax({
                        type: "POST",
                        url: "' . $GLOBALS['domain_root'] . '/sam/subirCustom1.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            result = data[0].result;
                            if(result == 0){
                                alert("No se cargó el archivo");
                            }
                            else if(result == 1){
                                alert("Se guardó el archivo con éxito");
                                $("#fechaCustom1").empty();
                                $("#fechaCustom1").append(data[0].archivo + " " + data[0].fecha);
                                $("#archivoCustom1").val(data[0].archivo);
                                $("#mostrarCustom1").show();
                                $("#eliminarCustom1").show();
                            }
                            else if(result == 2){
                                alert("No se creo la carpeta del usuario");
                            }
                            $("#fondo").hide();
                        }
                    })
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                });

                $("#otros").change(function(){
                    $("#fondo").show();
                    $("#tokenOtros").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#subirOtros")[0]);   
                    $.ajax({
                        type: "POST",
                        url: "' . $GLOBALS['domain_root'] . '/sam/subirOtros.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            result = data[0].result;
                            if(result == 0){
                                alert("No se cargó el archivo");
                            }
                            else if(result == 1){
                                alert("Se guardó el archivo con éxito");
                                $("#fechaOtros").empty();
                                $("#fechaOtros").append(data[0].archivo + " " + data[0].fecha);
                                $("#archivoOtros").val(data[0].archivo);
                                $("#mostrarOtros").show();
                                $("#eliminarOtros").show();
                            }
                            else if(result == 2){
                                alert("No se creo la carpeta del usuario");
                            }
                            $("#fondo").hide();
                        }
                    })
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                });
                
                $("#otros1").change(function(){
                    $("#fondo").show();
                    $("#tokenOtros1").val(localStorage.licensingassuranceToken);
                    var formData = new FormData($("#subirOtros1")[0]);  
                    $.ajax({
                        type: "POST",
                        url: "' . $GLOBALS['domain_root'] . '/sam/subirOtros1.php", 
                        data: formData,
                        contentType: false,
                        processData: false,
                        dataType: "json",  
                        cache:false,
                        success: function(data){
                            if(data[0].resultado === false){
                                location.href = "' . $GLOBALS["domain_root"] . '";
                                return false;
                            }
                            if(data[0].sesion === "false"){
                                $.alert.open("error", "Error: " + data[0].mensaje, {"Aceptar" : "Aceptar"}, function() {
                                    location.href = "' . $GLOBALS['domain_root'] . '";
                                    return false;
                                });
                            }
                            result = data[0].result;
                            if(result == 0){
                                alert("No se cargó el archivo");
                            }
                            else if(result == 1){
                                alert("Se guardó el archivo con éxito");
                                $("#fechaOtros1").empty();
                                $("#fechaOtros1").append(data[0].archivo + " " + data[0].fecha);
                                $("#archivoOtros1").val(data[0].archivo);
                                $("#mostrarOtros1").show();
                                $("#eliminarOtros1").show();
                            }
                            else if(result == 2){
                                alert("No se creo la carpeta del usuario");
                            }
                            $("#fondo").hide();
                        }
                    })
                    .fail(function( jqXHR ){
                        $("#fondo").hide();
                        $.alert.open("error", "Error: " + jqXHR.status, {"Aceptar" : "Aceptar"}, function() {
                        });
                    });
                });
            });'
        . '</script>';
        return $div;
    }
    
    function tablaDespliegue($fabricante, $fecha, $archivo){
        $tituloFabricante = ucwords($fabricante);
        if($fabricante === "ibm"){
            $tituloFabricante = strtoupper($fabricante);
        }
        else if($fabricante === "vmware"){
            $tituloFabricante = "VMWare";
        }
        else if($fabricante === "unixibm"){
            $tituloFabricante = "UNIX-IBM";
        }
        else if($fabricante === "unixoracle"){
            $tituloFabricante = "UNIX-Oracle";
        }
        else if($fabricante === "sap"){
            $tituloFabricante = "SAP";
        }
        else if($fabricante === "spla"){
            $tituloFabricante = "SPLA";
        }
        $div = '<thead class="' . $archivo . '" style="background-color:#4E81D4;">
                <tr>
                    <th class="filaDesplegue">&nbsp;</th>
                    <th class="filaDesplegue">&nbsp;</th>';
                if($fabricante !== "vmware" && $fabricante !== "unixibm" && $fabricante !== "unixoracle" && $fabricante !== "sap"){
                        $div .= '<th colspan="4" class="filaDesplegue">Resumen</th>
                            <th colspan="4" class="filaDesplegue">Detalle</th>';
                    }
                    else if($fabricante === "unixibm" || $fabricante === "unixoracle" || $fabricante === "sap"){
                        $div .= '<th colspan="1" class="filaDesplegue">Resumen</th>
                            <th colspan="4" class="filaDesplegue">Detalle</th>';
                    }
                    else{
                        $div .= '<th colspan="2" class="filaDesplegue">Resumen</th>
                        <th colspan="4" class="filaDesplegue">Detalle</th>';
                    }
                    
                $div .= '</tr>
                <tr>
                    <th class="filaDesplegue">&nbsp;</th>
                    <th class="filaDesplegue">Archivos</th>';
                    if($fabricante !== "vmware" && $fabricante !== "unixibm" && $fabricante !== "unixoracle" && $fabricante !== "sap"){
                        $div .= '<th class="filaDesplegue">Alcance</th>
                        <th class="filaDesplegue">Usabilidad</th>
                        <th class="filaDesplegue">Balanza</th>
                        <th class="filaDesplegue">Optimizaci&oacute;n</th>';
                    }
                    else if($fabricante === "unixibm" || $fabricante === "unixoracle"){
                        $div .= '<th class="filaDesplegue">Balanza</th>';
                    }
                    else if($fabricante === "sap"){
                        $div .= '<th class="filaDesplegue">Balanza</th>
                        <th class="filaDesplegue">Usabilidad</th>';
                    }
                    else{
                        $div .= '<th class="filaDesplegue">Listado Contratos</th>
                        <th class="filaDesplegue">Balanza</th>';
                    }
                    
                    if($fabricante == "microsoft" || $fabricante == "spla"){
                        $div .= '<th class="filaDesplegue">Clientes</th>
                        <th class="filaDesplegue">Servidores</th>
                        <th class="filaDesplegue">Equipo</th>';
                    }
                    else{
                        $div .= '<th class="filaDesplegue">' . $tituloFabricante . '</th>';
                    }
                $div .= '</tr>
            </thead>
            <tbody class="' . $archivo . '" style="font-size:12px;">
                <tr style="border-bottom-style: 1px solid #ddd; height:70px;">                   
                    <td style="text-align:center"><img src="' . $GLOBALS["domain_root"] . '/imagenes/png/glyphicons_207_remove_2.png" class="pointer" onclick="eliminarRepoSAM(' .  $archivo . ')"></td>
                    <td style="text-align:center">
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                        if($fecha == ""){ $div .= "display:none;"; } 
                        $div .= '" onclick="archivosDespliegueSAM(' . $archivo . ')">
                            <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                            <p style="text-align:center;">' . $fecha . '</p>
                        </div>
                    </td>';
                    
                    if($fabricante !== "vmware" && $fabricante !== "unixibm" && $fabricante !== "unixoracle" && $fabricante !== "sap"){
                        $div .= '<td style="text-align:center">
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="alcance" onclick="repAlcance(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>
                        <td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="usabilidad" onclick="repUsabilidad(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>
                        <td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="balanza" onclick="repBalanza(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>
                        <td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="optimizacion" onclick="repOptimizacion(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>';
                    }
                    else if($fabricante === "unixibm" || $fabricante === "unixoracle"){
                        $div .= '<td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="balanza" onclick="repBalanza(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>';
                    }
                    else if($fabricante === "sap"){
                        $div .= '<td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="balanza" onclick="repBalanza(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>
                        <td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="usabilidad" onclick="repUsabilidad(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>';
                    }
                    else{
                        $div .= '<td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="listadoContrato" onclick="listadoContrato(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>
                        <td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="balanza" onclick="repBalanza(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>';
                    }
                    
                    if($fabricante == "microsoft" || $fabricante ==  "spla"){
                        $div .= '<td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="detalle" onclick="repDetalle(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>
                        <td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="servidor" onclick="repServidor(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>
                        <td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="servidor" onclick="repEquipo(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>';
                    }
                    else{
                        $div .= '<td style="text-align:center">
                            <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                            if($fecha == ""){ $div .= "display:none;"; } 
                            $div .= '" id="servidor" onclick="repFabricante(' . $archivo . ')">
                                <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                                <p style="text-align:center;">' . $fecha . '</p>
                            </div>
                        </td>';
                    }
            $div .= '</tr>';
        return $div;
    }
    
    function tablaArchivoDespliegue($fabricante, $idFab, $custom, $custom1, $fechaCustom, $fechaCustom1, $otros, $otros1, $fechaOtros, $fechaOtros1){
        $div = '<thead style="background-color:#4E81D4;">
                    <tr>
                        <th class="filaDesplegue">Custom</th>
                        <th class="filaDesplegue">Custom 1</th>
                        <th class="filaDesplegue">Otros</th>
                        <th class="filaDesplegue">Otros 1</th>
                    </tr>
            </thead>
            <tbody style="font-size:12px;">
                <tr style="border-bottom-style: 1px solid #ddd; height:70px;">  
                
                    <input type="hidden" id="archivoCustom" name="archivoCustom" value="' . $custom . '">
                    <input type="hidden" id="archivoCustom1" name="archivoCustom1" value="' . $custom1 . '">
                    <input type="hidden" id="archivoOtros" name="archivoOtros" value="' . $otros . '">
                    <input type="hidden" id="archivoOtros1" name="archivoOtros1" value="' . $otros1 . '">
                    
                    <td style="text-align:center; width:25%;">
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                        if($fechaCustom == ""){ $div .= "display:none;"; } 
                        $div .= '" id="mostrarCustom" onclick="bajarCustom(' . $_SESSION["client_id"] . ', ' . $_SESSION["client_empleado"] . ', \'' . $fabricante . '\')">
                            <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                        </div>
                        <p style="text-align:center;" id="fechaCustom">' . $custom . ' ' . $fechaCustom . '</p>

                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer;">
                            <div style="width:80px; margin-bottom:10px; ';
                            if($fechaCustom == ""){  
                                $div .= ' display:none;';  
                            }
                            $div .= '" class="botonesSAM boton5" id="eliminarCustom" onclick="eliminarCustom()">Eliminar</div>
                            <form id="subirCustom" name="subirCustom" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="tokenCustom" name="token">
                                <input type="hidden" id="idFab" name="idFab" value="' . $idFab . '">
                                <input type="file" id="custom" name="custom" style="display:none">
                                <div style="width:80px;" class="botonesSAM boton5" id="buscarCustom">Subir</div>
                            </form>
                        </div>
                    </td>
                    
                    <td style="text-align:center; width:25%;">
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                        if($fechaCustom1 == ""){ $div .= "display:none;"; } 
                        $div .= '" id="mostrarCustom1" onclick="bajarCustom(' . $_SESSION["client_id"] . ', ' . $_SESSION["client_empleado"] . ', \'' . $fabricante . '\', 1)">
                            <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                        </div>
                        <p style="text-align:center;" id="fechaCustom1">' . $custom1 . ' ' . $fechaCustom1 . '</p>
                            
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer;">
                            <div style="width:80px; margin-bottom:10px; ';
                            if($fechaCustom1 == ""){  
                                $div .= ' display:none;';  
                            }
                            $div .= '" class="botonesSAM boton5" id="eliminarCustom1" onclick="eliminarCustom(1)">Eliminar</div>
                            <form id="subirCustom1" name="subirCustom1" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="tokenCustom1" name="token">
                                <input type="hidden" id="idFab1" name="idFab" value="' . $idFab . '">
                                <input type="file" id="custom1" name="custom" style="display:none">
                                <div style="width:80px;" class="botonesSAM boton5" id="buscarCustom1">Subir</div>
                            </form>
                        </div>
                    </td>
                    
                    <td style="text-align:center; width:25%;">
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                        if($fechaOtros == ""){ $div .= "display:none;"; } 
                        $div .= '" id="mostrarOtros" onclick="bajarOtros(' . $_SESSION["client_id"] . ', ' . $_SESSION["client_empleado"] . ', \'' . $fabricante . '\')">
                            <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                        </div>
                        <p style="text-align:center;" id="fechaOtros">' . $otros . ' ' . $fechaOtros . '</p>
                            
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer;">
                            <div style="width:80px; margin-bottom:10px; ';
                            if($fechaOtros == ""){  
                                $div .= ' display:none;';  
                            }
                            $div .= '" class="botonesSAM boton5" id="eliminarOtros" onclick="eliminarOtros()">Eliminar</div>
                            <form id="subirOtros" name="subirOtros" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="tokenOtros" name="token">
                                <input type="hidden" id="idFab" name="idFab" value="' . $idFab . '">
                                <input type="file" id="otros" name="otros" style="display:none;">
                                <div style="width:80px;" class="botonesSAM boton5" id="buscarOtros">Subir</div>
                            </form>
                        </div>
                    </td>
                    
                    <td style="text-align:center; width:25%;">
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer; ';
                        if($fechaOtros1 == ""){ $div .= "display:none;"; } 
                        $div .= '" id="mostrarOtros1" onclick="bajarOtros(' . $_SESSION["client_id"] . ', ' . $_SESSION["client_empleado"] . ', \'' . $fabricante . '\', 1)">
                            <img src="'.$GLOBALS["domain_root"].'/imagenes/carpeta.png" style="width:70%; height:auto; margin-top:5px;">
                        </div>
                        <p style="text-align:center;" id="fechaOtros1">' . $otros1 . ' ' . $fechaOtros1 . '</p>
                            
                        <div style="overflow:hidden; height:auto; width:80px; line-height:25px; margin:auto; cursor:pointer;">
                            <div style="width:80px; margin-bottom:10px; ';
                            if($fechaOtros1 == ""){  
                                $div .= ' display:none;';  
                            }
                            $div .= '" class="botonesSAM boton5" id="eliminarOtros1" onclick="eliminarOtros(1)">Eliminar</div>
                            <form id="subirOtros1" name="subirOtros1" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="tokenOtros1" name="token">
                                <input type="hidden" id="idFab1" name="idFab" value="' . $idFab . '">
                                <input type="file" id="otros1" name="otros" style="display:none;">
                                <div style="width:80px;" class="botonesSAM boton5" id="buscarOtros1">Subir</div>
                            </form>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">&nbsp;</td>
                </tr>';
        return $div;
    }
    
    function GANTT_inicio($cliente, $empleado){
        $this->conexion();
        try {
            $query = "SELECT contrato.idContrato,
                    contrato.idFabricante,
                    contrato.fechaExpiracion,
                    contrato.fechaProxima,
                    IFNULL(ganttDetalle.P1, IFNULL(ganttDetalle.P2, IFNULL(ganttDetalle.P3, IFNULL(ganttDetalle.P4, '0000-00-00')))) AS presupuesto
                FROM contrato
                    INNER JOIN (
                        SELECT MAX(idContrato) idContrato
                        FROM contrato
                        WHERE contrato.idCliente = :cliente AND contrato.empleado = :empleado
                        GROUP BY idCliente, empleado, idFabricante
                        )t1 ON t1.idContrato = contrato.idContrato
                    INNER JOIN gantt ON contrato.idCliente = gantt.cliente AND gantt.id = (SELECT MAX(id) FROM gantt WHERE gantt.cliente = contrato.idCliente)
                    INNER JOIN ganttDetalle ON gantt.id = ganttDetalle.idGantt";
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente' => $cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function balanzaAsignacionSAM($cliente, $fabricante, $asignacion, $asignaciones){
        $this->conexion();
        try {
            $array = array(':cliente' => $cliente, ':fabricante'=>$fabricante);
            if($asignacion == ""){
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " detalleContrato.asignacion IN (" . $asignacion . ")";
            } else{
                $where = " detalleContrato.asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } 
            // dd($fa);
            if($fabricante == 1){
                $resumen = "(SELECT resumen_adobe.cliente,
                            resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version,
                            detalles_equipo_adobe.asignacion
                        FROM resumen_adobe
                            INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND
                            resumen_adobe.equipo = detalles_equipo_adobe.equipo
                        WHERE resumen_adobe.cliente = :cliente
                        GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version)";
            } else if($fabricante == 2){
                $resumen = "(SELECT resumen_ibm.cliente,
                            resumen_ibm.familia,
                            resumen_ibm.edicion,
                            resumen_ibm.version,
                            detalles_equipo_ibm.asignacion
                        FROM resumen_ibm
                            INNER JOIN detalles_equipo_ibm ON resumen_ibm.cliente = detalles_equipo_ibm.cliente AND
                            resumen_ibm.equipo = detalles_equipo_ibm.equipo
                        WHERE resumen_ibm.cliente = :cliente
                        GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version)";
            } else if($fabricante == 3){
                $resumen = "(SELECT resumen_office2.cliente,
                            resumen_office2.familia,
                            resumen_office2.edicion,
                            resumen_office2.version,
                            detalles_equipo2.asignacion
                        FROM resumen_office2
                            INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND
                            resumen_office2.equipo = detalles_equipo2.equipo
                        WHERE resumen_office2.cliente = :cliente
                        GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version)";
            } else if($fabricante == 4){
                $resumen = "(SELECT resumen_oracle.cliente,
                            resumen_oracle.familia,
                            resumen_oracle.edicion,
                            resumen_oracle.version,
                            detalles_equipo_oracle.asignacion
                        FROM resumen_oracle
                            INNER JOIN detalles_equipo_oracle ON resumen_oracle.cliente = detalles_equipo_oracle.cliente AND
                            resumen_oracle.equipo = detalles_equipo_oracle.equipo
                        WHERE resumen_oracle.cliente = :cliente
                        GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version)";
            }
            
            $query = "SELECT detalleContrato.idDetalleContrato,
                    detalleContrato.idContrato,
                    contrato.numero,
                    fabricantes.nombre AS fabricante,
                    productos.nombre AS familia,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    IFNULL(COUNT(balanza.familia), 0) AS instalaciones,
                    detalleContrato.cantidad,
                    detalleContrato.precio,
                    detalleContrato.asignacion
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :cliente
                    AND contrato.fechaExpiracion > CURDATE() AND contrato.idFabricante = :fabricante
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                    LEFT JOIN " . $resumen . " AS balanza
                    ON productos.nombre = balanza.familia AND ediciones.nombre = balanza.edicion AND versiones.nombre = balanza.version
                    AND detalleContrato.asignacion = balanza.asignacion
                WHERE " . $where . " AND detalleContrato.status = 1
                GROUP BY detalleContrato.idContrato, productos.nombre, ediciones.nombre, versiones.nombre, detalleContrato.asignacion
                ORDER BY detalleContrato.idContrato, productos.nombre, ediciones.nombre, versiones.nombre, detalleContrato.asignacion";
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function balanzaAsignacionSAMPaginado($cliente, $fabricante, $asignacion, $asignaciones, $pagina, $limite){
        $this->conexion();
        try {
            if ($pagina == "ultima") {
                $inicio = ($this->ultimaPaginaLicenciasAsignacion($cliente, $fabricante, $asignacion, $asignaciones, $limite) - 1) * $limite;
            } else {
                $inicio = ($pagina - 1) * $limite;
            }
            
            $array = array(':cliente' => $cliente, ':fabricante'=>$fabricante);
            if($asignacion == ""){
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " detalleContrato.asignacion IN (" . $asignacion . ")";
            } else{
                $where = " detalleContrato.asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } 
            
            if($fabricante == 1){
                $resumen = "(SELECT resumen_adobe.cliente,
                            resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version,
                            detalles_equipo_adobe.asignacion
                        FROM resumen_adobe
                            INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND
                            resumen_adobe.equipo = detalles_equipo_adobe.equipo
                        WHERE resumen_adobe.cliente = :cliente
                        GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version)";
            } else if($fabricante == 2){
                $resumen = "(SELECT resumen_ibm.cliente,
                            resumen_ibm.familia,
                            resumen_ibm.edicion,
                            resumen_ibm.version,
                            detalles_equipo_ibm.asignacion
                        FROM resumen_ibm
                            INNER JOIN detalles_equipo_ibm ON resumen_ibm.cliente = detalles_equipo_ibm.cliente AND
                            resumen_ibm.equipo = detalles_equipo_ibm.equipo
                        WHERE resumen_ibm.cliente = :cliente
                        GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version)";
            } else if($fabricante == 3){
                $resumen = "(SELECT resumen_office2.cliente,
                            resumen_office2.familia,
                            resumen_office2.edicion,
                            resumen_office2.version,
                            detalles_equipo2.asignacion
                        FROM resumen_office2
                            INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND
                            resumen_office2.equipo = detalles_equipo2.equipo
                        WHERE resumen_office2.cliente = :cliente
                        GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version)";
            } else if($fabricante == 4){
                $resumen = "(SELECT resumen_oracle.cliente,
                            resumen_oracle.familia,
                            resumen_oracle.edicion,
                            resumen_oracle.version,
                            detalles_equipo_oracle.asignacion
                        FROM resumen_oracle
                            INNER JOIN detalles_equipo_oracle ON resumen_oracle.cliente = detalles_equipo_oracle.cliente AND
                            resumen_oracle.equipo = detalles_equipo_oracle.equipo
                        WHERE resumen_oracle.cliente = :cliente
                        GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version)";
            }
            
            $query = "SELECT detalleContrato.idDetalleContrato,
                    detalleContrato.idContrato,
                    contrato.numero,
                    fabricantes.nombre,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    IFNULL(COUNT(balanza.familia), 0) AS instalaciones,
                    detalleContrato.cantidad,
                    detalleContrato.cantManual,
                    detalleContrato.instManual,
                    (detalleContrato.cantManual - detalleContrato.instManual) AS dispManual,
                    detalleContrato.precio,
                    detalleContrato.asignacion
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :cliente
                    AND contrato.fechaExpiracion > CURDATE() AND contrato.idFabricante = :fabricante
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                    LEFT JOIN " . $resumen . " AS balanza
                    ON productos.nombre = balanza.familia AND ediciones.nombre = balanza.edicion AND versiones.nombre = balanza.version
                    AND detalleContrato.asignacion = balanza.asignacion
                WHERE " . $where . " AND detalleContrato.status = 1
                GROUP BY detalleContrato.idContrato, productos.nombre, ediciones.nombre, versiones.nombre, detalleContrato.asignacion
                ORDER BY detalleContrato.idContrato, productos.nombre, ediciones.nombre, versiones.nombre, detalleContrato.asignacion
                LIMIT " . $limite;
                // dd($query);
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function actualizarInstalacionManual($idDetalle, $cantManual, $cantidad){
        $this->conexion();
        try {
            $query = "UPDATE detalleContrato SET cantManual = :cantManual, instManual = :cantidad WHERE idDetalleContrato = :idDetalle";
            $sql = $this->conn->prepare($query);
            $sql->execute(array(":idDetalle"=>$idDetalle, ":cantManual"=>$cantManual, ":cantidad"=>$cantidad));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function licenciasTrasladoSAM($cliente, $fabricante, $contrato, $familia, $edicion, $version, $asignacion, $asignaciones){
        $this->conexion();
        try {
            $array = array(':cliente' => $cliente, ':fabricante'=>$fabricante, ':contrato'=>"%" . $contrato . "%", 
            ':familia'=>"%" . $familia . "%", ':edicion'=>"%" . $edicion . "%", ':version'=>"%" . $version . "%");
            if($asignacion == ""){
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " detalleContrato.asignacion IN (" . $asignacion . ")";
            } else{
                $where = " detalleContrato.asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } 
            
            $where .= " AND productos.nombre LIKE :familia AND ediciones.nombre LIKE :edicion AND versiones.nombre LIKE :version ";
            
            if($fabricante == 1){
                $resumen = "(SELECT resumen_adobe.cliente,
                            resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version,
                            detalles_equipo_adobe.asignacion
                        FROM resumen_adobe
                            INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND
                            resumen_adobe.equipo = detalles_equipo_adobe.equipo
                        WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.familia LIKE :familia AND 
                        (resumen_adobe.edicion LIKE :edicion OR resumen_adobe.edicion IS NULL) AND 
                        (resumen_adobe.version LIKE :version OR resumen_adobe.version IS NULL)
                        GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version)";
            } else if($fabricante == 2){
                $resumen = "(SELECT resumen_ibm.cliente,
                            resumen_ibm.familia,
                            resumen_ibm.edicion,
                            resumen_ibm.version,
                            detalles_equipo_ibm.asignacion
                        FROM resumen_ibm
                            INNER JOIN detalles_equipo_ibm ON resumen_ibm.cliente = detalles_equipo_ibm.cliente AND
                            resumen_ibm.equipo = detalles_equipo_ibm.equipo
                        WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.familia LIKE :familia AND
                        (resumen_ibm.edicion LIKE :edicion OR resumen_ibm.edicion IS NULL) AND 
                        (resumen_ibm.version LIKE :version OR resumen_ibm.version IS NULL)
                        GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version)";
            } else if($fabricante == 3){
                $resumen = "(SELECT resumen_office2.cliente,
                            resumen_office2.familia,
                            resumen_office2.edicion,
                            resumen_office2.version,
                            detalles_equipo2.asignacion
                        FROM resumen_office2
                            INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND
                            resumen_office2.equipo = detalles_equipo2.equipo
                        WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia LIKE :familia AND
                        resumen_office2.edicion LIKE :edicion AND resumen_office2.version LIKE :version
                        GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version)";
            } else if($fabricante == 4){
                $resumen = "(SELECT resumen_oracle.cliente,
                            resumen_oracle.familia,
                            resumen_oracle.edicion,
                            resumen_oracle.version,
                            detalles_equipo_oracle.asignacion
                        FROM resumen_oracle
                            INNER JOIN detalles_equipo_oracle ON resumen_oracle.cliente = detalles_equipo_oracle.cliente AND
                            resumen_oracle.equipo = detalles_equipo_oracle.equipo
                        WHERE resumen_oracle.cliente = :cliente AND resumen_oracle.familia LIKE :familia AND
                        (resumen_oracle.edicion LIKE :edicion OR resumen_oracle.edicion IS NULL) AND 
                        (resumen_oracle.version LIKE :version OR resumen_oracle.version IS NULL)
                        GROUP BY resumen_oracle.equipo, resumen_oracle.familia, resumen_oracle.edicion, resumen_oracle.version)";
            }
            
            $query = "SELECT detalleContrato.idDetalleContrato,
                    detalleContrato.idContrato,
                    contrato.numero,
                    fabricantes.nombre AS nombre,
                    productos.nombre AS producto,
                    ediciones.nombre AS edicion,
                    versiones.nombre AS version,
                    IFNULL(COUNT(balanza.familia), 0) AS instalaciones,
                    detalleContrato.cantidad,
                    detalleContrato.instManual,
                    detalleContrato.cantManual,
                    detalleContrato.precio,
                    detalleContrato.asignacion
                FROM detalleContrato
                    INNER JOIN contrato ON detalleContrato.idContrato = contrato.idContrato AND contrato.idCliente = :cliente
                    AND contrato.fechaExpiracion > CURDATE() AND contrato.idFabricante = :fabricante AND contrato.numero LIKE :contrato
                    INNER JOIN fabricantes ON contrato.idFabricante = fabricantes.idFabricante
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                    LEFT JOIN " . $resumen . " AS balanza
                    ON productos.nombre = balanza.familia AND ediciones.nombre = balanza.edicion AND versiones.nombre = balanza.version
                    AND detalleContrato.asignacion = balanza.asignacion
                WHERE " . $where . " AND detalleContrato.status = 1
                GROUP BY detalleContrato.idContrato, productos.nombre, ediciones.nombre, versiones.nombre, detalleContrato.asignacion
                ORDER BY detalleContrato.idContrato, productos.nombre, ediciones.nombre, versiones.nombre, detalleContrato.asignacion";
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function obtenerDescripProductos($id){
        $this->conexion();
        try {
            $query = "SELECT detalleContrato.idContrato,
                    productos.idProducto, 
                    productos.nombre AS producto,
                    ediciones.idEdicion,
                    ediciones.nombre AS edicion,
                    versiones.id,
                    versiones.nombre AS version,
                    detalleContrato.sku,
                    detalleContrato.tipo,
                    detalleContrato.precio
                FROM detalleContrato
                    INNER JOIN productos ON detalleContrato.idProducto = productos.idProducto
                    INNER JOIN ediciones ON detalleContrato.idEdicion = ediciones.idEdicion
                    INNER JOIN versiones ON detalleContrato.version = versiones.id
                WHERE detalleContrato.idDetalleContrato = :id";
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id' => $id));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array('producto'=>'', 'edicion'=>'', 'version'=>'');
        }
    }
    
    function comprobarDetalleContrato($idContrato, $producto, $edicion, $version, $asignacion){
        $this->conexion();
        try {
            $query = "SELECT detalleContrato.idDetalleContrato
                FROM detalleContrato
                WHERE idContrato = :idContrato AND idProducto = :idProducto AND idEdicion = :idEdicion 
                AND version = :version AND asignacion = :asignacion";
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':idContrato' => $idContrato, ':idProducto'=>$producto, ':idEdicion'=>$edicion,
            ':version'=>$version, ':asignacion'=>$asignacion));
            return $sql->fetch();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array('idDetalleContrato'=>'');
        }
    }
    
    function fechaProximaUltContrato($cliente, $fabricante){
        $this->conexion();
        try {
            $query = "SELECT DATE_FORMAT(fechaProxima, '%d/%m/%Y') AS fechaProxima
                FROM contrato
                    INNER JOIN (SELECT MAX(idContrato) AS idContrato FROM contrato 
                    WHERE idCliente = :cliente AND idFabricante = :fabricante AND status = 1) AS cont ON 
                    contrato.idContrato = cont.idContrato";
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente' => $cliente, ':fabricante'=>$fabricante));
            $row = $sql->fetch();
            return $row["fechaProxima"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return "";
        }
    }
}   