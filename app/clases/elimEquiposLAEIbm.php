<?php

namespace App\Clases;

class elimEquiposLAEIbm extends General{
    public  $error        = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $nombre) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO elimEquiposLAEIbm (cliente, empleado, nombre, fecha) VALUES '
            . '(:cliente, :empleado, :nombre, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM elimEquiposLAEIbm WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarArchivo($cliente, $empleado, $nombre) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM elimEquiposLAEIbm WHERE cliente = :cliente AND empleado = :empleado AND nombre = :nombre');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}