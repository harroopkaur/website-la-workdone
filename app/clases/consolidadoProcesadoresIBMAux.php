<?php

namespace App\Clases;

class consolidadoProcesadoresIBMAux extends General{
    public $error;
    
    // Insertar 
    function insertar($cliente, $empleado, $datoControl, $hostName, $tipoCpu, $cpu, $cores, $procesadoresLogicos, $tipoEscaneo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO consolidadoProcesadoresIBMAux (cliente, empleado, dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) 
            VALUES(:cliente, :empleado, :datoControl, :hostName, :tipoCpu, :cpu, :cores, :procesadoresLogicos, :tipoEscaneo)");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado, 'datoControl'=>$datoControl , 'hostName'=>$hostName, 'tipoCpu'=>$tipoCpu, 'cpu'=>$cpu, 'cores'=>$cores, 'procesadoresLogicos'=>$procesadoresLogicos, 'tipoEscaneo'=>$tipoEscaneo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidadoProcesadoresIBMAux (cliente, empleado, dato_control, host_name, tipo_CPU, cpu, cores, procesadores_logicos, tipo_escaneo) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente,$empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM consolidadoProcesadoresIBMAux WHERE cliente = :cliente");
            $sql->execute(array('cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoProcesadores($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT consolidadoProcesadoresIBMAux.cliente,
                    consolidadoProcesadoresIBMAux.empleado,
                    consolidadoProcesadoresIBMAux.host_name,
                    consolidadoProcesadoresIBMAux.tipo_CPU,
                    CONCAT(IFNULL(resumen_ibm.familia, ''), ' ', IFNULL(resumen_ibm.edicion, ''), ' ', IFNULL(resumen_ibm.version, '')) AS producto,
                    consolidadoProcesadoresIBMAux.cpu,
                    consolidadoProcesadoresIBMAux.cores
                FROM consolidadoProcesadoresIBMAux
                    INNER JOIN resumen_ibm ON consolidadoProcesadoresIBMAux.cliente = resumen_ibm.cliente
                    AND consolidadoProcesadoresIBMAux.empleado = resumen_ibm.empleado AND
                    consolidadoProcesadoresIBMAux.host_name = resumen_ibm.equipo
                WHERE consolidadoProcesadoresIBMAux.cliente = :cliente AND consolidadoProcesadoresIBMAux.empleado = :empleado AND cpu > 0
                GROUP BY consolidadoProcesadoresIBMAux.cliente, consolidadoProcesadoresIBMAux.empleado, consolidadoProcesadoresIBMAux.host_name, 
                consolidadoProcesadoresIBMAux.tipo_CPU, producto");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}