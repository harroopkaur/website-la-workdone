<?php

namespace App\Clases;

class clase_procesar_consolidado_general extends General{
    private $iDatoControl;
    private $iHostName;
    private $iRegistro; 
    private $iEditor;
    private $iVersion;
    private $iDiaInstalacion;
    private $iSoftware;
    private $opcion; 
    private $idDiagnostic;
    private $opcionDespliegue;
    private $bloque;
    private $bloqueValores;
    private $insertarBloque;
    private $campoConsolidado;
    private $tabConsolidado;
    private $tabConsolidadoAux;
    private $client_id;
    private $client_empleado;
    private $datoControl;
    private $hostName; 
    private $registro; 
    private $editor; 
    private $version;
    private $diaInstalacion;
    private $software;
    private $idCorreo;
    
    private $tabDiagnosticos;
    
    //inicio consolidado otros
    private $instalAdmin; 
    private $instalRed;
    private $oInstalAdmin; 
    private $oInstalRed;
    //fin consolidado otros
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO ' . $this->tabConsolidado . ' (' . $this->campoConsolidado . ', dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarAppEscaneo($cliente, $idCorreo, $LAD) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO appEscaneo (cliente, idCorreo, LAD, fechaCarga) VALUES (:cliente, :idCorreo, :LAD, CURDATE())');
            $sql->execute(array(':cliente'=>$cliente, ':idCorreo'=>$idCorreo, ':LAD'=>$LAD));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarAppEscaneo($id, $LAD) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE appEscaneo SET LAD = :LAD, fechaCarga = CURDATE() WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':LAD'=>$LAD));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function existeAppEscaneo($cliente, $idCorreo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id, COUNT(*) AS cantidad 
                FROM appEscaneo 
                WHERE cliente = :cliente AND idCorreo = :idCorreo');
            $sql->execute(array(':cliente'=>$cliente, ':idCorreo'=>$idCorreo));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array("id"=>"", "cantidad"=>0);
        }
    }
    
    function insertarEnBloqueOtros($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO ' . $this->tabConsolidado . ' (' . $this->campoConsolidado . ', dato_control, host_name, registro, editor, version, fecha_instalacion, sofware, instalacionesAdmin, instalacionesRed) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminar($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM ' . $this->tabConsolidado . ' WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarAppEscaneo($cliente, $idCorreo) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM appConsolidado_offices WHERE cliente = :cliente AND '
            . 'idCorreo = :idCorreo');
            $sql->execute(array(':cliente'=>$cliente, ':idCorreo'=>$idCorreo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
        
    function insertarEnBloqueAux($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO ' . $this->tabConsolidadoAux . ' (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarAux($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM ' . $this->tabConsolidadoAux . ' WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarDiagnoticos($LAD) {
        $query = "INSERT INTO " . $this->tabDiagnosticos . " (fecha, LAD) ";
        $query .= "VALUES (NOW(), :LAD)";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':LAD'=>$LAD));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function actualizar($id, $nombre, $email, $archivo){
        $query = "UPDATE " . $this->tabDiagnosticos . " SET nombre = :nombre, email = :email, archivo = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre, ':email'=>$email, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        } 
    }
    
    function ultId(){
        $query = "SELECT MAX(id) AS id FROM " . $this->tabDiagnosticos;
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return 0;
        } 
    }
    
    function setTabDiagnosticos($opcion){
        if($opcion == "Cloud"){
            $this->tabDiagnosticos = "MSCloud";
        } else if($opcion == "Diagnostic"){
            $this->tabDiagnosticos = "SAMDiagnostic";
        } else if($opcion == "appEscaneo"){
            $this->tabDiagnosticos = "appEscaneo";
        }
    } 
    
    function setTabConsolidado($tabConsolidado){
        $this->tabConsolidado = $tabConsolidado;
    }
    
    function setTabConsolidadoAux($tabConsolidadoAux){
        $this->tabConsolidadoAux = $tabConsolidadoAux;
    }
    
    function procesarConsolidado($client_id, $client_empleado, $archivoConsolidado, $procesarAddRemove, $iDatoControl, $iHostName, $iRegistro, 
    $iEditor, $iVersion, $iDiaInstalacion, $iSoftware, $opcionDespliegue, $opcion, $idDiagnostic = 0, $idCorreo = 0){        
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->iDatoControl = $iDatoControl;
        $this->iHostName = $iHostName;
        $this->iRegistro = $iRegistro; 
        $this->iEditor = $iEditor; 
        $this->iVersion = $iVersion; 
        $this->iDiaInstalacion = $iDiaInstalacion; 
        $this->iSoftware = $iSoftware;
        $this->opcionDespliegue = $opcionDespliegue;
        $this->opcion = $opcion;
        $this->idDiagnostic = $idDiagnostic;
        $this->idCorreo = $idCorreo;
                
        $this->cabeceraTablas();
        
        if($this->obtenerSeparadorUniversal($archivoConsolidado, 2, 7) === true && $procesarAddRemove === true){
            if (($fichero = fopen($archivoConsolidado, "r")) !== false) {                
                $this->cicloInsertar($fichero);

                $this->verificarRegistrosInsertar1();

                fclose($fichero);
            }
        }
    }
    
    function procesarConsolidadoOtros($client_id, $client_empleado, $archivoConsolidado, $oNombreEquipo, 
    $oSoftware, $oVersion, $oFechaInstal, $oFabricante, $oInstalAdmin, $oInstalRed){  
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->iHostName = $oNombreEquipo;
        $this->iSoftware = $oSoftware;
        $this->iVersion = $oVersion; 
        $this->iDiaInstalacion = $oFechaInstal;
        $this->iEditor = $oFabricante;
        $this->oInstalAdmin = $oInstalAdmin; 
        $this->oInstalRed = $oInstalRed;
        $this->opcion = "otros";
                
        $this->cabeceraTablas();
        
        if($this->obtenerSeparadorUniversal($archivoConsolidado, 1, 7) === true){
            if (($fichero = fopen($archivoConsolidado, "r")) !== false) {  
                $this->cicloInsertarOtros($fichero);

                $this->verificarRegistrosInsertarOtros1();

                fclose($fichero);
            }
        }
    }
    
    function insertarGeneral(){
        if($this->opcion == "microsoft"){
            if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
                //echo $this->error;
            }

            if($this->opcionDespliegue != "completo" && $this->opcionDespliegue != "segmentado"){
                if(!$this->insertarEnBloqueAux($this->bloque, $this->bloqueValores)){
                    //echo $this->error;
                }
            }
        } else if($this->opcion == "Cloud" || $this->opcion == "Diagnostic" || $this->opcion == "appEscaneo"){
            if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
                //echo $this->error;
            }
        }
    } 
    
    function insertarGeneralOtros(){
        if(!$this->insertarEnBloqueOtros($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        }
    } 

    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        } else if( $this->opcion == "appEscaneo"){
            $inicioBloque = "(:cliente" . $j . ", :idCorreo" . $j . ", ";
        }
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":dato_control" . $j . ",:host_name" . $j . ", "
            . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":dato_control" . $j . ",:host_name" . $j . ", "
            . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ")";
        } 
    }
    
    function crearBloqueOtros($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
                
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":dato_control" . $j . ",:host_name" . $j . ", "
            . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ", "
            . ":instalAdmin" . $j . ", :instalRed" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":dato_control" . $j . ",:host_name" . $j . ", "
            . ":registro" . $j . ", :editor" . $j . ", :version" . $j . ", :fecha_instalacion" . $j . ", :sofware" . $j . ", "
            . ":instalAdmin" . $j . ", :instalRed" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j){        
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } elseif($this->opcion == "appEscaneo"){
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":idCorreo" . $j] = $this->idCorreo;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }
        
        $this->bloqueValores[":dato_control" . $j] = $this->datoControl;
        $this->bloqueValores[":host_name" . $j] = $this->hostName;
        $this->bloqueValores[":registro" . $j] = $this->registro;
        $this->bloqueValores[":editor" . $j] = $this->editor;
        $this->bloqueValores[":version" . $j] = $this->version;
        $this->bloqueValores[":fecha_instalacion" . $j] = $this->diaInstalacion;
        $this->bloqueValores[":sofware" . $j] = $this->software;
    }
    
    function crearBloqueValoresOtros($j){        
        $this->bloqueValores[":cliente" . $j] = $this->client_id;
        $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        
        $this->bloqueValores[":dato_control" . $j] = $this->datoControl;
        $this->bloqueValores[":host_name" . $j] = $this->hostName;
        $this->bloqueValores[":registro" . $j] = $this->registro;
        $this->bloqueValores[":editor" . $j] = $this->editor;
        $this->bloqueValores[":version" . $j] = $this->version;
        $this->bloqueValores[":fecha_instalacion" . $j] = $this->diaInstalacion;
        $this->bloqueValores[":sofware" . $j] = $this->software;
        $this->bloqueValores[":instalAdmin" . $j] = $this->instalAdmin;
        $this->bloqueValores[":instalRed" . $j] = $this->instalRed;
    }
    
    function cicloInsertar($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {                 
            if($i > 2 && isset($datos[$this->iHostName]) && $datos[$this->iHostName] != "" && $datos[$this->iHostName] != "HostName"){
                $this->crearBloque($j);

                $this->setValores($datos);
                $this->crearBloqueValores($j);

                $j = $this->verificarRegistrosInsertar($j);
                
                $j++;
            }
            
            $i++; 
        }
    }
    
    function cicloInsertarOtros($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {  
            if($i > 2 && isset($datos[$this->iHostName]) && $datos[$this->iHostName] != ""){
                $this->crearBloqueOtros($j);

                $this->setValoresOtros($datos);
                $this->crearBloqueValoresOtros($j);

                $j = $this->verificarRegistrosInsertarOtros($j);
                
                $j++;
            }
            
            $i++; 
        }
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function verificarRegistrosInsertarOtros($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneralOtros();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
    
    function verificarRegistrosInsertarOtros1(){
        if($this->insertarBloque === true){
            $this->insertarGeneralOtros();
        }
    }
    
    function cabeceraTablas(){
        if($this->opcion == "otros"){
            $this->tabConsolidado = "consolidado_others";
            $this->campoConsolidado = "cliente, empleado";
        }else if($this->opcion == "Cloud"){
            $this->tabConsolidado = "consolidado_MSCloud";
            $this->campoConsolidado = "idDiagnostic";  
        }else if($this->opcion == "Diagnostic"){
            $this->tabConsolidado = "consolidado_SAMDiagnostic";
            $this->campoConsolidado = "idDiagnostic";  
        } else if($this->opcion == "microsoft"){
            $this->tabConsolidado = "consolidado_offices";
            $this->tabConsolidadoAux = "consolidado_officesAux";
            $this->campoConsolidado = "cliente, empleado";
        } else if($this->opcion == "appEscaneo"){
            $this->tabConsolidado = "appConsolidado_offices";
            $this->tabConsolidadoAux = "";
            $this->campoConsolidado = "cliente, idCorreo";
        }
    }
    
    function setValores($datos){
        $this->datoControl = "";
        $this->hostName = ""; 
        $this->registro = ""; 
        $this->editor = ""; 
        $this->version = "";
        $this->diaInstalacion = 0;
        $this->software = "";
        
        if(isset($datos[$this->iDatoControl])){
            $this->datoControl = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iDatoControl])), 250);
        }
        
        if(isset($datos[$this->iHostName])){
            $this->hostName = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($datos[$this->iHostName]))), 250);
        }
        
        if(isset($datos[$this->iRegistro])){
            $this->registro = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iRegistro])), 250);
        }
        
        if(isset($datos[$this->iEditor])){
            $this->editor = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iEditor])), 250);
        }        
        
        if(isset($datos[$this->iVersion])){
            $this->version = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iVersion])), 250);
        } 
        
        if(filter_var($datos[$this->iDiaInstalacion], FILTER_VALIDATE_INT) !== false){
            $this->diaInstalacion = $datos[$this->iDiaInstalacion];
        } 

        if(isset($datos[$this->iSoftware])){
            $this->software = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iSoftware])), 250);
        } 
    }
    
    function setValoresOtros($datos){
        $this->datoControl = null;
        $this->hostName = ""; 
        $this->registro = null; 
        $this->editor = ""; 
        $this->version = "";
        $this->diaInstalacion = 0;
        $this->software = "";
        $this->instalAdmin = 0;
        $this->instalRed = 0;
        
        if(isset($datos[$this->iHostName])){
            $this->hostName = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($datos[$this->iHostName]))), 250);
        }
        
        if(isset($datos[$this->iEditor])){
            $this->editor = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iEditor])), 250);
        }        
        
        if(isset($datos[$this->iVersion])){
            $this->version = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iVersion])), 250);
        } 
        
        if(filter_var($datos[$this->iDiaInstalacion], FILTER_VALIDATE_INT) !== false){
            $this->diaInstalacion = $datos[$this->iDiaInstalacion];
        } 

        if(isset($datos[$this->iSoftware])){
            $this->software = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iSoftware])), 250);
        } 
        
        if(filter_var($datos[$this->oInstalAdmin], FILTER_VALIDATE_INT) !== false){
            $this->instalAdmin = $datos[$this->oInstalAdmin];
        } 
        
        if(filter_var($datos[$this->oInstalRed], FILTER_VALIDATE_INT) !== false){
            $this->instalRed = $datos[$this->oInstalRed];
        } 
    }
}
