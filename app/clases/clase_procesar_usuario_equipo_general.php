<?php

namespace App\Clases;

class clase_procesar_usuario_equipo_general extends General{
    private $client_id;
    private $client_empleado;
    private $archivoUsuarioEquipos;
    private $procesarUsuarioEquipos;
    private $mDatoControl; 
    private $mHostName; 
    private $mDominio;
    private $mUsuario;
    private $mIP;
    private $opcion; 
    private $idDiagnostic;
    private $tabUsuarioEquipo;
    private $campoUsuarioEquipo;
    private $datoControl; 
    private $hostName; 
    private $dominio;
    private $usuario;
    private $ip; 
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabUsuarioEquipo . " (" . $this->campoUsuarioEquipo . ", dato_control, host_name, dominio, usuario, ip) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM " . $this->tabUsuarioEquipo . " WHERE cliente = :cliente");
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function setTabUsuarioEquipo($tabUsuarioEquipo){
        $this->tabUsuarioEquipo = $tabUsuarioEquipo;
    } 
    
    function procesarUsuarioEquipo($client_id, $client_empleado, $archivoUsuarioEquipos, $procesarUsuarioEquipos, $mDatoControl, 
    $mHostName, $mDominio, $mUsuario, $mIP, $opcion, $idDiagnostic = 0){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado; 
        $this->archivoUsuarioEquipos = $archivoUsuarioEquipos;
        $this->procesarUsuarioEquipos = $procesarUsuarioEquipos; 
        $this->mDatoControl = $mDatoControl; 
        $this->mHostName = $mHostName; 
        $this->mDominio = $mDominio;
        $this->mUsuario = $mUsuario;
        $this->mIP = $mIP; 
        $this->opcion = $opcion; 
        $this->idDiagnostic = $idDiagnostic;
        
        $this->cabeceraTablas();
        
        if($this->obtenerSeparadorUniversal($this->archivoUsuarioEquipos, 2, 5) === true && $this->procesarUsuarioEquipos === true){
            if (($fichero = fopen($this->archivoUsuarioEquipos, "r")) !== false) {
                $this->cicloInsertar($fichero);

                $this->verificarRegistrosInsertar1();
            
                fclose($fichero);
            }
        }
    }
            
    function cabeceraTablas(){
        if($this->opcion == "microsoft"){
            $this->tabUsuarioEquipo = "usuarioEquipoMicrosoft";
            $this->campoUsuarioEquipo = "cliente, empleado";
        }
    }
    
    function cicloInsertar($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {                 
            if($i > 2 && $datos[$this->mDatoControl] != "Dato de Control"){
                $this->crearBloque($j);

                $this->setValores($datos);                
                $this->crearBloqueValores($j);

                $j = $this->verificarRegistrosInsertar($j);
                
                $j++;
            }
            
            $i++; 
        }
    }
    
    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", " 
            . ":usuario" . $j . ", :ip" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":dato_control" . $j . ", :host_name" . $j . ", :dominio" . $j . ", " 
            . ":usuario" . $j . ", :ip" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j){
        $this->bloqueValores[":cliente" . $j] = $this->client_id;
        $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        $this->bloqueValores[":dato_control" . $j] = $this->datoControl;
        $this->bloqueValores[":host_name" . $j] = $this->hostName;
        $this->bloqueValores[":dominio" . $j] = $this->dominio;
        $this->bloqueValores[":usuario" . $j] = $this->usuario;
        $this->bloqueValores[":ip" . $j] = $this->ip;
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function insertarGeneral(){
        if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        } 
    }     
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
    
    function setValores($datos){
        $this->datoControl = ""; 
        $this->hostName = ""; 
        $this->dominio = "";
        $this->usuario = "";
        $this->ip = null; 
        
        if(isset($datos[$this->mDatoControl])){
            $this->datoControl = $this->truncarString($this->get_escape(utf8_encode($datos[$this->mDatoControl])), 70);
        }

        if(isset($datos[$this->mHostName])){
            $this->hostName = $this->truncarString($this->extraerEquipo($this->get_escape(utf8_encode($datos[$this->mHostName]))), 70);   
        }

        if(isset($datos[$this->mDominio])){
            $this->dominio = $this->truncarString($this->get_escape(utf8_encode($datos[$this->mDominio])), 70);
        }
        
        if(isset($datos[$this->mUsuario])){
            $this->usuario = $this->truncarString($this->get_escape(utf8_encode($datos[$this->mUsuario])), 70);
        }
        
        if(isset($datos[$this->mIP])){
            $this->ip = $this->truncarString($this->get_escape(utf8_encode($datos[$this->mIP])), 20);
        }
    }
}