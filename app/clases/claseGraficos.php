<?php

namespace App\Clases;

class claseGraficos{
    
    function graficoAlcance($contenedor, $titulo, $totalUnidades, $reconciliacion, $activo, $color1, $color2){
        $grafico = "$('#" . $contenedor . "').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    size: '75%'
                },
                title: {
                    text: '" . $titulo . "'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.percentage:.0f}%</b><br/> Equipos: <b>{point.y:.0f}</b> de <b>" . $totalUnidades . "'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        showInLegend: true,
                        dataLabels: {
                            enabled: false,
                            format: '<b>{point.name}: <b>{point.percentage:.0f}%</b>',
                        }
                    }
                },
                series: [{
                    name: 'Equipos',
                    colorByPoint: true,
                    data: [{
                        name: 'No Levantado',
                        y: " . $reconciliacion . ",
                        color: '" . $color1 . "'
                    },
                    {
                        name: 'Levantado',
                        y: " . $activo . ",
                        color: '" . $color2 . "'
                    }]
                }]
            });";
        return $grafico;
    }
}

