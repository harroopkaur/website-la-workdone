<?php

namespace App\Clases;

class Resumen_Of extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;

    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $familia, $edicion, $version, $fecha_instalacion) {
        $this->conexion();
        $familia = trim($familia);
        $edicion = trim($edicion);
        $version = trim($version);
        $query = "INSERT INTO resumen_office2(cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES(:cliente, :empleado, :equipo, TRIM(:familia), TRIM(:edicion), TRIM(:version), :fecha_instalacion)";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO resumen_office2(cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES " . $bloque;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Eliminar
    function eliminar($cliente, $empleado) {
        $this->conexion();
        $query = "DELETE FROM resumen_office2 WHERE cliente = :cliente";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    /*function datos($id) {
        $this->conexion();
        $query = "SELECT * FROM resumen_office2 WHERE id = :id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            $usuario = $sql->fetch();
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->equipo = $usuario['equipo'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            $this->fecha_instalacion = $usuario['fecha_instalacion'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listaDesarrolloPruebaVS($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version,
                    resumen_office2.fecha_instalacion
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND 
                    resumen_office2.equipo = detalles_equipo2.equipo
                WHERE resumen_office2.cliente = :cliente AND 
                resumen_office2.familia = 'Visual Studio'");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM resumen_office2 WHERE cliente = :cliente AND empleado = :empleado ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    // Obtener listado de todos los Usuarios paginados
    /*function listar_todo_paginado($cliente, $inicio, $fin) {
        $this->conexion();
        $query = "SELECT * FROM resumen_office2 WHERE cliente = :cliente ORDER BY  id LIMIT " . $inicio . ", " . $fin;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todas las Inventarios
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        $this->conexion();
        $query = "SELECT COUNT(tabla.equipo) AS total "
            . "FROM (SELECT equipo "
                . "FROM resumen_office2 "
                . "WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version "
                . "GROUP BY equipo, familia, edicion, version) AS tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, 'empleado'=>$empleado, ':familia'=>$familia . '%', ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            if ($row["total"] == ""){
                return 0;
            }
            else{
                return $row["total"];
            }
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    function listar_datos3($cliente, $empleado, $familia, $edicion, $edicion1, $version) {
        $this->conexion();
        $query = "SELECT COUNT(tabla.equipo) AS cantidad "
            . "FROM (SELECT equipo "
                . "FROM resumen_office2 "
                . "WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion LIKE :edicion AND edicion NOT LIKE :edicion1 AND version = :version "
                . "GROUP BY equipo, familia, edicion, version) AS tabla";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia . '%', ':edicion'=>$edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function instalResumenAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        $array = array();
    $where = "";
        $array = array(':cliente'=>$cliente, ':edicion'=>'%' . $edicion . '%');
        if($familia != "Others"){
            $where1 = " AND resumen_office2.familia = :familia ";
            $array[":familia"] = $familia;
        } else{
            $where1 = " AND resumen_office2.familia IN ('Visual Studio', 'Exchange Server', 'Sharepoint Server', 'Skype for Business', 'System Center') ";
        }
        
        if($asignacion != ""){
            $where = " AND detalles_equipo2.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion; 
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            if(!empty($asignacion)) {
            $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") ";
        }
        }
        
        if ($edicion == "Professional"){
            $edicion = " AND (resumen_office2.edicion LIKE :edicion OR resumen_office2.edicion LIKE '%ProPlus%') ";
        }
        else{
            $edicion = " AND resumen_office2.edicion LIKE :edicion ";
        }
            
        $this->conexion();
        $query = "SELECT tabla.rango,
                    COUNT(tabla.rango) AS instalaciones
            FROM (SELECT detalles_equipo2.rango
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . " 
                WHERE resumen_office2.cliente = :cliente " . $where1 . $edicion . "
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) tabla
            GROUP BY tabla.rango";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function instalResumenAsignacionOtros($cliente, $familia, $asignacion, $asignaciones){
        $array = array(':cliente'=>$cliente, ':familia'=>$familia);
        if($familia == "Office" || $familia == "Project" || $familia == "Visio"){
            $notIn = "'Standard', 'Professional', 'Professional Plus', 'Office 365 ProPlus'";
        } else if($familia == "SQL Server"){
            $notIn = "'Standard', 'Datacanter', 'Enterprise'";
        }
       
        if($asignacion != ""){
            $where = " AND detalles_equipo2.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") ";
        }
        
        $this->conexion();
        $query = "SELECT tabla.rango,
                    COUNT(tabla.rango) AS instalaciones
            FROM (SELECT detalles_equipo2.rango
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . " 
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia = :familia
                AND resumen_office2.edicion NOT IN (" . $notIn . ")
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) tabla
            GROUP BY tabla.rango";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function instalResumenAsignacion1($cliente, $asignacion, $asignaciones){
        $array = array();
    $where = "";
        $array = array(':cliente'=>$cliente);
        if($asignacion != ""){
            $where = " AND detalles_equipo2.asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            if(!empty($asignacion)) {
            $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") ";
        }
    }
        
        $this->conexion();
        $query = "SELECT tabla.rango,
                    COUNT(tabla.rango) AS instalaciones
            FROM (SELECT detalles_equipo2.rango
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    AND resumen_office2.cliente = detalles_equipo2.cliente " . $where . " 
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.familia NOT IN ('Office', 'SQL Server')
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) tabla
            GROUP BY tabla.rango";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function softwareDuplicate($cliente, $asignacion, $asignaciones, $tipo = null){
        $array = array();
        $where = "";
        try{
            $array = array(':cliente'=>$cliente);
            
            if($asignacion != ""){
                $where = " AND detalles_equipo2.asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                if(!empty($asignacion)) {
                $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") ";
            }
        }
            
            $where1 = "";
            if ($tipo != null && $tipo == "cliente"){
                $where1 = " INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND 
                resumen_office2.equipo = detalles_equipo2.equipo AND detalles_equipo2.tipo = 1 ";
            } else if($tipo != null && $tipo == "servidor") {
                $where1 = " INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND 
                resumen_office2.equipo = detalles_equipo2.equipo AND detalles_equipo2.tipo = 2 ";
            } else if($tipo == null) {
                $where1 = " INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND 
                resumen_office2.equipo = detalles_equipo2.equipo AND detalles_equipo2.tipo IN (1, 2) ";
            }
        
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version
                FROM resumen_office2
                    INNER JOIN (SELECT tabla.equipo,
                                    tabla.familia,
                                    tabla.edicion,
                                    tabla.version
                                FROM (SELECT resumen_office2.equipo,
                                        resumen_office2.familia,
                                        resumen_office2.edicion,
                                        resumen_office2.version
                                    FROM resumen_office2
                                    WHERE resumen_office2.cliente = :cliente
                                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion,
                                    resumen_office2.version) tabla
                                GROUP BY tabla.equipo, tabla.familia
                                HAVING COUNT(tabla.equipo) > 1) tabla1 ON resumen_office2.equipo = tabla1.equipo AND
                                resumen_office2.familia = tabla1.familia
                    " . $where1 . "
                WHERE resumen_office2.cliente = :cliente " . $where . "
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return array();
        }
    }
    
    function softwareDuplicateEquiposGrafico($cliente, $tipo, $asignacion, $asignaciones){
        $array = array();
        $where = "";
        try{
            $array = array(':cliente'=>$cliente);
            
            if($asignacion != ""){
                $where = " AND detalles_equipo2.asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                if(!empty($asignacion)) {
                $where = " AND detalles_equipo2.asignacion IN (" . $asignacion . ") ";
            }
        }
            
            if ($tipo != null && $tipo == "cliente"){
                $where1 = " INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND 
                resumen_office2.equipo = detalles_equipo2.equipo AND detalles_equipo2.tipo = 1 ";
            } else if($tipo != null && $tipo == "servidor") {
                $where1 = " INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND 
                resumen_office2.equipo = detalles_equipo2.equipo AND detalles_equipo2.tipo = 2 ";
            }
        
            $this->conexion();
            $sql = $this->conn->prepare("SELECT equipo
                FROM (SELECT resumen_office2.equipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version
                FROM resumen_office2
                    INNER JOIN (SELECT tabla.equipo,
                                    tabla.familia,
                                    tabla.edicion,
                                    tabla.version
                                FROM (SELECT resumen_office2.equipo,
                                        resumen_office2.familia,
                                        resumen_office2.edicion,
                                        resumen_office2.version
                                    FROM resumen_office2
                                    WHERE resumen_office2.cliente = :cliente
                                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion,
                                    resumen_office2.version) tabla
                                GROUP BY tabla.equipo, tabla.familia
                                HAVING COUNT(tabla.equipo) > 1) tabla1 ON resumen_office2.equipo = tabla1.equipo AND
                                resumen_office2.familia = tabla1.familia
                    " . $where1 . "
                WHERE resumen_office2.cliente = :cliente " . $where . "
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) tabla1
                GROUP BY equipo");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosSinSoporte($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_office2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    resumen_office2.familia,
                    resumen_office2.edicion,
                    resumen_office2.version
                FROM resumen_office2
                    INNER JOIN detalleMaestra ON resumen_office2.familia = detalleMaestra.descripcion AND
                    resumen_office2.edicion = detalleMaestra.campo1 AND resumen_office2.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                    INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND 
                    resumen_office2.equipo = detalles_equipo2.equipo 
                WHERE resumen_office2.cliente = :cliente
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version

                UNION

                SELECT detalles_equipo2.equipo,
                    IF(detalles_equipo2.tipo = 1, 'Cliente', 'Servidor') AS tipo,
                    detalles_equipo2.familia,
                    detalles_equipo2.edicion,
                    detalles_equipo2.version
                FROM detalles_equipo2
                    INNER JOIN detalleMaestra ON detalles_equipo2.familia = detalleMaestra.descripcion AND
                    detalles_equipo2.edicion = detalleMaestra.campo1 AND detalles_equipo2.version = detalleMaestra.campo2
                    AND detalleMaestra.idMaestra = 11
                WHERE detalles_equipo2.cliente = :cliente
                GROUP BY detalles_equipo2.equipo, detalles_equipo2.familia, detalles_equipo2.edicion, detalles_equipo2.version");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function productosSinSoporteGrafico($cliente, $tipo){
        try{
            $this->conexion();
            
            $where1 = " AND detalles_equipo2.tipo = 1 ";
            $where = " INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND
            resumen_office2.equipo = detalles_equipo2.equipo " . $where1;
            
            if($tipo == "servidor"){
                $where1 = " AND detalles_equipo2.tipo = 2 ";
                $where = " INNER JOIN detalles_equipo2 ON resumen_office2.cliente = detalles_equipo2.cliente AND
                resumen_office2.equipo = detalles_equipo2.equipo " . $where1;
            }
            
            
            $sql = $this->conn->prepare("SELECT familia,
                        COUNT(familia) AS cantidad
                FROM (SELECT resumen_office2.equipo,
                        resumen_office2.familia,
                        resumen_office2.edicion,
                        resumen_office2.version
                    FROM resumen_office2
                        INNER JOIN detalleMaestra ON resumen_office2.familia = detalleMaestra.descripcion AND
                        resumen_office2.edicion = detalleMaestra.campo1 AND resumen_office2.version = detalleMaestra.campo2
                        AND detalleMaestra.idMaestra = 11
                        " . $where . "
                    WHERE resumen_office2.cliente = :cliente
                    GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version

                    UNION

                    SELECT detalles_equipo2.equipo,
                        detalles_equipo2.familia,
                        detalles_equipo2.edicion,
                        detalles_equipo2.version
                    FROM detalles_equipo2
                        INNER JOIN detalleMaestra ON detalles_equipo2.familia = detalleMaestra.descripcion AND
                        detalles_equipo2.edicion = detalleMaestra.campo1 AND detalles_equipo2.version = detalleMaestra.campo2
                        AND detalleMaestra.idMaestra = 11
                    WHERE detalles_equipo2.cliente = :cliente " . $where1 . "
                    GROUP BY detalles_equipo2.equipo, detalles_equipo2.familia, detalles_equipo2.edicion, detalles_equipo2.version) tabla1 
                GROUP BY familia");
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}