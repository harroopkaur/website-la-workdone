<?php

namespace App\Clases;

class UsabilidadUsuariosSAP extends General{
    ########################################  Atributos  ########################################
    public  $lista;
    public  $listaBaja;
    public  $listaGrafico;
    public  $listaBajaGrafico;
    public  $listaServidor;
    public  $error;
   
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($cliente, $empleado, $usuarios, $fechaCreacion, $fechaBaja, $alta, $baja, $diasUltEntrada) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO usabilidadUsuariosSap (cliente, empleado, usuarios, fechaCreacion, fechaBaja, alta, baja, diasUltEntrada) '
            . 'VALUES (:cliente, :empleado, :usuarios, :fechaCreacion, :fechaBaja, :alta, :baja, :diasUltEntrada)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':usuarios'=>$usuarios, ':fechaCreacion'=>$fechaCreacion, ':fechaBaja'=>$fechaBaja, ':alta'=>$alta, ':baja'=>$baja, ':diasUltEntrada'=>$diasUltEntrada));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM usabilidadUsuariosSap WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function listado($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM usabilidadUsuariosSap '
                . 'WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoSAM($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM usabilidadUsuariosSapSam '
                . 'WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoSAMArchivo($archivo){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT usuarios,
                    fechaCreacion,
                    fechaBaja,
                    alta,
                    baja,
                    diasUltEntrada,
                    CASE
                        WHEN diasUltEntrada >= 0 AND diasUltEntrada <= 30 THEN
                            "En Uso"
                        WHEN diasUltEntrada > 30 AND diasUltEntrada <= 60 THEN
                            "Probablemente en Uso"
                        ELSE
                            "Obsoleto"
                    END usabilidad '
                . 'FROM usabilidadUsuariosSapSam '
                . 'WHERE archivo = :archivo');
            $sql->execute(array(':archivo'=>$archivo));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoAlta($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM usabilidadUsuariosSap '
                . 'WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoAltaGrafico($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM usabilidadUsuariosSap '
                . 'WHERE cliente = :cliente AND empleado = :empleado AND alta > 0');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaGrafico = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoBaja($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM usabilidadUsuariosSap '
                . 'WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaBaja = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoBajaGrafico($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
                . 'FROM usabilidadUsuariosSap '
                . 'WHERE cliente = :cliente AND empleado = :empleado AND baja > 0');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listaBajaGrafico = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoUsabilidad($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT usuarios,
                    fechaCreacion,
                    fechaBaja,
                    alta,
                    baja,
                    diasUltEntrada,
                    CASE
                        WHEN diasUltEntrada >= 0 AND diasUltEntrada <= 30 THEN
                            "En Uso"
                        WHEN diasUltEntrada > 30 AND diasUltEntrada <= 60 THEN
                            "Probablemente en Uso"
                        ELSE
                            "Obsoleto"
                    END usabilidad
                FROM usabilidadUsuariosSap
                WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoUsabilidadGrafico($cliente, $empleado){
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(id) cantidad,
                    CASE
                        WHEN diasUltEntrada >= 0 AND diasUltEntrada <= 30 THEN
                            "En Uso"
                         WHEN diasUltEntrada > 30 AND diasUltEntrada <= 60 THEN
                            "Probablemente en Uso"
                         ELSE
                            "Obsoleto"
                    END usabilidad
                FROM usabilidadUsuariosSap
                WHERE cliente = :cliente AND empleado = :empleado
                GROUP BY usabilidad');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
}