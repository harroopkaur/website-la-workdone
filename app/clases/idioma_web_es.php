<?php
namespace App\clases;

class idioma_web_es{
    public $lg0001 = "es";
    
    //inicio navegacion
    public $lg0002 = "Inicio";
    public $lg0003 = "Sobre Nosotros";
    public $lg0004 = "Servicios";
    public $lg0005 = "SAM como Servicio";
    public $lg0006 = "Defensa de Auditoría";
    public $lg0007 = "SAM como Servicio para SPLA";
    public $lg0008 = "Diagnóstico de Despliegue";
    public $lg0009 = "Resumen";
    public $lg0010 = "Artículos";
    public $lg0011 = "Eventos";
    public $lg0012 = "Colombia";
    public $lg0013 = "México";
    public $lg0014 = "Contáctenos";
    public $ln0001 = "Webinar: Conozca cómo administrar el software SPLA mitigando los riesgos de Auditorías";
    public $ln0002 = "Webinar Licenciamiento SAP";
    //fin navegacion
    
    //inicio index
    public $lg0015 = "Optimización de Software Fácil y Eficazmente";
    public $lg0016 = "INNOVADA Y PERSONALIZADA";
    public $lg0017 = "SOLUCIÓN DE CONTROL DE SOFTWARE";
    public $lg0018 = "circulo 3_es.png";
    public $lg0019 = "circulo 1_es.png";
    public $lg0020 = "circulo 2_es.png";
    public $lg0021 = "gartner_es.png";
    public $lg0022 = "SAM COMO SERVICIO";
    public $lg0023 = "La mejor solución para ahorrar eliminando";
    public $lg0024 = "el sobre gasto de software rápidamente y a bajo costo";
    public $lg0025 = "best award_es.png";
    public $lg0026 = "SAM Chantal_es.png";
    public $lg0027 = "Más";
    public $lg0028 = "https://www.youtube.com/embed/-FHh-LuZn2w";
    public $lg0029 = "https://www.youtube.com/embed/kssmN_caomA";
    public $lg0030 = "https://www.youtube.com/embed/rj7D-wgX4NI";
    
    public $lg0031 = "Resultados Garantizados";
    public $lg0032 = "Conozca SAM as a Service";
    public $lg0033 = "Testimonios de Clientes";
    //fin index
    
    
    
    
    
    //inicio SAM as a Service
    public $lg0071 = "SOLUCIÓN COMPLETA";
    public $lg0073 = "gartner_es.png";
    public $lg0074 = "Es una solución multiplataforma de herramientas, personal, procesos, controles, indicadores (KPI) y 
                    metodología, con el objetivo de optimizar y controlar el software.";
    public $lg0075 = "circulo 2_es.png";
    public $lg0076 = "CS_es.png";
    public $lg0077 = "Eliminamos el software en desuso al 0%";
    public $lg0078 = "TENEMOS NUESTRAS PROPIAS HERRAMIENTAS ENFOCADAS EN EL AHORRO";
    public $lg0079 = "HERRAMIENTA DE MEDICIÓN LA";
    public $lg0080 = "Es una herramienta que entrega los resultados. Usabilidad del software de un agente sin ser un agente.";
    public $lg0081 = "LA TOOL";
    public $lg0082 = "Herramienta propia para el descubrimiento en el entorno de Windows";
    public $lg0083 = "WEBLOGIC";
    public $lg0084 = "Es un script que permite identificar a los usuarios que se conectan a esta plataforma, así como también 
                    obtener la edición y la versión instalada.";
    public $lg0085 = "TODO EN UNO";
    public $lg0086 = "Es un script que se ejecuta en Unix e incluye un conjunto de herramientas que le permiten obtener 
                    información de varios productos de Oracle al mismo tiempo.";
    public $lg0087 = "LA VTOOL";
    public $lg0088 = "Permite obtener la infraestructura de entorno virtualizada bajo VMWare";
    public $lg0089 = "BENEFICIOS";
    public $lg0090 = "-Mayor control para mejor ahorro";
    public $lg0091 = "-El mejor TCO";
    public $lg0092 = "-Servicio multiplataforma";
    //fin SAM as a Service
    
    //inicio audit defense
    
   
    public $lg0096 = "Experiencia de expertos en auditoría de software para ayudarle y proteger los activos de la empresa durante 
                    una auditoría de software fabricante.";
    public $lg0097 = "Nuestro servicio está orientado a ayudar a eliminar las auditorías de software de su empresa, yá que al tener 
                    su sistema de licencias controlado y con 0% de software en desuso, el riesgo de ser llamado para la auditoría 
                    se reduce casi en su totalidad.";
    public $lg0098 = "sello3_es.png";
    public $lg0099 = "BENEFICIOS";
    public $lg0100 = "Mitigación de auditoría";
    public $lg0101 = "Evaluación de escenarios";
    public $lg0102 = "Tácticas de defensa de auditoría";
    public $lg0103 = "Negociación con los fabricantes para obtener un valor agregado adicional";
    //fin audit defense
    
    //inicio SPLA
    public $lg0104 = "OPTIMICE SUS INFORMES MENSUALES DE FORMA FÁCIL Y RÁPIDA";
    public $lg0105 = "SAM COMO SERVICIO SPLA";
    public $lg0106 = "Presentamos el mejor servicio para los informes de licencias de SPLA. Una solución multiplataforma que
                    incluye herramientas, personal, procesos, controles, KPI y metodología para optimizar y controlar el 
                    gasto de software.";
    public $lg0107 = "Sabemos:<br>
                    -Está expuesto a auditorías rutinarias de software<br>
                    -El uso de software es continuamente superior<br>
                    -Podemos aumentar su rentabilidad con controles de software más efectivos<br>
                    -Usted tiene derecho a gestionar su software sin conflicto de intereses";
    public $lg0108 = "SERVICIO SPLA";
    public $lg0109 = "spla chantal_es.png";
    public $lg0110 = "CONTROLA, GENERA INGRESOS Y MITIGA LOS RIESGOS";
    //fin SPLA
    
    
    public $lg0114 = "DDWEB_es.png";
    public $lg0115 = "BENEFICIOS";
    public $lg0116 = "sello2_es.png";
    public $lg0117 = "Optimización de: duplicados, erróneos, inactivos, desuso, virtualización";
    public $lg0118 = "Mejor y mejor control de aplicaciones";
    public $lg0119 = "Resultados rápidos";
    public $lg0120 = "Fácil y eficaz";
    public $lg0121 = "Mejor control,... mayor rentabilidad";
    public $lg0122 = "https://www.youtube.com/embed/5rSk2DVqaPw";
    public $lgvid22 = "https://www.youtube.com/embed/6gAjtmw66ak";
    //fin deployment diagnostic
    
    //inicio overview
    public $lg0123 = "oi2_es.png";
    public $lg0153 = '<map name="map">
        <!-- #$-:Image map file created by GIMP Image Map plug-in -->
        <!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
        <!-- #$-:Please do not edit lines starting with "#$" -->
        <!-- #$VERSION:2.3 -->
        <!-- #$AUTHOR:Dx -->
        <area shape="rect" coords="330,68,650,337"  href="contacto.php" target="_blank"/>
        <area shape="rect" coords="669,67,987,335"  href="contacto.php" target="_blank"/>
        <area shape="rect" coords="1001,67,1322,340" href="contacto.php" target="_blank"/>
        <area shape="rect" coords="1328,13,1708,339" href="contacto.php" target="_blank"/>
        </map>';
    //fin overview
    
    //inicio articles
    public $lg0124 = "INFORMACIÓN IMPORTANTE Y OPORTUNA";
    public $lg0125 = "ARTÍCULOS DE LICENSING ASSURANCE";
    public $lg0126 = "https://www.gartner.com/newsroom/id/3382317";
    public $lg0127 = "Gartner dice que las organizaciones pueden reducir los costos de software en un 30 por ciento usando 
                    tres mejores prácticas.";
    public $lg0128 = "Muchas organizaciones pueden reducir el gasto en software hasta en un 30 por ciento implementando tres 
                    mejores prácticas de optimización de licencias de software, según una investigación de Gartner, Inc. 
                    Las claves para reducir el gasto de licencias de software son optimización de configuración de aplicaciones, 
                    reciclaje de licencias de software y uso de activos de software herramientas de gestión (SAM).";
    public $lg0129 = "http://factorypyme.thestandardit.com/2015/08/17/es-el-software-independiente-una-solucion-para-las-empresas/";
    public $lg0130 = "¿El software independiente es una solución para las empresas?";
    public $lg0131 = "Licensing Assurance es una empresa pionera en el mercado de administración de software de forma independiente, 
                    enfocada en la optimización y licencia de software. La compañía afirma que en América Latina, es importante 
                    contar con asesoramiento independiente para tomar decisiones al comprar software.";
    public $lg0132 = "http://samforyou-es.blogspot.com/";
    public $lg0133 = "5 errores a evitar durante la ejecución de los procesos de Gestión de activos de software.";
    public $lg0134 = "El SAM funciona según cuatro pasos o fases: primero se lleva a cabo un análisis de las instalaciones 
                    para realizar un proceso de diagnóstico de su estado. Luego se determina la ejecución de la licencia, 
                    identificando fallas y derechos. En el siguiente paso, se prepara un informe de todo lo investigado y 
                    finalmente procede a llevar a cabo la negociación, es decir, la compra de licencias. Para que pueda 
                    obtener más información sobre este tema, le mostraremos una breve selección de 5 errores comunes que 
                    deben evitarse al ejecutar procesos SAM, para que puedan ser más rentables.";
    public $lg0135 = "Tenemos muchas cosas más que decir, ... Visita nuestro Blog";
    public $lg0136 = "Hablemos";
    public $lg0155 = "Webinar: Guía Práctica de Supervivencia para Auditorías SPLA";
    public $lg0156 = "/WebinarSPLA.mp4";
    public $lg0157 = "Las auditorías de software SPLA se han convertido en un hecho recurrente en los últimos años. 
                     Teniendo en cuenta que su próxima auditoría puede estar más cerca de lo que piensa, es importante 
                     implementar las mejores prácticas para sobrevivir a una auditoría de software sin morir en el intento. 
                     La manera en la que su organización logre gestionar una Auditoría SPLA podría ser la diferencia entre 
                     resolver su caso de manera inteligente y eficaz o tener una pesadilla muy costosa.";
    //fin articles
    
   
    //inicio redireccionamiento navegacion
    public $nv0001 = "index.php";
    public $nv0002 = "sobreNosotros.php";
    public $nv0003 = "servicios.php";
    public $nv0004 = "SAMcomoServicio.php";
    public $nv0005 = "defensaAuditoria.php";
    public $nv0006 = "SPLA.php";
    public $nv0007 = "despliegue.php";
    public $nv0008 = "resumen.php";
    public $nv0009 = "articulos.php";
    public $nv0010 = "eventosColombia.php";
    public $nv0011 = "eventosMexico.php";
    public $nv0012 = "contacto.php";
    public $nv0013 = "webinar.php";
    public $nv0014 = "webinar_licenciamiento_SAP.php";
    //fin redireccionamiento navegacion
    
    //inicio foot
    public $ft0001 = "NUESTROS PRODUCTOS";
    public $ft0002 = "NUESTRO EQUIPO";
    public $ft0003 = "index.php?videos=true";
    public $ft0004 = "¡¡CONTÁCTENOS!!";
    public $ft0005 = "Teléfono: (305) 851-3545";
    public $ft0006 = "info@licensingassurance.com";
    public $ft0007 = "Licensing Assurance LLC";
    public $ft0008 = "16192 Coastal Highway";
    public $ft0009 = "Lewes, DE 19958";
    public $ft0010 = "Estamos disponibles";
    public $ft0011 = "Lunes - Viernes";
    public $ft0012 = "8:00 am - 5:00 pm";
    public $ft0013 = "Hora del Este";
    public $ft0014 = "Copyright Licensing Assurance LLC. Todos los Derechos Reservados";
    public $ft0015 = "Síguenos y comparte";
    //fin foot

    //why us
        public $wh0001 = "porque nosotros?";
        public $wh0002 = "Empresa independiente más confiable";
        public $wh0003 = "No estamos vinculados a los fabricantes.";
        public $wh0004 = "No hay conflictos de intereses";
        public $wh0005 = "No tenemos conflictos de intereses (vender VS guardar)";
        public $wh0006 = "Confidencialidad";
        public $wh0007 = "100% de confidencialidad y privacidad en la información.";
        public $wh0008 = "COMBINAMOS METODOLOGÍA, SERVICIO, HERRAMIENTAS Y KPI PARA OPTIMIZAR Y CONTROLAR ACTIVOS DE SOFTWARE AYUDANDO A AHORRAR Y MAXIMIZAR SU INVERSIÓN EN LICENCIAS";
    //why us close

      //OUR CUSTOMERS     
         public $oc0001 = "QUE DICEN LOS CLIENTES SOBRE NOSOTROS";
          public $oc0002 = " “Gracias a los servicios de LA, ahora hay una gran visibilidad de las licencias adquiridas y las necesidades reales de crecimiento en la empresa” ";
         public $oc0003 = " “Gracias al consejo de LA, pudimos identificar claramente las licencias y, por tanto, tener una optimización significativa en el uso de licencias ...” ";
         public $oc0004 = " “Estamos muy satisfechos con el trabajo realizado por Licensing Assurance, donde actuó con gran profesionalidad ... demostrando un amplio conocimiento en materia de licencias y poniéndose en todo momento del lado del cliente, brindando alternativas que nos permitieron mejorar nuestro contrato con proveedor de licencias mediante la reducción de los costos de la misma.” ";
    //OUR CUSTOMERS  close   

         //Subscribe start
             public $subs0001 = "Más de 30,000 ejecutivos leen nuestro boletín.";
             public $subs0002 = "Deberías alse";
             public $subs0003 = "Y disfrute de un solo clic, la mejor información de la mano de los expertos en licencias de todas las marcas.";
             public $subs0004 = "Suscribir";
    //Subscribe close  

             

       
  

        
      
}
