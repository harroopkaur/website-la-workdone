<?php

namespace App\Clases;

class resumenUnixOracle extends General{
    public  $lista = array();
    public  $error = NULL;
    
    /*function listar_datos2($cliente, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumen_UnixSolaris WHERE cliente = :cliente AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_OracleSolaris.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleSolaris
                     INNER JOIN `detalles_equipo_OracleSolaris` ON resumen_OracleSolaris.equipo = `detalles_equipo_OracleSolaris`.equipo
                WHERE resumen_OracleSolaris.cliente = :cliente AND resumen_OracleSolaris.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_OracleSolaris.id
                
                UNION 
                
                SELECT resumen_OracleAIX.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleAIX
                     INNER JOIN `detalles_equipo_OracleAIX` ON resumen_OracleAIX.equipo = `detalles_equipo_OracleAIX`.equipo
                WHERE resumen_OracleAIX.cliente = :cliente AND resumen_OracleAIX.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_OracleAIX.id
                
                UNION
                
                SELECT resumen_OracleLinux.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleLinux
                     INNER JOIN `detalles_equipo_OracleLinux` ON resumen_OracleLinux.equipo = `detalles_equipo_OracleLinux`.equipo
                WHERE resumen_OracleLinux.cliente = :cliente AND resumen_OracleLinux.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_OracleLinux.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_datos5Sam($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_OracleSolarisSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleSolarisSam
                     INNER JOIN detalles_equipo_OracleSolarisSam ON resumen_OracleSolarisSam.equipo = detalles_equipo_OracleSolarisSam.equipo
                WHERE resumen_OracleSolarisSam.cliente = :cliente AND familia = :familia
                GROUP BY resumen_OracleSolarisSam.id

                UNION

                SELECT resumen_OracleAIXSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleAIXSam
                     INNER JOIN detalles_equipo_OracleAIXSam ON resumen_OracleAIXSam.equipo = detalles_equipo_OracleAIXSam.equipo
                WHERE resumen_OracleAIXSam.cliente = :cliente AND familia = :familia
                GROUP BY resumen_OracleAIXSam.id

                UNION

                SELECT resumen_OracleLinuxSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleLinuxSam
                     INNER JOIN detalles_equipo_OracleLinuxSam ON resumen_OracleLinuxSam.equipo = detalles_equipo_OracleLinuxSam.equipo
                WHERE resumen_OracleLinuxSam.cliente = :cliente AND familia = :familia
                GROUP BY resumen_OracleLinuxSam.id");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5SamArchivo($archivo, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_OracleSolarisSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleSolarisSam
                     INNER JOIN detalles_equipo_OracleSolarisSam ON resumen_OracleSolarisSam.equipo = detalles_equipo_OracleSolarisSam.equipo
                WHERE resumen_OracleSolarisSam.archivo = :archivo AND familia = :familia
                GROUP BY resumen_OracleSolarisSam.id

                UNION

                SELECT resumen_OracleAIXSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleAIXSam
                     INNER JOIN detalles_equipo_OracleAIXSam ON resumen_OracleAIXSam.equipo = detalles_equipo_OracleAIXSam.equipo
                WHERE resumen_OracleAIXSam.archivo = :archivo AND familia = :familia
                GROUP BY resumen_OracleAIXSam.id

                UNION

                SELECT resumen_OracleLinuxSam.equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleLinuxSam
                     INNER JOIN detalles_equipo_OracleLinuxSam ON resumen_OracleLinuxSam.equipo = detalles_equipo_OracleLinuxSam.equipo
                WHERE resumen_OracleLinuxSam.archivo = :archivo AND familia = :familia
                GROUP BY resumen_OracleLinuxSam.id");
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{       
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_OracleSolaris.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleSolaris
                     INNER JOIN `detalles_equipo_OracleSolaris` ON resumen_OracleSolaris.equipo = `detalles_equipo_OracleSolaris`.equipo
                WHERE resumen_OracleSolaris.cliente = :cliente AND resumen_OracleSolaris.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                GROUP BY resumen_OracleSolaris.id
                
                UNION 
                
                SELECT resumen_OracleAIX.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleAIX
                     INNER JOIN `detalles_equipo_OracleAIX` ON resumen_OracleAIX.equipo = `detalles_equipo_OracleAIX`.equipo
                WHERE resumen_OracleAIX.cliente = :cliente AND resumen_OracleAIX.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                GROUP BY resumen_OracleAIX.id
                
                UNION
                
                SELECT resumen_OracleLinux.equipo AS equipo,
                    os,
                    versionOs,
                    virtual,
                    memoria,
                    cpu,
                    versionCpu,
                    core,
                    familia,
                    edicion,
                    version
                FROM resumen_OracleLinux
                     INNER JOIN `detalles_equipo_OracleLinux` ON resumen_OracleLinux.equipo = `detalles_equipo_OracleLinux`.equipo
                WHERE resumen_OracleLinux.cliente = :cliente AND resumen_OracleLinux.empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                GROUP BY resumen_OracleLinux.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos5Agrupado($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT familia,
                            edicion,
                            version
                        FROM resumen_OracleSolaris
                        WHERE cliente = :cliente AND empleado = :empleado AND familia = :familia
                        GROUP BY id) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_OracleSolaris
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version

                UNION

                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_OracleAIX
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version
                
                UNION

                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_OracleLinux
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datosAgrupado($cliente, $empleado, $familia) {        
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_OracleSolaris
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version
                
                UNION
                
                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_OracleAIX
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version
                
                UNION
                
                SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_OracleLinux
                    WHERE cliente = :cliente AND empleado = :empleado AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function graficoBalanza($cliente, $familia){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion($cliente, $familia, $edicion){
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.edicion = :edicion
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion1($cliente, $familia){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia = :familia
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
}