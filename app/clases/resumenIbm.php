<?php

namespace App\Clases;

class resumenIbm extends General{
    public  $lista = array();
    public  $error = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $familia, $edicion, $version, $fecha_instalacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_ibm (cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) VALUES '
            . '(:cliente, :empleado, :equipo, :familia, :edicion, :version, :fecha_instalacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_ibm (cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumen_ibm WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumen_ibm WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos5($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_ibm.id,
                resumen_ibm.cliente,
                resumen_ibm.equipo,
                IF(filepcs_ibm.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_ibm.familia,
                resumen_ibm.edicion,
                resumen_ibm.version,
                DATE_FORMAT(resumen_ibm.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                CASE
                    WHEN detalles_equipo_ibm.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_ibm.rango = 2 OR detalles_equipo_ibm.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_ibm
            INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo
            INNER JOIN filepcs_ibm ON resumen_ibm.equipo = filepcs_ibm.cn
        WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado AND resumen_ibm.familia = :familia
        GROUP BY resumen_ibm.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_datos5Sam($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_ibmSam.id,
                resumen_ibmSam.cliente,
                resumen_ibmSam.equipo,
                IF(detalles_equipo_ibmSam.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_ibmSam.familia,
                resumen_ibmSam.edicion,
                resumen_ibmSam.version,
                DATE_FORMAT(resumen_ibmSam.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                CASE
                    WHEN detalles_equipo_ibmSam.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_ibmSam.rango = 2 OR detalles_equipo_ibmSam.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_ibmSam
            INNER JOIN detalles_equipo_ibmSam ON resumen_ibmSam.equipo = detalles_equipo_ibmSam.equipo
        WHERE resumen_ibmSam.cliente = :cliente AND resumen_ibmSam.empleado = :empleado AND resumen_ibmSam.familia = :familia
        GROUP BY resumen_ibmSam.id
        ORDER BY resumen_ibmSam.familia, resumen_ibmSam.edicion, resumen_ibmSam.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5SamArchivo($archivo, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_ibmSam.id,
                resumen_ibmSam.cliente,
                resumen_ibmSam.equipo,
                IF(detalles_equipo_ibmSam.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_ibmSam.familia,
                resumen_ibmSam.edicion,
                resumen_ibmSam.version,
                DATE_FORMAT(resumen_ibmSam.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                CASE
                    WHEN detalles_equipo_ibmSam.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_ibmSam.rango = 2 OR detalles_equipo_ibmSam.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_ibmSam
            INNER JOIN detalles_equipo_ibmSam ON resumen_ibmSam.equipo = detalles_equipo_ibmSam.equipo
        WHERE resumen_ibmSam.archivo = :archivo AND resumen_ibmSam.familia = :familia
        GROUP BY resumen_ibmSam.id
        ORDER BY resumen_ibmSam.familia, resumen_ibmSam.edicion, resumen_ibmSam.version");
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_ibm.id,
                    resumen_ibm.cliente,
                    resumen_ibm.equipo,
                    IF(filepcs_ibm.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_ibm.familia,
                    resumen_ibm.edicion,
                    resumen_ibm.version,
                    DATE_FORMAT(resumen_ibm.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_ibm.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_ibm.rango = 2 OR detalles_equipo_ibm.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_ibm
                    INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo
                    INNER JOIN filepcs_ibm ON resumen_ibm.equipo = filepcs_ibm.cn
                WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_ibm.edicion LIKE :edicion
                GROUP BY resumen_ibm.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Asignacion($cliente, $familia, $edicion, $asignacion, $arrayAsignacion) {        
        try{   
            $where = " AND resumen_ibm.edicion = :edicion ";
            $asig = "";
            if($asignacion == ""){
                $asig = $this->stringConsulta($arrayAsignacion, "asignacion");
            } else{
                $asig = "'" . $asignacion . "'";
            }

            if($edicion == ""){
                $where = " AND (resumen_ibm.edicion = :edicion OR resumen_ibm.edicion IS NULL) ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_ibm.id,
                    resumen_ibm.cliente,
                    resumen_ibm.equipo,
                    IF(detalles_equipo_ibm.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_ibm.familia,
                    resumen_ibm.edicion,
                    resumen_ibm.version,
                    DATE_FORMAT(resumen_ibm.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_ibm.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_ibm.rango = 2 OR detalles_equipo_ibm.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_ibm
                    INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo AND asignacion IN (" . $asig . ")
                WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) " . $where . "
                GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Todos($cliente, $empleado, $familia) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_ibm.id,
                    resumen_ibm.cliente,
                    resumen_ibm.equipo,
                    IF(filepcs_ibm.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_ibm.familia,
                    resumen_ibm.edicion,
                    resumen_ibm.version,
                    DATE_FORMAT(resumen_ibm.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_ibm.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_ibm.rango = 2 OR detalles_equipo_ibm.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_ibm
                    INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo
                    INNER JOIN filepcs_ibm ON resumen_ibm.equipo = filepcs_ibm.cn
                WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_ibm.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6TodosAsignacion($cliente, $familia, $asignacion, $arrayAsignacion) {        
        try{    
            $asig = "";
            if($asignacion == ""){
                $asig = $this->stringConsulta($arrayAsignacion, "asignacion");
            } else{
                $asig = "'" . $asignacion . "'";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_ibm.id,
                    resumen_ibm.cliente,
                    resumen_ibm.equipo,
                    IF(detalles_equipo_ibm.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_ibm.familia,
                    resumen_ibm.edicion,
                    resumen_ibm.version,
                    DATE_FORMAT(resumen_ibm.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_ibm.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_ibm.rango = 2 OR detalles_equipo_ibm.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_ibm
                    INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo AND asignacion IN (" . $asig . ")
                WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos5Agrupado($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT resumen_ibm.familia,
                            resumen_ibm.edicion,
                            resumen_ibm.version
                        FROM resumen_ibm
                            INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo
                            INNER JOIN filepcs_ibm ON resumen_ibm.equipo = filepcs_ibm.cn
                        WHERE resumen_ibm.cliente = :cliente resumen_ibm.empleado = :empleado AND resumen_ibm.familia = :familia
                        GROUP BY resumen_ibm.id) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_ibm.familia,
                        resumen_ibm.edicion,
                        resumen_ibm.version
                    FROM resumen_ibm
                        INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo
                        INNER JOIN filepcs_ibm ON resumen_ibm.equipo = filepcs_ibm.cn
                    WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_ibm.edicion LIKE :edicion
                    GROUP BY resumen_ibm.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoAsignacion($cliente, $familia, $edicion, $asignacion, $arrayAsignacion) {  
        $where = " AND resumen_ibm.edicion = :edicion ";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion);
        
        $asig = "";
        if($asignacion == ""){
            $asig = $this->stringConsulta($arrayAsignacion, "asignacion");
        } else{
            $asig = "'" . $asignacion . "'";
        }
       
        if($edicion == ""){
            $where = " AND (resumen_ibm.edicion = :edicion OR resumen_ibm.edicion IS NULL) ";
        }
        
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_ibm.familia,
                        resumen_ibm.edicion,
                        resumen_ibm.version
                    FROM resumen_ibm
                        INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo AND asignacion IN (" . $asig . ")
                    WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) " . $where . " 
                    GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoTodos($cliente, $empleado, $familia) {        
        try{      
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_ibm.familia,
                        resumen_ibm.edicion,
                        resumen_ibm.version
                    FROM resumen_ibm
                        INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo
                        INNER JOIN filepcs_ibm ON resumen_ibm.equipo = filepcs_ibm.cn
                    WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY resumen_ibm.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoTodosAsignacion($cliente, $familia, $asignacion, $arrayAsignacion) {        
        try{                  
            $asig = "";
            if($asignacion == ""){
                $asig = $this->stringConsulta($arrayAsignacion, "asignacion");
            } else{
                $asig = "'" . $asignacion . "'";
            }
        
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_ibm.familia,
                        resumen_ibm.edicion,
                        resumen_ibm.version
                    FROM resumen_ibm
                        INNER JOIN detalles_equipo_ibm ON resumen_ibm.equipo = detalles_equipo_ibm.equipo AND asignacion IN (" . $asig . ")
                    WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function graficoBalanza($cliente, $empleado, $familia){
        try{       
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_ibm`
                    WHERE detalles_equipo_ibm.equipo = resumen_ibm.equipo AND detalles_equipo_ibm.cliente = resumen_ibm.cliente AND detalles_equipo_ibm.empleado = resumen_ibm.empleado
                    GROUP BY detalles_equipo_ibm.equipo) rango
                    FROM resumen_ibm
                    WHERE resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function graficoBalanzaAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        try{    
            $where = " resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
            $where1 = "";
            $array = array(':cliente'=>$cliente, ':familia'=>$familia);
           
            if($edicion == ""){
                $where .= " AND (resumen_ibm.edicion = '' OR resumen_ibm.edicion IS NULL) ";
            } else if($edicion != "Todo"){
                $where .= " AND resumen_ibm.edicion = :edicion ";
                $array[':edicion'] = $edicion;
            }
            
            if($asignacion != ""){
                $where1 = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where1 = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT IFNULL(COUNT(tabla.familia), 0) AS conteo,
                        tabla.rango
                FROM (SELECT resumen_ibm.familia,
                        detalles_equipo_ibm.rango
                    FROM resumen_ibm
                        INNER JOIN detalles_equipo_ibm ON resumen_ibm.cliente = detalles_equipo_ibm.cliente
                        AND resumen_ibm.equipo = detalles_equipo_ibm.equipo " . $where1 . "
                    WHERE " . $where . "
                    AND resumen_ibm.cliente = :cliente 
                    GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version) tabla
                GROUP BY tabla.rango");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function graficoBalanzaEdicion($cliente, $empleado, $familia, $edicion){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_ibm`
                    WHERE detalles_equipo_ibm.equipo = resumen_ibm.equipo AND detalles_equipo_ibm.cliente = resumen_ibm.cliente AND detalles_equipo_ibm.empleado = :empleado
                    GROUP BY detalles_equipo_ibm.equipo) rango
                    FROM resumen_ibm
                    WHERE resumen_ibm.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_ibm.edicion = :edicion
                    AND resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function graficoBalanzaEdicion1($cliente, $empleado, $familia){
        try{      
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_ibm`
                    WHERE detalles_equipo_ibm.equipo = resumen_ibm.equipo AND detalles_equipo_ibm.cliente = resumen_ibm.cliente AND detalles_equipo_ibm.empleado = :resumen_ibm.empleado 
                    GROUP BY detalles_equipo_ibm.equipo) rango
                    FROM resumen_ibm
                    WHERE resumen_ibm.familia = :familia
                    AND resumen_ibm.cliente = :cliente AND resumen_ibm.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function duplicado($cliente, $equipo, $familia, $edicion, $version){  
        // dd($cliente . ' & ' . $equipo . ' & ' . $familia . ' & ' . $edicion . ' & ' . $version);  
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.familia) AS cantidad
                FROM (SELECT resumen_ibm.familia
                    FROM resumen_ibm
                    WHERE resumen_ibm.cliente = :cliente AND resumen_ibm.equipo = :equipo AND resumen_ibm.familia = :familia AND
                    resumen_ibm.edicion = :edicion AND resumen_ibm.version = :version
                    GROUP BY resumen_ibm.equipo, resumen_ibm.familia, resumen_ibm.edicion, resumen_ibm.version) tabla");
            // 
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion,
            ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    } 
    
    /*function duplicadoSam($cliente, $equipo, $familia, $edicion, $version){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM resumen_ibmSam
                WHERE resumen_ibmSam.cliente = :cliente AND resumen_ibmSam.equipo = :equipo AND resumen_ibmSam.familia = :familia 
                AND resumen_ibmSam.edicion = :edicion AND resumen_ibmSam.version = :version");
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/   
    
    function duplicadoSamArchivo($archivo, $equipo, $familia){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM resumen_ibmSam
                WHERE resumen_ibmSam.archivo = :archivo AND resumen_ibmSam.equipo = :equipo AND resumen_ibmSam.familia = :familia");
            $sql->execute(array(':archivo'=>$archivo, ':equipo'=>$equipo, ':familia'=>$familia));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }   
}