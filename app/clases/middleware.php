<?php

namespace App\Clases;

//session_start();
class Middleware {

    private $llave_server = "5/98==79*brjFRH5l/03=f09x0e911*42";
    private $path;

    function __construct($path) {
        $this->path = $path;
    }
    
    private function comp_token($opcion = false) {
        return '<script>
            function comparar_tokens(token){
                if(localStorage.licensingassuranceToken  !==  token){
                    location.href = "' . $this->path . '";
                }
            }
            comparar_tokens("' . $this->obtener_token($opcion) . '");
        </script>';
    }
    
    private function comp_NADD_token() {
        return '<script>
            function comparar_tokens(token){
                if(localStorage.licensingassuranceNADDToken  !==  token){
                    location.href = "' . $this->path . '";
                }
            }
            comparar_tokens("' . $this->obtener_tokenNADD() . '");
        </script>';
    }
    
    private function comp_tokenAjax($token, $opcion) {
        $result = false;
        if($token === $this->obtener_token($opcion)){
            $result =  true;
        }
        return $result;
    }
    
    private function comp_NADD_tokenAjax($token) {
        $result = false;
        if($token === $this->obtener_tokenNADD()){
            $result =  true;
        }
        return $result;
    }

    public function obtener_token($opcion = false) {
        //$_SESSION["token"] = $this->encriptar_token();
        $header = [
                'typ'=>'JWT', 
                'alg'=>'HS256'
            ];
        $header = json_encode($header);
        $header = base64_encode($header);   
        
        
        
        if($opcion === true){
            $playload = [
                'iss'      =>'https://www.licensingassurance.com', 
                'nombre'   => $_SESSION['client_nombre'],
                'apellido' => $_SESSION['client_apellido'],
                'fecha'    => $_SESSION['client_fecha']
            ];
        }
        else{
            $playload = [
                'iss'      =>'https://www.licensingassurance.com', 
                'nombre'   => $_SESSION['usuario_nombre'],
                'apellido' => $_SESSION['usuario_apellido'],
                'fecha'    => $_SESSION['usuario_fecha']
            ];
        }
        
        $playload = json_encode($playload);
        $playload = base64_encode($playload);  
        
        $signature = hash_hmac('sha256', $header.".".$playload, $this->llave_server, true);
        $signature = base64_encode($signature);
        
        return $header.".".$playload.".".$signature; //$this->llave;
    }
    
    public function obtener_tokenNADD() {
        $header = [
                'typ'=>'JWT', 
                'alg'=>'HS256'
            ];
        $header = json_encode($header);
        $header = base64_encode($header);   
        
        $playload = [
            'iss'      =>'https://www.licensingassurance.com/NADD', 
            'nombre'   => $_SESSION['client_NADD_nombre'],
            'apellido' => $_SESSION['client_NADD_apellido'],
            'fecha'    => $_SESSION['client_NADD_fecha']
        ];
        
        $playload = json_encode($playload);
        $playload = base64_encode($playload);  
        
        $signature = hash_hmac('sha256', $header.".".$playload, $this->llave_server, true);
        $signature = base64_encode($signature);
        
        return $header.".".$playload.".".$signature; //$this->llave;
    }

    public function comparar($opcion = false) {
        return $this->comp_token($opcion);
    }
    
    public function compararNADD() {
        return $this->comp_NADD_token();
    }
    
    public function compararAjax($token, $opcion = false) {
        return $this->comp_tokenAjax($token, $opcion);
    }
    
    public function compararNADDAjax($token) {
        return $this->comp_NADD_tokenAjax($token);
    }
}
?>