<?php

namespace App\Clases;

class pvuClienteSolaris extends General {
    public $error;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $tipoProcesador, $producto, $conteoProcesadores, $conteoCore, 
    $usaProcesadores, $usaCore, $pvu, $conteo, $totalPvu) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO pvuClienteSolaris (cliente, empleado, servidor, tipoProcesador, "
            . "producto, conteoProcesadores, conteoCore, usaProcesadores, usaCore, pvu, conteo, totalPvu) "
            . "VALUES (:cliente, :empleado, :equipo, :tipoProcesador, :producto, :conteoProcesadores, :conteoCore, :usaProcesadores, "
            . ":usaCore, :pvu, :conteo, :totalPvu)");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':tipoProcesador'=>$tipoProcesador, 
            ':producto'=>$producto, ':conteoProcesadores'=>$conteoProcesadores, ':conteoCore'=>$conteoCore, ':usaProcesadores'=>$usaProcesadores, 
            ':usaCore'=>$usaCore, ':pvu'=>$pvu, ':conteo'=>$conteo, ':totalPvu'=>$totalPvu));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO pvuClienteSolaris (cliente, empleado, servidor, tipoProcesador, "
            . "producto, conteoProcesadores, conteoCore, usaProcesadores, usaCore, pvu, conteo, totalPvu) "
            . "VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function eliminar($cliente,$empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("DELETE FROM pvuClienteSolaris WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizar($id, $usaProcesadores, $usaCore) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare("UPDATE pvuClienteSolaris SET usaProcesadores = :usaProcesadores, usaCore = :usaCore "
            . "WHERE id = :id");
            $sql->execute(array(':id'=>$id,':usaProcesadores'=>$usaProcesadores, ':usaCore'=>$usaCore));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function procesarConsolidadoProcesadores($procesador){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT procesador.descripcion AS procesador,
                    modelo.descripcion AS modelo,
                    tablaPVU.pvu
                FROM tablaPVU
                    INNER JOIN detalleMaestra procesador ON tablaPVU.idProcesador = procesador.idDetalle AND
                    tablaPVU.idMaestraProcesador = procesador.idMaestra AND procesador.idMaestra = 8
                    INNER JOIN detalleMaestra modelo ON tablaPVU.idModeloServidor = modelo.idDetalle AND
                    tablaPVU.idMaestraModelo = modelo.idMaestra AND modelo.idMaestra = 9
                WHERE :procesador LIKE CONCAT('%', procesador.descripcion ,'%')
                AND (:procesador LIKE CONCAT('%', modelo.descripcion ,'%') OR modelo.descripcion = 'todos')");
            $sql->execute(array(':procesador'=>$procesador));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('procesador'=>'', 'modelo'=>'', 'pvu'=>'');
        }
    }
    
    function listadoPVUCliente($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT * "
                . "FROM pvuClienteSolaris "
                . "WHERE cliente = :cliente AND empleado = :empleado");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listadoProcesadoresUnixSolaris($cliente, $empleado){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT detalles_equipo_UnixSolaris.cliente,
                    detalles_equipo_UnixSolaris.empleado,
                    detalles_equipo_UnixSolaris.equipo,
                    detalles_equipo_UnixSolaris.versionCPU AS tipo_CPU,
                    CONCAT(IFNULL(resumen_UnixSolaris.familia, ''), ' ', IFNULL(resumen_UnixSolaris.edicion, ''), ' ', IFNULL(resumen_UnixSolaris.version, '')) AS producto,
                    detalles_equipo_UnixSolaris.cpu,
                    detalles_equipo_UnixSolaris.core
                FROM detalles_equipo_UnixSolaris
                     INNER JOIN resumen_UnixSolaris ON detalles_equipo_UnixSolaris.cliente = resumen_UnixSolaris.cliente AND
                     detalles_equipo_UnixSolaris.empleado = resumen_UnixSolaris.empleado AND detalles_equipo_UnixSolaris.equipo = resumen_UnixSolaris.equipo
                WHERE detalles_equipo_UnixSolaris.cliente = :cliente AND detalles_equipo_UnixSolaris.empleado = :empleado AND detalles_equipo_UnixSolaris.cpu > 0
                GROUP BY detalles_equipo_UnixSolaris.equipo, detalles_equipo_UnixSolaris.cpu, detalles_equipo_UnixSolaris.core");
            $sql->execute(array('cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}