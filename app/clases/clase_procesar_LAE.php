<?php

namespace App\Clases;

class clase_procesar_LAE extends General{
    private $client_id;
    private $client_empleado;
    private $idDiagnostic;
    private $fecha;
    private $baseLAE;
    private $archivo;
    private $tabLAE;
    private $tabLAEAux;
    private $campoLAE;
    private $campoLAE1;
    private $arrayLAE;
    private $bloqueValoresLAE1;
    private $opcionDespliegue;
    private $opcion;
    private $procesarLAE;
    
    private $iDN;
    private $iobjectClass;
    private $icn;
    private $iuserAccountControl;
    private $ilastLogon;
    private $ipwdLastSet;
    private $ioperatingSystem;
    private $ilastLogonTimestamp;
    private $iWhenCreated;
    private $DN;
    private $objectClass;
    private $cn;
    private $userAccountControl;
    private $lastLogon;
    private $pwdLastSet;
    private $sistema;
    private $lastLogonTimestamp;
    private $WhenCreated;
    
    private $tabDiagnosticos;
    
    function insertarEnBloque($bloque, $bloqueValores){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabLAE . " (" . $this->campoLAE . ", dn, objectclass, 
            cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloqueAux($bloque, $bloqueValores){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO " . $this->tabLAEAux . " (" . $this->campoLAE . ", dn, objectclass, 
            cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) 
            VALUES " . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertar($cliente, $idCorreo, $cn, $os){
        try{
            $this->conexion();
            $sql = $this->conn->prepare("INSERT INTO appFilepcs (cliente, idCorreo, dn, objectclass, 
            cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) 
            VALUES (:cliente, :idCorreo, NULL, NULL, :cn, NULL, NULL, NULL, :os, NULL)");
            $sql->execute(array(":cliente"=>$cliente, ":idCorreo"=>$idCorreo, ":cn"=>$cn, ":os"=>$os));
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarEquipo(){
        $this->conexion();
        $query = "UPDATE " . $this->tabLAE . " SET dn = :dn, objectclass = :objectclass, useracountcontrol = :useracountcontrol, 
        lastlogon = :lastlogon, pwdlastset = :pwdlastset, os = :os, lastlogontimes = :lastlogontimes WHERE cliente = :cliente 
        AND cn = :cn";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$this->client_id, ':dn'=>$this->dn, ':objectclass'=>$this->objectclass, ':cn'=>$this->cn, 
            ':useracountcontrol'=>$this->useracountcontrol, ':lastlogon'=>$this->lastlogon, ':pwdlastset'=>$this->pwdlastset, 
            ':os'=>$this->os,':lastlogontimes'=>$this->lastlogontimes));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function eliminar() {
        $this->conexion();
        $query = "DELETE FROM " . $this->tabLAE . " WHERE cliente = :cliente";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$this->client_id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function eliminarAux() {
        $this->conexion();
        $query = "DELETE FROM " . $this->tabLAEAux . " WHERE cliente = :cliente";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$this->client_id));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function eliminarAppEscaneo($cliente, $idCorreo) {
        $this->conexion();
        $query = "DELETE FROM appFilepcs WHERE cliente = :cliente AND idCorreo = :idCorreo";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente, ":idCorreo"=>$idCorreo));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function actualizarLAE($id, $archivo){
        $query = "UPDATE " . $this->tabDiagnosticos . " SET LAE = :archivo WHERE id = :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':id'=>$id, ':archivo'=>$archivo));
            return true;
        } catch (PDOException $e) {
            echo $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        } 
    }
    
    function codigo_existe($codigo, $id) {
        $query = "SELECT * FROM tabla_conversion WHERE codigo = :codigo AND id != :id";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':codigo'=>$codigo, ':id'=>$id));
            $row = $sql->fetch();
            if( $sql->rowCount() > 0){
                return true;
            }
            else{
                return false;
            }
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function datos2($codigo) {
        $query = "SELECT * FROM tabla_conversion WHERE codigo = :codigo";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':codigo'=>$codigo));
            $usuario = $sql->fetch();
            
            return $usuario['os'];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array("os"=>"");
        }
    }
    
    function obtenerSistema($sistema){
        if ($this->codigo_existe($sistema, 0)) {
            $sistema = $this->datos2($sistema);
        }
        
        return $sistema;
    }
    
    function existeEquipo() {
        $this->conexion();
        $query = "SELECT COUNT(cn) AS cantidad "
               . "FROM " . $this->tabLAE . " "
               . "WHERE " . $this->campoLAE1 . " AND cn = :cn";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute($this->bloqueValoresLAE1);
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return 0;
        }
    }
    
    function listadoEquiposDuplicados(){
        try {
            $this->conexion();
            $query = "SELECT MIN(id) AS id
                FROM " . $this->tabLAE . " 
                WHERE " . $this->campoLAE1 . "
                GROUP BY cn
                HAVING COUNT(cn) > 1";
            $sql = $this->conn->prepare($query);    
            $sql->execute($this->arrayLAE);
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return array();
        }
    } 
    
    function eliminarEquiposDespliegue() {
        try {
            $tabla = $this->listadoEquiposDuplicados();
            while(count($tabla) > 0){
                $elimId = $this->stringConsulta($tabla, "id");
                $query = "DELETE FROM " . $this->tabLAE . " WHERE " . $this->campoLAE1 . " AND id IN (" . $elimId . ")";
        
                $sql = $this->conn->prepare($query);        
                $sql->execute($this->arrayLAE);
                $tabla = $this->listadoEquiposDuplicados();
            }
 
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function setTabConsolidado($tabLAE){
        $this->tabLAE = $tabLAE;
    }
    
    function setTabConsolidadoAux($tabLAEAux){
        $this->tabLAEAux = $tabLAEAux;
    }
    
    function setTabDiagnosticos($opcion){
        if($opcion == "Cloud"){
            $this->tabDiagnosticos = "MSCloud";
        } else if($opcion == "Diagnostic"){
            $this->tabDiagnosticos = "SAMDiagnostic";
        }
    } 
    
    function moverArchivoLAE($client_id, $client_empleado, $temp, $incremArchivo, $opcionDespliegue, $opcion, $idDiagnostic = 0){
        $this->client_id = $client_id;
        $this->client_empleado = $client_empleado;
        $this->idDiagnostic = $idDiagnostic;
        $this->fecha = date("dmYHis");
        $this->opcionDespliegue = $opcionDespliegue;
        $this->opcion = $opcion;
       
        
        $this->cabeceraTablas();

        $this->archivo = "";
        if($this->client_id > 0 && $this->client_empleado > 0){
            $this->baseLAE = storage_path('app/files/'.$this->opcion.'/archivos_csvf2/');            
            
            $this->archivo .= $this->client_id . $this->client_empleado . "_";
        } else{
            $this->baseLAE = storage_path('app/files/archivosLAE');
        }
        $this->archivo .= "LAE_Output" . $this->fecha . ".csv";
        
        if($this->opcionDespliegue == "completo" || $this->opcionDespliegue == "segmentado"){
            move_uploaded_file($temp, $this->baseLAE . $this->archivo); 
            $this->eliminar();            
        } else {
            $this->baseLAE .= "incremento/";
            
            if(!file_exists($this->baseLAE)){
                mkdir($this->baseLAE, 0755, true);
            }
            
            move_uploaded_file($temp, $this->baseLAE . $this->archivo); 
            if($incremArchivo == 0){
                $this->eliminar();
            }
            $this->eliminarAux();
        }
       
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->setTabDiagnosticos($this->opcion);
            $this->actualizarLAE($idDiagnostic, $this->archivo);
        }
    }
    
    function procesarFilePCs($iDN, $iobjectClass, $icn, $iuserAccountControl, $ilastLogon, $ipwdLastSet, $ioperatingSystem, 
    $ilastLogonTimestamp, $iWhenCreated, $procesarLAE, $opcionDespliegue){        
        $this->iDN = $iDN;
        $this->iobjectClass = $iobjectClass;
        $this->icn = $icn;
        $this->iuserAccountControl = $iuserAccountControl;
        $this->ilastLogon = $ilastLogon;
        $this->ipwdLastSet = $ipwdLastSet;
        $this->ioperatingSystem = $ioperatingSystem;
        $this->ilastLogonTimestamp = $ilastLogonTimestamp;
        $this->iWhenCreated = $iWhenCreated;
        $this->procesarLAE = $procesarLAE;
        $this->opcionDespliegue = $opcionDespliegue;
                
        if($this->obtenerSeparadorUniversal($this->baseLAE . $this->archivo, 2, 10) === true && $this->procesarLAE === true){
            if (($fichero = fopen($this->baseLAE . $this->archivo, "r")) !== false) {
                $this->cicloInsertar($fichero);

                $this->verificarRegistrosInsertar1();
            
                fclose($fichero);
                
                $this->eliminarEquiposDespliegue();
            }
        }
    }
    
    function procesarLAEAppEscaneo($archivo, $procesarSO, $cliente, $idCorreo){
        if($this->obtenerSeparadorUniversal($archivo, 2, 4) === true && $procesarSO === true){
            if (($fichero = fopen($archivo, "r")) !== false) {
                $i = 1;
                while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {  
                    if($i > 2){
                        $cn = "";
                        if(isset($datos[1])){
                            $cn = $this->truncarString(utf8_encode($datos[1]), 250);
                        }

                        $this->insertar($cliente, $idCorreo, $cn, $datos[2]);
                    }
                    $i++;
                }
            
                fclose($fichero);
            }
        }
    }
    
    function cabeceraTablas(){
        if($this->opcion == "Cloud"){
            $this->tabLAE = "filepcsMSCloud";
            $this->campoLAE = "idDiagnostic";
            $this->campoLAE1 = "idDiagnostic = :idDiagnostic";
            $this->bloqueValoresLAE1 = array(":idDiagnostic"=>$this->idDiagnostic, ":cn"=>$this->cn);
            $this->arrayLAE = array(":idDiagnostic"=>$this->idDiagnostic);
        }else if($this->opcion == "Diagnostic"){
            $this->tabLAE = "filepcsSAMDiagnostic";
            $this->campoLAE = "idDiagnostic";
            $this->campoLAE1 = "idDiagnostic = :idDiagnostic";
            $this->bloqueValoresLAE1 = array(":idDiagnostic"=>$this->idDiagnostic, ":cn"=>$this->cn);
            $this->arrayLAE = array(":idDiagnostic"=>$this->idDiagnostic);
        } else if($this->opcion == "microsoft"){
            $this->tabLAE = "filepcs2";
            $this->tabLAEAux = "filepcsAux";
            $this->campoLAE = "cliente, empleado";
            $this->campoLAE1 = "cliente = :cliente";
            $this->bloqueValoresLAE1 = array(":cliente"=>$this->client_id, ":cn"=>$this->cn);
            $this->arrayLAE = array(":cliente"=>$this->client_id);
        } else if($this->opcion == "appEscaneo"){
            $this->tabLAE = "appFilepcs";
            $this->campoLAE = "idDiagnostic, idCorreo";
            $this->campoLAE1 = "idDiagnostic = :idDiagnostic";
            $this->bloqueValoresLAE1 = array(":idDiagnostic"=>$this->idDiagnostic, ":cn"=>$this->cn);
            $this->arrayLAE = array(":idDiagnostic"=>$this->idDiagnostic);
        } else if($this->opcion == "oracle"){
            $this->tabLAE = "filepcs_oracle";
            $this->tabLAEAux = "filepcs_oracleAux";
            $this->campoLAE = "cliente, empleado";
            $this->campoLAE1 = "cliente = :cliente";
            $this->bloqueValoresLAE1 = array(":cliente"=>$this->client_id, ":cn"=>$this->cn);
            $this->arrayLAE = array(":cliente"=>$this->client_id);
        }
    }
    
    function cicloInsertar($fichero){
        $i = 1;
        $j = 0;
        $this->bloque = "";
        $this->bloqueValores = array();
        $this->insertarBloque = false;
        
        while (($datos = fgetcsv($fichero, 1000, $this->separador)) !== false) {  
            $this->cn = "";
            if(isset($datos[$this->icn])){
                $this->cn = $this->truncarString(utf8_encode($datos[$this->icn]), 250);
            }
            
            $existeEquipo = $this->existeEquipo();     
            
            if ($i > 1 && $existeEquipo == 0) {
                $this->crearBloque($j);

                $this->setValores($datos);                
                $this->crearBloqueValores($j);

                $j = $this->verificarRegistrosInsertar($j);
                
                $j++;
            } else if ($i > 1 && $existeEquipo > 0) {
                $this->setValores($datos);      
                $this->actualizarEquipo();
            }
            
            $i++; 
        }
    }
    
    function crearBloque($j){
        $inicioBloque = "(:cliente" . $j . ", :empleado" . $j . ", ";
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $inicioBloque = "(:idDiagnostic" . $j . ", ";
        } else if($this->opcion == "appEscaneo"){
            $inicioBloque = "(:idDiagnostic" . $j . ", idCorreo" . $j . ", ";
        }
        
        if($j == 0){
            $this->insertarBloque = true;
            $this->bloque .= $inicioBloque . ":dn" . $j . ", :objectclass" . $j . ", "
            . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
            . ":os" . $j . ", :lastlogontimes" . $j . ")";
        } else {
            $this->bloque .= ", " . $inicioBloque . ":dn" . $j . ", :objectclass" . $j . ", "
            . ":cn" . $j . ", :useracountcontrol" . $j . ", :lastlogon" . $j . ", :pwdlastset" . $j . ", "
            . ":os" . $j . ", :lastlogontimes" . $j . ")";
        } 
    }
    
    function crearBloqueValores($j){
        if($this->opcion == "Cloud" || $this->opcion == "Diagnostic"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
        } else if($this->opcion == "appEscaneo"){
            $this->bloqueValores[":idDiagnostic" . $j] = $this->idDiagnostic;
            $this->bloqueValores[":idCorreo" . $j] = $this->idCorreo;
        } else{
            $this->bloqueValores[":cliente" . $j] = $this->client_id;
            $this->bloqueValores[":empleado" . $j] = $this->client_empleado;
        }
        
        $this->bloqueValores[":dn" . $j] = $this->DN;
        $this->bloqueValores[":objectclass" . $j] = $this->objectClass;
        $this->bloqueValores[":cn" . $j] = $this->cn;
        $this->bloqueValores[":useracountcontrol" . $j] = $this->userAccountControl;
        $this->bloqueValores[":lastlogon" . $j] = $this->lastLogon;
        $this->bloqueValores[":pwdlastset" . $j] = $this->pwdLastSet;
        $this->bloqueValores[":os" . $j] = $this->sistema;
        $this->bloqueValores[":lastlogontimes" . $j] = $this->lastLogonTimestamp;
    }
    
    function verificarRegistrosInsertar($j){
        if($j == $this->registrosBloque){
            $this->insertarGeneral();

            $this->bloque = "";
            $this->bloqueValores = array();
            $j = -1;
            $this->insertarBLoque = false; 
        }
        
        return $j;
    }
    
    function insertarGeneral(){
        if(!$this->insertarEnBloque($this->bloque, $this->bloqueValores)){    
            //echo $this->error;
        } 
        
        if($this->opcionDespliegue != "completo" && $this->opcionDespliegue != "segmentado"){
            if(!$this->insertarEnBloqueAux($this->bloque, $this->bloqueValores)){
                //echo $this->error;
            }
        }
    }     
    
    function verificarRegistrosInsertar1(){
        if($this->insertarBloque === true){
            $this->insertarGeneral();
        }
    }
    
    function setValores($datos){
        $this->DN = "";
        if(isset($datos[$this->iDN])){
            $this->DN = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iDN])), 250);
        }
        
        $this->objectClass = "";
        if(isset($datos[$this->iobjectClass])){
            $this->objectClass = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iobjectClass])), 250);
        }
        
        $this->userAccountControl = "";
        if(isset($datos[$this->iuserAccountControl])){
            $this->userAccountControl = $this->truncarString($this->get_escape(utf8_encode($datos[$this->iuserAccountControl])), 250);
        }

        $this->lastLogon = "";
        if(isset($datos[$this->ilastLogon])){
            $this->lastLogon = $this->truncarString($this->get_escape(utf8_encode($datos[$this->ilastLogon])), 250);
        }
        
        $this->pwdLastSet = "";
        if(isset($datos[$this->ipwdLastSet])){
            $this->pwdLastSet = $this->truncarString($this->get_escape(utf8_encode($datos[$this->ipwdLastSet])), 250);
        }
                
        $this->sistema = "";
        if(isset($datos[$this->ioperatingSystem])){
            $this->sistema = $this->obtenerSistema($datos[$this->ioperatingSystem]);
        }            

        $this->lastLogonTimestamp = "";
        if(isset($datos[$this->ilastLogonTimestamp])){
            $this->lastLogonTimestamp = $this->truncarString($this->get_escape(utf8_encode($datos[$this->ilastLogonTimestamp])), 250);
        }
    }
}
