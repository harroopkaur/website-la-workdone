<?php

namespace App\Clases;

class resumenUnixLinux extends General{
    public  $lista = array();
    public  $error = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $producto, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_UnixLinux (cliente, empleado, equipo, familia, edicion, version) VALUES '
            . '(:cliente, :empleado, :equipo, :producto, :edicion, :version)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':producto'=>$producto, ':edicion'=>$edicion, ':version'=>$version));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumen_UnixLinux WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumen_UnixLinux WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>"%".$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    /*function listar_datos5($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
                from resumen_UnixLinux
                WHERE cliente = :cliente AND familia = :familia
                GROUP BY id");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function listar_datos5Sam($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
                FROM resumen_UnixLinux
                WHERE cliente = :cliente AND familia = :familia
                GROUP BY id
                ORDER BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function listar_datos6($cliente, $familia, $edicion) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT *
                FROM resumen_UnixLinux
                WHERE cliente = :cliente AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                GROUP BY id");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function listar_datos5Agrupado($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT familia,
                            edicion,
                            version
                        FROM resumen_UnixLinux
                        WHERE cliente = :cliente AND familia = :familia
                        GROUP BY id) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function listar_datos6Agrupado($cliente, $familia, $edicion) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixLinux
                    WHERE cliente = :cliente AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND edicion = :edicion
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function listar_datosAgrupado($cliente, $familia) {        
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        familia,
                        edicion,
                        version
                    FROM resumen_UnixAIX
                    WHERE cliente = :cliente AND familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanza($cliente, $familia){
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion($cliente, $familia, $edicion){
        try{      
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_oracle.edicion = :edicion
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    /*function graficoBalanzaEdicion1($cliente, $familia){
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_oracle`
                    WHERE detalles_equipo_oracle.equipo = resumen_oracle.equipo AND detalles_equipo_oracle.cliente = resumen_oracle.cliente
                    GROUP BY detalles_equipo_oracle.equipo) rango
                    FROM resumen_oracle
                    WHERE resumen_oracle.familia = :familia
                    AND resumen_oracle.cliente = :cliente) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
}