<?php

namespace App\Clases;

class pais_licenc extends clase_general_licenciamiento{
    function listar_todo() {
        $this->conexion();
        $query = "SELECT * FROM admin002 WHERE status!=0 ORDER BY nombre";
        try {
            $sql = $this->conex->prepare($query);
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return array();
        }
    }
}