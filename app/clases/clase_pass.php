<?php

namespace App\Clases;

use Illuminate\Support\Facades\Crypt;

class clase_pass extends General
{
    private $semilla = "84k=hu*ekDHEe34=rf/m.72*Qsdp43**";
    private $iv      = 74859372;
    public $error;

    public function encriptar($valor)
    {
        $encrypted = Crypt::encryptString($valor);
        return base64_encode($encrypted);
    }

    public function desencriptar($valor)
    {
         //$decryptedPassword = decrypt($valor);
        //$encrypted = Crypt::encryptString($valor);
        //dd($encrypted);

        $valor = base64_decode($valor);

        $decrypted = Crypt::decryptString( $valor );
        //dd($decrypted);
        return $decrypted;

    }

    public function insertar($idDetalle, $descripcion)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO detalleMaestra (idDetalle, idMaestra, descripcion, campo1) '
                . 'VALUES (:idDetalle, 25, :descripcion, CURDATE())');
            $sql->execute(array(':idDetalle' => $idDetalle, ':descripcion' => $descripcion));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function actualizar($idDetalle, $descripcion)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE detalleMaestra SET descripcion = :descripcion WHERE idDetalle = :idDetalle '
                . 'AND idMaestra = 25');
            $sql->execute(array(':idDetalle' => $idDetalle, ':descripcion' => $descripcion));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function eliminar($idDetalle)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM detalleMaestra WHERE idDetalle = :idDetalle AND idMaestra = 25');
            $sql->execute(array(':idDetalle' => $idDetalle));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            return false;
        }
    }

    public function ultId()
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(MAX(idDetalle), 0) AS idDetalle
                FROM detalleMaestra
                WHERE detalleMaestra.idMaestra = 25');
            $sql->execute();
            $row = $sql->fetch();
            return $row["idDetalle"];
        } catch (PDOException $e) {
            return 0;
        }
    }

    public function total()
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total
                FROM detalleMaestra
                WHERE detalleMaestra.idMaestra = 25');
            $sql->execute();
            $row = $sql->fetch();
            return $row["total"];
        } catch (PDOException $e) {
            return 0;
        }
    }

    public function listar_todo()
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idDetalle, descripcion, campo2, DATE_FORMAT(campo1, "%d/%m/%Y") campo1
                FROM detalleMaestra
                WHERE detalleMaestra.idMaestra = 25');
            $sql->execute();
            return $sql->fetchAll();
        } catch (PDOException $e) {
            return array();
        }
    }

    public function listar_todo_paginado($inicio)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idDetalle, descripcion, DATE_FORMAT(campo1, "%d/%m/%Y") campo1
                FROM detalleMaestra
                WHERE detalleMaestra.idMaestra = 25
                LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute();
            $this->listado = $sql->fetchAll();
            return $this->listado;
        } catch (PDOException $e) {
            return $this->listado;
        }
    }

    public function datos($id)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT idDetalle, descripcion, DATE_FORMAT(campo1, "%d/%m/%Y") campo1
                FROM detalleMaestra
                WHERE detalleMaestra.idMaestra = 25 AND detalleMaestra.idDetalle = :id');
            $sql->execute(array(':id' => $id));
            return $sql->fetch();
        } catch (PDOException $e) {
            return array("idDetalle" => "", "descripcion" => "", "campo1" => "00/00/0000");
        }
    }

    public function pass_existe($pass, $id)
    {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
                FROM detalleMaestra
                WHERE detalleMaestra.idMaestra = 25 AND detalleMaestra.idDetalle != :id AND
                detalleMaestra.descripcion = :descripcion');
            $sql->execute(array(':id' => $id, ':descripcion' => $pass));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            return 0;
        }
    }
}
