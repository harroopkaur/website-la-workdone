<?php

namespace App\Clases;

class Consolidado_othersAux extends General{
    ########################################  Atributos  ########################################

    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertarResumenAux($cliente, $tablaExcluir){
        try{
            $this->conexion();
            $where = "";
            foreach($tablaExcluir as $row){
                $where .= " AND sofware NOT LIKE '%" . $row["descripcion"] . "%' ";
            }
            
            $sql = $this->conn->prepare('INSERT INTO consolidado_othersAux (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware, instalacionesAdmin, instalacionesRed) ' 
                . 'SELECT cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware, instalacionesAdmin, instalacionesRed '
                . 'FROM consolidado_others '
                . 'WHERE cliente = :cliente AND editor NOT IN ("UnKnown", "Your Company Name", "Nombre de su organizaciÃ³n") '
                . 'AND sofware NOT IN ("-", ". .", ". . .")' . $where);
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            echo $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_othersAux WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}