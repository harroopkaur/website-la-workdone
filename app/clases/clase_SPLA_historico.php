<?php

namespace App\Clases;

class clase_SPLA_historico extends General{ 
    public $error = null;
    
    function insertarCabecera($cliente, $descripcion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO historicoSPLA (cliente, descripcion, fecha) VALUES (:cliente, :descripcion, CURDATE())');
            $sql->execute(array(':cliente'=>$cliente, ':descripcion'=>$descripcion));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
   
    function obtenerUltId() {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id '
                . 'FROM historicoSPLA');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function obtenerIdFecha($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT id '
                . 'FROM historicoSPLA '
                . 'WHERE cliente = :cliente AND DATE_FORMAT(fecha, "%m") = DATE_FORMAT(CURDATE(), "%m") AND '
                . 'DATE_FORMAT(fecha, "%Y") = DATE_FORMAT(CURDATE(), "%Y")');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function existeHistorico($cliente) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT IFNULL(COUNT(id), 0) AS cantidad '
                . 'FROM historicoSPLA '
                . 'WHERE cliente = :cliente AND DATE_FORMAT(fecha, "%m") = DATE_FORMAT(CURDATE(), "%m") AND '
                . 'DATE_FORMAT(fecha, "%Y") = DATE_FORMAT(CURDATE(), "%Y")');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function insertarHistoricoCustomer($cliente, $empleado, $id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO historicoCustomerSPLA (idHistorico, nombreCliente, contrato, inicioContrato, finContrato) '
                . 'SELECT ' . $id . ' AS id, '
                    . 'nombreCliente, '
                    . 'contrato, '
                    . 'inicioContrato, '
                    . 'finContrato '
                . 'FROM SPLACliente '
                . 'WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }  
    
    function insertarHistoricoVM($cliente, $empleado, $id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO historicoVMSPLA (idHistorico, DC, cluster, host, '
                . 'VM, edicion, sockets, cores, cpu, hostDate, VMDate, customers, Qty, SKU, type) '
                . 'SELECT ' . $id . ', DC, cluster, host, VM, edicion, sockets, cores, cpu, hostDate, VMDate, '
                . 'IFNULL(customers, ""), Qty, IFNULL(SKU, ""), IFNULL(type, "") '
                . 'FROM SPLAVirtualMachine '
                . 'WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }  
    
    function insertarHistoricoApps($cliente, $empleado, $id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO historicoAppsSPLA (idHistorico, VM, customers, VMDate, '
                . 'hostDate, prod, edicion, sockets, cores, cpu, Qty, SKU, type, installedDate, tipo) '
                . 'SELECT ' . $id . ',
                    tabla.VM,
                    IFNULL(tabla.nombreCliente, ""),
                    tabla.VMDate,
                    tabla.hostDate,
                    tabla.familia,
                    tabla.edicion,
                    tabla.sockets,
                    tabla.cores,
                    tabla.cpu,
                    IFNULL(tabla.Qty, 0),
                    IFNULL(tabla.SKU, ""),
                    IFNULL(tabla.type, ""),
                    tabla.installedDate,
                    tabla.tipo
                FROM (SELECT SPLAVirtualMachine.id,
                        "VM" AS tipo,
                        SPLAVirtualMachine.VM,
                        SPLAVirtualMachine.customers AS nombreCliente,
                        SPLAVirtualMachine.VMDate AS VMDate,
                        SPLAVirtualMachine.hostDate AS hostDate,
                        SPLAVirtualMachine.edicion AS familia,
                        "" AS edicion,
                        SPLAVirtualMachine.sockets,
                        SPLAVirtualMachine.cores,
                        SPLAVirtualMachine.cpu,
                        SPLAVirtualMachine.Qty,
                        SPLAVirtualMachine.SKU,
                        SPLAVirtualMachine.type,
                        SPLAVirtualMachine.VMDate AS installedDate 
                    FROM SPLAVirtualMachine
                    WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                    GROUP BY SPLAVirtualMachine.VM, SPLAVirtualMachine.host

                    UNION 

                    SELECT resumen_SPLA.id,
                        "App" AS tipo,
                        SPLAVirtualMachine.VM,
                        SPLAVirtualMachine.customers AS nombreCliente,
                        SPLAVirtualMachine.VMDate AS VMDate,
                        SPLAVirtualMachine.hostDate AS hostDate,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        SPLAVirtualMachine.sockets,
                        SPLAVirtualMachine.cores,
                        SPLAVirtualMachine.cpu,
                        resumen_SPLA.Qty,
                        resumen_SPLA.SKU,
                        resumen_SPLA.type,
                        resumen_SPLA.fecha_instalacion AS installedDate
                    FROM SPLAVirtualMachine
                        INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                        SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                    WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                    GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarHistoricoCurrentSPLA($cliente, $empleado, $id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO historicoCurrentSPLA (idHistorico, customers, prod, SKU, inst, Qty, price, QtySale, priceSale, tipo) '
                . 'SELECT ' . $id . ',
                    tabla.nombreCliente,
                    tabla.familia,
                    tabla.SKU,
                    SUM(tabla.installed) AS installed,
                    SUM(tabla.Qty) AS Qty,
                    SUM(tabla.price) AS price,
                    SUM(tabla.QtySale) AS QtySale,
                    SUM(tabla.priceSale) AS priceSale,
                    tabla.tipo
                FROM (SELECT "VM" AS tipo,
                        SPLAVirtualMachine.SKU,
                        SPLAVirtualMachine.customers AS nombreCliente,
                        SPLAVirtualMachine.edicion AS familia,
                        1 AS installed,
                        SPLAVirtualMachine.Qty,
                        IFNULL(SPLASKU.price, 0) AS price,
                        SPLAVirtualMachine.QtySale,
                        SPLAVirtualMachine.priceSale
                    FROM SPLAVirtualMachine
                         LEFT JOIN SPLASKU ON SPLAVirtualMachine.SKU = SPLASKU.SKU
                    WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado

                    UNION

                    SELECT "App" AS tipo,
                        resumen_SPLA.SKU,
                        SPLAVirtualMachine.customers AS nombreCliente,
                        TRIM(CONCAT(resumen_SPLA.familia, " ", IFNULL(resumen_SPLA.edicion, ""), " ", IFNULL(resumen_SPLA.version, ""))) AS familia,
                        COUNT(resumen_SPLA.familia) AS installed,
                        resumen_SPLA.Qty,
                        IFNULL(SPLASKU.price, 0) AS price,
                        resumen_SPLA.QtySale,
                        resumen_SPLA.priceSale
                    FROM SPLAVirtualMachine
                        INNER JOIN resumen_SPLA ON SPLAVirtualMachine.VM = resumen_SPLA.equipo AND
                        SPLAVirtualMachine.cliente = resumen_SPLA.cliente AND SPLAVirtualMachine.empleado = resumen_SPLA.empleado
                        LEFT JOIN SPLASKU ON resumen_SPLA.SKU = SPLASKU.SKU
                    WHERE SPLAVirtualMachine.cliente = :cliente AND SPLAVirtualMachine.empleado = :empleado
                    GROUP BY resumen_SPLA.equipo, resumen_SPLA.familia, resumen_SPLA.edicion, resumen_SPLA.version) tabla
                GROUP BY tabla.nombreCliente, tabla.familia, tabla.SKU');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function eliminarHistoricos($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM historicoCustomerSPLA WHERE idHistorico = :id;
                DELETE FROM historicoVMSPLA WHERE idHistorico = :id;
                DELETE FROM historicoAppsSPLA WHERE idHistorico = :id;
                DELETE FROM historicoCurrentSPLA WHERE idHistorico = :id;');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function reportHistoricalVMPaginado($cliente, $VM, $fechaInicio, $fechaFin, $pagina, $limite){
        try{
            $array = array(':cliente'=>$cliente, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin);
            $where = "";
            if($VM != ""){
                $array[":VM"] = $VM;
                $where = " WHERE VM = :VM ";
            }
            
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaHistoricalReportingVM($cliente, $VM, $fechaInicio, $fechaFin, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $mesInicio = date("n", strtotime($fechaInicio)); 
            $mesFin = date("n", strtotime($fechaFin));
            $yearInicio = date("Y", strtotime($fechaInicio)); 
            $yearFin = date("Y", strtotime($fechaFin));

            if($yearFin > $yearInicio){
                $mesFin += 12;
            }

            $mesFin++;
            
            $selectFechas = "";
            for($index = $mesInicio; $index < $mesFin; $index++){
                $valor = $this->completarCeroNumeros($index);
                $campo = $valor . '/'.  $yearInicio;
                if($index > 12){
                    $campo = $valor . '/'.  $yearFin;
                }
                $selectFechas .= ', SUM(IF(mes = "' . $valor . '", cantidad, 0)) "' . $campo . '" ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT host,
                    DATE_FORMAT(hostDate, "%m/%d/%Y") AS hostDate,
                    VM,
                    DATE_FORMAT(VMDate, "%m/%d/%Y") AS VMDate ' . $selectFechas . '
                FROM (SELECT host,
                        hostDate,
                        VM,
                        VMDate,
                        1 AS cantidad,
                        DATE_FORMAT(historicoSPLA.fecha, "%m") AS mes
                    FROM historicoVMSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoVMSPLA.idHistorico AND historicoSPLA.fecha BETWEEN :fechaInicio AND :fechaFin
                        AND historicoSPLA.cliente = :cliente
                    ' . $where . ') tabla
                GROUP BY VM
                LIMIT ' . $pagina . ', ' . $limite);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function reportHistoricalVMTotal($cliente, $VM, $fechaInicio, $fechaFin){
        try{
            $array = array(':cliente'=>$cliente, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin);
            $where = "";
            if($VM != ""){
                $array[":VM"] = $VM;
                $where = " WHERE VM = :VM ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(host) AS cantidad
                FROM (SELECT host
                    FROM historicoVMSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoVMSPLA.idHistorico AND historicoSPLA.fecha BETWEEN :fechaInicio AND :fechaFin
                        AND historicoSPLA.cliente = :cliente
                    ' . $where . '
                GROUP BY VM) tabla');
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function ultimaPaginaHistoricalReportingVM($cliente, $VM, $fechaInicio, $fechaFin, $limite) {
        return ceil($this->reportHistoricalVMTotal($cliente, $VM, $fechaInicio, $fechaFin) / $limite);
    }
    
    function VMHistorical($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT VM
                    FROM historicoVMSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoVMSPLA.idHistorico
                        AND historicoSPLA.cliente = :cliente
                    GROUP BY VM');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function reportHistoricalAppsPaginado($cliente, $producto, $fechaInicio, $fechaFin, $pagina, $limite){
        try{
            $array = array(':cliente'=>$cliente, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin);
            $where = "";
            if($producto != ""){
                $array[":prod"] = $producto;
                $where = " AND prod = :prod ";
                //$where = " WHERE prod = :prod ";
            }
            
            if ($pagina == "ultima") {
                $pagina = ($this->ultimaPaginaHistoricalReportingApps($cliente, $producto, $fechaInicio, $fechaFin, $limite) - 1) * $limite;
            } else {
                $pagina = ($pagina - 1) * $limite;
            }
            
            $mesInicio = date("n", strtotime($fechaInicio)); 
            $mesFin = date("n", strtotime($fechaFin));
            $yearInicio = date("Y", strtotime($fechaInicio)); 
            $yearFin = date("Y", strtotime($fechaFin));

            if($yearFin > $yearInicio){
                $mesFin += 12;
            }

            $mesFin++;
            
            $selectFechas = "";
            for($index = $mesInicio; $index < $mesFin; $index++){
                $valor = $this->completarCeroNumeros($index);
                $campo = $valor . '/'.  $yearInicio;
                if($index > 12){
                    $campo = $valor . '/'.  $yearFin;
                }
                $selectFechas .= ', SUM(IF(mes = "' . $valor . '", cantidad, 0)) "' . $campo . '" ';
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT VM,
                    DATE_FORMAT(VMDate, "%m/%d/%Y") AS VMDate,
                    edicion,
                    DATE_FORMAT(hostDate, "%m/%d/%Y") AS hostDate,
                    DATE_FORMAT(installedDate, "%m/%d/%Y") AS installedDate,
                    type ' . $selectFechas . '
                FROM (SELECT VM,
                        VMDate,
                        IFNULL(edicion, prod) AS edicion,
                        hostDate,
                        installedDate,
                        type,
                        1 AS cantidad,
                        DATE_FORMAT(historicoSPLA.fecha, "%m") AS mes
                    FROM historicoAppsSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoAppsSPLA.idHistorico AND historicoSPLA.fecha BETWEEN :fechaInicio AND :fechaFin
                        AND historicoSPLA.cliente = :cliente
                    WHERE historicoAppsSPLA.tipo != "VM" ' . $where . ') tabla
                GROUP BY VM
                LIMIT ' . $pagina . ', ' . $limite);
            
            /*$sql = $this->conn->prepare('SELECT VM,
                    DATE_FORMAT(VMDate, "%m/%d/%Y") AS VMDate,
                    edicion,
                    DATE_FORMAT(hostDate, "%m/%d/%Y") AS hostDate,
                    type ' . $selectFechas . '
                FROM (SELECT VM,
                        VMDate,
                        IFNULL(edicion, prod) AS edicion,
                        hostDate,
                        type,
                        1 AS cantidad,
                        DATE_FORMAT(historicoSPLA.fecha, "%m") AS mes
                    FROM historicoAppsSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoAppsSPLA.idHistorico AND historicoSPLA.fecha BETWEEN :fechaInicio AND :fechaFin
                        AND historicoSPLA.cliente = :cliente
                    ' . $where . ') tabla
                GROUP BY VM
                LIMIT ' . $pagina . ', ' . $limite);*/
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function reportHistoricalAppsTotal($cliente, $producto, $fechaInicio, $fechaFin){
        try{
            $array = array(':cliente'=>$cliente, ':fechaInicio'=>$fechaInicio, ':fechaFin'=>$fechaFin);
            $where = "";
            if($producto != ""){
                $array[":prod"] = $producto;
                $where = " AND prod = :prod ";
                //$where = " WHERE prod = :prod ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(VM) AS cantidad
                FROM (SELECT VM
                    FROM historicoAppsSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoAppsSPLA.idHistorico AND historicoSPLA.fecha BETWEEN :fechaInicio AND :fechaFin
                        AND historicoSPLA.cliente = :cliente
                    WHERE historicoAppsSPLA.tipo != "VM" ' . $where . '
                GROUP BY VM) tabla');
            /*$sql = $this->conn->prepare('SELECT COUNT(VM) AS cantidad
                FROM (SELECT VM
                    FROM historicoAppsSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoAppsSPLA.idHistorico AND historicoSPLA.fecha BETWEEN :fechaInicio AND :fechaFin
                        AND historicoSPLA.cliente = :cliente
                    ' . $where . '
                GROUP BY VM) tabla');*/
            $sql->execute($array);
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function ultimaPaginaHistoricalReportingApps($cliente, $producto, $fechaInicio, $fechaFin, $limite) {
        return ceil($this->reportHistoricalAppsTotal($cliente, $producto, $fechaInicio, $fechaFin) / $limite);
    }
    
    function ProductoHistorical($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT prod
                    FROM historicoAppsSPLA
                        INNER JOIN historicoSPLA ON historicoSPLA.id = historicoAppsSPLA.idHistorico
                        AND historicoSPLA.cliente = :cliente
                    GROUP BY prod');
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function cambiarIndiceLetra($indice, $row){
        $valor = "E" . $row;
        if($indice == 1){
            $valor = "F" . $row;
        } else if($indice == 2){
            $valor = "G" . $row;
        } else if($indice == 3){
            $valor = "H" . $row;
        } else if($indice == 4){
            $valor = "I" . $row;
        } else if($indice == 5){
            $valor = "J" . $row;
        } else if($indice == 6){
            $valor = "K" . $row;
        } else if($indice == 7){
            $valor = "L" . $row;
        } else if($indice == 8){
            $valor = "M" . $row;
        } else if($indice == 9){
            $valor = "N" . $row;
        } else if($indice == 10){
            $valor = "O" . $row;
        } else if($indice == 11){
            $valor = "P" . $row;
        } else if($indice == 12){
            $valor = "Q" . $row;
        }
        return $valor;
    }
}