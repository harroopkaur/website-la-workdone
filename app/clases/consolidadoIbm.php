<?php

namespace App\Clases;

class consolidadoIbm extends General{
    public  $lista        = array();
    public  $listaEdicion = array();
    public  $listaVersion = array();
    public  $edicion      = "";
    public  $version      = "";
    public  $error        = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $registro, $editor, $version, $fecha_instalacion, $software) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_ibm(cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) VALUES '
            . '(:cliente, :empleado, :dato_control, :host_name, :registro, :editor, :version, :fecha_instalacion, :software)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control, ':host_name'=>$host_name, ':registro'=>$registro, ':editor'=>$editor, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion, ':software'=>$software));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_ibm (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_ibm WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductos($cliente, $empleado, $software) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_ibm
            WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE :software
            ORDER BY fecha_instalacion DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':software'=>"%" . $software . "%"));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}