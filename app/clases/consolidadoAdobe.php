<?php

namespace App\Clases;

class consolidadoAdobe extends General{
    public  $lista        = array();
    public  $listaEdicion = array();
    public  $listaVersion = array();
    public  $edicion      = "";
    public  $version      = "";
    public  $error        = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $dato_control, $host_name, $registro, $editor, $version, $fecha_instalacion, $software) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_adobe (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) VALUES '
            . '(:cliente, :empleado, :dato_control, :host_name, :registro, :editor, :version, :fecha_instalacion, :software)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dato_control'=>$dato_control, ':host_name'=>$host_name, ':registro'=>$registro, ':editor'=>$editor, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion, ':software'=>$software));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_adobe (cliente, empleado, dato_control, host_name, registro, editor, version, fecha_instalacion, sofware) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_adobe WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductosAdobe($cliente, $empleado, $in = null, $out = null) {        
        try{
            $this->conexion();
            
            $agregar = "";
            if($in != null){
                $inAux = explode(",", $in);
            }
            
            if(trim($inAux[0]) == "Adobe After Effects" && isset($inAux[1]) && (trim($inAux[1] == "Presets" || trim($inAux[1] == "Third")))){
                $agregar .= " AND (";
            
                for($i = 0; $i < count($inAux); $i++){
                    if($i > 0){
                        $agregar .= " AND ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            }else {
                $agregar .= " AND (";
            
                for($i = 0; $i < count($inAux); $i++){
                    if($i > 0){
                        $agregar .= " OR ";
                    }
                    $agregar .= "sofware LIKE '%" . trim($inAux[$i]) . "%'";
                }
                
                $agregar .= ")";
            }
                
            $quitar = "";
            if($out != ""){
                $outAux = explode(",", $out);
            
                $quitar = "WHERE tabla.id NOT IN (
				  SELECT id
				  FROM consolidado_adobe
				  WHERE cliente = :cliente AND empleado = :empleado AND (";
            
                for($i = 0; $i < count($outAux); $i++){
                    if($i > 0){
                        $quitar .= " OR ";
                    }

                    $quitar .= "sofware LIKE '%" . trim($outAux[$i]) . "%'"; 
                }
                $quitar .= ")";
            }
            
            $sql = $this->conn->prepare("SELECT * FROM (SELECT * FROM consolidado_adobe 
                WHERE cliente = :cliente AND empleado = :empleado " . $agregar . "
                ORDER BY fecha_instalacion DESC) AS tabla " . $quitar);
            // if ($quitar != ''){ dd($quitar ); };
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return $sql->fetchAll();
        }catch(\PDOException $e){
            // dd($e);
            $this->error = $e->getMessage();
            return array();
        }catch(\Exception $e){
            // dd($e);
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listarProductosAdobeAcrobat($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_adobe
            WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE "%Adobe Reader%" OR sofware LIKE "%Adobe Acrobat%"
            ORDER BY fecha_instalacion DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductosAdobeAfterEffects($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_adobe
            WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE "%Adobe After Effects%" AND sofware NOT LIKE "%Presets%" AND sofware NOT LIKE "%Third%"
            ORDER BY fecha_instalacion DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductosAdobeAfterEffectsPresets($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_adobe
            WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE "%Adobe After Effects%" AND sofware LIKE "%Presets%"
            ORDER BY fecha_instalacion DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductosAdobeAfterEffectsThird($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_adobe
            WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE "%Adobe After Effects%" AND sofware LIKE "%Third%"
            ORDER BY fecha_instalacion DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductosAdobeGenerales($cliente, $empleado, $producto) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_adobe
            WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE :producto
            ORDER BY fecha_instalacion DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':producto'=>"%".$producto."%"));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductosAdobeBridge($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *
            FROM consolidado_adobe
            WHERE cliente = :cliente AND empleado = :empleado AND sofware LIKE "%Adobe Bridge%" AND sofware NOT LIKE "%Start%"
            ORDER BY fecha_instalacion DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}