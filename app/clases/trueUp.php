<?php

namespace App\Clases;

class trueUp extends General{
    public  $error = NULL;
    
    // Insertar 
    function insertar($cliente, $descripcion, $ruta) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO trueUp (cliente, descripcion, ruta, fecha) VALUES '
            . '(:cliente, :descripcion, :ruta, NOW())');
            $sql->execute(array(':cliente'=>$cliente, ':descripcion'=>$descripcion, ':ruta'=>$ruta));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE trueUp SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listadoPaginado($cliente, $inicio){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * '
            . 'FROM trueUp '
            . 'WHERE cliente = :cliente AND estado = 1 '
            . 'ORDER BY fecha DESC '
            . 'LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function total($cliente){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad '
            . 'FROM trueUp '
            . 'WHERE cliente = :cliente AND estado = 1');
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function obtenerUltId(){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT MAX(id) AS id '
            . 'FROM trueUp');
            $sql->execute();
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function datosTrueUp($id){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT clientes.empresa,
                    trueUp.cliente,
                    trueUp.fecha,
                    trueUp.ruta
                FROM trueUp
                    INNER JOIN clientes ON trueUp.cliente = clientes.id
                WHERE trueUp.id = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetch();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array('empresa'=>"", 'fecha'=>'0000-00-00 00:00:00');
        }
    }
}