<?php

namespace App\Clases;

class resumenAdobe extends General{
    public $lista = array();
    public $error = NULL;
    private $lg0000 = "En Uso";
    private $lg0001 = "Uso Probable";
    private $lg0002 = "Obsoleto";
    private $lg0003 = "Servidor";
    private $lg0004 = "Cliente";
    
/*    function __construct() {
       if($_SESSION["idioma"] == 2){
           $this->lg0000 = "In Use";
           $this->lg0001 = "Probably in Use";
           $this->lg0002 = "Obsolete";
           $this->lg0003 = "Server";
           $this->lg0004 = "Cliente";
       }
    }*/
    
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $familia, $edicion, $version, $fecha_instalacion) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO resumen_adobe (cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) VALUES '
            . '(:cliente, :empleado, :equipo, :familia, :edicion, :version, :fecha_instalacion)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO resumen_adobe (cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES " . $bloque;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM resumen_adobe WHERE cliente = :cliente');
            $sql->execute(array(':cliente'=>$cliente));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS total FROM resumen_adobe WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version GROUP BY edicion, version');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia."%", ':edicion'=>$edicion, ':version'=>$version));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos5($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_adobe.id,
                resumen_adobe.cliente,
                resumen_adobe.equipo,
                IF(filepcs_adobe.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_adobe.familia,
                resumen_adobe.edicion,
                resumen_adobe.version,
                DATE_FORMAT(resumen_adobe.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                CASE
                    WHEN detalles_equipo_adobe.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_adobe
            INNER JOIN detalles_equipo_adobe ON resumen_adobe.equipo = detalles_equipo_adobe.equipo
            INNER JOIN filepcs_adobe ON resumen_adobe.equipo = filepcs_adobe.cn
        WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado AND resumen_adobe.familia = :familia
        GROUP BY resumen_adobe.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos5Asignacion($cliente, $familia, $asignacion, $asignaciones){        
        try{
            $array = array(':cliente'=>$cliente);
            
            if($familia != "Todo"){
                $where = " AND resumen_adobe.familia = :familia ";
                $array[':familia'] = $familia;
            }else {
                $where = " AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
            }
            
            if($asignacion != ""){
                $where1 = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where1 = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT resumen_adobe.id,
                resumen_adobe.cliente,
                resumen_adobe.equipo,
                IF(detalles_equipo_adobe.os LIKE ('%Windows Server%'), '" . $this->lg0003 . "', '" . $this->lg0004 . "') AS tipo,
                resumen_adobe.familia,
                resumen_adobe.edicion,
                resumen_adobe.version,
                DATE_FORMAT(MAX(resumen_adobe.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                CASE
                    WHEN detalles_equipo_adobe.rango = 1 THEN
                         '" . $this->lg0000 . "'
                    WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                         '" . $this->lg0001 . "'
                    ELSE
                         '" . $this->lg0002 . "'
                END AS rango
        FROM resumen_adobe
            INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND resumen_adobe.equipo = detalles_equipo_adobe.equipo " . $where1 . "
        WHERE resumen_adobe.cliente = :cliente " . $where . "
        GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    /*function listar_datos5Sam($cliente, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_adobeSam.id,
                resumen_adobeSam.cliente,
                resumen_adobeSam.equipo,
                IF(detalles_equipo_adobeSam.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_adobeSam.familia,
                resumen_adobeSam.edicion,
                resumen_adobeSam.version,
                DATE_FORMAT(resumen_adobeSam.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                CASE
                    WHEN detalles_equipo_adobeSam.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_adobeSam.rango = 2 OR detalles_equipo_adobeSam.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_adobeSam
            INNER JOIN detalles_equipo_adobeSam ON resumen_adobeSam.equipo = detalles_equipo_adobeSam.equipo
        WHERE resumen_adobeSam.cliente = :cliente AND resumen_adobeSam.familia = :familia
        GROUP BY resumen_adobeSam.id
        ORDER BY resumen_adobeSam.familia, resumen_adobeSam.edicion, resumen_adobeSam.version");
            $sql->execute(array(':cliente'=>$cliente, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/
    
    function listar_datos5SamArchivo($archivo, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_adobeSam.id,
                resumen_adobeSam.cliente,
                resumen_adobeSam.equipo,
                IF(detalles_equipo_adobeSam.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                resumen_adobeSam.familia,
                resumen_adobeSam.edicion,
                resumen_adobeSam.version,
                DATE_FORMAT(resumen_adobeSam.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                CASE
                    WHEN detalles_equipo_adobeSam.rango = 1 THEN
                         'En Uso'
                    WHEN detalles_equipo_adobeSam.rango = 2 OR detalles_equipo_adobeSam.rango = 3 THEN
                         'Uso Probable'
                    ELSE
                         'Obsoleto'
                END AS rango
        from resumen_adobeSam
            INNER JOIN detalles_equipo_adobeSam ON resumen_adobeSam.equipo = detalles_equipo_adobeSam.equipo
        WHERE resumen_adobeSam.archivo = :archivo AND resumen_adobeSam.familia = :familia
        GROUP BY resumen_adobeSam.id
        ORDER BY resumen_adobeSam.familia, resumen_adobeSam.edicion, resumen_adobeSam.version");
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_datos6($cliente, $empleado, $familia, $edicion) {
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_adobe.id,
                    resumen_adobe.cliente,
                    resumen_adobe.equipo,
                    IF(filepcs_adobe.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_adobe.familia,
                    resumen_adobe.edicion,
                    resumen_adobe.version,
                    DATE_FORMAT(resumen_adobe.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_adobe.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_adobe
                    INNER JOIN detalles_equipo_adobe ON resumen_adobe.equipo = detalles_equipo_adobe.equipo
                    INNER JOIN filepcs_adobe ON resumen_adobe.equipo = filepcs_adobe.cn
                WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_adobe.edicion = :edicion
                GROUP BY resumen_adobe.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Asignacion($cliente, $familia, $edicion, $asignacion, $asignaciones) {
        $where1 = " AND resumen_adobe.edicion = :edicion ";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        if($edicion == ""){
            $where1 = " AND (resumen_adobe.edicion = :edicion OR resumen_adobe.edicion IS NULL) ";
        }
                
        try{     
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_adobe.id,
                    resumen_adobe.cliente,
                    resumen_adobe.equipo,
                    IF(detalles_equipo_adobe.os LIKE ('%Windows Server%'), '" . $this->lg0003 . "', '" . $this->lg0004 . "') AS tipo,
                    resumen_adobe.familia,
                    resumen_adobe.edicion,
                    resumen_adobe.version,
                    DATE_FORMAT(MAX(resumen_adobe.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_adobe.rango = 1 THEN
                             '" . $this->lg0000 . "'
                        WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                             '" . $this->lg0001 . "'
                        ELSE
                             '" . $this->lg0002 . "'
                    END AS rango
                from resumen_adobe
                    INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND resumen_adobe.equipo = detalles_equipo_adobe.equipo " . $where . "
                WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) " . $where1 . "
                GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Todos($cliente, $empleado, $familia) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_adobe.id,
                    resumen_adobe.cliente,
                    resumen_adobe.equipo,
                    IF(filepcs_adobe.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                    resumen_adobe.familia,
                    resumen_adobe.edicion,
                    resumen_adobe.version,
                    DATE_FORMAT(resumen_adobe.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_adobe.rango = 1 THEN
                             'En Uso'
                        WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                             'Uso Probable'
                        ELSE
                             'Obsoleto'
                    END AS rango
                from resumen_adobe
                    INNER JOIN detalles_equipo_adobe ON resumen_adobe.equipo = detalles_equipo_adobe.equipo
                    INNER JOIN filepcs_adobe ON resumen_adobe.equipo = filepcs_adobe.cn
                WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_adobe.id");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6TodosAsignacion($cliente, $familia, $asignacion, $asignaciones) {        
        try{    
            $array = array(':cliente'=>$cliente, ':familia'=>$familia);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("select resumen_adobe.id,
                    resumen_adobe.cliente,
                    resumen_adobe.equipo,
                    IF(detalles_equipo_adobe.os LIKE ('%Windows Server%'), '" . $this->lg0003 . "', '" . $this->lg0004 . "') AS tipo,
                    resumen_adobe.familia,
                    resumen_adobe.edicion,
                    resumen_adobe.version,
                    DATE_FORMAT(MAX(resumen_adobe.fecha_instalacion), '%d/%m/%Y') AS fecha_instalacion,
                    CASE
                        WHEN detalles_equipo_adobe.rango = 1 THEN
                             '" . $this->lg0000 . "'
                        WHEN detalles_equipo_adobe.rango = 2 OR detalles_equipo_adobe.rango = 3 THEN
                             '" . $this->lg0001 . "'
                        ELSE
                             '" . $this->lg0002 . "'
                    END AS rango
                from resumen_adobe
                    INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND resumen_adobe.equipo = detalles_equipo_adobe.equipo " . $where . "
                WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos5Agrupado($cliente, $empleado, $familia) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version
                        FROM resumen_adobe
                            INNER JOIN detalles_equipo_adobe ON resumen_adobe.equipo = detalles_equipo_adobe.equipo
                            INNER JOIN filepcs_adobe ON resumen_adobe.equipo = filepcs_adobe.cn
                        WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado AND resumen_adobe.familia = :familia
                        GROUP BY resumen_adobe.id) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos5AgrupadoAsignacion($cliente, $familia, $asignacion, $asignaciones) {        
        try{
            $array = array(':cliente'=>$cliente);
            
            if($familia != "Todo"){
                $where = " AND resumen_adobe.familia = :familia ";
                $array[':familia'] = $familia;
            }else {
                $where = " AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
            }
            
            if($asignacion != ""){
                $where1 = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where1 = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT resumen_adobe.familia,
                            resumen_adobe.edicion,
                            resumen_adobe.version
                        FROM resumen_adobe
                            INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND resumen_adobe.equipo = detalles_equipo_adobe.equipo " . $where1 . "
                        WHERE resumen_adobe.cliente = :cliente " . $where . "
                        GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version) AS tabla
                GROUP BY familia, edicion, version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6Agrupado($cliente, $empleado, $familia, $edicion) {        
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_adobe.familia,
                        resumen_adobe.edicion,
                        resumen_adobe.version
                    FROM resumen_adobe
                        INNER JOIN detalles_equipo_adobe ON resumen_adobe.equipo = detalles_equipo_adobe.equipo
                        INNER JOIN filepcs_adobe ON resumen_adobe.equipo = filepcs_adobe.cn
                    WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) AND resumen_adobe.edicion = :edicion
                    GROUP BY resumen_adobe.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones) {
        $where1 = " AND resumen_adobe.edicion = :edicion ";
        $array = array(':cliente'=>$cliente, ':familia'=>$familia, ':edicion'=>$edicion);
        if($asignacion != ""){
            $where = " AND asignacion = :asignacion ";
            $array[":asignacion"] = $asignacion;
        } else{
            $asignacion = $this->stringConsulta($asignaciones, "asignacion");
            $where = " AND asignacion IN (" . $asignacion . ") ";
        }
        
        if($edicion == ""){
            $where1 = " AND (resumen_adobe.edicion = :edicion OR resumen_adobe.edicion IS NULL) ";
        }
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_adobe.familia,
                        resumen_adobe.edicion,
                        resumen_adobe.version
                    FROM resumen_adobe
                        INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND resumen_adobe.equipo = detalles_equipo_adobe.equipo " . $where . "
                    WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) " . $where1 . "
                    GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoTodos($cliente, $empleado, $familia) {        
        try{       
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_adobe.familia,
                        resumen_adobe.edicion,
                        resumen_adobe.version
                    FROM resumen_adobe
                        INNER JOIN detalles_equipo_adobe ON resumen_adobe.equipo = detalles_equipo_adobe.equipo
                        INNER JOIN filepcs_adobe ON resumen_adobe.equipo = filepcs_adobe.cn
                    WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY resumen_adobe.id) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6AgrupadoTodosAsignacion($cliente, $familia, $asignacion, $asignaciones) {        
        try{       
            $array = array(':cliente'=>$cliente, ':familia'=>$familia);
            if($asignacion != ""){
                $where = " AND asignacion = :asignacion ";
                $array[":asignacion"] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.familia,
                    tabla.edicion,
                    tabla.version,
                    COUNT(*) AS cantidad
                FROM (SELECT
                        resumen_adobe.familia,
                        resumen_adobe.edicion,
                        resumen_adobe.version
                    FROM resumen_adobe
                        INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente AND resumen_adobe.equipo = detalles_equipo_adobe.equipo " . $where . "
                    WHERE resumen_adobe.cliente = :cliente AND resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version) AS tabla
                GROUP BY tabla.familia, tabla.edicion, tabla.version");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function graficoBalanza($cliente, $empleado, $familia){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_adobe`
                    WHERE detalles_equipo_adobe.equipo = resumen_adobe.equipo AND detalles_equipo_adobe.cliente = resumen_adobe.cliente AND detalles_equipo_adobe.empleado = resumen_adobe.empleado
                    GROUP BY detalles_equipo_adobe.equipo) rango
                    FROM resumen_adobe
                    WHERE resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function graficoBalanzaAsignacion($cliente, $familia, $edicion, $asignacion, $asignaciones){
        try{    
            $array = array(':cliente'=>$cliente);
            if($familia != "Otros"){
                $where = " resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1) ";
                $array[':familia'] = $familia;
            } else{
                $where = " resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 NOT IN ('Acrobat', 'Creative Cloud', 'Creative Suite') AND idMaestra = 1) ";
            }
            
            if($familia == "Otros" && $edicion != "Todo"){
                $where .= " AND resumen_adobe.familia = :familia ";
                $array[':familia'] = $edicion;
            }
            else if($edicion == ""){
                $where .= " AND (resumen_adobe.edicion = '' OR resumen_adobe.edicion IS NULL) ";
            } else if($edicion != "Todo"){
                $where .= " AND resumen_adobe.edicion = :edicion ";
                $array[':edicion'] = $edicion;
            }
            
            if($asignacion != ""){
                $where1 = " AND asignacion = :asignacion ";
                $array[':asignacion'] = $asignacion;
            } else{
                $asignacion = $this->stringConsulta($asignaciones, "asignacion");
                $where1 = " AND asignacion IN (" . $asignacion . ") ";
            }
            
            $this->conexion();
            $sql = $this->conn->prepare("SELECT IFNULL(COUNT(tabla.familia), 0) AS conteo,
                        tabla.rango
                FROM (SELECT resumen_adobe.familia,
                        detalles_equipo_adobe.rango
                    FROM resumen_adobe
                         INNER JOIN detalles_equipo_adobe ON resumen_adobe.cliente = detalles_equipo_adobe.cliente
                         AND resumen_adobe.equipo = detalles_equipo_adobe.equipo " . $where1 . "
                    WHERE " . $where . "
                    AND resumen_adobe.cliente = :cliente
                    GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version) tabla
                GROUP BY tabla.rango");
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function graficoBalanzaEdicion($cliente, $empleado, $familia, $edicion){
        try{   
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_adobe`
                    WHERE detalles_equipo_adobe.equipo = resumen_adobe.equipo AND detalles_equipo_adobe.cliente = resumen_adobe.cliente AND detalles_equipo_adobe.empleado = resumen_adobe.empleado
                    GROUP BY detalles_equipo_adobe.equipo) rango
                    FROM resumen_adobe
                    WHERE resumen_adobe.familia IN (SELECT descripcion FROM detalleMaestra WHERE campo1 = :familia AND idMaestra = 1)
                    AND resumen_adobe.edicion = :edicion
                    AND resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia, ':edicion'=>$edicion));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function graficoBalanzaEdicion1($cliente, $empleado, $familia){
        try{    
            $this->conexion();
            $sql = $this->conn->prepare("SELECT tabla.rango,
                    COUNT(tabla.rango) AS conteo
                FROM (SELECT (SELECT rango
                    FROM `detalles_equipo_adobe`
                    WHERE detalles_equipo_adobe.equipo = resumen_adobe.equipo AND detalles_equipo_adobe.cliente = resumen_adobe.cliente AND detalles_equipo_adobe.empleado = resumen_adobe.empleado
                    GROUP BY detalles_equipo_adobe.equipo) rango
                    FROM resumen_adobe
                    WHERE resumen_adobe.familia = :familia
                    AND resumen_adobe.cliente = :cliente AND resumen_adobe.empleado = :empleado) AS tabla
                GROUP BY tabla.rango");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function duplicado($cliente, $empleado, $equipo, $familia, $edicion, $version){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(tabla.familia) AS cantidad
                FROM (SELECT resumen_adobe.familia
                    FROM resumen_adobe
                    WHERE resumen_adobe.cliente = :cliente AND empleado = :empleado AND resumen_adobe.equipo = :equipo 
                    AND resumen_adobe.familia = :familia AND resumen_adobe.edicion = :edicion AND resumen_adobe.version = :version
                    GROUP BY resumen_adobe.equipo, resumen_adobe.familia, resumen_adobe.edicion, resumen_adobe.version) tabla");
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, 
            ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }  
    
    /*function duplicadoSam($cliente, $equipo, $familia, $edicion, $version){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM resumen_adobeSam
                WHERE resumen_adobeSam.cliente = :cliente AND resumen_adobeSam.equipo = :equipo AND resumen_adobeSam.familia = :familia 
                AND resumen_adobeSam.edicion = :edicion AND resumen_adobeSam.version = :version");
            $sql->execute(array(':cliente'=>$cliente, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/   
    
    function duplicadoSamArchivo($archivo, $equipo, $familia){    
        try{
            $this->conexion();
            $sql = $this->conn->prepare("SELECT COUNT(*) AS cantidad
                FROM resumen_adobeSam
                WHERE resumen_adobeSam.archivo = :archivo AND resumen_adobeSam.equipo = :equipo AND resumen_adobeSam.familia = :familia");
            $sql->execute(array(':archivo'=>$archivo, ':equipo'=>$equipo, ':familia'=>$familia));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }   
    
    function instalResumenAsignacion($cliente, $empleado, $familia, $edicion, $asignacion){
        $where = "";
        if($familia != "Others"){
            $in = "'" . $familia . "'";
        } else{
            $in = "'Visual Studio', 'Exchange Server', 'Sharepoint Server', 'Skype for Business', 'System Center'";
        }
        $array = array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%');
        if($asignacion != ""){
            $where = " AND detalles_equipo2.asignacion = :asignacion ";
            $array = array(':cliente'=>$cliente, ':empleado'=>$empleado, ':edicion'=>'%' . $edicion . '%', ':asignacion'=>$asignacion);
        }
        $this->conexion();
        $query = "SELECT tabla.rango,
                    COUNT(tabla.rango) AS instalaciones
            FROM (SELECT detalles_equipo2.rango
                FROM resumen_office2
                    INNER JOIN detalles_equipo2 ON resumen_office2.equipo = detalles_equipo2.equipo
                    AND resumen_office2.cliente = detalles_equipo2.cliente AND resumen_office2.empleado = detalles_equipo2.empleado " . $where . " 
                WHERE resumen_office2.cliente = :cliente AND resumen_office2.empleado = :empleado AND resumen_office2.familia IN (" . $in . ")
                AND resumen_office2.edicion LIKE :edicion
                GROUP BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version
                ORDER BY resumen_office2.equipo, resumen_office2.familia, resumen_office2.edicion, resumen_office2.version) tabla
            GROUP BY tabla.rango";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($array);
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}