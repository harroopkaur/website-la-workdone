<?php

namespace App\Clases;

class consolidadoVMWare extends General{
    public  $lista        = array();
    public  $listaEdicion = array();
    public  $listaVersion = array();
    public  $edicion      = "";
    public  $version      = "";
    public  $error        = NULL;
    
    // Insertar 
    function insertar($cliente, $empleado, $licenseKey, $product, $usage, $capacity, $label, $expires, $assigned) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO licenciasESXiVCenter(cliente, empleado, licenseKey, product, usages, capacity, labels, expires, assigned) VALUES '
            . '(:cliente, :empleado, :licenseKey, :product, :usage, :capacity, :label, :expires, :assigned)');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':licenseKey'=>$licenseKey, ':product'=>$product, ':usage'=>$usage, ':capacity'=>$capacity, ':label'=>$label, ':expires'=>$expires, ':assigned'=>$assigned));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM licenciasESXiVCenter WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductos($cliente, $empleado, $software, $noIncluir) {
        try{
            $this->conexion();
            $notIn = "";
            if($noIncluir != ""){
                $data = explode(",", $noIncluir);
                for($i=0; $i < count($data); $i++){
                    $notIn .= " AND product NOT LIKE '%" . $data[$i] . "%'";
                }
            }
            $sql = $this->conn->prepare('SELECT *
            FROM licenciasESXiVCenter
            WHERE cliente = :cliente AND empleado = :empleado AND product LIKE :product ' . $notIn . '
            ORDER BY licenseKey DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':product'=>"%" . $software . "%"));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}