<?php

namespace App\Clases;

class Resumen_SPLA extends General{
    ########################################  Atributos  ########################################
    var $id;
    var $cliente;
    var $dato_control;
    var $host_name;
    var $tipo;
    var $registro;
    var $editor;
    var $version;
    var $feha_instalacion;
    var $sofware;
    var $error = NULL;
    var $rango;
    
    #######################################  Operaciones  #######################################

    function convertirFecha($fecha) {
        $valor = explode("-", $fecha);
        $fecha = $valor[2] . "/" . $valor[1] . "/" . $valor[0];
        return $fecha;
    }

    // Insertar 
    function insertar($cliente, $empleado, $equipo, $familia, $edicion, $version, $fecha_instalacion) {
        $this->conexion();
        $query = "INSERT INTO resumen_SPLA (cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES (:cliente, :empleado, :equipo, TRIM(:familia), TRIM(:edicion), TRIM(:version), :fecha_instalacion)";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia,
            ':edicion'=>$edicion, ':version'=>$version, ':fecha_instalacion'=>$fecha_instalacion));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        $this->conexion();
        $query = "INSERT INTO resumen_SPLA (cliente, empleado, equipo, familia, edicion, version, fecha_instalacion) ";
        $query .= "VALUES " . $bloque;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function actualizarApps($id, $cantidad, $sk, $tipo){
        $this->conexion();
        $query = "UPDATE resumen_SPLA SET Qty = :Qty, SKU = :SKU, type = :type WHERE id = :id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id, ':Qty'=>$cantidad, ':SKU'=>$sk, ':type'=>$tipo));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function actualizarPriceSale($id, $QtySale, $priceSale){
        $this->conexion();
        $query = "UPDATE resumen_SPLA SET QtySale = :QtySale, priceSale = :priceSale WHERE id = :id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id, ':QtySale'=>$QtySale, ':priceSale'=>$priceSale));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($cliente, $empleado) {
        $this->conexion();
        $query = "DELETE FROM resumen_SPLA WHERE cliente = :cliente AND empleado = :empleado";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    // Obtener datos 
    /*function datos($id) {
        $this->conexion();
        $query = "SELECT id,
                    cliente,
                    equipo,
                    familia,
                    edicion,
                    version,
                    DATE_FORMAT(fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion
                FROM resumen_SPLA WHERE id = :id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':id'=>$id));
            
            $this->id = $usuario['id'];
            $this->cliente = $usuario['cliente'];
            $this->equipo = $usuario['equipo'];
            $this->familia = $usuario['familia'];
            $this->edicion = $usuario['edicion'];
            $this->version = $usuario['version'];
            $this->fecha_instalacion = $usuario['fecha_instalacion'];
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }*/

    /*function datos3($id, $cliente, $equipo, $tipo, $familia, $edicion, $version, $fecha, $rango) {
        $this->id = $id;
        $this->cliente = $cliente;
        $this->equipo = $equipo;
        $this->tipo = $tipo;
        $this->familia = $familia;
        $this->edicion = $edicion;
        $this->version = $version;
        $this->fecha_instalacion = $fecha;
        $this->rango = $rango;
        return true;
    }*/

    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {
        $this->conexion();
        $query = "SELECT * FROM resumen_SPLA WHERE cliente = :cliente AND empleado = :empleado ORDER BY id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'>$empleado));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function duplicado($cliente, $empleado, $equipo, $familia, $edicion, $version) {
        $this->conexion();
        $query = "SELECT COUNT(*) AS cantidad
                    FROM resumen_SPLA
                    WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.equipo = :equipo AND resumen_SPLA.familia = :familia 
                    AND resumen_SPLA.edicion = :edicion AND resumen_SPLA.version = :version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    function duplicadoSam($cliente, $empleado, $equipo, $familia, $edicion, $version) {
        $this->conexion();
        $query = "SELECT COUNT(*) AS cantidad
                    FROM resumen_SPLASam AS resumen_SPLA
                    WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.equipo = :equipo AND resumen_SPLA.familia = :familia 
                    AND resumen_SPLA.edicion = :edicion AND resumen_SPLA.version = :version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function duplicadoSamArchivo($archivo, $equipo, $familia, $edicion, $version) {
        $this->conexion();
        $query = "SELECT COUNT(*) AS cantidad
                    FROM resumen_SPLASam AS resumen_SPLA
                    WHERE resumen_SPLA.archivo = :archivo AND resumen_SPLA.equipo = :equipo AND resumen_SPLA.familia = :familia 
                    AND resumen_SPLA.edicion = :edicion AND resumen_SPLA.version = :version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':equipo'=>$equipo, ':familia'=>$familia, ':edicion'=>$edicion, 
            ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    // Obtener listado de todos los Usuarios paginados
    /*function listar_todo_paginado($cliente, $inicio, $fin) {
        $this->conexion();
        $query = "SELECT * FROM resumen_SPLA WHERE cliente = :cliente ORDER BY id LIMIT " . $inicio . ", " . $fin;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/

    // Obtener listado de todas las Inventarios
    /*function listar_datos($cliente) {
        $this->conexion();
        $query = "select edicion, version, count(*) from resumen_SPLA WHERE cliente = :cliente group by edicion, version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }*/
    
    function listar_datos2($cliente, $empleado, $familia, $edicion, $version) {
        $this->conexion();
        $query = "select  count(*) AS total from resumen_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion = :edicion AND version = :version group by edicion, version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia . '%', ':edicion'=>$edicion, ':version'=>$version));
            $row = $sql->fetch();
            if ($row["total"] == ""){
                return 0;
            }
            else{
                return $row["total"];
            }
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }
    
    function listar_datos3($cliente, $empleado, $familia, $edicion, $edicion1, $version) {
        $this->conexion();
        $query = "select  count(*) AS cantidad from resumen_SPLA WHERE cliente = :cliente AND empleado = :empleado AND familia LIKE :familia AND edicion LIKE :edicion AND edicion NOT LIKE :edicion1 AND version = :version group by edicion, version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>$familia . '%', ':edicion'=>$edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':version'=>$version));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }
    }

    function listar_datos6($cliente, $empleado, $familia, $edicion) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        detalles_equipo2.rango
		from resumen_SPLA 
                    INNER JOIN detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                    INNER JOIN filepcs2 ON resumen_SPLA.equipo = filepcs2.cn
		WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion LIKE :edicion
		GROUP BY resumen_SPLA.id
                ORDER BY resumen_SPLA.version DESC, resumen_SPLA.fecha_instalacion DESC";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos7($cliente, $empleado, $familia, $edicion, $edicion1) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        detalles_equipo2.rango
                    from resumen_SPLA 
                        INNER JOIN detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_SPLA.equipo = filepcs2.cn
                    WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE :edicion1:
                    GROUP BY resumen_SPLA.id
                    ORDER BY resumen_SPLA.version DESC, resumen_SPLA.fecha_instalacion DESC";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos8($cliente, $empleado, $familia, $edicion, $edicion1, $edicion2) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        detalles_equipo2.rango
                    from resumen_SPLA 
                        INNER JOIN detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_SPLA.equipo = filepcs2.cn
                    WHERE resumen_SPLA.cliente = :cliente AND empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE :edicion1 AND resumen_SPLA.edicion NOT LIKE :edicion2
                    GROUP BY resumen_SPLA.id
                    ORDER BY resumen_SPLA.version DESC, resumen_SPLA.fecha_instalacion DESC";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos6Ordenar($cliente, $empleado, $familia, $edicion, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        if ($ordenar == "fecha_instalacion") {
            $ordenar = "resumen_SPLA." . $ordenar;
        }

        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
		from resumen_SPLA 
                    INNER JOIN detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                    INNER JOIN filepcs2 ON resumen_SPLA.equipo = filepcs2.cn
		WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion LIKE :edicion
		GROUP BY resumen_SPLA.id
                ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos6SamDetalle($cliente, $empleado, $familia, $edicion) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
		from resumen_SPLASam AS resumen_SPLA
                    INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
		WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion LIKE :edicion
		GROUP BY resumen_SPLA.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos6SamDetalleArchivo($archivo, $familia, $edicion) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
		from resumen_SPLASam AS resumen_SPLA
                    INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
		WHERE resumen_SPLA.archivo = :archivo AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion LIKE :edicion
		GROUP BY resumen_SPLA.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos7Ordenar($cliente, $empleado, $familia, $edicion, $edicion1, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_SPLA 
                        INNER JOIN detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_SPLA.equipo = filepcs2.cn
                    WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE :edicion1
                    GROUP BY resumen_SPLA.id
                    ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos7SamDetalle($cliente, $empleado, $familia, $edicion, $edicion1) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_SPLASam AS resumen_SPLA
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                    WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE edicion1
                    GROUP BY resumen_SPLA.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
    
    function listar_datos7SamDetalleArchivo($archivo, $familia, $edicion, $edicion1) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                        CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_SPLASam AS resumen_SPLA
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                    WHERE resumen_SPLA.archivo = :archivo AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE edicion1
                    GROUP BY resumen_SPLA.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos8Ordenar($cliente, $empleado, $familia, $edicion, $edicion1, $edicion2, $ordenar, $direccion) {
        $this->conexion();
        if ($ordenar == "") {
            $ordenar = "id";
        }

        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(filepcs2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                         CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_SPLA 
                        INNER JOIN detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                        INNER JOIN filepcs2 ON resumen_SPLA.equipo = filepcs2.cn
                    WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE :edicion1 AND resumen_SPLA.edicion NOT LIKE :edicion2
                    GROUP BY resumen_SPLA.id
                    ORDER BY " . $ordenar . " " . $direccion;
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }

    function listar_datos8SamDetalle($cliente, $empleado, $familia, $edicion, $edicion1, $edicion2) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                         CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_SPLASam AS resumen_SPLA
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                    WHERE resumen_SPLA.cliente = :cliente AND resumen_SPLA.empleado = :empleado AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE :edicion1 AND resumen_SPLA.edicion NOT LIKE :edicion2
                    GROUP BY resumen_SPLA.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }    
    }
    
    function listar_datos8SamDetalleArchivo($archivo, $familia, $edicion, $edicion1, $edicion2) {
        $this->conexion();
        $query = "select resumen_SPLA.id,
                        resumen_SPLA.cliente,
                        resumen_SPLA.equipo,
                        IF(detalles_equipo2.os LIKE ('%Windows Server%'), 'Servidor', 'Cliente') AS tipo,
                        resumen_SPLA.familia,
                        resumen_SPLA.edicion,
                        resumen_SPLA.version,
                        DATE_FORMAT(resumen_SPLA.fecha_instalacion, '%d/%m/%Y') AS fecha_instalacion,
                         CASE
                            WHEN detalles_equipo2.rango = 1 THEN
                                 'En Uso'
                            WHEN detalles_equipo2.rango = 2 OR detalles_equipo2.rango = 3 THEN
                                 'Uso Probable'
                            ELSE
                                 'Obsoleto'
                        END AS rango 
                    from resumen_SPLASam AS resumen_SPLA
                        INNER JOIN detalles_equipo2Sam AS detalles_equipo2 ON resumen_SPLA.equipo = detalles_equipo2.equipo
                    WHERE resumen_SPLA.archivo = :archivo AND resumen_SPLA.familia LIKE :familia AND resumen_SPLA.edicion NOT LIKE :edicion AND resumen_SPLA.edicion NOT LIKE :edicion1 AND resumen_SPLA.edicion NOT LIKE :edicion2
                    GROUP BY resumen_SPLA.id";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':archivo'=>$archivo, ':familia'=>'%' . $familia . '%', ':edicion'=>'%' . $edicion . '%', ':edicion1'=>'%' . $edicion1 . '%', ':edicion2'=>'%' . $edicion2 . '%'));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }    
    }

    // Obtener listado de todas las Inventarios
    /*function total_datos($cliente) {
        $this->conexion();
        $query = "select edicion, version, count(*) AS cantidad from resumen_SPLA  WHERE cliente = :cliente group by edicion, version";
        try{
            $sql = $this->conn->prepare($query);
            $sql->execute(array(':cliente'=>$cliente));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return 0;
        }    
    }*/
}