<?php

namespace App\Clases;

class trueUpDetalle extends General{
    public  $error = NULL;
    
    // Insertar 
    function insertar($idTrueUp, $producto, $edicion, $version, $precioEstimado, $cantAct, $totalAct, $cantProp, 
    $totalProp, $comentarios) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO trueUpDetalle (idTrueUp, producto, edicion, version, precioEstimado, '
            . 'cantidad, total, cantidadPropuesta, totalPropuesto, comentarios) VALUES (:idTrueUp, :producto, :edicion, :version, '
            . ':precioEstimado, :cantAct, :totalAct, :cantProp, :totalProp, :comentarios)');
            $sql->execute(array(':idTrueUp'=>$idTrueUp, ':producto'=>$producto, ':edicion'=>$edicion, ':version'=>$version, 
            ':precioEstimado'=>$precioEstimado, ':cantAct'=>$cantAct, ':totalAct'=>$totalAct, ':cantProp'=>$cantProp, 
            ':totalProp'=>$totalProp, ':comentarios'=>$comentarios));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function eliminar($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE trueUpDetalle SET estado = 0 WHERE id = :id');
            $sql->execute(array(':id'=>$id));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function detalleTrueUp($id) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT *'
            . 'FROM trueUpDetalle '
            . 'WHERE idTrueUp = :id');
            $sql->execute(array(':id'=>$id));
            return $sql->fetchAll();
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return array();
        }
    }
}