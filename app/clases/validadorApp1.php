<?php

namespace App\Clases;

class validadorApp1 extends clase_general_licenciamiento1{ 
    function verificarSerial($email, $serial, $productoApp) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT admin005.id, admin005.fechaFin, admin005.nivelServicio
                FROM admin001
                    INNER JOIN admin005 ON admin001.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 1
                    AND productoApp = :productoApp 
                WHERE admin001.email = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email, ':productoApp'=>$productoApp));
            $row = $sql->fetch();
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function verificarSerial1($email, $serial, $productoApp) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT admin005.id
                FROM admin001
                    INNER JOIN admin005 ON admin001.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 2
                    AND productoApp = :productoApp 
                WHERE admin001.email = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email, ':productoApp'=>$productoApp));
            $row = $sql->fetch();
            return $row["id"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function verificarSerial2($email, $serial, $serialHHD, $productoApp) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT admin005.id, admin005.fechaFin, admin005.nivelServicio
                FROM admin001
                    INNER JOIN admin005 ON admin001.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 2
                    AND serialHHD = :serialHHD AND productoApp = :productoApp
                WHERE admin001.email = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email, ':serialHHD'=>$serialHHD, ':productoApp'=>$productoApp));
            $row = $sql->fetch();
            if($row["id"] == ""){
                $row["id"] = 0;
                $row["fechaFin"] = null;
            }
            return $row;
        }catch(PDOException $e){
            return array("id"=>0, "fechaFin"=>"1981-01-01");
        }
    }
    
    function serialActivado($email, $productoApp, $serial) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT COUNT(*) AS cantidad
                FROM admin001
                    INNER JOIN admin005 ON admin001.id = admin005.empresa AND admin005.serial = :serial AND admin005.status = 2
                    AND productoApp = :productoApp
                WHERE admin001.email = :email');
            $sql->execute(array(":serial"=>$serial, ":email"=>$email, ':productoApp'=>$productoApp));
            $row = $sql->fetch();
            return $row["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
    
    function activarSerial($id) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('UPDATE admin005 SET status = 2 WHERE id = :id');
            $sql->execute(array(":id"=>$id));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function activarSerial1($id, $serialHHD) {
        try{
            $this->conexion();
            $sql = $this->conex->prepare('UPDATE admin005 SET serialHHD = :serialHHD, status = 2 WHERE id = :id');
            $sql->execute(array(":id"=>$id, ":serialHHD"=>$serialHHD));
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function permisosSerial($id){
        try{
            $this->conexion();
            $sql = $this->conex->prepare('SELECT admin003.nombre,
                    IFNULL(admin004.accesos, 0) AS accesos
                FROM admin003
                    LEFT JOIN admin004 ON admin003.id = admin004.accesos AND admin004.serial = :id AND admin004.status = 1
                WHERE admin003.status = 1
                GROUP BY admin003.id
                ORDER BY admin003.nombre');
            $sql->execute(array(":id"=>$id));
            return $sql->fetchAll();
        }catch(PDOException $e){
            return array();
        }
    }
}