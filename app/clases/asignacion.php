<?php

namespace App\Clases;

class asignacion extends General{
    
    #######################################  Operaciones  #######################################
    // Insertar 
    function insertar($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO asignacion (nombre) VALUES (:nombre)');
            $sql->execute(array(':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }

    function actualizar($id, $nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE asignacion SET nombre = :nombre WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':nombre'=>$nombre));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    // Eliminar
    function activarDesactivar($id, $estado) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('UPDATE asignacion SET estado = :estado WHERE id = :id');
            $sql->execute(array(':id'=>$id, ':estado'=>$estado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listar_asignacion($nombre, $inicio) {
        try{
            $this->conexion();          
            $sql = $this->conn->prepare('SELECT *
            FROM asignacion
            WHERE nombre LIKE :nombre
            LIMIT ' . $inicio . ', ' . $this->limit_paginacion);
            $sql->execute(array(':nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetchAll();
            return $resultado;
        }catch(PDOException $e){
            return false;
        }
    }
    
    function totalAsignacion($nombre) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM asignacion
            WHERE nombre LIKE :nombre');
            $sql->execute(array(':nombre'=>"%" . $nombre . "%"));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return false;
        }
    }
    
    function existeAsignacion($nombre){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT COUNT(*) AS cantidad
            FROM asignacion
            WHERE nombre = :nombre');
            $sql->execute(array(':nombre'=>$nombre));
            $resultado = $sql->fetch();
            return $resultado["cantidad"];
        }catch(PDOException $e){
            return 0;
        }
    }
}