<?php

namespace App\Clases;

class filepcsOracle extends General{
    public  $error;
    public  $listado;

    // Insertar 
    function insertar($cliente, $empleado, $dn, $objectclass, $cn, $useracountcontrol, $lastlogon, $pwdlastset, $os, $lastlogontimes) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO filepcs_oracle (cliente, empleado, dn, objectclass, cn, useracountcontrol, lastlogon, pwdlastset, os, lastlogontimes) VALUES '
            . '(:cliente, :empleado, :dn, :objectclass, :cn, :useracountcontrol, :lastlogon, :pwdlastset, :os, :lastlogontimes)');        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':dn'=>$dn, ':objectclass'=>$objectclass, ':cn'=>$cn, ':useracountcontrol'=>$useracountcontrol, ':lastlogon'=>$lastlogon, ':pwdlastset'=>$pwdlastset, ':os'=>$os, ':lastlogontimes'=>$lastlogontimes));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    //Eliminar
    function eliminar($cliente, $empleado) {
        try {
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM filepcs_oracle WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente' =>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = "No se pudo eliminar los registro";
            return false;
        }
    }
    
    function eliminarEquipos($cliente, $empleado, $equipos) {
        $this->conexion();
        $query = "DELETE FROM filepcs_oracle WHERE cliente = :cliente AND empleado = :empleado AND (" . $equipos . ")";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function actualizarEquipo($cliente, $dn, $objectclass, $cn, $useracountcontrol, $lastlogon, $pwdlastset, $os, $lastlogontimes){
        $this->conexion();
        $query = "UPDATE filepcs_oracle SET dn = :dn, objectclass = :objectclass, useracountcontrol = :useracountcontrol, 
        lastlogon = :lastlogon, pwdlastset = :pwdlastset, os = :os, lastlogontimes = :lastlogontimes WHERE cliente = :cliente 
        AND cn = :cn";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente, ':dn'=>$dn, ':objectclass'=>$objectclass, ':cn'=>$cn, ':useracountcontrol'=>$useracountcontrol, 
            ':lastlogon'=>$lastlogon, ':pwdlastset'=>$pwdlastset, ':os'=>$os,':lastlogontimes'=>$lastlogontimes));
            return true;
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return false;
        }
    }
    
    function existeEquipo($cliente, $cn) {
        $this->conexion();
        $query = "SELECT COUNT(cn) AS cantidad "
               . "FROM filepcs_oracle "
               . "WHERE cliente = :cliente AND cn = :cn";
        try {
            $this->conexion();
            $sql = $this->conn->prepare($query);        
            $sql->execute(array(':cliente'=>$cliente, ':cn'=>$cn));
            $row = $sql->fetch();
            return $row["cantidad"];
        } catch (PDOException $e) {
            $this->error = $e->getMessage(); //"No se pudo agregar el registro";
            return 0;
        }
    }
    
    // Obtener listado de todos los Usuarios
    function listar_todo($cliente, $empleado) {        
        try {
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM filepcs_oracle WHERE cliente = :cliente AND empleado = :empleado ORDER BY id');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            $this->listado = $sql->fetchAll();
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    // Cargar Archivo de la Base de Datos
    function cargar_archivo($titulo_archivo, $temporal) {
        if ($titulo_archivo != "") {
            $ruta = $GLOBALS['app_root']."/oracle/archivos_csvf2/" . $titulo_archivo;

            if (is_uploaded_file($temporal)) {
                move_uploaded_file($temporal, $ruta);
                return true;
            } else {
                $this->error = 'No se pudo cargar el archivo';
                return false;
            }
        } else {
            $this->error = 'No hay archivo';
            return false;
        }
    }
}