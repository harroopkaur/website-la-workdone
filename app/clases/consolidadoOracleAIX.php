<?php

namespace App\Clases;

class consolidadoOracleAIX extends General{
    public  $lista        = array();
    public  $listaEdicion = array();
    public  $listaVersion = array();
    public  $edicion      = "";
    public  $version      = "";
    public  $error        = NULL;
    public  $result;
    
    // Insertar 
    function insertar($cliente, $empleado, $equipo, $software) {        
        try{
            $this->conexion();
            if($this->verificarRegistro($cliente, $empleado, $equipo, $software) === 0){
                $sql = $this->conn->prepare('INSERT INTO consolidado_OracleAIX(cliente, empleado, equipo, software) VALUES '
                . '(:cliente, :empleado, :equipo, :software)');
                $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':software'=>$software));
            }
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function insertarEnBloque($bloque, $bloqueValores) {
        try{
            $this->conexion();
            $sql = $this->conn->prepare('INSERT INTO consolidado_OracleAIX (cliente, empleado, equipo, software) '
            . 'VALUES ' . $bloque);
            $sql->execute($bloqueValores);
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
        
    // Eliminar
    function eliminar($cliente, $empleado) {        
        try{
            $this->conexion();
            $sql = $this->conn->prepare('DELETE FROM consolidado_OracleAIX WHERE cliente = :cliente AND empleado = :empleado');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado));
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function listarProductos($cliente, $empleado, $software, $noIncluir) {
        try{
            $this->conexion();
            $notIn = "";
            if($noIncluir != ""){
                $data = explode(",", $noIncluir);
                for($i=0; $i < count($data); $i++){
                    $notIn .= " AND software NOT LIKE '%" . $data[$i] . "%'";
                }
            }
            $sql = $this->conn->prepare('SELECT id,
                cliente,
                equipo,
                software AS product
            FROM consolidado_OracleAIX
            WHERE cliente = :cliente AND empleado = :empleado AND software LIKE :product ' . $notIn . '
            GROUP BY equipo, product
            ORDER BY product DESC');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':product'=>"%" . $software . "%"));
            $this->lista = $sql->fetchAll();
            return true;
        }catch(PDOException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
    
    function verificarRegistro($cliente, $empleado, $equipo, $software){
        try{
            $this->conexion();
            $sql = $this->conn->prepare('SELECT * FROM consolidado_OracleAIX WHERE cliente = :cliente AND empleado = :empleado AND equipo = :equipo AND software = :software');
            $sql->execute(array(':cliente'=>$cliente, ':empleado'=>$empleado, ':equipo'=>$equipo, ':software'=>$software));
            return count($sql->fetchAll());
        }catch(PDOException $e){
            return -1;
        }
    }
    
    function verificarRegistroArray($array, $software){
        $result = false;
        if(count($array) == 1 && $array[0] ==  $software){
            $result = false;
        } else{
            for($k = 0; $k < count($array); $k++){
                if($array[$k] == $software){
                    $result = true;
                    break;
                } 
            }
        }
        return $result;
    }
}