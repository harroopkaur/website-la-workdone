<?php

namespace App\Clases;

class email {

    function recuperar_clave($nombre, $apellido, $login, $correo, $clave) {
        $arrhtml_mail = file('plantillas/email_clave.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#LOGIN#", $login, $html_mail);
        $html_mail = str_replace("#CLAVE#", $clave, $html_mail);

        $contenido = $html_mail;
        $asunto = "Trial LA Tool - Seguridad Clave";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail($correo, $asunto, $contenido, $headers);
        
    }

    function recuperar_clave2($nombre, $apellido, $email, $clave) {
        $arrhtml_mail = file('../plantillas/email_clave.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#EMAIL#", $email, $html_mail);
        $html_mail = str_replace("#CLAVE#", $clave, $html_mail);

        $contenido = $html_mail;
        $asunto = "Trial LA Tool - Seguridad Clave";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail($email, $asunto, $contenido, $headers);
    }

    function enviar_suscripcionr($correo, $nombre) {
        $arrhtml_mail = file('plantillas/email_suscripcionr.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);

        $contenido = $html_mail;
        $asunto = " Trial LA Tool- Registro";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail($correo, $asunto, $contenido, $headers);
    }

    function enviar_suscripcion($correo, $nombre, $apellido, $login) {
        $arrhtml_mail = file('plantillas/email_suscripcion.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        $html_mail = str_replace("#CORREO#", $correo, $html_mail);
        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#LOGIN#", $login, $html_mail);

        $contenido = $html_mail;
        $asunto = " Trial LA Tool- Registro";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Trial LA Tool <info@licensingassurance.com>\r\n";
        mail('info@licensingassurance.com', $asunto, $contenido, $headers);
    }
    
    function enviar_alerta_renovacion($correo, $nombre, $apellido, $contrato) {
        $copy_email   = "<m_acero_n@hotmail.com>, <paola@licensingassurance.com>, <gabriel@licensingassurance.com>";
        $arrhtml_mail = file('../../plantillas/email_renovacion.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        $html_mail = str_replace("#NOMBRE#", utf8_decode($nombre), $html_mail);
        $html_mail = str_replace("#APELLIDO#", utf8_decode($apellido), $html_mail);
        $html_mail = str_replace("#CONTRATO#", $contrato, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode("Licensing Assurance Anualidad/Renovación");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        $headers .= "cc: {$copy_email}";
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_cotizacionLiensing($mensaje) {
        $arrhtml_mail = file('../../plantillas/email_cotizacionLicensing.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#COTIZACION#", utf8_decode("Cotización Licensing Assurance"), $html_mail);
        $html_mail = str_replace("#MENSAJE#", utf8_decode($mensaje), $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode("Cotización Licensing Assurance");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        return mail("Dimitri@licensingassurance.com", $asunto, $contenido, $headers);
    }

    function mail_file($to, $from, $subject, $body, $file, $nameFile){
        $copy_email   = "<m_acero_n@hotmail.com>, <paola@licensingassurance.com>";
        $boundary = md5(rand());
        
        $headers = array(
            "MIME-Version: 1.0",
            "Content-Type: multipart/mixed; boundary=\"{$boundary}\"",
            "From: {$from}",
            "cc: {$copy_email}"
        );
            
        $message = array(
            "--{$boundary}",
            "Content-Type: text/plain",
            "Content-Transfer-Encoding: 7-bit",
            "",
            chunk_split($body),
            "--{$boundary}",      
            "Content-Type: {mime_content_type($file)}; name=\"{$nameFile}\"",
            "Content-Disposition: attachment; filename=\"{$nameFile}\"",
            "Content-Transfer-Encoding: base64",
            "",
            chunk_split(base64_encode(file_get_contents($file))),
            "--{$boundary}--",
            
        );
            
        return mail($to, $subject, implode("\r\n", $message), implode("\r\n", $headers));
    }
    
    function enviar_app_correo($correo, $usuario) {
        $copy_email   = $correo;
        $copy_email .= ", <m_acero_n@hotmail.com>";
        $arrhtml_mail = file('../../webtool/plantillas/email_app_escaneo.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }

        $contenido = $html_mail;
        $asunto = utf8_decode($usuario . " invites you to download the NADD application");

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        $headers .= "cc: {$copy_email}";
        return mail("m_acero_n@hotmail.com", $asunto, $contenido, $headers);
    }
    
    function enviar_reporteVisitas($correo, $copy_email, $tabla, $titulo) {
        $arrhtml_mail = file('/home/vamoscloud2014/public_html/licensingassurance.com/webtool/plantillas/email_visitas.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#TABLA#", $tabla, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode($titulo);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "cc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_reporteVisitasSemanal($correo, $copy_email, $tabla, $titulo) {
        $arrhtml_mail = file('/home/vamoscloud2014/public_html/licensingassurance.com/webtool/plantillas/email_visitasSemanal.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#TABLA#", $tabla, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode($titulo);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "cc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
    
    function enviar_asistenciaEvento($correo, $copy_email, $nombre, $email, $telefono, $direccion, $empresa, $titulo) {
        $arrhtml_mail = file('../webtool/plantillas/email_asistenciaEvento.php');
        for ($i = 0; $i < count($arrhtml_mail); $i++) {
            if($i == 0){
                $html_mail = $arrhtml_mail[$i];
            }
            else{
                $html_mail .= $arrhtml_mail[$i];
            }
        }
        
        $html_mail = str_replace("#NOMBRE#", $nombre, $html_mail);
        $html_mail = str_replace("#CORREO#", $email, $html_mail);
        $html_mail = str_replace("#TELEFONO#", $telefono, $html_mail);
        $html_mail = str_replace("#DIRECCION#", $direccion, $html_mail);
        $html_mail = str_replace("#EMPRESA#", $empresa, $html_mail);

        $contenido = $html_mail;
        $asunto = utf8_decode($titulo);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= "From: Webtool <info@licensingassurance.com>\r\n";
        
        if($copy_email != ""){
            $headers .= "cc: {$copy_email}";
        }
        return mail($correo, $asunto, $contenido, $headers);
    }
}