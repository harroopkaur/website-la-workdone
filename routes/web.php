<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"HomeController@index");
Route::get('/es',"HomeController@index_es");
// Route::get('/aboutUs',"HomeController@aboutUs");


Route::get('/aboutUs',"HomeController@aboutUs");
Route::get('/SAMasaService',"HomeController@SamAsaServices");
Route::get('/auditDefense',"HomeController@auditDefense");
Route::get('/SPLA',"HomeController@SPLA");
////////Route::get('/SPLA/{lang?}',"HomeController@SPLA");
Route::get('/deployment',"HomeController@deployment");
Route::get('/overview',"HomeController@overview");
Route::get('/articles',"HomeController@articles");
Route::get('/contact',"HomeController@contact");
Route::get('/webinar_licenciamiento_SAP',"HomeController@webinar_licenciamiento_SAP");
Route::get('/services',"HomeController@services");
Route::get('/datacenters',"HomeController@datacenters");
Route::get('/webinar',"HomeController@webinar");
Route::get('/sitemap',"HomeController@sitemap");
Route::get('/news',"HomeController@news");
Route::get('/webcast',"HomeController@webcast");
Route::get('/webcast2',"HomeController@webcast2");
Route::get('/toolsapps',"HomeController@toolsapps");
Route::get('/blog',"HomeController@blog");
Route::get('/blogdetail',"HomeController@blogdetail");
Route::get('language/{language}', 'LanguageController@changeLanguage');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
